# Gradbot

## Setup

1. By default Gradbot requires a `~/db` directory (`$HOME/db`) – create it if it doesn't exist OR provide another SQLite database file path using the `--databasePath` option (see point 6).
1. You'll need `SQLite` (you can download it from [here](https://www.sqlite.org/download.html)).
1. Install `Node.js` from [here](https://nodejs.org/en/download/). Note, version 20.17.0 is known to work, you might want to consider using a node version manager, i.e. [nvm](https://github.com/nvm-sh/nvm#install--update-script).
1. To run the application with `DCIntegration` and/or `DiscordIntegration` you will need some additional environment setup. Please refer to the README in [this project](https://gitlab.com/gradbot-team/gradbot-v3-dev-secrets)
1. Run `npm ci`.
1. `npm run dev -- --chatIntegrations <one or more chat integrations> [--databasePath <SQLite database file path>]` command will start the application locally and automatically re-build it when you save files.

## Feature development

1. Run `npm run generate-feature` and follow the instructions. This will create `src/features/<your-feature-name>-feature/` directory containing your new feature and a unit test file for this feature.
1. The command above will also place your feature at the end of the priority list in `src/features/ordered-features.ts` – adjust the order there, if needed. This file determines feature priority, so that only the best answer to a given message comes back to the chat. The engine waits for some time (see `FeatureRaceTimeout` in `src/gradbot/gradbot.ts`) for the promises returned by features to resolve, then times out and chooses the answer from non-null ones that has the highest priority.
1. `GradbotFeature` abstract class, that all Gradbot features must extend, includes plenty of utilities, documented in the class itself. These include: detection of messages addressed to Gradbot, persistent database access, scheduling "unsolicited" messages. These utilities should cover you in most situations but if you write something that could be useful cross-feature, feel free to add it to the abstract class.
1. That's it! Off you go. Do some work. Shoo!

## Chat integration development

1. Run `npm run generate-chat-integration` and follow the instructions. This will create `src/chat-integrations/<your-integration-name>-integration/` directory containing your new integration.
1. The command above will also place your integration at the end of the integrations list in `src/chat-integrations/chat-integration-providers.ts`.
1. An integration is responsible for:
   - Receiving messages from a chat and converting them into a Gradbot `Message` object, assigning Metadata\* to it (use `this.createMetadata()`), and emitting it to the application
   - Converting `FeatureMessage` object coming from the application into an appropriate format and sending it to the integrated chat
   - Fetching users for a given server
1. Now go and spread Gradbot!

\* `Metadata` consists of:

- chatId – it's your integration class name; this is automatically assigned by `this.createMetadata()` and it helps Gradbot's messages find the way back to your integration
- dbSchema – this should be used for dividing Gradbot's knowledge; e.g. if your integration serves multiple channels on multiple servers, and only the channels are supposed to use Gradbot's shared knowledge, use server ID; if channels aren't supposed shared knowledge, use channel ID
- channel
- serverId

Every time you assign Metadata to a message and this message triggers a response (or an unsolicited message later on), this response will contain that metadata, so that you know where to route the message.

Channel can be overriden by plugins (e.g. to send a private message to a user when facilitating a game), server cannot.

Channel can also contain information about whether the message is private or not.

## Testing

When you run Gradbot using the steps above, it will listen to DammitChat messages in the dev instance of CWT website which you can access [here](https://cwt-team-dev.firebaseapp.com/). You can create your own account or use these credentials:

| email           | password |
| --------------- | -------- |
| `user@cwt.team` | `aaaaaa` |

The dev instance of the site is limited but its DammitChat is fully operational. Unfortunately multiple people working on Gradbot at the same time, will mean multiple replies 😬 Soz.

More importantly, **write unit tests**! `jest` makes it super-simple. Files named `<something>.test.ts` will be picked up by `jest` automatically. You can run all unit tests with `npm test`.

For testing, we run an in-memory database to keep things clean.

## Debugging

Visual Studio Code has built-in support for debugging node applications, with breakpoints, stepping through code, and various other features. A guide to debugging with VSCode can be found [here](https://code.visualstudio.com/docs/nodejs/nodejs-debugging).

This project provides a `debug` script to enable VSCode debugging. You can set up your `launch.json` as per the guide, to enable debugging in the following ways:

- Run `npm run debug -- --chatIntegrations <one or more chat integrations>` to launch Gradbot in debug mode, then attach the debugger to the running project.
- Launch Gradbot via NPM with the debugger attached from within VSCode's Debug console.

## Code review

Once you've done your changes, push your branch. In GitLab, use this branch to create a merge request. Once it's reviewed a Project Maintainer can merge your changes. This will trigger the pipeline that will release Gradbot.
