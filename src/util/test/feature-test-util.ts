import { container } from 'tsyringe';

import { API } from '../../gradbot/api/api';
import { Chat } from '../../gradbot/chat/chat';
import { User } from '../../gradbot/chat/user/user';
import { Database } from '../../gradbot/database/database';
import { Dictionary } from '../../gradbot/dictionary/dictionary';
import { Feature } from '../../gradbot/feature/feature';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { ImageGenerator } from '../../gradbot/image/image-generator';
import { Scheduler } from '../../gradbot/scheduler/scheduler';

import { getTestMessage } from './get-test-message';
import { TestAPI } from './test-api';
import { TestChatIntegration } from './test-chat-integration';
import { TestDictionary } from './test-dictionary';
import { TestImageGenerator } from './test-image-generator';
import { TestScheduler } from './test-scheduler';

export class FeatureTestUtil {
  public static createForFeatureType(FeatureType: new () => Feature) {
    const db = new Database(':memory:');
    const chat = new TestChatIntegration();
    const scheduler = new TestScheduler();
    const api = new TestAPI();
    const imageGenerator = new TestImageGenerator();
    const dictionary = new TestDictionary();
    container.register<Database>(Database.name, { useValue: db });
    container.register<Scheduler>(Scheduler.name, { useValue: scheduler });
    container.register<Chat>(Chat.name, { useValue: chat });
    container.register<API>(API.name, { useValue: api });
    container.register<ImageGenerator>(ImageGenerator.name, {
      useValue: imageGenerator,
    });
    container.register<Dictionary>(Dictionary.name, { useValue: dictionary });

    const featureTestUtil = new FeatureTestUtil(
      db,
      chat,
      scheduler,
      api,
      imageGenerator,
      new FeatureType(),
    );
    featureTestUtil.addUser('User');
    return featureTestUtil;
  }

  private userCounter = 1;
  private dbReady: Promise<void>;

  private constructor(
    public readonly db: Database,
    public readonly chat: TestChatIntegration,
    public readonly scheduler: TestScheduler,
    public readonly api: TestAPI,
    public readonly imageGenerator: TestImageGenerator,
    public readonly feature: Feature,
  ) {
    this.dbReady = db.prepareTables();
  }

  public async sendMessage(messageText: string, user?: User) {
    await this.dbReady;

    await this.feature
      .getMessageHandler(
        getTestMessage(messageText, user || this.chat.testUsers[0]),
      )
      .getResponse();
  }

  public async getReplyForMessage(messageText: string, user?: User) {
    await this.dbReady;

    return this.feature
      .getMessageHandler(
        getTestMessage(messageText, user || this.chat.testUsers[0]),
      )
      .getResponse();
  }

  public async expectReplyToBeNull(messageText: string, user?: User) {
    expect(await this.getReplyForMessage(messageText, user)).toBeNull();
  }

  public async expectReplyFromUserNameToBeNull(
    messageText: string,
    userName: string,
  ) {
    expect(
      await this.getReplyForMessage(
        messageText,
        this.getUserFromName(userName),
      ),
    ).toBeNull();
  }

  public async expectReplyToBeTruthy(messageText: string, user?: User) {
    expect(await this.getReplyForMessage(messageText, user)).toBeTruthy();
  }

  public async expectReplyToHaveProperty(
    messageText: string,
    property: string,
    value: string | RegExp,
    user?: User,
  ) {
    this.expectFeatureMessageToHaveProperty(
      await this.getReplyForMessage(messageText, user),
      property,
      value instanceof RegExp ? expect.stringMatching(value) : value,
    );
  }

  public async expectReplyToNotHavePropertyWithValue(
    messageText: string,
    property: string,
    value: string | RegExp,
    user?: User,
  ) {
    this.expectFeatureMessageToHaveProperty(
      await this.getReplyForMessage(messageText, user),
      property,
      expect.not.stringMatching(value),
    );
  }

  public async expectReplyToNotHaveProperty(
    messageText: string,
    property: string,
    value: string | RegExp,
    user?: User,
  ) {
    this.expectFeatureMessageToHaveProperty(
      await this.getReplyForMessage(messageText, user),
      property,
      expect.not.stringMatching(value),
    );
  }

  public async expectReplyToHaveText(
    messageText: string,
    val: string | RegExp,
    user?: User,
  ) {
    await this.expectReplyToHaveProperty(messageText, 'text', val, user);
  }

  public async expectReplyFromUserNameToHaveText(
    messageText: string,
    val: string | RegExp,
    userName: string,
  ) {
    await this.expectReplyToHaveProperty(
      messageText,
      'text',
      val,
      this.getUserFromName(userName),
    );
  }

  public async expectPrivateReplyToHaveText(
    messageText: string,
    val: string | RegExp,
    user?: User,
  ) {
    // get response from message
    const reply = await this.getReplyForMessage(messageText, user);

    this.expectPrivateFeatureMessageToHaveText(reply, val);
  }

  public async expectPublicReplyToHaveText(
    messageText: string,
    val: string,
    user?: User,
  ) {
    // get response from message
    const reply = await this.getReplyForMessage(messageText, user);

    this.expectPublicFeatureMessageToHaveText(reply, val);
  }

  public async expectReplyToBePrivate(messageText: string, user?: User) {
    const reply = await this.getReplyForMessage(messageText, user);
    this.expectFeatureMessageToBePrivate(reply);
  }

  public async expectReplyToBePublic(messageText: string, user?: User) {
    const reply = await this.getReplyForMessage(messageText, user);
    this.expectFeatureMessageToBePublic(reply);
  }

  public async expectReplyToHaveImage(messageText: string, user?: User) {
    const reply = await this.getReplyForMessage(messageText, user);
    expect(reply ? reply.image : false).toBeTruthy();
  }

  public expectFeatureMessageToBePrivate(message: FeatureMessage | null) {
    expect(message?.channelOverride?.isPrivate).toBeTruthy();
  }

  public expectFeatureMessageToBePublic(message: FeatureMessage | null) {
    expect(message?.channelOverride?.isPrivate).toBeFalsy();
  }

  public async expectReplyToNotHaveTextWithValue(
    messageText: string,
    val: string | RegExp,
    user?: User,
  ) {
    await this.expectReplyToNotHavePropertyWithValue(
      messageText,
      'text',
      val,
      user,
    );
  }

  /**
   * Expects a FeatureMessage to have a specified property and, optionally, the
   * property to have a specified value.
   */
  public expectFeatureMessageToHaveProperty(
    message: FeatureMessage | null,
    property: string,
    value?: any,
  ) {
    expect(message).toHaveProperty(property, value);
  }

  /**
   * Expects a FeatureMessage to be addressed to a specified user.
   */
  public expectFeatureMessageToBeForUser(
    message: FeatureMessage,
    userIndex: number,
  ) {
    this.expectFeatureMessageToHaveProperty(
      message,
      'channelOverride.id',
      this.chat.testUsers[userIndex].id,
    );
  }

  /**
   * Expects a feature message to have specified text.
   */
  public expectFeatureMessageToHaveText(
    message: FeatureMessage | null,
    value: string | RegExp,
  ) {
    if (value instanceof RegExp) {
      this.expectFeatureMessageToHaveProperty(
        message,
        'text',
        expect.stringMatching(value),
      );
    } else {
      this.expectFeatureMessageToHaveProperty(message, 'text', value);
    }
  }

  /**
   * Expects a feature message to not have specified text.
   */
  public expectFeatureMessageToNotHaveText(
    message: FeatureMessage | null,
    value: string | RegExp,
  ) {
    this.expectFeatureMessageToHaveProperty(
      message,
      'text',
      expect.not.stringMatching(value as RegExp),
    );
  }

  public async expectReplyToNotHaveText(messageText: string, user?: User) {
    const reply = await this.getReplyForMessage(messageText, user);
    expect(reply ? reply.text : true).toBeFalsy();
  }

  /**
   * Expects a feature message to be private and to have specified text.
   */
  public expectPrivateFeatureMessageToHaveText(
    message: FeatureMessage | null,
    value: string | RegExp,
  ) {
    this.expectFeatureMessageToBePrivate(message);
    this.expectFeatureMessageToHaveText(message, value);
  }

  /**
   * Expects a feature message to be public and to have specified text.
   */
  public expectPublicFeatureMessageToHaveText(
    message: FeatureMessage | null,
    value: string,
  ) {
    this.expectFeatureMessageToBePublic(message);
    this.expectFeatureMessageToHaveText(message, value);
  }

  public addUser(name: string): User {
    const user = new User(`test_user_${this.userCounter}`, name.trim());
    this.chat.addUser(user);
    this.userCounter++;
    return user;
  }

  public addUsers(names: string[]): void {
    names.forEach((name: string) => this.addUser(name));
  }

  public getIdFromName(name: string): string {
    const user = this.chat.testUsers.find(
      (element) => element.displayName === name,
    );

    if (user) {
      return user.id;
    }

    throw new Error('Failed to find the user: ' + name);
    return '';
  }

  public getUserFromName(name: string): User | undefined {
    const retUser: User | undefined = this.chat.testUsers.find(
      (user) => user.displayName === name,
    );

    if (retUser !== undefined) {
      return retUser;
    }

    throw new Error('Failed to find the user: ' + name);
    return undefined;
  }
}
