export const expectRegexToMatchSubstring = (
  text: string,
  matcher: string | RegExp,
  substring: string,
) => {
  const matches = text.match(matcher);
  expect(matches).not.toBeNull();
  if (matches) {
    expect(matches.length).toBeGreaterThan(0);
    expect(matches[0].trim()).toBe(substring);
  }
};
