import { Image } from '../../gradbot/image/image';
import { ImageGenerator } from '../../gradbot/image/image-generator';

export class TestImageGenerator extends ImageGenerator {
  public returnImage = true;

  public fromPath(
    {
      /* path*/
    },
    {
      /* reqTimeout*/
    },
  ): Promise<Image | null> {
    if (this.returnImage) {
      return Promise.resolve(new Image(Buffer.alloc(0), Buffer.alloc(0), ''));
    } else {
      return Promise.resolve(null);
    }
  }

  public fromBuffer(
    {
      /* buffer*/
    },
  ): Promise<Image> {
    return Promise.resolve(new Image(Buffer.alloc(0), Buffer.alloc(0), ''));
  }
}
