import { Observable } from 'rxjs';
import { ChatIntegration } from '../../gradbot/chat/integration/chat-integration';
import { Message } from '../../gradbot/chat/message/message';
import { User } from '../../gradbot/chat/user/user';
import { Channel } from '../../gradbot/chat/message/channel';
import { Image } from '../../gradbot/image/image';

export class TestChatIntegration extends ChatIntegration {
  private readonly messages: TestMessage[] = [];

  public init() {
    return Promise.resolve();
  }

  public addUser(user: User) {
    const userList = this.getUserListForServer('');
    userList.items = [...userList.items, user];
  }

  get testUsers() {
    return this.getUserListForServer('').items;
  }

  get incomingMessages$(): Observable<Message> {
    return new Observable();
  }

  protected async fetchUsers(): Promise<void> {
    await new Promise((resolve) => setTimeout(resolve));
  }

  protected sendChatMessage(
    text: string,
    channel: Channel,
    serverId: string,
    image?: Image,
  ): Promise<void> {
    this.messages.push({ text, channel, serverId, image });
    return Promise.resolve();
  }
}

interface TestMessage {
  text: string;
  channel: Channel;
  serverId: string;
  image?: Image;
}
