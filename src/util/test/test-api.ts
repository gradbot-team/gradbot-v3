import { API } from '../../gradbot/api/api';

export class TestAPI extends API {
  public readonly urlReturnValues: { [key: string]: any } = {};
  public delay = 0;

  public async fetch<T>(url: string): Promise<T | null> {
    return new Promise((resolve) =>
      setTimeout(() => resolve(this.urlReturnValues[url] as T), this.delay),
    );
  }

  public async fetchText(url: string): Promise<string | null> {
    return new Promise((resolve) =>
      setTimeout(
        () => resolve(this.urlReturnValues[url] as string),
        this.delay,
      ),
    );
  }
}
