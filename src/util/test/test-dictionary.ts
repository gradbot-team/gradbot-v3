import { Dictionary } from '../../gradbot/dictionary/dictionary';

export class TestDictionary extends Dictionary {
  private dictionary = '';

  public setDictionary(dict: string) {
    this.dictionary = dict;
  }

  public init(): null {
    return null;
  }

  public getDict(): string {
    return this.dictionary;
  }
}
