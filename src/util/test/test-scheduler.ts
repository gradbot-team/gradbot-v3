import { Scheduler } from '../../gradbot/scheduler/scheduler';
import { CronBuilder } from '../../gradbot/scheduler/cron-builder';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class TestScheduler extends Scheduler {
  public readonly cronMessages: {
    [key: string]: { cronText: string; message: FeatureMessage };
  } = {};
  public readonly scheduledMessages: {
    [key: string]: { date: Date; message: FeatureMessage };
  } = {};

  public scheduleCronMessage(
    id: string,
    cronBuilder: CronBuilder,
    message: FeatureMessage,
  ) {
    this.cronMessages[id] = { cronText: cronBuilder.build(), message };
  }

  public cancelCronMessage(id: string) {
    delete this.cronMessages[id];
  }

  public scheduleMessage(
    id: string,
    date: Date,
    message: FeatureMessage,
  ): Promise<void> {
    this.scheduledMessages[id] = { date, message };
    return Promise.resolve();
  }

  public cancelScheduledMessage(id: string): Promise<void> {
    delete this.scheduledMessages[id];
    return Promise.resolve();
  }
}
