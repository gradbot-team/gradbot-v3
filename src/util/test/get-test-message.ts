import { Message } from '../../gradbot/chat/message/message';
import { Metadata } from '../../gradbot/chat/message/metadata';
import { Channel } from '../../gradbot/chat/message/channel';
import { User } from '../../gradbot/chat/user/user';

const Test = 'test';

export const getTestMessage = (text = '', user: User = new User('', '')) =>
  new Message(
    new Metadata(Test, Test, new Channel(Test)),
    text,
    new Date(),
    user,
  );
