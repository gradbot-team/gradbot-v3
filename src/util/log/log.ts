export const log = (text: string, err?: any) => {
  if (err) {
    console.error(text, err);
  } else {
    console.log(`[${new Date().toLocaleString()}] ${text}`);
  }
};
