/**
 * @description Returns a fixed length version of the input string
 *
 * @param s
 */

export const getFixedWidthString = (s: string): string => {
  let fixedWidthString = '';

  for (const ch of s) {
    if (ch === '-') {
      fixedWidthString += String.fromCharCode(0xff0d); // Full-width dash
    } else if (ch === ' ') {
      fixedWidthString += String.fromCharCode(0x2003); // Full-width space
    } else if (ch !== '\n') {
      fixedWidthString += String.fromCharCode(ch.charCodeAt(0) + 0xfee0);
    } else {
      fixedWidthString += ch;
    }
  }

  return fixedWidthString;
};
