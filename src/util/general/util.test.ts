import * as Ordinals from './ordinals';
import { toString } from './string-extensions';

describe('General Utils:', () => {
  const numberSuffixTestData: {
    inputNum: number;
    suffix: string | undefined;
  }[] = [
    { inputNum: 1, suffix: 'st' },
    { inputNum: 2, suffix: 'nd' },
    { inputNum: 3, suffix: 'rd' },
    { inputNum: 4, suffix: 'th' },
    { inputNum: 5, suffix: 'th' },
    { inputNum: 6, suffix: 'th' },
    { inputNum: 7, suffix: 'th' },
    { inputNum: 8, suffix: 'th' },
    { inputNum: 9, suffix: 'th' },
    { inputNum: 0, suffix: 'th' },
    { inputNum: 11, suffix: 'th' },
    { inputNum: 12, suffix: 'th' },
    { inputNum: 13, suffix: 'th' },
    { inputNum: 21, suffix: 'st' },
    { inputNum: 22, suffix: 'nd' },
    { inputNum: 23, suffix: 'rd' },
    { inputNum: -1, suffix: 'st' },
    { inputNum: -2, suffix: 'nd' },
    { inputNum: -3, suffix: 'rd' },
    { inputNum: -4, suffix: 'th' },
    { inputNum: 0.1, suffix: undefined },
  ];

  numberSuffixTestData.forEach((item) => {
    test(`getOrdinalNumberSuffix outputs the correct suffix for ${item.inputNum}`, () => {
      expect(Ordinals.getOrdinalNumberSuffix(item.inputNum)).toBe(item.suffix);
    });
  });

  const incrementOrdinalsTestData: {
    input: string;
    output: string | undefined;
  }[] = [
    { input: '0th', output: '1st' },
    { input: '1st', output: '2nd' },
    { input: '2nd', output: '3rd' },
    { input: '3rd', output: '4th' },
    { input: '4th', output: '5th' },
    { input: '5th', output: '6th' },
    { input: '6th', output: '7th' },
    { input: '7th', output: '8th' },
    { input: '8th', output: '9th' },
    { input: '9th', output: '10th' },
    { input: '10th', output: '11th' },
    { input: '11th', output: '12th' },
    { input: '12th', output: '13th' },
    { input: '13th', output: '14th' },
    { input: '20th', output: '21st' },
    { input: '21st', output: '22nd' },
    { input: '22nd', output: '23rd' },
    { input: '23rd', output: '24th' },
    { input: '101st', output: '102nd' },
    { input: '111th', output: '112th' },
    { input: '-1st', output: undefined },
    { input: '1th', output: undefined },
    { input: '1rd', output: undefined },
    { input: '1nd', output: undefined },
    { input: '2th', output: undefined },
    { input: '3th', output: undefined },
    { input: '13rd', output: undefined },
    { input: '12nd', output: undefined },
    { input: '11st', output: undefined },
  ];

  incrementOrdinalsTestData.forEach((item) => {
    test(`incrementOrdinal outputs ${toString(
      item.output,
    )} for input ${toString(item.input)}`, () => {
      expect(Ordinals.incrementOrdinal(item.input)).toBe(item.output);
    });
  });

  const decrementOrdinalsTestData: {
    input: string;
    output: string | undefined;
  }[] = [
    { input: '1st', output: '0th' },
    { input: '2nd', output: '1st' },
    { input: '3rd', output: '2nd' },
    { input: '4th', output: '3rd' },
    { input: '5th', output: '4th' },
    { input: '6th', output: '5th' },
    { input: '7th', output: '6th' },
    { input: '8th', output: '7th' },
    { input: '9th', output: '8th' },
    { input: '10th', output: '9th' },
    { input: '11th', output: '10th' },
    { input: '12th', output: '11th' },
    { input: '13th', output: '12th' },
    { input: '14th', output: '13th' },
    { input: '21st', output: '20th' },
    { input: '101st', output: '100th' },
    { input: '111th', output: '110th' },
    { input: '0th', output: 'bestest' },
    { input: '-1st', output: undefined },
    { input: '1th', output: undefined },
    { input: '1rd', output: undefined },
    { input: '1nd', output: undefined },
    { input: '2th', output: undefined },
    { input: '3th', output: undefined },
    { input: '13rd', output: undefined },
    { input: '12nd', output: undefined },
    { input: '11st', output: undefined },
  ];

  decrementOrdinalsTestData.forEach((item) => {
    test(`decrementOrdinal outputs ${toString(
      item.output,
    )} for input ${toString(item.input)}`, () => {
      expect(Ordinals.decrementOrdinal(item.input)).toBe(item.output);
    });
  });
});
