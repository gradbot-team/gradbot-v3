import { or } from './string-extensions';

const ordinalPattern =
  /^([1-9][0-9]*)?((1[1-3]|[04-9])th|(?<!1)(1st|2nd|3rd))$/i;

/**
 * Gets the ordinal number suffix for an integer. Returns undefined if the input
 * is not an integer.
 */
export const getOrdinalNumberSuffix = (integer: number): string | undefined => {
  let suffix;
  if (Number.isInteger(integer)) {
    suffix = 'th';
    if (integer < 0) {
      integer = -integer;
    }
    if (Math.floor((integer % 100) / 10) !== 1) {
      switch (integer % 10) {
        case 1:
          suffix = 'st';
          break;
        case 2:
          suffix = 'nd';
          break;
        case 3:
          suffix = 'rd';
          break;
      }
    }
  }
  return suffix;
};

/**
 * Increments a non-negative ordinal number, i.e. an integer with suffix 'th'/'st'/'nd'/'rd'.
 */
export const incrementOrdinal = (ordinalString: string): string | undefined => {
  let result;
  if (ordinalString.match(ordinalPattern)) {
    const nums = ordinalString.match(/[0-9]+/);
    if (nums) {
      let numVal = parseInt(nums[0], 10);
      numVal++;
      result = numVal.toString() + or(getOrdinalNumberSuffix(numVal), '');
    }
  }
  return result;
};

/**
 * Decrements a positive ordinal number, i.e. an integer with suffix 'th'/'st'/'nd'/'rd'.
 */
export const decrementOrdinal = (ordinalString: string): string | undefined => {
  let result;
  if (ordinalString.match(ordinalPattern)) {
    const nums = ordinalString.match(/[0-9]+/);
    if (nums) {
      let numVal = parseInt(nums[0], 10);
      if (numVal === 0) {
        result = 'bestest';
      } else {
        numVal--;
        result = numVal.toString() + or(getOrdinalNumberSuffix(numVal), '');
      }
    }
  }
  return result;
};
