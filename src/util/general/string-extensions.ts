export const noIndent = (text: string) => {
  return text.replace(/$\n^\s*/gm, '\n');
};

export const isNullUndefinedOrEmpty = (str?: string) => {
  return str ? str.length === 0 : true;
};

export const isNullUndefinedOrWhiteSpace = (str?: string) => {
  return str ? str.trim().length === 0 : true;
};

export const toString = (str: string | null | undefined): string => {
  if (str !== null && str !== undefined) {
    return str;
  } else if (str === null) {
    return 'null';
  } else if (str === undefined) {
    return 'undefined';
  } else {
    return 'unknown';
  }
};

export const or = (
  str: string | null | undefined,
  replacement: string,
): string => {
  if (str !== null && str !== undefined) {
    return str;
  } else {
    return replacement;
  }
};
