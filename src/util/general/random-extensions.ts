/**
 * @description Returns a random int greater than or equal to 0 and less than
 * exclusiveMax.
 * @param exclusiveMax
 */
export const getRandomInt = (exclusiveMax: number) => {
  return Math.floor(Math.random() * Math.floor(exclusiveMax));
};

/**
 * @description Returns a random integer greater than or equal to min and less
 * than exclusiveMax.
 * @param min
 * @param exclusiveMax
 */
export const getRandomIntInRange = (min: number, exclusiveMax: number) => {
  min = Math.ceil(min); // Integers only please.
  exclusiveMax = Math.floor(exclusiveMax);
  return Math.floor(Math.random() * (exclusiveMax - min)) + min;
};

/**
 * @description returns "true" with probability equal to the supplied parameter.
 * E.g., if the parameter is 0.3 then the function returns "true" 30% of the time.
 * @param probability - a number between 0 and 1
 */
export const getTrueWithProbability = (probability: number) => {
  return Math.random() < probability;
};
