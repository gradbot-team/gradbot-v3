import 'reflect-metadata';
import { Gradbot } from './gradbot/gradbot';
import { log } from './util/log/log';
import { Injector } from './gradbot/injector/injector';
import { ChatProvider } from './gradbot/chat/chat-provider';
import * as yargs from 'yargs';
import { homedir } from 'os';
import { SQLiteDbRelativePath } from './gradbot/constants/constants';

process.on('uncaughtException', (err) => {
  log('Uncaught Exception', err);
  process.exit(1);
});

const argv = yargs.options({
  chatIntegrations: {
    array: true,
    choices: ChatProvider.getAllowedChatIntegrations(),
    demandOption: true,
    description: 'Chat integrations',
    type: 'string',
  },
  databasePath: {
    default: `${homedir()}/${SQLiteDbRelativePath}`,
    description: 'Database file path',
    type: 'string',
  },
}).argv;

const start = (): void => {
  (async (): Promise<void> => {
    await Injector.inject(
      await ChatProvider.createChatAggregator(argv.chatIntegrations),
      argv.databasePath,
    );
    const gradbot = new Gradbot();
    gradbot.start();
  })();
};

start();
