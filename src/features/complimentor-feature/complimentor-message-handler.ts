import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { getRandomInt } from '../../util/general/random-extensions';

export class ComplimentorMessageHandler extends FeatureMessageHandler {
  private readonly adjectives: string[] = [
    'adorable',
    'amazing',
    'awesome',
    'beautiful',
    'better',
    'blithesome',
    'charming',
    'creative',
    'excellent',
    'fabulous',
    'fantastic',
    'favourable',
    'fortuitous',
    'good',
    'gorgeous',
    'great',
    'helpful',
    'impeccable',
    'impressive',
    'incredible',
    'ineffable',
    'inspiring',
    'interesting',
    'kindness',
    'lucky',
    'magnificent',
    'mirthful',
    'outstanding',
    'perfect',
    'propitious',
    'remarkable',
    'rousing',
    'special',
    'spectacular',
    'splendid',
    'stellar',
    'stunning',
    'stupendous',
    'super',
    'sympathetic',
    'thoughtful',
    'upbeat',
    'unassuming',
    'unbelievable',
    'wonderful',
    'wondrous',
    'witty',
  ];
  private readonly compliments: (() => string)[] = [
    () => 'You’re that “Nothing” when people ask me what I’m thinking about,',
    () => `You look ${this._getRandomAdjective()} today,`,
    () =>
      `You’re ${this._startsWithAVowel(this._getRandomAdjective())} cookie,`,
    () => 'I bet you make babies smile,',
    () => `You have ${this._getRandomAdjective()} manners,`,
    () => 'I like your style,',
    () => 'You have the best laugh,',
    () => 'I appreciate you,',
    () => 'You are the most perfect you there is,',
    () =>
      'Our system of inside jokes is so advanced that only you and I get it. ' +
      'And I like that,',
    () => 'You’re strong,',
    () => `Your perspective is ${this._getRandomAdjective()},`,
    () =>
      `You’re ${this._startsWithAVowel(this._getRandomAdjective())} friend,`,
    () => 'You light up the room,',
    () => 'You deserve a hug right now,',
    () => 'You should be proud of yourself,',
    () => `You’re more ${this._getRandomAdjective()} than you realise,`,
    () =>
      `You have ${this._startsWithAVowel(this._getRandomAdjective())} ` +
      'sense of humour,',
    () => 'You’ve got all the right moves!',
    () =>
      `Is that your picture next to “${this._getRandomAdjective()}” in the ` +
      'dictionary?',
    () =>
      `Your ${this._getRandomAdjective()} is a balm to all who encounter it,`,
    () => 'You’re all that and a super-size bag of chips,',
    () => 'On a scale from 1 to 10, you’re an 11,',
    () =>
      `On a scale from ${this._getRandomAdjective()} to ` +
      `${this._getRandomAdjective()}, you’re ` +
      `${this._startsWithAVowel(this._getRandomAdjective())} .`,
    () => 'You are brave,',
    () =>
      `You’re even more ${this._getRandomAdjective()} on the inside than you ` +
      'are on the outside,',
    () => 'You have the courage of your convictions,',
    () => 'Aside from food. You’re my favourite,',
    () =>
      'If cartoon bluebirds were real, a bunch of them would be sitting on ' +
      'your shoulders singing right now,',
    () => 'You are making a difference,',
    () => 'You’re like sunshine on a rainy day,',
    () => 'You bring out the best in other people,',
    () =>
      'Your ability to recall random factoids at just the right time is ' +
      `${this._getRandomAdjective()},`,
    () =>
      `You’re ${this._startsWithAVowel(this._getRandomAdjective())} ` +
      'listener,',
    () =>
      `How is it that you always look ${this._getRandomAdjective()}, even in ` +
      'sweatpants?',
    () =>
      `Everything would be ${this._getRandomAdjective()} if more people ` +
      'were like you!',
    () => 'I bet you sweat glitter,',
    () => 'You were cool way before hipsters were cool,',
    () => 'That colour is perfect on you,',
    () => 'Hanging out with you is always a blast,',
    () =>
      'You always know — and say — exactly what I need to hear when I need ' +
      'to hear it,',
    () => `You smell really ${this._getRandomAdjective()},`,
    () =>
      'You may dance like no one’s watching, but everyone’s watching because ' +
      `you're ${this._startsWithAVowel(this._getRandomAdjective())} dancer!`,
    () => `Being around you makes everything ${this._getRandomAdjective()}!`,
    () => 'When you say, “I meant to do that,” I totally believe you,',
    () =>
      'When you’re not afraid to be yourself is when you’re most ' +
      `${this._getRandomAdjective()},`,
    () => 'Colours seem brighter when you’re around,',
    () =>
      'You’re more fun than a ball pit filled with candy. (And seriously,' +
      ' what could be more fun than that?)',
    () =>
      'That thing you don’t like about yourself is what makes you so ' +
      `${this._getRandomAdjective()},`,
    () => `You’re ${this._getRandomAdjective()},`,
    () => 'Everyday is just BLAH when I don’t see you for reals!',
    () => 'Jokes are funnier when you tell them,',
    () =>
      `You’re ${this._getRandomAdjective()} than a triple-scoop ice cream ` +
      'cone. With sprinkles,',
    () => `Your bellybutton is kind of ${this._getRandomAdjective()},`,
    () => `Your hair looks ${this._getRandomAdjective()},`,
    () => 'You’re one of a kind!',
    () => `You’re ${this._getRandomAdjective()},`,
    () =>
      'If you were a box of crayons, you’d be the giant name-brand one with ' +
      'the built-in sharpener,',
    () => 'You should be thanked more often. So thank you!!',
    () =>
      `Our community is ${this._getRandomAdjective()} because you’re in it,`,
    () =>
      'Someone is getting through something hard right now because you’ve got' +
      ' their back,',
    () => 'You have the best ideas,',
    () => 'You always know how to find that silver lining,',
    () =>
      'Everyone gets knocked down sometimes, but you always get back up and ' +
      'keep going,',
    () => 'You’re a candle in the darkness,',
    () =>
      `You’re ${this._startsWithAVowel(this._getRandomAdjective())} ` +
      'example to others,',
    () => 'Being around you is like being on a happy little vacation,',
    () => 'You always know just what to say,',
    () =>
      'You’re always learning new things and trying to ' +
      `${this._getRandomAdjective()} yourself, which is awesome,`,
    () =>
      'If someone based an Internet meme on you, it would have ' +
      `${this._getRandomAdjective()} grammar,`,
    () => 'You could survive a Zombie apocalypse,',
    () => 'You’re more fun than bubble wrap,',
    () => 'When you make a mistake, you fix it,',
    () => 'Who raised you? They deserve a medal for a job well done,',
    () => `You’re ${this._getRandomAdjective()} at figuring stuff out,`,
    () => `Your voice is ${this._getRandomAdjective()},`,
    () =>
      `The people you love are ${this._getRandomAdjective()} to have you in ` +
      'their lives,',
    () => 'You’re like a breath of fresh air,',
    () =>
      `You’re ${this._getRandomAdjective()} — and that’s the least ` +
      'interesting thing about you, too,',
    () => `You’re so ${this._getRandomAdjective()},`,
    () => `Your ${this._getRandomAdjective()} potential seems limitless,`,
    () =>
      'You’re the coolest person I know. And I consider myself best friends ' +
      'with like all celebrities, so. . . ,',
    () => 'You’re irresistible when you blush,',
    () =>
      'Actions speak louder than words, and yours tell ' +
      `${this._startsWithAVowel(this._getRandomAdjective())} story,`,
    () => 'Somehow you make time stop and fly at the same time,',
    () =>
      'When you make up your mind about something, nothing stands in your way,',
    () => 'You seem to really know who you are,',
    () => `Any team would be ${this._getRandomAdjective()} to have you on it,`,
    () =>
      'In high school I bet you were voted “most likely to keep being ' +
      `${this._getRandomAdjective()}.”`,
    () => 'I bet you do the crossword puzzle in ink,',
    () =>
      'If you were a scented candle they’d call it Perfectly Imperfect ' +
      '(and it would smell like summer),',
    () => 'There’s ordinary, and then there’s you,',
    () => 'You’re someone’s reason to smile,',
    () =>
      `You’re even ${this._getRandomAdjective()} than a unicorn, because ` +
      'you’re real,',
    () => 'How do you keep being so funny and making everyone laugh?',
    () =>
      `You have a ${this._startsWithAVowel(this._getRandomAdjective())} ` +
      'head on your shoulders,',
    () =>
      `Has anyone ever told you that you have ${this._getRandomAdjective()} ` +
      'posture?',
    () =>
      `The way you treasure your loved ones is ${this._getRandomAdjective()},`,
    () => `You’re really something ${this._getRandomAdjective()},`,
    () => 'You’re a gift to those around you,',
  ];

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.isMessageForGradbot()) {
      const messageText: string = this.getSanitisedText();

      if (messageText === 'compliment') {
        return this.createMessage(
          this._getRandomCompliment() + ` ${this.message.user.displayName}.`,
          'I was asked for a compliment',
        );
      } else if (messageText.match(/^compliment\s+/)) {
        const name: string = messageText.replace(/^compliment\s+/, '').trim();

        const complimentee = await this.getUserWithDisplayName(name);

        if (complimentee) {
          return this.createMessage(
            `${this._getRandomCompliment()} ${complimentee.displayName}.`,
            `i was asked to compliment ${complimentee.displayName}`,
          );
        } else {
          return this.createMessage(
            `I don't know who ${name} is.`,
            `i was asked to compliment ${name}` + 'but they did not exist.',
          );
        }
      }
    }

    return null;
  }

  private _getRandomCompliment() {
    return this.compliments[getRandomInt(this.compliments.length)]();
  }

  private _getRandomAdjective() {
    return this.adjectives[getRandomInt(this.adjectives.length)];
  }

  private _startsWithAVowel(str: string): string {
    return /[aeiouAEIOU]/.test(str.charAt(0)) ? 'an' : 'a' + ` ${str}`;
  }
}
