import { noIndent } from '../../util/general/string-extensions';
import { Feature } from '../../gradbot/feature/feature';
import { ComplimentorMessageHandler } from './complimentor-message-handler';

export class ComplimentorFeature extends Feature {
  public readonly name = 'Complimentor';
  public readonly iCanInfo = 'give compliments';
  public readonly instructions = noIndent(
    `• *g compliment <user name>* – I'll compliment another person in the chat
     • *g compliment* – I'll compliment you`,
  );
  protected readonly messageHandlerType = ComplimentorMessageHandler;
}
