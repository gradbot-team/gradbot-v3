import 'reflect-metadata';
import { ComplimentorFeature } from './complimentor-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(ComplimentorFeature);
util.addUser('test_user_2');

describe('ComplimentorFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  test('responds with a compliment to originator', async () =>
    await util.expectReplyToBeTruthy('g compliment'));

  test('responds with a compliment to another user', async () =>
    await util.expectReplyToBeTruthy('g compliment test_user_2'));

  test('does not respond to compliment to fake user', async () =>
    await util.expectReplyToHaveText(
      'g compliment fakeUser',
      "I don't know who fakeUser is.",
    ));
});
