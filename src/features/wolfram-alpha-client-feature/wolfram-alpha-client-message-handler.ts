import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { toString } from '../../util/general/string-extensions';

export class WolframAlphaClientMessageHandler extends FeatureMessageHandler {
  private static readonly apiKey: string = 'WOLFRAM_ALPHA_API_KEY';
  private static readonly urlBase: string =
    'https://api.wolframalpha.com/v1/result?units=metric&appid=';
  private static readonly urlInputParamName: string = '&i=';

  protected readonly triggerProbability = 0.02;

  public isMessageTrigger(): boolean {
    // Triggered by single-sentence questions
    const sanitisedText = this.getSanitisedText();
    return (
      sanitisedText.endsWith('?') &&
      !sanitisedText.substring(0, sanitisedText.length - 1).match(/[.?!]/)
    );
  }

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const url =
      WolframAlphaClientMessageHandler.urlBase +
      toString(process.env[WolframAlphaClientMessageHandler.apiKey]) +
      WolframAlphaClientMessageHandler.urlInputParamName +
      encodeURIComponent(this.getSanitisedText());

    const apiResult = await this.fetchAPIText(url).catch(() => null);

    if (apiResult) {
      return this.createMessage(
        apiResult,
        `${this.message.user.displayName} asked me a question so I looked it up online.`,
      );
    } else {
      return null;
    }
  }
}
