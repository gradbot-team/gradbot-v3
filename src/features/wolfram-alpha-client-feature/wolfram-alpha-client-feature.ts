import { Feature } from '../../gradbot/feature/feature';
import { WolframAlphaClientMessageHandler } from './wolfram-alpha-client-message-handler';

export class WolframAlphaClientFeature extends Feature {
  public readonly name = 'Wolfram Alpha Client';
  public readonly iCanInfo =
    'look up the answers to questions online using Wolfram Alpha';
  public readonly instructions =
    'This feature responds to any message that is ' +
    'addressed directly to Gradbot; looks like a single sentence; and ends with a question mark.';
  protected readonly messageHandlerType = WolframAlphaClientMessageHandler;
}
