import 'reflect-metadata';
import { WolframAlphaClientFeature } from './wolfram-alpha-client-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(WolframAlphaClientFeature);
const apiKey = 'test_api_key';
const apiBase = `https://api.wolframalpha.com/v1/result?units=metric&appid=${apiKey}&i=`;
process.env.WOLFRAM_ALPHA_API_KEY = apiKey;

describe('WolframAlphaClientFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  const validTriggers = [
    'this is a question?',
    'This is 1 question - it has punctuation: ,/><@~#\';[]}{"£$%^&*()_+=-¬\\|`¦}?',
  ];

  for (const trigger of validTriggers) {
    test(`responds to valid trigger ${trigger}`, async () => {
      util.api.urlReturnValues[apiBase + encodeURIComponent(trigger)] =
        'test response';
      await util.expectReplyToHaveText('g ' + trigger, 'test response');
    });
  }

  const invalidTriggers = [
    'this is a sentence.',
    'this is a question? This is a question?',
    'this is a sentence. This is a question?',
    'this is a question? And some more text',
    'this is a sentence!?',
    'this is a poorly formatted question.?',
  ];

  for (const trigger of invalidTriggers) {
    test(`does not respond to invalid trigger ${trigger}`, async () => {
      util.api.urlReturnValues[apiBase + encodeURIComponent(trigger)] =
        'test response';
      await util.expectReplyToBeNull('g ' + trigger);
    });
  }
});
