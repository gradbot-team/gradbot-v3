import { Feature } from '../../gradbot/feature/feature';
import { ChuckNorrisMessageHandler } from './chuck-norris-message-handler';

export class ChuckNorrisFeature extends Feature {
  public readonly name = 'Chuck Norris';
  public readonly iCanInfo = 'tell you about Chuck Norris';
  public readonly instructions = '• *g chuck*\n• *g Chuck Norris*';
  protected readonly messageHandlerType = ChuckNorrisMessageHandler;
}
