import 'reflect-metadata';
import { ChuckNorrisFeature } from './chuck-norris-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(ChuckNorrisFeature);

const setValidAPIResponse = () =>
  (util.api.urlReturnValues['https://api.icndb.com/jokes/random'] = {
    value: { joke: 'test' },
  });

describe('ChuckNorrisFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds to "chuck"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText('Gradbot, chuck', 'test');
  });

  test('responds to "Chuck Norris"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText('Chuck Norris, gradbot', 'test');
  });

  test('deals with invalid API data', async () => {
    util.api.urlReturnValues['https://api.icndb.com/jokes/random'] = {
      value: 'test',
    };
    await util.expectReplyToHaveText(
      'chuck norris, gradbot',
      "I can't remember any Chuck Norris jokes right now",
    );
  });

  test('deals with missing API data', async () => {
    util.api.urlReturnValues['https://api.icndb.com/jokes/random'] = null;
    await util.expectReplyToHaveText(
      'chuck norris, gradbot',
      "I can't remember any Chuck Norris jokes right now",
    );
  });
});
