import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class ChuckNorrisMessageHandler extends FeatureMessageHandler {
  private readonly CHUCK_REGEX = /^chuck(\W+norris)?\W*$/;
  private readonly CHUCK_API_URL = 'https://api.icndb.com/jokes/random';

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.matchSanitisedLowercaseTextIfForGradbot(this.CHUCK_REGEX)) {
      const data = await this.fetchAPI<ChuckAPIData>(this.CHUCK_API_URL);

      if (data && data.value && data.value.joke) {
        return this.createMessage(
          data.value.joke,
          `${this.message.user.displayName} asked me to tell them a Chuck Norris joke`,
        );
      }

      return this.createMessage(
        "I can't remember any Chuck Norris jokes right now",
        `${this.message.user.displayName} asked me to tell them a Chuck Norris joke, but API is down`,
      );
    }
    return null;
  }
}

interface ChuckAPIData {
  value?: ChuckAPIValue;
}
interface ChuckAPIValue {
  joke?: string;
}
