import { Feature } from '../../gradbot/feature/feature';
import { TeamMessageHandler } from './team-message-handler';
import { TEAM_HELP_TEXT } from './team-constants';

export class TeamFeature extends Feature {
  public readonly name = 'Team';
  public readonly iCanInfo = 'build a team name from initials';
  public readonly instructions = TEAM_HELP_TEXT;
  protected readonly messageHandlerType = TeamMessageHandler;
}
