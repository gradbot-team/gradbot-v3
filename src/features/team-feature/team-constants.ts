import { noIndent } from '../../util/general/string-extensions';
export const TEAM_HELP_TEXT: string = noIndent(
  `Team builds a team name from the initials you provide.
  Usage:
   • team 'initials'`,
);

export const TEAM_TOO_BIG =
  'Your team is too big! Please use 5 or fewer initials.';

export const TEAM_TOO_SMALL =
  'Your team is too small! Make some friends, stat!';

export const TEAM_ALPHA_ONLY =
  'Initials must all be alphabetic characters. You know how it is.';

export const TEAM_NO_TEAM_FOUND =
  "Your initials don't make a cool team name! Find better friends.";
