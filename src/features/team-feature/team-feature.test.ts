import 'reflect-metadata';
import { TeamFeature } from './team-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import {
  TEAM_HELP_TEXT,
  TEAM_TOO_BIG,
  TEAM_TOO_SMALL,
  TEAM_ALPHA_ONLY,
  TEAM_NO_TEAM_FOUND,
} from './team-constants';
import { container } from 'tsyringe';
import { Dictionary } from '../../gradbot/dictionary/dictionary';
import { TestDictionary } from '../../util/test/test-dictionary';
import * as randomExtensions from '../../util/general/random-extensions';

const util = FeatureTestUtil.createForFeatureType(TeamFeature);
const dict = container.resolve<TestDictionary>(Dictionary.name);

describe('TeamFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  test('replies to a no argument call with the help string', async () => {
    await util.expectReplyToHaveText('gradbot, team', TEAM_HELP_TEXT);
  });

  test('replies to a too-many argument call with the help string', async () => {
    await util.expectReplyToHaveText('gradbot, team dave dave', TEAM_HELP_TEXT);
  });

  test('returns with an appropriate team name (one valid option)', async () => {
    dict.setDictionary(`Dave
Juniper
Sarah`);
    await util.expectReplyToHaveText(
      'gradbot, team JNPR',
      'Your team name is: JUNIPER (JNPR; JuNiPeR)',
    );
  });

  test('returns an appropriate team name based off the random number generator response', async () => {
    dict.setDictionary(`Janiper
Juniper
Jyniper`);

    const spy = jest.spyOn(randomExtensions, 'getRandomInt');
    spy.mockReturnValue(0);

    await util.expectReplyToHaveText(
      'gradbot, team JNPR',
      'Your team name is: JANIPER (JNPR; JaNiPeR)',
    );

    spy.mockReturnValue(1);
    await util.expectReplyToHaveText(
      'gradbot, team JNPR',
      'Your team name is: JUNIPER (JNPR; JuNiPeR)',
    );

    spy.mockReturnValue(2);
    await util.expectReplyToHaveText(
      'gradbot, team JNPR',
      'Your team name is: JYNIPER (JNPR; JyNiPeR)',
    );

    spy.mockRestore();
  });

  test('returns the error response if no team name can be found', async () => {
    dict.setDictionary(`Dave
Juniper
Sarah`);
    await util.expectReplyToHaveText('gradbot, team BILL', TEAM_NO_TEAM_FOUND);
  });

  test('rejects too many initials', async () => {
    await util.expectReplyToHaveText('gradbot, team BILLYJ', TEAM_TOO_BIG);
  });

  test('rejects too few initials', async () => {
    await util.expectReplyToHaveText('gradbot, team B', TEAM_TOO_SMALL);
  });

  test('rejects initials that include numerics or special characters', async () => {
    await util.expectReplyToHaveText('gradbot, team B123', TEAM_ALPHA_ONLY);

    await util.expectReplyToHaveText('gradbot, team 1B23', TEAM_ALPHA_ONLY);

    await util.expectReplyToHaveText('gradbot, team 123', TEAM_ALPHA_ONLY);

    await util.expectReplyToHaveText('gradbot, team !AB', TEAM_ALPHA_ONLY);

    await util.expectReplyToHaveText('gradbot, team A!C', TEAM_ALPHA_ONLY);
  });
});
