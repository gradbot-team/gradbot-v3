import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { getRandomInt } from '../../util/general/random-extensions';
import {
  TEAM_HELP_TEXT,
  TEAM_TOO_BIG,
  TEAM_TOO_SMALL,
  TEAM_ALPHA_ONLY,
  TEAM_NO_TEAM_FOUND,
} from './team-constants';

export class TeamMessageHandler extends FeatureMessageHandler {
  protected readonly triggerProbability = 1;

  readonly maxLength: number = 5;

  readonly teamTriggerRegexp: RegExp = new RegExp('^team', 'i');

  readonly initialsRegexp: RegExp = new RegExp('^[A-Za-z]+$', 'i');

  public isMessageTrigger(): boolean {
    if (this.isMessageForGradbot()) {
      const msg = this.getSanitisedText();
      return this.teamTriggerRegexp.test(msg);
    }
    return false;
  }

  // Ripped off StackOverflow: https://stackoverflow.com/questions/39927452/recursively-print-all-permutations-of-a-string-javascript
  private getPermutations(initials: string): string[] {
    if (initials.length < 2) {
      return [initials];
    }

    const permutations: string[] = [];
    for (let i = 0; i < initials.length; i++) {
      const char = initials[i];
      if (initials.indexOf(char) !== i) {
        continue;
      }
      const remainingInitials: string =
        initials.slice(0, i) + initials.slice(i + 1, initials.length);
      for (const subInitials of this.getPermutations(remainingInitials)) {
        permutations.push(char + subInitials);
      }
    }
    return permutations;
  }

  private getDictionaryQueries(permutations: string[]): string[] {
    const queries: string[] = [];
    for (const permutation of permutations) {
      const permArray = permutation.split('');
      const query = permArray
        .flatMap((value, index, array) =>
          array.length - 1 !== index ? value + '[A-Za-z]{0,3}' : value,
        )
        .join('');
      queries.push('^' + query + '(?:/.*)?$');
    }
    return queries;
  }

  private preprocessDictionary(): string {
    // Load the dictionary
    let dictionary = this.getDictionary();

    // Expand every entry that contains '/S' to also add that word with an S on the end.
    const regex = new RegExp('^([A-Za-z]+)/[A-Z]*S[A-Z]*$', 'gim');
    const matches = this.getAllMatches(dictionary, regex);
    for (const match of matches) {
      if (!match[1].includes('/')) {
        dictionary += '\n' + match[1] + 's';
      }
    }

    return dictionary;
  }

  // Normally you would use String.matchAll, but it's part of ES2020
  // and TS doesn't support the transpilation, which means
  // jest doesn't support it... it's a mess.
  private getAllMatches(searchText: string, regex: RegExp) {
    const matches: string[] = [];
    let match = regex.exec(searchText);
    while (match !== null) {
      matches.push(match[0]);
      match = regex.exec(searchText);
    }
    return matches;
  }

  private getCandidateWords(dictionaryQueries: string[]): string[] {
    const dictionary = this.preprocessDictionary();

    let candidates: string[] = [];
    for (const query of dictionaryQueries) {
      const regex = new RegExp(query, 'gim');
      candidates = candidates.concat(this.getAllMatches(dictionary, regex));
    }
    return candidates;
  }

  private prepareTeamName(
    candidate: string,
    initials: string,
  ): [string, string] {
    candidate = this.stripTrailingDictionaryCharacters(candidate);
    const result: [string, string] = this.capitaliseTeamInitials(
      candidate,
      initials,
    );
    return result;
  }

  private stripTrailingDictionaryCharacters(candidate: string): string {
    const stripped = candidate.replace(/\/.*/, '');
    return stripped;
  }

  private capitaliseTeamInitials(
    candidate: string,
    initials: string,
  ): [string, string] {
    if (initials.length === candidate.length) {
      return [candidate.toUpperCase(), candidate.toUpperCase()];
    } else {
      // Capitalise first and last letters, discard those initials immediately
      initials = initials.toLowerCase();
      candidate = candidate.toLowerCase();
      initials = initials.replace(candidate.charAt(0), '');
      initials = initials.replace(candidate.charAt(candidate.length - 1), '');
      candidate = candidate.charAt(0).toUpperCase() + candidate.slice(1);
      candidate =
        candidate.slice(0, candidate.length - 1) +
        candidate.charAt(candidate.length - 1).toUpperCase();
      for (const initial of initials) {
        candidate = candidate.replace(initial, initial.toUpperCase());
      }

      // We could have carried this with us the whole time, but it
      // seemed like a faff, so recalculate the abbreviation here.
      let abbreviation = '';
      for (const char of candidate) {
        if (char === char.toUpperCase()) {
          abbreviation += char;
        }
      }
      return [candidate, abbreviation];
    }
  }

  private generateTeam(initials: string): FeatureMessage {
    // Team works by generating every permutation of the input initials, then
    // adding a '[A-Za-z]{0,3}' regex pattern between every letter
    // and doing a grep on the dictionary, returning a random result.

    if (initials.length > this.maxLength) {
      return this.createMessage(
        TEAM_TOO_BIG,
        'I rejected generating a team name as the input was too long.',
      );
    }

    if (initials.length < 2) {
      return this.createMessage(
        TEAM_TOO_SMALL,
        'I rejected generating a team name as the input was too short.',
      );
    }

    if (!this.initialsRegexp.test(initials)) {
      return this.createMessage(
        TEAM_ALPHA_ONLY,
        "I rejected generating a team name as the input wasn't alphabetic.",
      );
    }

    // Generate all permutations of the initials
    const permutations: string[] = this.getPermutations(initials);

    // Generate all the query strings
    const dictionaryQueries: string[] = this.getDictionaryQueries(permutations);

    // Get the list of candidate words
    const candidateWords: string[] = this.getCandidateWords(dictionaryQueries);

    if (candidateWords.length === 0) {
      return this.createMessage(
        TEAM_NO_TEAM_FOUND,
        `I failed to generate a team name from "${initials}"`,
      );
    }

    // Return a meaningful team name!
    const candidateWord = candidateWords[getRandomInt(candidateWords.length)];
    const result: [string, string] = this.prepareTeamName(
      candidateWord,
      initials,
    );

    let teamNameString: string = result[0].toUpperCase();
    if (teamNameString.length !== initials.length) {
      teamNameString += ' (' + result[1] + '; ' + result[0] + ')';
    }

    return this.createMessage(
      `Your team name is: ${teamNameString}`,
      `I was asked to generate a team name from "${initials}"`,
    );
  }

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    // Input is a string that takes one parameter:
    // the initials of the people you want to build a team name for.
    const msg: string = this.getSanitisedText();
    const tokens: string[] = msg.split(/\s+/);

    // isMessageTrigger guarantees token 1 is 'team'
    let message: FeatureMessage;
    if (tokens.length === 2) {
      const initials: string = tokens[1].toLowerCase();
      message = this.generateTeam(initials);
    } else {
      message = this.createMessage(
        TEAM_HELP_TEXT,
        "I didn't understand the input so I showed the usage.",
      );
    }
    return Promise.resolve(message);
  }
}
