export class ReminderUtil {
  // Supports the following formats:
  //   n minute(s)/hour(s)/day(s)/week(s)/year(s)
  //   hh:mm[am/pm]
  public static readonly TimeSpanPattern =
    '[0-9]+(\\s+)?(m(in(ute)?)?|h(our)?|d(ay)?|w(eek)?|y(ear)?)s?';
  public static readonly TimePattern = '[0-9]{1,2}((:[0-9]{2}([ap]m)?)|[ap]m)';
  public static readonly GetUserPattern = '(every(one|body)|all|me)';
  /* TODO?
  const dateRegExp =
    new RegExp(
      '^[0-9]{2}\/[0-9]{2}(\/([0-9]{2}){1,2})?(\s+' + ReminderUtil.TimePattern + ')?|' + ReminderUtil.TimePattern,
      'i');
    */

  /**
   * @description Uses a regexp to match everything between the start of a string
   * and the first occurrence of a time or timespan phrase. Returns undefined if
   * there is not time(span) phrase.
   */
  public static getUserString(text: string): string | undefined {
    const matches = text.match(
      new RegExp(
        '^.*?' +
          '(?=(' +
          ReminderUtil.TimeSpanPattern +
          '|' +
          ReminderUtil.TimePattern +
          '))',
        'i',
      ),
    );
    return matches ? matches[0].trim() : undefined;
  }

  public static getRemindTimeFromTimeSpan(timeSpanString: string): Date | null {
    if (
      !timeSpanString.match(
        ReminderUtil.buildTimeRegExp(ReminderUtil.TimeSpanPattern),
      )
    ) {
      return null;
    }

    const counts = timeSpanString.match(/[0-9]+/);
    const count = counts && counts.length > 0 ? parseInt(counts[0], 10) : 0;
    if (count <= 0) {
      return null;
    } else {
      // Constructing the date this way makes it easier to mock the Date constructor.
      const now = new Date(Date.now());
      const year = now.getFullYear();
      const month = now.getMonth();
      const day = now.getDate();
      const hour = now.getHours();
      const minute = now.getMinutes();
      const second = now.getSeconds();
      let date = null;
      const unit = timeSpanString
        .replace(count.toString(), '')
        .trim()
        .toLowerCase()
        .replace(/s$/, '');
      switch (unit) {
        case 'minute':
        case 'min':
        case 'm':
          if (count <= 60 * 24 * 365 * 10) {
            date = new Date(year, month, day, hour, minute + count, second);
          }
          break;
        case 'hour':
        case 'h':
          if (count <= 24 * 365 * 10) {
            date = new Date(year, month, day, hour + count, minute, second);
          }
          break;
        case 'day':
        case 'd':
          if (count <= 365 * 10) {
            date = new Date(year, month, day + count, hour, minute, second);
          }
          break;
        case 'week':
        case 'w':
          if (count <= 521) {
            date = new Date(year, month, day + count * 7, hour, minute, second);
          }
          break;
        case 'year':
        case 'y':
          if (count <= 10) {
            date = new Date(year + count, month, day, hour, minute, second);
          }
          break;
        default:
          return null;
      }

      return date;
    }
  }

  public static getRemindTimeFromTime(timeString: string): Date | null {
    if (
      !timeString.match(ReminderUtil.buildTimeRegExp(ReminderUtil.TimePattern))
    ) {
      return null;
    }

    // Constructing the date this way makes it easier to mock the Date constructor.
    const now = new Date(Date.now());
    let day = now.getDate();
    let hours = now.getHours();
    let minutes = now.getMinutes();
    const ampm = timeString.match(new RegExp(/[a|p]m$/i));
    if (ampm) {
      // Strip off the am/pm.
      timeString = timeString.substring(0, timeString.length - 2);
    }
    // Split into hours and minutes
    const timeParts = timeString.split(':');

    let inputHours = parseInt(timeParts[0], 10);
    if (inputHours < 0 || inputHours > 24) {
      // Invalid hours value
      return null;
    }

    if (ampm) {
      if (inputHours < 1 || inputHours > 12) {
        // Invalid value for 12-hour format
        return null;
      }

      if (
        (ampm[0] === 'pm' && inputHours < 12) ||
        (ampm[0] === 'am' && inputHours === 12)
      ) {
        // Convert to 24-hour time
        inputHours = inputHours + 12;
      }
    }

    // Standard 24-hour format
    inputHours = inputHours % 24;

    let inputMinutes = 0;
    if (timeParts.length > 1) {
      inputMinutes = parseInt(timeParts[1], 10);
    }

    if (inputMinutes < 0 || inputMinutes > 59) {
      // Invalid minutes value
      return null;
    }

    if (
      inputHours < hours ||
      (inputHours === hours && inputMinutes <= minutes)
    ) {
      // Time is in the past, so increment the day.
      day++;
    }

    hours = inputHours;
    minutes = inputMinutes;

    return new Date(now.getFullYear(), now.getMonth(), day, hours, minutes);
  }

  public static formatDate(date: Date): string {
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear() % 100;
    return `${hours}:${minutes} on ${day}/${month}/${year}`;
  }

  private static buildTimeRegExp(timePattern: string): RegExp {
    return new RegExp('^' + timePattern + '$', 'i');
  }
}
