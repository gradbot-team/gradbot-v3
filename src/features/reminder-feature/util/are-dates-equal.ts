export const areDatesEqual = (date1: Date | null, date2: Date | null) => {
  return (
    (date1 === null && date2 === null) ||
    (date1 !== null &&
      date2 !== null &&
      date1.getFullYear() === date2.getFullYear() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getDate() === date2.getDate() &&
      date1.getHours() === date2.getHours() &&
      date1.getMinutes() === date2.getMinutes())
  );
};
