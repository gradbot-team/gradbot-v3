import { Feature } from '../../gradbot/feature/feature';
import { ReminderMessageHandler } from './reminder-message-handler';

export class ReminderFeature extends Feature {
  public readonly name = 'Reminder';
  public readonly iCanInfo = 'set public or private reminders';
  public readonly instructions =
    'Syntax: *g remind [who] <time> <text>*\n' +
    '  • *who:* can be any of:\n' +
    '    - "me" - sets a private reminder for yourself\n' +
    '    - "all"/"everyone"/"everybody" - sets a reminder for the whole chat\n' +
    '    If [who] is omitted, the reminder will be set for the whole chat.' +
    '  • *time:* takes the format "n minute/hour/day/week/year(s)" or "12(:00)" or "5(:00)am/pm"\n' +
    '  • *text:* the content of the reminder message';
  protected readonly messageHandlerType = ReminderMessageHandler;
}
