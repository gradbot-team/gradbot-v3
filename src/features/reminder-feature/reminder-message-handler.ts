import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { ReminderUtil } from './reminder-util';
import * as uuid from 'uuid';

export class ReminderMessageHandler extends FeatureMessageHandler {
  public readonly featureName = 'Reminder';
  public readonly iCanInfo = '';
  public readonly instructions = '';

  private readonly remindRegExp = new RegExp(
    '^remind\\s(.+\\s)?(' +
      ReminderUtil.TimeSpanPattern +
      '|' +
      ReminderUtil.TimePattern +
      ')',
    'i',
  );
  private readonly remindReplaceRegExp = /^remind/i;

  private sanitisedText = '';

  public isMessageTrigger(): boolean {
    this.sanitisedText = this.getSanitisedText();
    return (
      this.isMessageForGradbot() &&
      this.sanitisedText.match(this.remindRegExp) !== null
    );
  }

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.isMessageTrigger()) {
      let replyText = '';
      let replyReason = `${this.message.user.displayName} asked me to set a reminder for`;

      let remindText = this.sanitisedText
        .replace(this.remindReplaceRegExp, '')
        .trim();
      const remindWhoString = ReminderUtil.getUserString(remindText);

      let validWhoString = true;
      let remindMe = false;
      if (
        remindWhoString === '' ||
        remindWhoString?.match(/^(every(one|body)|all)$/i)
      ) {
        replyReason += ' everyone in the chat';
      } else if (remindWhoString?.match(/^me$/i)) {
        remindMe = true;
        replyReason += ' themselves';
      } else {
        validWhoString = false;
        replyText =
          'You may only set reminders for yourself or for everyone in the chat.';
        replyReason += " an invalid user'";
      }

      if (validWhoString) {
        let timePhrase = '';
        let remindTime: Date | null = null;

        // Remove the user string
        if (remindWhoString) {
          remindText = remindText.replace(remindWhoString, '').trim();
        }

        // Try matching a time span phrase eg. "2 hours", "1 week".
        // The phrase must be at the start of the message after removing the
        // user string.
        const timeSpanMatch = remindText.match(
          ReminderMessageHandler.buildTimeRegExp(ReminderUtil.TimeSpanPattern),
        );
        if (timeSpanMatch) {
          timePhrase = timeSpanMatch[0];
          remindTime = ReminderUtil.getRemindTimeFromTimeSpan(timePhrase);
          replyReason += ` in ${timePhrase}`;
        } else {
          // Try matching a time, eg. "17:00" or "5pm"
          const timeMatch = remindText.match(
            ReminderMessageHandler.buildTimeRegExp(ReminderUtil.TimePattern),
          );
          if (timeMatch) {
            timePhrase = timeMatch[0];
            remindTime = ReminderUtil.getRemindTimeFromTime(timePhrase);
            replyReason += ` at ${timePhrase}`;
          }
        }

        if (remindTime) {
          // Remove the time phrase and add an appropriate prefix.
          const userDisplayName = this.message.user.displayName;
          const remindTextPrefix = remindMe
            ? `Hey ${userDisplayName}, you asked me to remind you: `
            : `${userDisplayName} asked me to remind everyone: `;
          remindText =
            remindTextPrefix + remindText.replace(timePhrase, '').trim();

          const message = remindMe
            ? this.createMessageToUser(
                remindText,
                replyReason,
                this.message.user.id,
              )
            : this.createMessage(remindText, replyReason);
          await this.scheduleMessage(uuid.v4(), remindTime, message);
          replyText = `Ok, I'll remind ${
            remindMe ? 'you' : 'everyone'
          } at ${ReminderUtil.formatDate(remindTime)}.`;
        } else {
          replyText = "Sorry, I'm not sure when you want me to set a reminder.";
          replyReason +=
            ' at an unknown time, or a time that was either in the past, or more than 10 years in the future';
        }
      }

      replyReason += '.';
      return Promise.resolve(this.createMessage(replyText, replyReason));
    } else {
      return Promise.resolve(null);
    }
  }

  private static buildTimeRegExp(timePattern: string) {
    return new RegExp('^' + timePattern + '(?=\\s)', 'i');
  }
}
