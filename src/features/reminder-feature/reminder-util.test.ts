import 'reflect-metadata';
import { ReminderUtil } from './reminder-util';

jest
  .spyOn(global.Date, 'now')
  .mockImplementation(() => new Date(2020, 3, 18, 12, 0, 0).valueOf());

describe('ReminderUtil', () => {
  const messageTexts: { [key: string]: string | undefined } = {
    '1 hour go to bed at 10pm': '',
    'me 1 hour your appointment is in 10 minutes': 'me',
    'all 1 hour it is wednesday my dudes': 'all',
    'everyone 1 hour 1 hour 1 hour': 'everyone',
    'everybody 5:00pm test': 'everybody',
    'me 5pm test': 'me',
    'me 17:00 test': 'me',
    'ME 17:00 test': 'ME',
    'me     1 hour test': 'me',
    'some invalid garbage 1 hour test': 'some invalid garbage',
    'me test': undefined,
  };

  for (const key of Object.keys(messageTexts)) {
    test(`gets the user string from message text ${key}`, () => {
      expect(ReminderUtil.getUserString(key)).toBe(messageTexts[key]);
    });
  }

  const timeSpansTestData: { [key: string]: Date } = {
    '1 minute': new Date(2020, 3, 18, 12, 1),
    '10 minutes': new Date(2020, 3, 18, 12, 10),
    '1 Minute': new Date(2020, 3, 18, 12, 1),
    '1 MINUTE': new Date(2020, 3, 18, 12, 1),
    '2 hours': new Date(2020, 3, 18, 14, 0),
    '3 days': new Date(2020, 3, 21, 12, 0),
    '4 weeks': new Date(2020, 4, 16, 12, 0),
    '5 years': new Date(2025, 3, 18, 12, 0),
    '6 min': new Date(2020, 3, 18, 12, 6),
    '7m': new Date(2020, 3, 18, 12, 7),
    '8 mins': new Date(2020, 3, 18, 12, 8),
    '9h': new Date(2020, 3, 18, 21, 0),
    '10d': new Date(2020, 3, 28, 12, 0),
    '11w': new Date(2020, 6, 4, 12, 0),
    '1y': new Date(2021, 3, 18, 12, 0),
    '12minutes': new Date(2020, 3, 18, 12, 12),
  };

  for (const key of Object.keys(timeSpansTestData)) {
    test(`formats time span phrase ${key} properly`, () => {
      expect(ReminderUtil.getRemindTimeFromTimeSpan(key)).toStrictEqual(
        timeSpansTestData[key],
      );
    });
  }

  const invalidTimeSpansTestData = [
    '0 minutes',
    '0.5 minutes',
    '-1 minute',
    'ten minutes',
    '30 seconds',
    '1 month',
    'tomorrow',
  ];

  for (const item of invalidTimeSpansTestData) {
    test(`rejects invalid time span phrase ${item}`, () => {
      expect(ReminderUtil.getRemindTimeFromTimeSpan(item)).toBe(null);
    });
  }

  const tooLongTimeSpanPhrases = [
    '5256001 minutes',
    '87601 hours',
    '3651 days',
    '522 weeks',
    '11 years',
  ];

  for (const item of tooLongTimeSpanPhrases) {
    test(`rejects time span of ${item} because it's over 10 years/3560 days`, () => {
      expect(ReminderUtil.getRemindTimeFromTimeSpan(item)).toBe(null);
    });
  }

  const timesTestData: { [key: string]: Date } = {
    '18:00': new Date(2020, 3, 18, 18, 0),
    '6:00': new Date(2020, 3, 19, 6, 0),
    '06:00': new Date(2020, 3, 19, 6, 0),
    '12:45': new Date(2020, 3, 18, 12, 45),
    '24:30': new Date(2020, 3, 19, 0, 30),
    '12pm': new Date(2020, 3, 19, 12, 0),
    '12am': new Date(2020, 3, 19, 0, 0),
    '6am': new Date(2020, 3, 19, 6, 0),
    '5:30pm': new Date(2020, 3, 18, 17, 30),
  };

  for (const key of Object.keys(timesTestData)) {
    test(`formats time phrase ${key} properly`, () => {
      expect(ReminderUtil.getRemindTimeFromTime(key)).toStrictEqual(
        timesTestData[key],
      );
    });
  }

  const invalidTimesTestData = [
    '0am',
    '-1am',
    '13pm',
    '-1:00',
    '25:00',
    '12:60',
    '12:-1',
    '5',
    'am',
  ];

  for (const item of invalidTimesTestData) {
    test(`rejects invalid time phrase ${item}`, () => {
      expect(ReminderUtil.getRemindTimeFromTime(item)).toBe(null);
    });
  }

  test('formats a date string correctly', () => {
    const date = new Date(Date.now());
    date.setHours(6);
    expect(ReminderUtil.formatDate(date)).toBe('06:00 on 18/4/20');
  });
});
