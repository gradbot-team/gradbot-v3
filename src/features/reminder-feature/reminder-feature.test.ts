import 'reflect-metadata';
import { ReminderFeature } from './reminder-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import { areDatesEqual } from './util/are-dates-equal';

const util = FeatureTestUtil.createForFeatureType(ReminderFeature);
let expectedMessageCount = 0;

jest
  .spyOn(global.Date, 'now')
  .mockImplementation(() => new Date(2020, 3, 18, 12, 0, 0).valueOf());

const getScheduledMessageCount = () =>
  Object.entries(util.scheduler.scheduledMessages).length;

const getScheduledItem = (index: number) =>
  Object.values(util.scheduler.scheduledMessages)[index];

const expectDatesToBeEqual = (date1: Date, date2: Date) =>
  expect(areDatesEqual(date1, date2)).toBe(true);

describe('ReminderFeature', () => {
  const invalidMessageTexts = [
    'g test',
    'g remindme 1 hour test',
    'g remind me1 hour test',
    'g remind me test',
  ];

  invalidMessageTexts.forEach((text) => {
    test(`does not respond to invalid message "${text}"`, async () => {
      await util.expectReplyToBeNull(text);
    });
  });

  test('schedules a message and replies to "g remind [time] [body]"', async () => {
    await util.expectReplyToHaveText(
      'g remind 1 hour test',
      "Ok, I'll remind everyone at 13:00 on 18/4/20.",
    );

    expectedMessageCount++;
    expect(getScheduledMessageCount()).toBe(expectedMessageCount);

    const scheduledItem = getScheduledItem(expectedMessageCount - 1);
    const remindMessage = scheduledItem.message;
    util.expectPublicFeatureMessageToHaveText(
      remindMessage,
      'User asked me to remind everyone: test',
    );
    expectDatesToBeEqual(scheduledItem.date, new Date(2020, 3, 18, 13));
  });

  const allTriggers = [
    'g remind all 1 hour test',
    'g remind everyone 1 hour test',
    'g remind everybody 1 hour test',
  ];

  allTriggers.forEach((trigger) => {
    test(`schedules a message and replies to "${trigger}"`, async () => {
      await util.expectReplyToHaveText(
        trigger,
        "Ok, I'll remind everyone at 13:00 on 18/4/20.",
      );

      expectedMessageCount++;
      expect(getScheduledMessageCount()).toBe(expectedMessageCount);

      const scheduledItem = getScheduledItem(expectedMessageCount - 1);
      const remindMessage = scheduledItem.message;
      util.expectPublicFeatureMessageToHaveText(
        remindMessage,
        'User asked me to remind everyone: test',
      );
      expectDatesToBeEqual(scheduledItem.date, new Date(2020, 3, 18, 13));
    });
  });

  test('schedules a private message and replies to "g remind me [time] [body]"', async () => {
    await util.expectReplyToHaveText(
      'g remind me 1 hour test',
      "Ok, I'll remind you at 13:00 on 18/4/20.",
    );

    expectedMessageCount++;
    expect(getScheduledMessageCount()).toBe(expectedMessageCount);

    const scheduledItem = getScheduledItem(expectedMessageCount - 1);
    const remindMessage = scheduledItem.message;
    util.expectFeatureMessageToBeForUser(remindMessage, 0);
    util.expectPrivateFeatureMessageToHaveText(
      remindMessage,
      'Hey User, you asked me to remind you: test',
    );
    expectDatesToBeEqual(scheduledItem.date, new Date(2020, 3, 18, 13));
  });

  test('does not schedule a message and replies with error text, when prompted with an invalid user phrase', async () => {
    const message = await util.getReplyForMessage('g remind User 1 hour test');

    util.expectPublicFeatureMessageToHaveText(
      message,
      'You may only set reminders for yourself or for everyone in the chat.',
    );
    // Expect the same number of scheduled messages that there were before this test.
    expect(getScheduledMessageCount()).toBe(expectedMessageCount);
  });
});
