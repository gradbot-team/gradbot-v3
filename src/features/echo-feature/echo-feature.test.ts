import 'reflect-metadata';
import { EchoFeature } from './echo-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(EchoFeature);

describe('EchoFeature', () => {
  test('echoes messages addressed to Gradbot', async () => {
    await util.expectReplyToHaveText('gradbot, test', 'test, User');
    await util.expectReplyToHaveText('g, test', 'test, User');
  });

  test('does not echo messages not addressed to Gradbot', async () => {
    await util.expectReplyToBeNull('test, j');
    await util.expectReplyToBeNull('gradbot_test');
  });
});
