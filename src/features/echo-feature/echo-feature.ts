import { Feature } from '../../gradbot/feature/feature';
import { EchoMessageHandler } from './echo-message-handler';

export class EchoFeature extends Feature {
  public readonly name = 'Echo';
  public readonly iCanInfo = 'echo messages addressed to me';
  public readonly instructions =
    "If I have no better idea of what to answer to your message, I'll just send it right back.";
  protected readonly messageHandlerType = EchoMessageHandler;
}
