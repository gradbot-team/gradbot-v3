import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class EchoMessageHandler extends FeatureMessageHandler {
  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (!this.isMessageForGradbot()) {
      return Promise.resolve(null);
    }

    return Promise.resolve(
      this.createMessage(
        `${this.getSanitisedText()}, ${
          this.message.user ? this.message.user.displayName : 'User'
        }`,
        `I echoed what ${
          this.message.user ? this.message.user.displayName : 'User'
        } said.`,
      ),
    );
  }
}
