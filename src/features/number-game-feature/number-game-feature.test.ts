import 'reflect-metadata';
import { NumberGameFeature } from './number-game-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(NumberGameFeature);

describe('NumberGameFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  /* test(
    'responds to a number when winning',
    async () => {
      global.Math.random = () => 0.5;
      await util.expectReplyToHaveText('15', '16! I win!');
    }
  );

  test(
    'responds to a number when winning',
    async () => {
      global.Math.random = () => 0.5;
      await util.expectReplyToHaveText('test 15 test', '16! I win!');
    }
  );

  test(
    'responds to multiple numbers when winning',
    async () => {
      global.Math.random = () => 0.5;
      await util.expectReplyToHaveText('test 15 test 28 test', '16, 29! I win!');
    }
  );

  test(
    'responds to negative numbers when winning',
    async () => {
      global.Math.random = () => 0.5;
      await util.expectReplyToHaveText('test -15 test -28 test', '-14, -27! I win!');
    }
  );

  test(
    'responds to a number when losing',
    async () => {
      global.Math.random = () => 0;
      Object.keys(util.scheduler.scheduledMessages).forEach(
        key => delete util.scheduler.scheduledMessages[key]);

      await util.expectReplyToHaveText('test 15 test', '14! I win!');
      const firstMessageKey = Object.keys(util.scheduler.scheduledMessages)[0];
      expect(util.scheduler.scheduledMessages[firstMessageKey].message.text).toBe('Oh, wait... _You_ win!');
    }
  );

  test(
    'responds to multiple numbers when losing',
    async () => {
      global.Math.random = () => 0;
      Object.keys(util.scheduler.scheduledMessages).forEach(
        key => delete util.scheduler.scheduledMessages[key]);

      await util.expectReplyToHaveText('test 15 test 28 test', '14, 27! I win!');
      const firstMessageKey = Object.keys(util.scheduler.scheduledMessages)[0];
      expect(util.scheduler.scheduledMessages[firstMessageKey].message.text).toBe('Oh, wait... _You_ win!');
    }
  );

  test(
    'responds to negative numbers when losing',
    async () => {
      global.Math.random = () => 0;
      Object.keys(util.scheduler.scheduledMessages).forEach(
        key => delete util.scheduler.scheduledMessages[key]);

      await util.expectReplyToHaveText('test -15 test -28 test', '-16, -29! I win!');
      const firstMessageKey = Object.keys(util.scheduler.scheduledMessages)[0];
      expect(util.scheduler.scheduledMessages[firstMessageKey].message.text).toBe('Oh, wait... _You_ win!');
    }
  );

  test(
    'responds to 69',
    async () => {
      global.Math.random = () => 0.5;
      await util.expectReplyToHaveText('test 69 test', '70! I win! Also, nice.');
    }
  );

  test(
    'does not respond to float',
    async () => await util.expectReplyToBeNull('test 15.8 test')
  );

  test(
    'does not respond if word-adjescent',
    async () => await util.expectReplyToBeNull('test15 test')
  );

  test(
    'does not respond if word-adjescent',
    async () => await util.expectReplyToBeNull(`${Number.MAX_VALUE}0`)
  ); */
});
