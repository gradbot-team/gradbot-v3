import { Feature } from '../../gradbot/feature/feature';
import { NumberGameMessageHandler } from './number-game-message-handler';

export class NumberGameFeature extends Feature {
  public readonly name = 'Number game';
  public readonly iCanInfo = 'play the number game';
  public readonly instructions =
    'The one who says the higher number wins. And I feel lucky!';
  protected readonly messageHandlerType = NumberGameMessageHandler;
}
