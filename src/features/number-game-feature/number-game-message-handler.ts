import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class NumberGameMessageHandler extends FeatureMessageHandler {
  private readonly NUMBER_GAME_REGEX = /(?:^|\s)(-?\d{1,15})(?:$|\s)/g;
  private readonly CORRECTION_TIMEOUT = 3;

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    /* const match = this.message.text.match(this.NUMBER_GAME_REGEX);

    if (match) {
      const numbers: number[] = [];
      let has69 = false;

      match.forEach(numberString => {
        const num = parseInt(numberString, 10);
        numbers.push(num);
        has69 = has69 || Math.abs(num) === 69;
      });

      const losing = Math.random() < 0.5;

      for (let i = 0; i < numbers.length; i++) {
        numbers[i] = numbers[i] + (losing ? -1 : 1);
      }

      const numbersString = numbers.map(num => num.toString()).join(', ');

      if (losing) {
        this.scheduleMessageWithTimeout(
          uuid.v4(),
          this.CORRECTION_TIMEOUT,
          this.createMessage(
            'Oh, wait... _You_ win!',
            `I played the number game with ${this.message.user.displayName}`
          ));
      }

      return this.createMessage(`${numbersString}! I win!` + (has69 ? ' Also, nice.' : ''),
        `I played the number game with ${this.message.user.displayName}`);
    }*/

    return Promise.resolve(null);
  }
}
