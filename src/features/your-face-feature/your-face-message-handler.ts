import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class YourFaceMessageHandler extends FeatureMessageHandler {
  // This will never actually matter, because all triggers include a direct
  // address to Gradbot. The property is left here for interface consitency,
  // and because it's most likely to be the correct behaviour if the trigger
  // condition is ever changed in a future version.
  protected readonly triggerProbability = 1;

  public isMessageTrigger(): boolean {
    // Triggers for a single sentence of less than 25 characters, beginning
    // "Gradbot is" and containing at least one more word.
    const text = this.message.text;
    return (
      this.getSanitisedText().length < 25 &&
      text.match(/^g(radbot)?\s+is\s+\w+[^.?!]*[.?!]*$/i) !== null &&
      text.match(/[?][.!]*$/) === null
    );
  }

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const text = this.message.text
      .replace(/^g(radbot)?\s+/i, '')
      .replace(/[.?!]*$/, '');
    return Promise.resolve(
      this.createMessage(
        `*Your face* ${text}.`,
        `Just stating a fact about ${this.message.user.displayName}'s face.`,
      ),
    );
  }
}
