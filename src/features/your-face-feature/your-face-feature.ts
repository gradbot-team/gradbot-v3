import { Feature } from '../../gradbot/feature/feature';
import { YourFaceMessageHandler } from './your-face-message-handler';

export class YourFaceFeature extends Feature {
  public readonly name = 'your face';
  public readonly iCanInfo = 'give as good as I get.';
  public readonly instructions =
    'A short message that begins "G(radbot) is" will receive a mildly insulting reply.';
  protected readonly messageHandlerType = YourFaceMessageHandler;
}
