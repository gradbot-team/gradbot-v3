import 'reflect-metadata';
import { YourFaceFeature } from './your-face-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(YourFaceFeature);

describe('YourFaceFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  test("does not respond to a mesage that's too long", async () =>
    await util.expectReplyToBeNull(
      'Gradbot is a potato, yes he is indeed a potato; a potato is Gradbot, this is known.',
    ));

  test('does not respond to messages that contain more than one sentence', async () => {
    await util.expectReplyToBeNull('Gradbot is a potato. Gradbot is a potato.');
    await util.expectReplyToBeNull('Gradbot is a potato? Gradbot is a potato.');
    await util.expectReplyToBeNull('Gradbot is a potato! Gradbot is a potato!');
  });

  test("does not respond to a message that's too long", async () => {
    await util.expectReplyToBeNull(
      'Gradbot is the very model of a modern Major-General',
    );
    await util.expectReplyToBeNull('Gradbot is several sacks of potatoes.');
  });

  test('does not respond to questions', async () => {
    await util.expectReplyToBeNull('Gradbot is a potato?');
    await util.expectReplyToBeNull('Gradbot is a potato?!');
  });

  test('responds correctly to valid trigger', async () => {
    await util.expectReplyToHaveText(
      'G is a potato!!',
      '*Your face* is a potato.',
    );
    await util.expectReplyToHaveText(
      'g is a sack of potatoes',
      '*Your face* is a sack of potatoes.',
    );
  });
});
