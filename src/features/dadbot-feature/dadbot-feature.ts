import { Feature } from '../../gradbot/feature/feature';
import { DadbotMessageHandler } from './dadbot-message-handler';

export class DadbotFeature extends Feature {
  public readonly name = 'Dadbot';
  public readonly iCanInfo = 'irritate you, my children, with dreadful jokes';
  public readonly instructions =
    'responds to short sentences beginning "I\'m/Im/I am..."';
  protected readonly messageHandlerType = DadbotMessageHandler;
}
