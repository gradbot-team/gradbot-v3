import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class DadbotMessageHandler extends FeatureMessageHandler {
  // Triggers 20% of the time for trigger messages not addressed to Gradbot
  protected readonly triggerProbability = 0.2;

  private readonly triggerPattern = "^I('?m|\\s+am)\\s+";
  private readonly endSentencePattern = '[.?!]*$';

  public isMessageTrigger(): boolean {
    const text = this.getSanitisedText();
    // Message should be a single sentence less than 100 characters long,
    // beginning with "I'm"/"Im"/"I am", and containing at least one more word.
    return (
      text.length <= 50 &&
      text.match(
        new RegExp(
          `${this.triggerPattern}\\w+[^.?!]*${this.endSentencePattern}`,
          'i',
        ),
      ) !== null
    );
  }

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const text = this.getSanitisedText()
      .replace(new RegExp(this.triggerPattern, 'i'), '')
      .replace(new RegExp(this.endSentencePattern), '')
      .trim();
    return Promise.resolve(
      this.createMessage(
        `Hi ${text}, I'm Dad!`,
        `${this.message.user.displayName} set me up for a dad joke and I couldn't resist.`,
      ),
    );
  }
}
