import 'reflect-metadata';
import { DadbotFeature } from './dadbot-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(DadbotFeature);

describe('DadbotFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  test("does not respond to a message that's too long", async () =>
    await util.expectReplyToBeNull(
      "g I'm a message that has over 100 characters, that is to say I'm too" +
        'long to generate a response from this feature.',
    ));

  test('does not respond to messages that contain more than one sentence', async () => {
    await util.expectReplyToBeNull(
      'g I contain 2 sentences. The feature will not respond.',
    );
    await util.expectReplyToBeNull(
      'g I contain 2 sentences? The feature will not respond?',
    );
    await util.expectReplyToBeNull(
      'g I contain 2 sentences! The feature will not respond!',
    );
  });

  test('responds to valid triggers', async () => {
    await util.expectReplyToHaveText('g I am tired.', "Hi tired, I'm Dad!");
    await util.expectReplyToHaveText("g I'm tired.", "Hi tired, I'm Dad!");
    await util.expectReplyToHaveText('g Im tired.', "Hi tired, I'm Dad!");
  });
});
