import { SwearJarMessageHandler } from './swear-jar-message-handler';
import { Feature } from '../../gradbot/feature/feature';
import { noIndent } from '../../util/general/string-extensions';

export class SwearJarFeature extends Feature {
  public readonly name = 'Swear jar';
  public readonly iCanInfo = 'count swear jar money';
  public readonly instructions = noIndent(
    `I'll take your money if you're a potty mouth. You can ask me about your naughty credit like this:
     • *g how much money in my swear jar?*
     • *g swear jar*`,
  );

  protected readonly messageHandlerType = SwearJarMessageHandler;
}
