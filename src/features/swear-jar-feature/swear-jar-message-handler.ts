import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { SwearJarData } from './swear-jar-data';
import { swearList } from './swear-jar-swear-list';

export class SwearJarMessageHandler extends FeatureMessageHandler {
  private static readonly SWEAR_JAR_REGEX = new RegExp(
    '(^|\\W)(' + swearList.join('|') + ')($|\\W)',
    'i',
  );
  private static readonly SWEAR_JAR_QUERY_REGEX =
    /^(how\W+much\W+)?((money|is\W+there)\W+)?(in\W+)?(my\W+)?swear\W+jar\W*$/;

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (
      !this.message.metadata.channel.isPrivate &&
      this.message.text
        .toLowerCase()
        .match(SwearJarMessageHandler.SWEAR_JAR_REGEX)
    ) {
      let swearJarData = await this.getSwearJarDataForUser();
      let swearCount = 1;

      if (swearJarData) {
        swearCount = swearJarData.swearCount.get() + 1;
      } else {
        swearJarData = new SwearJarData(this.message);
        swearJarData.userId.set(this.message.user.id);
      }

      swearJarData.swearCount.set(swearCount);
      await swearJarData.write();

      return this.createMessage(
        `I'm taking £1 from ${this.message.user.displayName} and putting it in their swear jar. They now have £${swearCount} in it.`,
        `${this.message.user.displayName} has potty mouth`,
      );
    } else if (
      this.matchSanitisedLowercaseTextIfForGradbot(
        SwearJarMessageHandler.SWEAR_JAR_QUERY_REGEX,
      )
    ) {
      const swearJarData = await this.getSwearJarDataForUser();

      let swearCount = 0;

      if (swearJarData) {
        swearCount = swearJarData.swearCount.get();
      }

      return this.createMessage(
        `You have £${swearCount} in your swear jar, ${this.message.user.displayName}`,
        `${this.message.user.displayName} asked me how much they have in their swear jar`,
      );
    }

    return null;
  }

  private async getSwearJarDataForUser() {
    const dataRetriever = new SwearJarData(this.message);
    dataRetriever.userId.set(this.message.user.id);
    const existingData = await dataRetriever.retrieveAlike();

    if (existingData.length > 0) {
      return existingData[0];
    }

    return;
  }
}
