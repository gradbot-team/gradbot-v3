export const swearList = [
  'bollock',
  'bollocking',
  'bollocks',
  'cunt',
  'cunted',
  'cunting',
  'cunts',
  'fuck',
  'fucks',
  'fucked',
  'fucker',
  'fuckers',
  'fuckhead',
  'fuckheads',
  'fuckhole',
  'fuckholes',
  'fucking',
  'motherfucker',
  'motherfuckers',
  'motherfucking',
  'scunthorpe',
  'shat',
  'shit',
  'shits',
  'shite',
  'shites',
  'shitey',
  'shithead',
  'shitheads',
  'shithole',
  'shitholes',
  'shitted',
  'shitter',
  'shitters',
  'shittier',
  'shittiest',
  'shitting',
  'shitty',
];
