import { FeatureDbObject } from '../../gradbot/database/db-objects/feature-db-object';
import { DbText } from '../../gradbot/database/types/db-text';
import { DbInteger } from '../../gradbot/database/types/db-integer';
import { DBO } from '../../gradbot/database/db-objects/registry/dbo-decorator';
import { DbIsolationLevel } from '../../gradbot/database/db-objects/db-isolation-level';

@DBO
export class SwearJarData extends FeatureDbObject<SwearJarData> {
  isolationLevel = DbIsolationLevel.SERVER;

  userId = new DbText();
  swearCount = new DbInteger();
}
