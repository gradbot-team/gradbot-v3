import 'reflect-metadata';
import { SwearJarFeature } from './swear-jar-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(SwearJarFeature);
const secondUser = util.addUser('User 2');
const thirdUser = util.addUser('User 3');

describe('SwearJarFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds to swear word in front', async () =>
    await util.expectReplyToBeTruthy('fuck abc'));

  test('responds to swear word in the middle', async () =>
    await util.expectReplyToBeTruthy('abc fuck abc'));

  test('responds to swear word at the end', async () =>
    await util.expectReplyToBeTruthy('abc fuck'));

  test('responds to swear word with no spaces', async () =>
    await util.expectReplyToBeTruthy('abc-fuck-abc'));

  test('responds to "f**k"', async () =>
    await util.expectReplyToBeTruthy('fuck'));

  test('responds to "f**king"', async () =>
    await util.expectReplyToBeTruthy('fucking'));

  test('responds to "f**ked"', async () =>
    await util.expectReplyToBeTruthy('fucked'));

  test('responds to "motherf**ker"', async () =>
    await util.expectReplyToBeTruthy('motherfucker'));

  test('responds to "c**t"', async () =>
    await util.expectReplyToBeTruthy('cunt'));

  test('responds to "s**t"', async () =>
    await util.expectReplyToBeTruthy('shit'));

  test('keeps correct count', async () => {
    let counter = 1;
    const expectedReply = () =>
      `I'm taking £1 from User 2 and putting it in their swear jar. They now have £${counter++} in it.`;

    await util.expectReplyToHaveText('shit', expectedReply(), secondUser);
    await util.expectReplyToHaveText('shit', expectedReply(), secondUser);
  });

  test('allows querying', async () => {
    let counter = 0;
    const expectedReply = () =>
      `You have £${counter++} in your swear jar, User 3`;

    await util.expectReplyToHaveText('g swear jar', expectedReply(), thirdUser);
    await util.getReplyForMessage('shit', thirdUser);
    await util.expectReplyToHaveText(
      'g HOW MUCH swear jar',
      expectedReply(),
      thirdUser,
    );
    await util.getReplyForMessage('shit', thirdUser);
    await util.expectReplyToHaveText(
      'g how much money in my swear jar',
      expectedReply(),
      thirdUser,
    );
  });

  test('does not respond to swear word with suffix', async () =>
    await util.expectReplyToBeNull('fuckxxx'));

  test('does not respond to swear word with prefix', async () =>
    await util.expectReplyToBeNull('xxxfuck'));

  test('does not respond to swear word at the end with suffix', async () =>
    await util.expectReplyToBeNull('xxx fuckxxx'));

  test('does not respond to swear word in front with prefix', async () =>
    await util.expectReplyToBeNull('xxxfuck xxx'));

  test('does not respond to swear word in the middle with prefix', async () =>
    await util.expectReplyToBeNull('xxx xxxfuck xxx'));
});
