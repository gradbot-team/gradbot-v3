import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class TrumpMessageHandler extends FeatureMessageHandler {
  public readonly featureName = 'Trump';
  public readonly iCanInfo = 'quote Trump';
  public readonly instructions = '• *g Donald Trump*\n• *g quote Trump*';

  private readonly TRUMP_REGEX = /^(quote\W+)?(trump|donald|donald trump)\W*$/;
  private readonly TRUMP_API_URL = 'https://tronalddump.io/random/quote';

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.matchSanitisedLowercaseTextIfForGradbot(this.TRUMP_REGEX)) {
      const data = await this.fetchAPI<TrumpAPIData>(this.TRUMP_API_URL);

      if (data && data.appeared_at && data.value) {
        const date = new Date(data.appeared_at);

        return this.createMessage(
          `_${data.value}_\nDonald Trump, ${date.getFullYear()}`,
          `${this.message.user.displayName} loves Donald Trump`,
        );
      }

      return this.createMessage(
        "I happily forgot every single thing Trump's ever said",
        `${this.message.user.displayName} loves Donald Trump, but API is down`,
      );
    }
    return null;
  }
}

interface TrumpAPIData {
  value?: string;
  appeared_at?: string;
}
