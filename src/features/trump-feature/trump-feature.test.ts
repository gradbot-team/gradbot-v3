import 'reflect-metadata';
import { TrumpFeature } from './trump-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(TrumpFeature);

const setValidAPIResponse = () =>
  (util.api.urlReturnValues['https://tronalddump.io/random/quote'] = {
    appeared_at: '2016-05-09T16:56:36.000Z',
    value: 'test',
  });

describe('TrumpFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds to "Trump"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText(
      'Gradbot, Trump',
      '_test_\nDonald Trump, 2016',
    );
  });

  test('responds to "donald"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText('g donald', '_test_\nDonald Trump, 2016');
  });

  test('responds to "donald trump"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText(
      'donald trump, gradbot',
      '_test_\nDonald Trump, 2016',
    );
  });

  test('deals with invalid API data', async () => {
    util.api.urlReturnValues['https://tronalddump.io/random/quote'] = {
      value: 'test',
      wrong_property: 'test',
    };

    await util.expectReplyToHaveText(
      'Gradbot, Trump',
      "I happily forgot every single thing Trump's ever said",
    );
  });

  test('deals with missing API data', async () => {
    util.api.urlReturnValues['https://tronalddump.io/random/quote'] = null;
    await util.expectReplyToHaveText(
      'Gradbot, Trump',
      "I happily forgot every single thing Trump's ever said",
    );
  });
});
