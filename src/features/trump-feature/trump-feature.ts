import { Feature } from '../../gradbot/feature/feature';
import { TrumpMessageHandler } from './trump-message-handler';

export class TrumpFeature extends Feature {
  public readonly name = 'Trump';
  public readonly iCanInfo = 'quote Trump';
  public readonly instructions = '• *g Donald Trump*\n• *g quote Trump*';
  protected readonly messageHandlerType = TrumpMessageHandler;
}
