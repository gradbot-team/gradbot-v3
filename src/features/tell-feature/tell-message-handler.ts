import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { User } from '../../gradbot/chat/user/user';
import { TELL_REGEX } from './tell-constants';
import { TellData } from './tell-data';

export class TellMessageHandler extends FeatureMessageHandler {
  protected readonly triggerProbability = 1;

  public isMessageTrigger(): boolean {
    return true;
  }

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    // First deal with pending messages
    const retMessages: FeatureMessage | null =
      await this.retrievePendingMessages_();

    if (retMessages) {
      return retMessages;
    }

    // and then deal with adding new pending messages

    if (this.isMessageForGradbot()) {
      const messageText: string = this.getSanitisedText().trim();

      const matches = messageText.match(TELL_REGEX);

      if (matches && matches.length === 3) {
        let response = '';
        let reason = '';

        const displayName: string = matches[1].trim();
        const message: string = matches[2].trim();

        if (displayName.length > 0 && message.length > 0) {
          const targetUser: User | undefined =
            await this.getUserWithDisplayName(displayName);

          if (targetUser !== undefined) {
            // we have a message and a target user, store the message
            // if there isn't one already

            if (targetUser.displayName !== this.message.user.displayName) {
              const data: TellData = new TellData(this.message);
              data.senderId.set(this.message.user.id);
              data.targetId.set(targetUser.id);
              const records: TellData[] = await data.retrieveAlike();

              if (records.length === 0) {
                // No records, can add one
                data.messageText.set(message);
                await data.write();

                response =
                  `Thanks, I will tell ${targetUser.displayName} ` +
                  'when I see them next.';
                reason =
                  'A user asked me to store a message and send it ' +
                  'it to another user when I next see them.';
              } else {
                // previous message
                response =
                  'You already have a waiting record for ' +
                  `'${targetUser.displayName}'`;
                reason =
                  'A user tried to tell a user but they already ' +
                  'had a pending message so I rejected it.';
              }
            } else {
              response = 'You cannot send a message to yourself.';
              reason = 'User tried to send a message to themselves.';
            }
          } else {
            // Unknown user
            response = `I do not recognise user '${displayName}'`;
            reason =
              'A user tried to tell a user but I was unable to ' +
              'identify the user.';
          }
        } else {
          // Missing username or message
          response = 'Either the name or the message is missing.';
          reason =
            'A user tried to tell a user but I was unable to ' +
            'get a user or a message so I rejected it.';
        }

        return this.createMessage(response, reason);
      }
      // If the tell regex is not matched, it is treated as not
      // intended for tell, thus ignore and reply null
    }

    return null;
  }

  private async retrievePendingMessages_(): Promise<FeatureMessage | null> {
    const data: TellData = new TellData(this.message);
    data.targetId.set(this.message.user.id);
    const records: TellData[] = await data.retrieveAlike();

    if (records.length === 0) {
      return null;
    } else {
      let response = "You've got mail!";
      const nameCache = new Map();

      for (const record of records) {
        let name = '';
        const senderId: string = record.senderId.get();

        if (nameCache.has(senderId)) {
          name = nameCache.get(senderId) as string;
        } else {
          const senderUser: User | undefined = await this.getUserWithId(
            record.senderId.get(),
          );

          if (senderUser === undefined) {
            name = 'undefined';
          } else {
            name = senderUser.displayName;
            nameCache.set(senderId, name);
          }
        }

        response += `\n${name}: "${record.messageText.get()}"`;

        // record has been processed so delete it
        await record.delete();
      }

      return this.createMessage(
        response,
        'I returned the pending messages for ' +
          `${this.message.user.displayName}.`,
      );
    }

    return null;
  }
}
