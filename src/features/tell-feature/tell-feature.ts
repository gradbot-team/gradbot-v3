import { Feature } from '../../gradbot/feature/feature';
import { TellMessageHandler } from './tell-message-handler';
import { TELL_HELP_TEXT } from './tell-constants';

export class TellFeature extends Feature {
  public readonly name = 'Tell';
  public readonly iCanInfo = 'help users to send messages to each other.';
  public readonly instructions = TELL_HELP_TEXT;
  protected readonly messageHandlerType = TellMessageHandler;
}
