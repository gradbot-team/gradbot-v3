import { noIndent } from '../../util/general/string-extensions';

// tell regex
export const TELL_REGEX = /^tell\s+(.*)<m(?:essage)?>(.*)$/i;

// help/instructions text
export const TELL_HELP_TEXT = noIndent(
  'The Tell feature will store a message for a user and send it when that ' +
    `user next sends a message.

  Users are only permitted to send 1 message to 1 user at a time.

  To store a message please use g tell [name] <m(essage)?> [message].`,
);
