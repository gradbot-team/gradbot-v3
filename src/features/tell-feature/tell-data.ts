import { DBO } from '../../gradbot/database/db-objects/registry/dbo-decorator';
import { FeatureDbObject } from '../../gradbot/database/db-objects/feature-db-object';
import { DbText } from '../../gradbot/database/types/db-text';
import { DbIsolationLevel } from '../../gradbot/database/db-objects/db-isolation-level';

@DBO
export class TellData extends FeatureDbObject<TellData> {
  isolationLevel = DbIsolationLevel.SERVER;

  senderId = new DbText();
  targetId = new DbText();
  messageText = new DbText();
}
