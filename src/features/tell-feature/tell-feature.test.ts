import 'reflect-metadata';
import { TellFeature } from './tell-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(TellFeature);

// Get 1 user for free (user id is test_user_1)
// add additional users -> UserId will be test_user_2/3/.../10
util.addUsers([
  'Zeus',
  'Hera',
  'Poseidon',
  'Hades',
  'Athena',
  'Ares',
  'Apollo',
  'Artemis',
  'Demeter',
  'Hermes',
]);

describe('TellFeature', () => {
  test('does not respond to test message', async () => {
    await util.expectReplyToBeNull('g test');
  });

  test('does respond with the pending message', async () => {
    // add a message from User
    await util.expectReplyToHaveText(
      'g tell Ares <m> what is up',
      'Thanks, I will tell Ares when I see them next.',
    );

    // add a message from Zeus (use the other supported message handle)
    await util.expectReplyFromUserNameToHaveText(
      "g tell Ares <message> I'm asking the questions",
      'Thanks, I will tell Ares when I see them next.',
      'Zeus',
    );

    // add a message from Poseidon to another user
    await util.expectReplyFromUserNameToHaveText(
      'g tell Hermes <m> Nothing to see here',
      'Thanks, I will tell Hermes when I see them next.',
      'Poseidon',
    );

    // expect the default user to not get a response
    await util.expectReplyToBeNull('g test');

    // expect another user to not get a response
    await util.expectReplyFromUserNameToBeNull('g test', 'Hera');

    // expect another user that hasn't sent a message to not get a response
    await util.expectReplyFromUserNameToBeNull('g test', 'Zeus');

    // expect the correct user to get a response
    await util.expectReplyFromUserNameToHaveText(
      'g test',
      "You've got mail!\n" +
        'User: "what is up"\n' +
        'Zeus: "I\'m asking the questions"',
      'Ares',
    );

    // expect the no reply after it has been processed
    await util.expectReplyFromUserNameToBeNull('g test', 'Ares');
  });

  test('Rejects messages to yourself', async () => {
    // add a message from User
    await util.expectReplyFromUserNameToHaveText(
      'g tell Ares <m> what is up',
      'You cannot send a message to yourself.',
      'Ares',
    );
  });

  test('Rejects messages for an unknown user', async () => {
    // add a message from User
    await util.expectReplyFromUserNameToHaveText(
      'g tell Gandalf <m> what is up',
      "I do not recognise user 'Gandalf'",
      'Ares',
    );
  });

  test('Rejects second pending message - there can only be one!', async () => {
    // add a message from Ares
    await util.expectReplyFromUserNameToHaveText(
      'g tell Hades <m> I like your game',
      'Thanks, I will tell Hades when I see them next.',
      'Ares',
    );

    // Reject second message
    await util.expectReplyFromUserNameToHaveText(
      'g tell Hades <m> There can only be one',
      "You already have a waiting record for 'Hades'",
      'Ares',
    );
  });

  test('Rejects missing name or messages', async () => {
    // add a message from User
    await util.expectReplyFromUserNameToHaveText(
      'g tell <m> what is up',
      'Either the name or the message is missing.',
      'Ares',
    );

    // add a message from User
    await util.expectReplyFromUserNameToHaveText(
      'g tell Ares <message> ',
      'Either the name or the message is missing.',
      'Poseidon',
    );
  });
});
