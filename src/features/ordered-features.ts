import { Feature } from '../gradbot/feature/feature';
import { ChessFeature } from './chess-feature/chess-feature';
import { ComplimentorFeature } from './complimentor-feature/complimentor-feature';
import { DiceFeature } from './dice-feature/dice-feature';
import { EchoFeature } from './echo-feature/echo-feature';
import { BoredFeature } from './bored-feature/bored-feature';
import { TrumpFeature } from './trump-feature/trump-feature';
import { MemeFeature } from './meme-feature/meme-feature';
import { PetsFeature } from './pets-feature/pets-feature';
import { ChuckNorrisFeature } from './chuck-norris-feature/chuck-norris-feature';
import { JokesFeature } from './jokes-feature/jokes-feature';
import { BankHolidaysFeature } from './bank-holidays-feature/bank-holidays-feature';
import { SwearJarFeature } from './swear-jar-feature/swear-jar-feature';
import { WeatherFeature } from './weather-feature/weather-feature';
import { NumberGameFeature } from './number-game-feature/number-game-feature';
import { ReminderFeature } from './reminder-feature/reminder-feature';
import { LiarsDiceFeature } from './liars-dice-feature/liars-dice-feature';
import { OneUpFeature } from './one-up-feature/one-up-feature';
import { YourFaceFeature } from './your-face-feature/your-face-feature';
import { DadbotFeature } from './dadbot-feature/dadbot-feature';
import { ClubFeature } from './club-feature/club-feature';
import { FactoidsFeature } from './factoids-feature/factoids-feature';
import { TeamFeature } from './team-feature/team-feature';
import { TellFeature } from './tell-feature/tell-feature';
import { DictionaryFeature } from './dictionary-feature/dictionary-feature';
import { DeciderFeature } from './decider-feature/decider-feature';
import { WolframAlphaClientFeature } from './wolfram-alpha-client-feature/wolfram-alpha-client-feature';

export const OrderedFeatureTypes: (new () => Feature)[] = [
  DiceFeature,
  ChessFeature,
  ComplimentorFeature,
  BoredFeature,
  TrumpFeature,
  MemeFeature,
  PetsFeature,
  ChuckNorrisFeature,
  JokesFeature,
  BankHolidaysFeature,
  WeatherFeature,
  ReminderFeature,
  LiarsDiceFeature,
  ClubFeature,
  FactoidsFeature,
  DeciderFeature,
  OneUpFeature,
  YourFaceFeature,
  DadbotFeature,
  TeamFeature,
  WolframAlphaClientFeature, // Near the bottom so as not to block set responses for any other feature
  DictionaryFeature, // WolframAlphaClient needs to have priority for "what is...?" questions, but Dictionary can be a fallback
  NumberGameFeature,
  TellFeature, // Keep this low priority as it can reply to random messages
  SwearJarFeature,
  EchoFeature,
];
