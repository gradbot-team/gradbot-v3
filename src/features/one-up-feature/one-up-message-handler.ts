import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import * as Ordinals from '../../util/general/ordinals';
import * as RandomExtensions from '../../util/general/random-extensions';

export class OneUpMessageHandler extends FeatureMessageHandler {
  /**
   * Returns a one-upped version of the input text if it contains any numbers,
   * or undefined if it doesn't.
   */
  public static getOneUp(text: string): string | undefined {
    let outputText;
    let index = 0;
    while (index < text.length) {
      if (
        text[index].match(/\d/) &&
        (index === 0 || text[index - 1].match(/[^.!?\w]/))
      ) {
        // Matches:
        //  * ordinal number 0th when followed by a non-word character;
        //  * ordinal numbers, omitting leading 0s, when followed by a non-word
        //  * character: suffix is 'th' except where penultimate digit is not 1
        //  * and final digit is 1 (st), 2(nd), or 3(rd);
        //  * Non-negative integers, including leading zeroes;
        //  * Non-negative decimals, including leading zeroes
        // Omitting leading zeroes from the match pattern means they'll be
        // preserved in the reply message text. Matching only positive integers
        // and fractions means that the one-up always increments away from 0, to
        // give more extreme positive and negative values.
        const matches = text
          .substring(index)
          .match(
            /^\d*((1[1-3]|[04-9])th|(?<!1)(1st|2nd|3rd))(?=\W|$)|^\d*(\.\d+)?/i,
          );
        let numString;
        if (matches) {
          numString = matches[0];
        }
        if (numString) {
          let oneUpString = Ordinals.decrementOrdinal(numString);
          if (!oneUpString) {
            // We're working with an integer or decimal fraction.
            const splits = numString.split('.');
            let integer = splits[0];
            const integerLength = integer.length;
            if (splits.length > 1) {
              // If there are n digits after the decimal point (n > 0), increment
              // the fractional part by 1*10^-n.
              let fraction = splits[1];
              const fractionLength = fraction.length;
              fraction = (parseInt(fraction, 10) + 1).toString();
              fraction = OneUpMessageHandler.padNumberWithZeroes(
                fraction,
                fractionLength,
              );
              if (
                fraction.length === fractionLength + 1 &&
                fraction.match(/^10+$/)
              ) {
                // Special case where original was n.9...
                fraction = fraction.substring(1);
                integer = OneUpMessageHandler.padNumberWithZeroes(
                  (parseInt(numString, 10) + 1).toString(),
                  integerLength,
                );
              }
              oneUpString = integer + '.' + fraction;
            } else {
              // For integers, increment by 1.
              oneUpString = OneUpMessageHandler.padNumberWithZeroes(
                (parseInt(numString, 10) + 1).toString(),
                integerLength,
              );
            }
          }
          if (oneUpString) {
            // Replace the original number with the one-upped one.
            text =
              text.substring(0, index) +
              oneUpString +
              text.substring(index + numString.length);
            // We've one-upped the user, so set the output.
            outputText = text;
            // Skip past the incremented number text and continue.
            index += oneUpString.length;
          } else {
            // Something has gone wrong; move on and try again.
            index++;
          }
        } else {
          // Something has gone wrong; move on and try again.
          index++;
        }
      } else {
        // Char at the index is not a number, so move on to the next char.
        index++;
      }
    }
    return outputText;
  }

  private static padNumberWithZeroes(
    num: string,
    maxPaddedLength: number,
  ): string {
    if (maxPaddedLength > num.length) {
      const pad = maxPaddedLength - num.length;
      num = '0'.repeat(pad) + num;
    }
    return num;
  }

  private static readonly replyPrefixes = [
    'Oh yeah? Well,',
    'Well,',
    'Not bad, but',
    'Not bad.',
    "That's nice, but",
    null,
    null,
  ];

  private static buildReply(body: string): string {
    const prefix =
      OneUpMessageHandler.replyPrefixes[
        RandomExtensions.getRandomInt(OneUpMessageHandler.replyPrefixes.length)
      ];
    const suffixConjunction = prefix ? 'and' : 'but';
    const suffix =
      RandomExtensions.getRandomInt(3) === 0
        ? suffixConjunction + " you don't see me bragging about it"
        : null;
    return (
      (prefix ? prefix + ' ' : '') +
      (suffix ? body.replace(/[.?!]*$/, ', ') + suffix + '.' : body)
    );
  }

  // Trigger 10% of the time when not directly addressed to Gradbot
  protected readonly triggerProbability = 0.1;

  public isMessageTrigger(): boolean {
    // Match criteria:
    //  * single sentence - contains no full stops, question marks, or
    //  * exclamation marks, except optionally at the end
    //  * contains one (or more) number separated from the preceding word by non-
    //    word characters, not including [.!?]
    //  * total length less than 100 characters
    const text = this.getSanitisedText();
    if (
      text.length < 100 &&
      text
        .trim()
        .match(
          /^(I|(I(m|d|ll|ve))|My)(?=[^.?!\w])[^.?!]*[^.?!\w]\d+(\.\d+)?[^.?!]*[.?!]*$/i,
        )
    ) {
      return true;
    } else {
      return false;
    }
  }

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const oneUpText = OneUpMessageHandler.getOneUp(this.getSanitisedText());
    if (oneUpText) {
      return Promise.resolve(
        this.createMessage(
          OneUpMessageHandler.buildReply(oneUpText),
          `I'm incrementally better than ${this.message.user.displayName}`,
        ),
      );
    } else {
      return Promise.resolve(null);
    }
  }
}
