import 'reflect-metadata';
import { OneUpFeature } from './one-up-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import { OneUpMessageHandler } from './one-up-message-handler';

const util = FeatureTestUtil.createForFeatureType(OneUpFeature);

describe('OneUpFeature', () => {
  const nonTriggerMessageTexts = [
    'g test',
    'g You are 6 months old',
    "g I'm 27 years old. My friend is 28 years old",
    'g hi every1 im new *holds up spork*',
    'g I am counting to 1 ha ha ha! 2 ha ha ha!',
    'g My friend wants to know, are you 12? Or 13?',
    'g I every1',
    "g it's -3°C outside",
  ];

  nonTriggerMessageTexts.forEach((item) => {
    test(`does not respond to non-trigger message "${item}"`, async () =>
      await util.expectReplyToBeNull(item));
  });

  test("does not respond to a message that's too long", async () =>
    await util.expectReplyToBeNull(
      'G my hat it has 3 corners, 3 corners has my hat, and had it not 3 corners it would not be my hat my hat',
    ));

  const triggerMessageTexts = [
    "g I'm 27 years old",
    "g I'm 27 years old.",
    "g I'm 27 years old...",
    "g I'm 27 years old!?!?!?!",
    'g Im 27 years old',
    'g Ill have 1 of those',
    'g Ive 3 apples',
    'g Id like 1 art please',
    "g i don't know how to capitalise the 26 letters",
    'g My friend is 28 years old.',
    'g I weigh 80kg',
    'g I am 1.85m tall',
    "g I'm 27 years old, I weigh 80kg, and I'm 1.85m tall.",
    'g I gave every1 10 sweets',
    'G my hat it has 3 corners, 3 corners has my hat, and had it not 3 corners it would not be my hat.', // just under 100 chars
  ];

  triggerMessageTexts.forEach((item) => {
    test(`responds to trigger message "${item}"`, async () =>
      await util.expectReplyToBeTruthy(item));
  });

  const testData: { input: string; output: string | undefined }[] = [
    { input: "I'm 27 years old.'", output: "I'm 28 years old.'" },
    {
      input: 'I have the 3rd highest score',
      output: 'I have the 2nd highest score',
    },
    { input: 'me 2nd', output: 'me 1st' },
    { input: 'me 2nd!', output: 'me 1st!' },
    { input: "It's -3°C outside", output: "It's -4°C outside" },
    { input: 'I am 1.85m tall', output: 'I am 1.86m tall' },
    {
      input: 'My friend has a big 4head.',
      output: 'My friend has a big 5head.',
    },
    { input: '1, ha ha ha!', output: '2, ha ha ha!' },
    { input: 'My bank balance is -£1', output: 'My bank balance is -£2' },
    { input: 'I have £1.99', output: 'I have £2.00' },
    { input: 'I have £1.09', output: 'I have £1.10' },
    {
      input: '-1 is the -1st number in my sequence',
      output: '-2 is the -0th number in my sequence',
    },
    { input: 'hi every1 im new!!! *holds up spork*', output: undefined },
    { input: 'hi every1 123', output: 'hi every1 124' },
    { input: '123456789.987654321', output: '123456789.987654322' },
    { input: '987654321.123456789', output: '987654321.123456790' },
    { input: '0', output: '1' },
    { input: '000000', output: '000001' },
    { input: '000001', output: '000002' },
    { input: '000.100', output: '000.101' },
    { input: '000.000', output: '000.001' },
    { input: '000.001', output: '000.002' },
    { input: '0', output: '1' },
  ];

  testData.forEach((item) => {
    test(`outputs one-up text for input "${item.input}"`, () => {
      expect(OneUpMessageHandler.getOneUp(item.input)).toBe(item.output);
    });
  });
});
