import { Feature } from '../../gradbot/feature/feature';
import { OneUpMessageHandler } from './one-up-message-handler';

export class OneUpFeature extends Feature {
  public readonly name = 'OneUp';
  public readonly iCanInfo = '';
  public readonly instructions = '';
  protected readonly messageHandlerType = OneUpMessageHandler;
}
