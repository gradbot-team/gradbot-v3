import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import * as uuid from 'uuid';

export class JokesMessageHandler extends FeatureMessageHandler {
  private readonly JOKE_REGEX = /^(tell\W+me\W+)?(a\W+)?joke\W*$/;
  private readonly JOKE_API_URL =
    'https://official-joke-api.appspot.com/jokes/general/random';
  private readonly ANSWER_TIMEOUT = 3;

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.matchSanitisedLowercaseTextIfForGradbot(this.JOKE_REGEX)) {
      const data = await this.fetchAPI<JokeAPIData[]>(this.JOKE_API_URL);

      if (data && data[0] && data[0].setup && data[0].punchline) {
        await this.scheduleMessageWithTimeout(
          uuid.v4(),
          this.ANSWER_TIMEOUT,
          this.createMessage(
            data[0].punchline,
            `${this.message.user.displayName} asked me to tell them a joke`,
          ),
        );

        return this.createMessage(
          data[0].setup,
          `${this.message.user.displayName} asked me to tell them a joke`,
        );
      }

      return this.createMessage(
        "I can't remember any jokes right now",
        `${this.message.user.displayName} asked me to tell them a joke, but API is down`,
      );
    }
    return null;
  }
}

interface JokeAPIData {
  setup?: string;
  punchline?: string;
}
