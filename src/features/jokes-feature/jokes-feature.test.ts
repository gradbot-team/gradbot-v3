import 'reflect-metadata';
import { JokesFeature } from './jokes-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(JokesFeature);

describe('JokesFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds to targetted messages and schedules punchline messages', async () => {
    util.api.urlReturnValues[
      'https://official-joke-api.appspot.com/jokes/general/random'
    ] = [
      {
        punchline: 'test punchline',
        setup: 'test setup',
      },
    ];

    await util.expectReplyToHaveText('Gradbot, tell me a joke', 'test setup');
    await util.expectReplyToHaveText('g joke', 'test setup');

    let scheduledMessagesCount = 0;

    for (const key of Object.keys(util.scheduler.scheduledMessages)) {
      const value = util.scheduler.scheduledMessages[key];
      expect(value.message.text).toEqual('test punchline');
      scheduledMessagesCount++;
    }

    expect(scheduledMessagesCount).toEqual(2);
  });

  test('deals with invalid API data', async () => {
    util.api.urlReturnValues[
      'https://official-joke-api.appspot.com/jokes/general/random'
    ] = {
      wrong_property: 'test',
    };
    await util.expectReplyToHaveText(
      'joke, gradbot',
      "I can't remember any jokes right now",
    );
  });

  test('deals with missing API data', async () => {
    util.api.urlReturnValues[
      'https://official-joke-api.appspot.com/jokes/general/random'
    ] = null;
    await util.expectReplyToHaveText(
      'joke, gradbot',
      "I can't remember any jokes right now",
    );
  });
});
