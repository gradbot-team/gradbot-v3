import { Feature } from '../../gradbot/feature/feature';
import { JokesMessageHandler } from './jokes-message-handler';

export class JokesFeature extends Feature {
  public readonly name = 'Jokes';
  public readonly iCanInfo = 'tell jokes';
  public readonly instructions = '• *g joke*\n• *g tell me a joke*';
  protected readonly messageHandlerType = JokesMessageHandler;
}
