import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { toString } from '../../util/general/string-extensions';

export class BankHolidaysMessageHandler extends FeatureMessageHandler {
  private readonly BANK_HOLIDAYS_REGEX =
    /^(when\W+are\W+)?(the\W+)?(bank\W+)?holidays\W*$/;
  private readonly BANK_HOLIDAYS_API_URL =
    'https://www.gov.uk/bank-holidays.json';

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (
      this.matchSanitisedLowercaseTextIfForGradbot(this.BANK_HOLIDAYS_REGEX)
    ) {
      const data = await this.fetchAPI<BankHolidaysAPIData>(
        this.BANK_HOLIDAYS_API_URL,
      );

      if (
        data &&
        data['england-and-wales'] &&
        data['england-and-wales'].events
      ) {
        const todaysDate = new Date();
        todaysDate.setHours(0, 0, 0, 0);

        const holidaysText = data['england-and-wales'].events
          .filter(
            (event) =>
              event.date && event.title && new Date(event.date) >= todaysDate,
          )
          .map(
            (event) =>
              `• ${toString(event.date)}: ${toString(event.title)}` +
              (event.notes ? ` (${event.notes})` : ''),
          )
          .slice(0, 3)
          .join('\n');

        return this.createMessage(
          holidaysText,
          `${this.message.user.displayName} asked me about Bank Holidays`,
        );
      }

      return this.createMessage(
        "I asked the Government but they didn't answer",
        `${this.message.user.displayName} asked me about Bank Holidays, but API is down`,
      );
    }
    return null;
  }
}

interface BankHolidaysAPIData {
  'england-and-wales'?: BankHolidaysEnglandAndWales;
}
interface BankHolidaysEnglandAndWales {
  events?: BankHolidayEvent[];
}
interface BankHolidayEvent {
  title?: string;
  date?: string;
  notes?: string;
}
