import 'reflect-metadata';
import { BankHolidaysFeature } from './bank-holidays-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(BankHolidaysFeature);

const setValidAPIResponse = () =>
  (util.api.urlReturnValues['https://www.gov.uk/bank-holidays.json'] = {
    'england-and-wales': {
      events: [
        {
          date: '2005-01-01',
          notes: '',
          title: 'Test A',
        },
        {
          date: '2200-01-01',
          title: 'Test B',
        },
        {
          date: '2200-01-02',
          notes: 'test note',
          title: 'Test C',
        },
        {
          date: '2200-01-03',
          notes: '',
          title: 'Test D',
        },
        {
          date: '2200-01-04',
          notes: '',
          title: 'Test E',
        },
      ],
    },
  });

const expectedReply = `• 2200-01-01: Test B
• 2200-01-02: Test C (test note)
• 2200-01-03: Test D`;

describe('BankHolidaysFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds to "holidays"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText('Gradbot, holidays', expectedReply);
  });

  test('responds to "bank holidays"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText('g bank holidays', expectedReply);
  });

  test('deals with invalid API data', async () => {
    util.api.urlReturnValues['https://www.gov.uk/bank-holidays.json'] = {
      wrong_property: 'test',
    };
    await util.expectReplyToHaveText(
      'Gradbot, holidays',
      "I asked the Government but they didn't answer",
    );
  });

  test('deals with missing API data', async () => {
    util.api.urlReturnValues['https://www.gov.uk/bank-holidays.json'] = null;
    await util.expectReplyToHaveText(
      'Gradbot, holidays',
      "I asked the Government but they didn't answer",
    );
  });
});
