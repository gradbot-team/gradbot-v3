import { Feature } from '../../gradbot/feature/feature';
import { BankHolidaysMessageHandler } from './bank-holidays-message-handler';

export class BankHolidaysFeature extends Feature {
  public readonly name = 'Bank Holidays';
  public readonly iCanInfo = 'tell you about Bank Holidays';
  public readonly instructions = '• *g when are holidays*\n• *g Bank Holidays*';
  protected readonly messageHandlerType = BankHolidaysMessageHandler;
}
