Liar's dice manual

Actions

To interact with liar's dice use ld <action>

Supported actions

# any time

- []/[status] shows the current status of the game
- [help] shows help information
- [quit] you leave the game current game
- [abort] in case of emergencies, abort the game

# Before a game

- [join]/[j] allows you to join the games
- [start] starts the game (requires a minimum of 2 players)

# During a game

- [bid ndm]/[ndm] you enter a bid for n dice of m face
- [call] you claim that the previous player is a liar, round is
  resolved and the next round is started
- [exact] you claim that there is exactly that many dice of that face,
- [peek]/[p] look at your current dice

# database strategy

Liars dice uses two database objects,

LiarsDiceStateData [state] - contains a string of the game's state information
LiarsDicePlayerData [playerId,dice] - contains a player's id and dice
