import { noIndent } from '../../util/general/string-extensions';

// game state enum
export enum LIARS_DICE_STATE {
  ACTIVE = 'Y',
  INACTIVE = 'N',
}

// move enum
export enum LIARS_DICE_MOVE {
  EXACT = 'exact',
  CALL = 'call',
}

// private game variables
export const LIARS_DICE_CALLER_NUMBER = 0;
export const LIARS_DICE_MAX_PLAYERS = 10;
export const LIARS_DICE_MIN_PLAYERS = 2;
export const LIARS_DICE_MAX_DICE = 5;

// help/instructions text
export const LIARS_DICE_HELP_TEXT = noIndent(
  `Available commands:
   • *ld ''/status* – show current status of liar's dice
   • *ld abort* – aborts an active game
   • *ld abandon* – abandons a joining stage of the game
   • *ld join/j* – join the liar's dice game
   • *ld quit* – quit the liar's dice game
   • *ld start* – start the liar's dice game
   • *ld bid ndm/ndm* – make a bid of n dice of face m
   • *ld call* – call the previous bid
   • *ld exact* – claim the last bid was exact
   • *ld peek/p* – look at your dice`,
);
