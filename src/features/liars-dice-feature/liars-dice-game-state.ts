import { LiarsDicePlayer } from './liars-dice-player';
import {
  LIARS_DICE_MAX_PLAYERS,
  LIARS_DICE_MIN_PLAYERS,
  LIARS_DICE_STATE,
} from './liars-dice-constants';
import { LiarsDiceStateData } from './liars-dice-state-data';
import { LiarsDicePlayerData } from './liars-dice-player-data';
import { Message } from '../../gradbot/chat/message/message';

export class LiarsDiceGameState {
  // game state variables
  private gameState: LIARS_DICE_STATE = LIARS_DICE_STATE.INACTIVE;
  private curNumber = '00';
  private curFace = '0';
  private players: LiarsDicePlayer[] = [];

  constructor(state: string | null, playersData: LiarsDicePlayerData[] | null) {
    if (state) {
      // set up the game state
      this.gameState = state.charAt(0) as LIARS_DICE_STATE;
      this.curNumber = state.slice(1, 3);
      this.curFace = state.charAt(3);

      // Set up the players
      if (playersData) {
        for (const player of playersData) {
          this.players.push(
            new LiarsDicePlayer(player.playerId.get(), player.dice.get()),
          );
        }
      }
    }
  }

  public isActive() {
    return this.gameState === LIARS_DICE_STATE.ACTIVE;
  }

  public getPlayers(): LiarsDicePlayer[] {
    return this.players;
  }

  public isFull() {
    return this.players.length >= LIARS_DICE_MAX_PLAYERS;
  }

  public isFirstTurn() {
    return this.curNumber === '00';
  }

  public isNotYourTurn(userId: string) {
    return userId !== this.players[0].playerId;
  }

  public getNumberOfPlayers(): number {
    return this.players.length;
  }

  public isEmpty() {
    return this.players.length === 0;
  }

  public enoughPlayersToStart() {
    return this.players.length >= LIARS_DICE_MIN_PLAYERS;
  }

  public startGame(): void {
    this.gameState = LIARS_DICE_STATE.ACTIVE;
    this.resetBid();
  }

  public amountOfDiceWithFace(): number {
    let count = 0;

    this.players.forEach((player: LiarsDicePlayer) => {
      player.dice.forEach((die: string) => {
        if (die === this.curFace) {
          count++;
        }
      });
    });

    return count;
  }

  public currentBidAmount(): number {
    return Number(this.curNumber);
  }

  public currentBidFace(): number {
    return Number(this.curFace);
  }

  public isWon() {
    return this.players.length === 1;
  }

  public storeBid(amount: number, face: number): void {
    // story the amount number
    this.curNumber = amount.toString();

    // pad if necessary
    if (amount < 10) {
      this.curNumber = '0' + this.curNumber;
    }

    // store the face value
    this.curFace = face.toString();
  }

  public nextTurn() {
    // grab 1:end of the array
    const newPlayers: LiarsDicePlayer[] = this.players.slice(1);

    // add the last bidder to the end of the queue
    newPlayers.push(this.players[0]);

    // and store the updated list
    this.players = newPlayers;
  }

  public reRoll(): void {
    this.players.forEach((player: LiarsDicePlayer) => {
      player.reRoll();
    });
  }

  public resetBid(): void {
    this.curNumber = '00';
    this.curFace = '0';
  }

  public getPlayerByIndex(index: number): LiarsDicePlayer {
    return this.players[index];
  }

  public getPlayer(userId: string): LiarsDicePlayer | undefined {
    // return the result of trying to find the player
    return this.players.find((u) => u.playerId === userId);
  }

  public isInGame(userId: string) {
    return this.getPlayer(userId) !== undefined;
  }

  public addUser(userId: string): void {
    this.players.push(LiarsDicePlayer.newLDPlayer(userId));
  }

  public removeUser(userId: string): void {
    this.players = this.players.filter(
      (player: LiarsDicePlayer) => player.playerId !== userId,
    );
  }

  // Returns true if the player was removed from the game as a result
  public removeDiceFromPlayer(index: number): boolean {
    // remove the dice
    this.players[index].dice.pop();

    // remove the player if they have no dice left
    if (this.players[index].dice.length === 0) {
      this.removeUser(this.players[index].playerId);
      return true;
    }

    return false;
  }

  public stateRecord(message: Message): LiarsDiceStateData {
    const record = new LiarsDiceStateData(message);
    record.state.set(this.gameState + this.curNumber + this.curFace);
    return record;
  }
}
