import { DBO } from '../../gradbot/database/db-objects/registry/dbo-decorator';
import { FeatureDbObject } from '../../gradbot/database/db-objects/feature-db-object';
import { DbText } from '../../gradbot/database/types/db-text';
import { DbIsolationLevel } from '../../gradbot/database/db-objects/db-isolation-level';

@DBO
export class LiarsDicePlayerData extends FeatureDbObject<LiarsDicePlayerData> {
  isolationLevel = DbIsolationLevel.SERVER;

  playerId = new DbText();
  dice = new DbText();
}
