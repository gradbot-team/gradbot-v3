import { LIARS_DICE_MAX_DICE } from './liars-dice-constants';
import { LiarsDicePlayerData } from './liars-dice-player-data';
import { Message } from '../../gradbot/chat/message/message';

export class LiarsDicePlayer {
  public static newLDPlayer(userId: string): LiarsDicePlayer {
    return new LiarsDicePlayer(
      userId,
      this.generateNDice(LIARS_DICE_MAX_DICE).join(''),
    );
  }

  private static _getDiceRoll(): number {
    return Math.floor(Math.random() * Math.floor(6)) + 1;
  }

  private static generateNDice(n: number): string[] {
    const dice: string[] = [];

    for (let _i = 0; _i < n; _i++) {
      dice.push(this._getDiceRoll().toString());
    }

    return dice;
  }

  public playerId = '';
  public dice: string[] = [];

  constructor(userId: string, dice: string) {
    // add the player ID
    this.playerId = userId;

    // and the dice
    dice.split('').forEach((num: string) => {
      this.dice.push(num);
    });
  }

  public reRoll(): void {
    for (let _i = 0; _i < this.dice.length; _i++) {
      this.dice[_i] = LiarsDicePlayer._getDiceRoll().toString();
    }
  }

  public playerRecord(message: Message): LiarsDicePlayerData {
    const record = new LiarsDicePlayerData(message);
    record.playerId.set(this.playerId);
    record.dice.set(this.dice.join(''));
    return record;
  }
}
