import 'reflect-metadata';

import { User } from '../../gradbot/chat/user/user';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import { getTestMessage } from '../../util/test/get-test-message';

import {
  LIARS_DICE_HELP_TEXT,
  LIARS_DICE_MAX_PLAYERS,
} from './liars-dice-constants';
import { LiarsDiceFeature } from './liars-dice-feature';
import { LiarsDicePlayerData } from './liars-dice-player-data';
import { LiarsDiceStateData } from './liars-dice-state-data';

const util = FeatureTestUtil.createForFeatureType(LiarsDiceFeature);

// Get 1 user for free (user id is test_user_1)
// add additional users -> UserId will be test_user_2/3/.../10
util.addUsers([
  'Zeus',
  'Hera',
  'Poseidon',
  'Hades',
  'Athena',
  'Ares',
  'Apollo',
  'Artemis',
  'Demeter',
  'Hermes',
]);

// ---------------- //
// lambda functions //
// ---------------- //

const statusText = (names: string, totalDice = 0, bid?: string) => {
  if (bid) {
    return (
      `Current bid: [${bid}]\n` +
      `Players: [${names}]\n` +
      `Total dice: [${totalDice}]`
    );
  } else {
    return `Players: [${names}]`;
  }
};

const garbageCollectorText = (cmd: string) =>
  `The Liar's dice feature did not understand the command ${cmd.trim()}.`;

const helpText = () => LIARS_DICE_HELP_TEXT;

const getUsers = (): User[] => util.chat.testUsers;
const getUser = (n: number): User => getUsers()[n];
const getUserName = (n: number): string => getUser(n).displayName;
const getUserRef = (n: number): string => `<@${getUser(n).id}>`;

const nPlayersJoin = async (n: number): Promise<void> => {
  // n players join the game
  for (let _i = 0; _i < n; _i++) {
    await util.getReplyForMessage('ld j', getUsers()[_i]);
  }
};

const deleteDb = async (): Promise<void> => {
  const state = await new LiarsDiceStateData(getTestMessage()).retrieveAlike();

  if (state[0]) {
    await state[0].delete();
  }

  const players = await new LiarsDicePlayerData(
    getTestMessage(),
  ).retrieveAlike();
  if (players) {
    for (const player of players) {
      await player.delete();
    }
  }
};

const writeDb = async (s: string): Promise<void> => {
  await deleteDb();

  const stateSplit: string[] = s.split('+');

  const state = new LiarsDiceStateData(getTestMessage());
  state.state.set(stateSplit[0]);
  await state.write();

  for (const playerString of stateSplit[1].split('|')) {
    const playerSplit: string[] = playerString.split('-');

    const player = new LiarsDicePlayerData(getTestMessage());
    player.playerId.set(playerSplit[0]);
    player.dice.set(playerSplit[1]);
    await player.write();
  }
};

const readDb = async (): Promise<string | null> => {
  let gameString = '';
  const state = await new LiarsDiceStateData(getTestMessage()).retrieveAlike();

  if (state[0]) {
    gameString = state[0].state.get() + '+';

    const players = await new LiarsDicePlayerData(
      getTestMessage(),
    ).retrieveAlike();

    if (players) {
      for (const player of players) {
        if (!gameString.endsWith('+')) {
          gameString += '|';
        }

        gameString += player.playerId.get() + '-' + player.dice.get();
      }
    }
  }

  if (gameString !== '') {
    return gameString;
  }

  return null;
};

afterEach(async () => {
  await deleteDb();
});

describe('LiarsDiceFeature', () => {
  // **************************************************************************//

  test("Does not respond to messages not directed at Liar's dice", async () => {
    await util.expectReplyToBeNull('g test');
    await util.expectReplyToBeNull('ldtest');
    await util.expectReplyToBeNull('g testld');
    await util.expectReplyToBeNull('g testldtest');
  });

  // **************************************************************************//

  test('Does not respond to garbage', async () => {
    await util.expectReplyToHaveText(
      'ld garbage',
      garbageCollectorText('garbage'),
    );

    // add garbage to all cmds to ensure we get the right behaviour
    const commands: string[] = [
      'help',
      'quit',
      'join',
      'j',
      'start',
      'bid 15d6',
      '16d6',
      'call',
      'exact',
      'peek',
      'p',
    ];

    for (const cmd of commands) {
      await util.expectReplyToHaveText(
        `ld ${cmd}dsf`,
        garbageCollectorText(`${cmd}dsf`),
      );
    }
  });

  // **************************************************************************//

  test('Does respond to help', async () => {
    await util.expectReplyToHaveText('ld help', helpText());
    await util.expectReplyToHaveText('ld h', helpText());
  });

  // **************************************************************************//

  test('Successfully gets the status with no active game', async () => {
    // game state is empty
    await util.expectReplyToHaveText('ld ', statusText(''));

    await util.expectReplyToHaveText('ld status ', statusText(''));
  });

  // **************************************************************************//

  test('Successfully gets the status with active game', async () => {
    // game state is empty
    // 2 players join
    await nPlayersJoin(2);

    // start the game
    await util.getReplyForMessage('ld start');

    const players = `${getUserName(0)}/5, ${getUserName(1)}/5`;

    // check the status
    await util.expectReplyToHaveText('ld ', statusText(players, 10, '0d0'));

    await util.expectReplyToHaveText(
      'ld status ',
      statusText(players, 10, '0d0'),
    );
  });

  // **************************************************************************//

  test('Join rejected because player is already in the game', async () => {
    // 2 players join
    await nPlayersJoin(2);

    // new player tries to join and gets rejected
    await util.expectReplyToHaveText(
      'ld join',
      "You are already in the Liar's dice game.",
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /N000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );
  });

  // **************************************************************************//

  test('Join rejected because the game is active', async () => {
    // 2 players join
    await nPlayersJoin(2);

    // start the game
    await util.getReplyForMessage('ld start');

    // new player tries to join and gets rejected
    await util.expectReplyToHaveText(
      'ld join',
      'You cannot join while a game is active.',
      getUser(2),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );
  });

  // **************************************************************************//

  test('Join rejected because the game is full', async () => {
    // get the first max+1 players
    const players: User[] = getUsers().slice(0, LIARS_DICE_MAX_PLAYERS + 1);

    // the max+1th/last player is the rejected player
    // and reduce players to max players
    const rejectedPlayer: User | undefined = players.pop();

    if (rejectedPlayer) {
      // get the first max players names
      const playersNames: string[] = players.map((user) => user.displayName);

      // max players join
      await nPlayersJoin(LIARS_DICE_MAX_PLAYERS);

      // the max+1 one get rejected
      await util.expectReplyToHaveText(
        'ld join',
        `Sorry ${rejectedPlayer.displayName}, but the game is full.`,
        rejectedPlayer,
      );

      // the status should show all players that have joined
      await util.expectReplyToHaveText(
        'ld status',
        statusText(playersNames.join(', ')),
      );

      const playersRegExp: string =
        '([a-z_0-9]+-[1-6]{5}|)' +
        `{${LIARS_DICE_MAX_PLAYERS - 1}}` +
        '[a-z_0-9]+-[1-6]{5}';

      // check the game state looks correct
      expect(await readDb()).toMatch(
        new RegExp('N000\\+' + playersRegExp, 'i'),
      );

      const playersNames2: string[] = players.map(
        (user) => `${user.displayName}/5`,
      );

      // start the game and get the status
      await util.getReplyForMessage('ld start');
      await util.expectReplyToHaveText(
        'ld',
        statusText(playersNames2.join(', '), playersNames2.length * 5, '0d0'),
      );

      // check the game state looks correct
      expect(await readDb()).toMatch(
        new RegExp('Y000\\+' + playersRegExp, 'i'),
      );
    } else {
      throw new Error('Failed to get the last user in the users list');
    }
  });

  // **************************************************************************//

  test('Successfully joining the game', async () => {
    // join the game and expect the join text (use join command)
    await util.expectReplyToHaveText(
      'ld join',
      `${getUserName(0)} has joined the Liar's dice game.`,
    );

    await util.expectReplyToHaveText('ld status', statusText('User'));

    // check the game state looks correct
    expect(await readDb()).toMatch(/N000\+[a-z_0-9]+-[1-6]{5}/i);

    // join the game and expect the join text (use j command)
    await util.expectReplyToHaveText(
      'ld j',
      `${getUserName(1)} has joined the Liar's dice game.`,
      getUser(1),
    );

    await util.expectReplyToHaveText('ld status', statusText('User, Zeus'));

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /N000\+[a-z_0-9]+-[1-6]{5}|[a-z_0-9]+-[1-6]{5}/i,
    );
  });

  // **************************************************************************//

  test('Quit rejected as player is not in the game', async () => {
    // the status should show all players that have joined
    await util.expectReplyToHaveText(
      'ld quit',
      `You can't quit ${getUserName(0)}, because you ` + 'are not in the game.',
    );
  });

  // **************************************************************************//

  test('Successful quit while there is no active game', async () => {
    // 2 players join the game
    await nPlayersJoin(2);

    // the status should show all players that have joined
    await util.expectReplyToHaveText(
      'ld status',
      statusText(getUserName(0) + ', ' + getUserName(1)),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /N000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );

    // Player 1 quits the game
    await util.expectReplyToHaveText(
      'ld quit',
      `${getUserName(0)} has quit the Liar's dice game.`,
    );

    // Player checks the status
    await util.expectReplyToHaveText('ld', statusText(getUserName(1)));

    // check the game state looks correct
    expect(await readDb()).toMatch(/^N000\+[a-z_0-9]+-[1-6]{5}$/i);
  });

  // **************************************************************************//

  test('Successful quit while there is an active game, game continues afterwards', async () => {
    // 3 players join the game
    await nPlayersJoin(3);

    // start game
    await util.getReplyForMessage('ld start');

    // the status should show all players that have joined
    await util.expectReplyToHaveText(
      'ld status',
      statusText(
        `${getUserName(0)}/5, ${getUserName(1)}/5, ${getUserName(2)}/5`,
        15,
        '0d0',
      ),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y000\+[a-z_0-9]+-[1-6]{5}(\|[a-z_0-9]+-[1-6]{5}){2}/i,
    );

    // first bid
    await util.getReplyForMessage('ld bid 3d4');

    // the status should show all players that have joined
    await util.expectReplyToHaveText(
      'ld status',
      statusText(
        `${getUserName(1)}/5, ${getUserName(2)}/5, ${getUserName(0)}/5`,
        15,
        '3d4',
      ),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y034\+[a-z_0-9]+-[1-6]{5}(\|[a-z_0-9]+-[1-6]{5}){2}/i,
    );

    // Player 0 quits the game
    await util.expectReplyToHaveText(
      'ld quit',
      `${getUserName(0)} has quit the Liar's dice game.\n\n` +
        `A new round has started, ${getUserRef(1)} is to start.`,
    );

    // Player checks the status
    await util.expectReplyToHaveText(
      'ld',
      statusText(`${getUserName(1)}/5, ${getUserName(2)}/5`, 10, '0d0'),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /^Y000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}$/i,
    );
  });

  // **************************************************************************//

  test('Successful quit while there is no active game, and there is 1 player', async () => {
    // 2 players join the game
    await nPlayersJoin(2);

    // the status should show all players that have joined
    await util.expectReplyToHaveText(
      'ld status',
      statusText(getUserName(0) + ', ' + getUserName(1)),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /N000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );

    // Player 0 quits the game
    await util.expectReplyToHaveText(
      'ld quit',
      `${getUserName(0)} has quit the Liar's dice game.`,
    );

    // Player checks the status
    await util.expectReplyToHaveText('ld', statusText(getUserName(1)));

    // check the game state looks correct
    expect(await readDb()).toMatch(/^N000\+[a-z_0-9]+-[1-6]{5}$/i);

    // Player 1 quits the game
    await util.expectReplyToHaveText(
      'ld quit',
      `${getUserName(1)} has quit the Liar's dice game.`,
      getUser(1),
    );

    // Player checks the status
    await util.expectReplyToHaveText('ld', statusText(''));

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Start rejected because the game has less than 2 players', async () => {
    // 1 players join the game
    await nPlayersJoin(1);

    // Cannot start the game with one players
    await util.expectReplyToHaveText(
      'ld start',
      "Liar's dice can only start when 2 or more players have joined.",
    );
  });

  // **************************************************************************//

  test('Start rejected when a game is active', async () => {
    // 2 players join the game
    await nPlayersJoin(2);

    // Start the game
    await util.getReplyForMessage('ld start');

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );

    // Cannot start the game with one players
    await util.expectReplyToHaveText(
      'ld start',
      'A game is already active, cannot start a second.',
    );
  });

  // **************************************************************************//

  test('Start rejected as player is not in the game', async () => {
    // Add 2 users
    await nPlayersJoin(2);

    // Third player attempts to start the game but is not in the game
    await util.expectReplyToHaveText(
      'ld start',
      "You are not in the current Liar's dice game, so you cannot start it.",
      getUser(2),
    );
  });

  // **************************************************************************//

  test('Start rejected and game aborted if user does not exist in the db', async () => {
    // add a fake user
    await writeDb('N000+fake_people_1-12345');

    // Add 2 users
    await nPlayersJoin(2);

    // Attempting to start the game fails as fake_people_1 does not exist
    await util.expectReplyToHaveText(
      'ld start',
      'Could not identify user ID fake_people_1.\n\n' +
        "The current Liar's dice game is aborted.",
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Successfully starting the game', async () => {
    // 2 players join the game
    await nPlayersJoin(2);

    // Cannot start the game with one players
    await util.expectReplyToHaveText(
      'ld start',
      "Welcome to a new Liar's dice game!\n\n" +
        `${getUserRef(0)} is to kick us off with a bid.`,
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );
  });

  // **************************************************************************//

  test('Abort rejected when there is no active game', async () => {
    // abort is rejected when there is no active game
    await util.expectReplyToHaveText(
      'ld abort',
      "Cannot abort a Liar's dice game if it is not running.",
    );
  });

  // **************************************************************************//

  test('Successfully abort game', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game
    await util.getReplyForMessage('ld start');

    // Third player aborts the game because of a problem
    await util.expectReplyToHaveText(
      'ld abort',
      `${getUserName(2)} has aborted the game.`,
      getUser(2),
    );
  });

  // **************************************************************************//

  test('Abandon rejected when there is an active game', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game
    await util.getReplyForMessage('ld start');

    // abort is rejected when there is no active game
    await util.expectReplyToHaveText(
      'ld abandon',
      "Cannot abandon a Liar's dice game if it is running.",
    );
  });

  // **************************************************************************//

  test('Successfully abandon game', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // Third player aborts the game because of a problem
    await util.expectReplyToHaveText(
      'ld abandon',
      `${getUserName(2)} has abandoned the game.`,
      getUser(2),
    );
  });

  // **************************************************************************//

  test('Bid rejected when there is no active game', async () => {
    // Bidding while the game is not running is rejected
    await util.expectReplyToHaveText(
      'ld 1d1',
      "Cannot bid when Liar's dice is not running.",
    );
  });

  // **************************************************************************//

  test('Bid rejected when you are not in the game', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');

    // Bid rejected when it is not their turn
    await util.expectReplyToHaveText(
      'ld 1d1',
      'You cannot bid because you are not in the current game ' +
        "of Liar's dice",
      getUser(2),
    );
  });

  // **************************************************************************//

  test('Bid rejected when it is not your turn', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');

    // Bid rejected when it is not their turn
    await util.expectReplyToHaveText(
      'ld 1d1',
      'Cannot bid when it is not your turn.',
      getUser(1),
    );
  });

  // **************************************************************************//

  test('Bid rejected when too low on face or amount', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');

    // Bidding too low an amount is rejected
    await util.expectReplyToHaveText(
      'ld 0d4',
      'Bid rejected: amount value is too low.',
    );

    // set the initial bid
    await util.getReplyForMessage('ld bid 2d3');

    // Bidding too low a face is rejected
    await util.expectReplyToHaveText(
      'ld 2d3',
      'Bid rejected: face value is too low.',
      getUser(1),
    );

    // Bidding too low an amount is rejected
    await util.expectReplyToHaveText(
      'ld 1d4',
      'Bid rejected: amount value is too low.',
      getUser(1),
    );

    // Bidding a face that doesn't make sense failes
    await util.expectReplyToHaveText(
      'ld 3d0',
      'Bid rejected: face value is not 1-6.',
      getUser(1),
    );

    // Bidding a face that doesn't make sense failes
    await util.expectReplyToHaveText(
      'ld 3d8',
      'Bid rejected: face value is not 1-6.',
      getUser(1),
    );
  });

  // **************************************************************************//

  test('Successful bid', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game
    await util.getReplyForMessage('ld start');

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y000\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );

    // Successful bid, single digit so has to be padded
    await util.expectReplyToHaveText(
      'ld bid 2d3',
      `${getUserName(0)} has made a bid of 2 dice of face 3.\n\n` +
        `${getUserRef(1)}, you're up next.`,
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y023\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );

    // Successful bid, double digit so shouldn't be padded
    // and use other bid command
    await util.expectReplyToHaveText(
      'ld bid 34d2',
      `${getUserName(1)} has made a bid of 34 dice of face 2.\n\n` +
        `${getUserRef(0)}, you're up next.`,
      getUser(1),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /Y342\+[a-z_0-9]+-[1-6]{5}\|[a-z_0-9]+-[1-6]{5}/i,
    );
  });

  // **************************************************************************//

  test('Call rejected because no game is active', async () => {
    // call rejected because no game is running
    await util.expectReplyToHaveText(
      'ld call',
      "Cannot call when Liar's dice is not running.",
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Call rejected because player is not in the game', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');
    await util.getReplyForMessage('ld bid 1d5');

    // call rejected because the player is not in the game
    await util.expectReplyToHaveText(
      'ld call',
      'You cannot call because you are not in the ' +
        "current game of Liar's dice.",
      getUser(2),
    );
  });

  // **************************************************************************//

  test("Call rejected because it is not the player's turn", async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');
    await util.getReplyForMessage('ld bid 1d5');

    // call rejected because it is not the players turn
    await util.expectReplyToHaveText(
      'ld call',
      'Cannot call when it is not your turn.',
    );
  });

  // **************************************************************************//

  test('Call rejected because it is the first turn', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');

    // call rejected because it is the first turn
    await util.expectReplyToHaveText(
      'ld call',
      'Cannot call when it is the first turn.',
    );
  });

  // **************************************************************************//

  test('Winning call', async () => {
    // fake set up
    await writeDb('Y011+test_user_1-22222|test_user_2-22222');

    // call is successful -> expect winning message
    await util.expectReplyToHaveText(
      'ld call',
      `${getUserName(0)} was correct to doubt Zeus's bid of 1d1, there was ` +
        `in fact 0d1.\n` +
        'User: [X,X,X,X,X]\n' +
        'Zeus: [X,X,X,X,X]\n' +
        `${getUserName(1)} loses a die\n\n` +
        `A new round has started, ${getUserRef(0)} is to start.`,
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /^Y000\+test_user_1-[1-6]{5}\|test_user_2-[1-6]{4}$/,
    );
  });

  // **************************************************************************//

  test('Losing call', async () => {
    // fake set up
    await writeDb('Y102+test_user_1-22223|test_user_2-22223|test_user_3-22223');

    // call that fails -> expect fail response
    await util.expectReplyToHaveText(
      'ld call',
      `${getUserName(2)} was correct with the bid of 10d2, ` +
        `there was in fact 12d2.\n` +
        'User: [2,2,2,2,X]\n' +
        'Zeus: [2,2,2,2,X]\n' +
        'Hera: [2,2,2,2,X]\n' +
        `${getUserName(0)} loses a die\n\n` +
        `A new round has started, ${getUserRef(0)} is to start.`,
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /^Y000\+test_user_1-[1-6]{4}\|test_user_2-[1-6]{5}\|test_user_3-[1-6]{5}$/,
    );
  });

  // **************************************************************************//

  test('Exact rejected because no game is active', async () => {
    // Exact rejected when the game isn't running
    await util.expectReplyToHaveText(
      'ld exact',
      "Cannot exact when Liar's dice is not running.",
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Exact rejected because player is not in the game', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');
    await util.getReplyForMessage('ld bid 1d5');

    // exact rejected because the player isn't in the game
    await util.expectReplyToHaveText(
      'ld exact',
      'You cannot exact because you are not in the ' +
        "current game of Liar's dice.",
      getUser(2),
    );
  });

  // **************************************************************************//

  test("Exact rejected because it is not the player's turn", async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game and bid
    await util.getReplyForMessage('ld start');
    await util.getReplyForMessage('ld bid 1d5');

    // exact rejected because it is not the player's turn
    await util.expectReplyToHaveText(
      'ld exact',
      'Cannot exact when it is not your turn.',
    );
  });

  // **************************************************************************//

  test('Exact rejected because it is the first turn', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game
    await util.getReplyForMessage('ld start');

    // exact rejected because it is the first turn
    await util.expectReplyToHaveText(
      'ld exact',
      'Cannot exact when it is the first turn.',
    );
  });

  // **************************************************************************//

  test('Winning exact', async () => {
    // fake set up
    await writeDb('Y102+test_user_1-22222|test_user_3-33333|test_user_2-22222');

    // exact is correct -> expect a winning message
    await util.expectReplyToHaveText(
      'ld exact',
      `Well done to ${getUserName(0)} for correctly guessing there ` +
        'was exactly 10d2.\n' +
        'User: [2,2,2,2,2]\n' +
        'Hera: [X,X,X,X,X]\n' +
        'Zeus: [2,2,2,2,2]\n' +
        'Everyone except User now loses a die\n\n' +
        `A new round has started, ${getUserRef(0)} is to start.`,
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /^Y000\+test_user_1-[1-6]{5}(\|test_user_[23]-[1-6]{4}){2}$/,
    );
  });

  // **************************************************************************//

  test('Losing exact', async () => {
    // fake set up
    await writeDb('Y103+test_user_1-22222|test_user_2-22222');

    // exact is incorrect so we expect a failed situation
    await util.expectReplyToHaveText(
      'ld exact',
      `${getUserName(0)} was incorrect on the ` +
        `exact bid, there was in fact 0d3.\n` +
        'User: [X,X,X,X,X]\n' +
        'Zeus: [X,X,X,X,X]\n' +
        `${getUserName(0)} loses a die.\n\n` +
        `A new round has started, ${getUserRef(0)} is to start.`,
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /^Y000\+test_user_1-[1-6]{4}\|test_user_2-[1-6]{5}$/,
    );
  });

  // **************************************************************************//

  test('Peek rejected because no game is active', async () => {
    // Peek rejected when the game isn't running
    await util.expectReplyToHaveText(
      'ld peek',
      "Cannot peek when Liar's dice is not running.",
      getUser(2),
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Peek rejected because player is not in the game', async () => {
    // 2 people join
    await nPlayersJoin(2);

    // initiate the game
    await util.getReplyForMessage('ld start');

    // exact rejected because the player isn't in the game
    await util.expectReplyToHaveText(
      'ld peek',
      'You cannot peek because you are not in the ' +
        "current game of Liar's dice.",
      getUser(2),
    );
  });

  // **************************************************************************//

  test('Successfully peeking at dice', async () => {
    // 5 players join
    await nPlayersJoin(5);

    // start the game
    await util.getReplyForMessage('ld start');

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /^Y000\+test_user_1-[1-6]{5}(\|test_user_[0-9]-[1-6]{5}){4}$/,
    );

    // all 5 players check their dice
    // check the text is correct and it is private to the user
    for (let _i = 0; _i < 5; _i++) {
      await util.expectPrivateReplyToHaveText(
        'ld p',
        /\[[1-6](,[1-6]){4}\]/,
        getUser(_i),
      );
    }

    // use the other 'peek' command to peek
    await util.expectPrivateReplyToHaveText(
      'ld peek',
      /\[[1-6](,[1-6]){4}\]/,
      getUser(0),
    );
  });

  // **************************************************************************//

  test('Winning from a quit', async () => {
    // fake set up
    await writeDb('Y103+test_user_4-22222|test_user_2-22222');

    // Hades wins the game as Zeus quits
    await util.expectReplyToHaveText(
      'ld quit',
      `${getUserName(1)} has quit the Liar's dice game.\n\n` +
        "And it's all over.\n\n" +
        `After the long battle ... ${getUserName(3)} emerged ` +
        'victorious.\n\n' +
        `Will anyone best ${getUserName(3)}? Only time will tell.\n\n` +
        "Until the next exciting Liar's dice game, farewell",
      getUser(1),
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Winning from a call', async () => {
    // fake set up
    await writeDb('Y103+test_user_6-22222|test_user_4-2');

    // User wins the game with a call
    await util.expectReplyToHaveText(
      'ld call',

      `Athena was correct to doubt Poseidon's bid of 10d3, ` +
        `there was in fact 0d3.\n` +
        `Athena: [X,X,X,X,X]\n` +
        `Poseidon: [X]\n` +
        `Poseidon loses a die\n\n` +
        "And it's all over.\n\n" +
        `After the long battle ... ${getUserName(5)} emerged ` +
        'victorious.\n\n' +
        `Will anyone best ${getUserName(5)}? Only time will tell.\n\n` +
        "Until the next exciting Liar's dice game, farewell",
      getUser(5),
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Winning from a exact', async () => {
    // fake set up
    await writeDb(
      'Y092+test_user_3-22222|test_user_1-2|test_user_2-2|' +
        'test_user_4-2|test_user_5-2',
    );

    // Hera wins the game with an exact
    // knocking out 4 other players!
    await util.expectReplyToHaveText(
      'ld exact',
      `Well done to Hera for correctly guessing there was exactly 9d2.\n` +
        `Hera: [2,2,2,2,2]\n` +
        `User: [2]\n` +
        `Zeus: [2]\n` +
        `Poseidon: [2]\n` +
        `Hades: [2]\n` +
        `Everyone except ${getUserName(2)} now loses a die\n\n` +
        "And it's all over.\n\n" +
        `After the long battle ... ${getUserName(2)} emerged ` +
        'victorious.\n\n' +
        `Will anyone best ${getUserName(2)}? Only time will tell.\n\n` +
        "Until the next exciting Liar's dice game, farewell",
      getUser(2),
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//

  test('Test game', async () => {
    // 2 players join
    await nPlayersJoin(2);

    // start the game
    await util.getReplyForMessage('ld start');

    // play 4 rounds -> Zeus loses all of them, oof
    for (let _i = 0; _i < 4; _i++) {
      // user makes a bid, Zeus make a bid that is too large
      await util.getReplyForMessage('ld bid 1d1');
      await util.getReplyForMessage(`ld bid ${11 - _i}d1`, getUser(1));

      // check the status
      await util.expectReplyToHaveText(
        'ld status ',
        statusText(`User/5, Zeus/${5 - _i}`, 10 - _i, `${11 - _i}d1`),
      );

      // User calls Zeus a liar as it can't possibly be that many!
      await util.getReplyForMessage('ld call');

      // check the game state looks correct
      expect(await readDb()).toMatch(
        new RegExp(/^Y000\+test_user_1-[1-6]{5}\|test_user_2-[1-6]{1,4}$/),
      );
    }

    // check the game state looks correct
    // more strict than the for loop check
    expect(await readDb()).toMatch(
      /^Y000\+test_user_1-[1-6]{5}\|test_user_2-[1-6]$/,
    );

    // Zeus again bids too much!
    await util.getReplyForMessage('ld bid 1d1');
    await util.getReplyForMessage('ld bid 7d1', getUser(1));

    // check the status looks correct
    await util.expectReplyToHaveText(
      'ld status ',
      statusText('User/5, Zeus/1', 6, '7d1'),
    );

    // check the game state looks correct
    expect(await readDb()).toMatch(
      /^Y071\+test_user_1-[1-6]{5}\|test_user_2-[1-6]$/,
    );

    // User wins the game with an call after this!
    await util.expectReplyToHaveText(
      'ld call',

      new RegExp(
        `User was correct to doubt Zeus's bid of 7d1, there was in fact [0-6]d1.\n` +
          'User: \\[([1-6X],?){5}\\]\n' +
          'Zeus: \\[[1-6X]\\]\n' +
          'Zeus loses a die\n\n' +
          "And it's all over.\n\n" +
          `After the long battle ... ${getUserName(0)} emerged ` +
          'victorious.\n\n' +
          `Will anyone best ${getUserName(0)}\\? Only time will tell.\n\n` +
          "Until the next exciting Liar's dice game, farewell",
        'm' /* multiline*/,
      ),
    );

    // check the game state looks correct
    expect(await readDb()).toBeNull();
  });

  // **************************************************************************//
});
