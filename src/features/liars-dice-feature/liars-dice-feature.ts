import { Feature } from '../../gradbot/feature/feature';
import { LiarsDiceMessageHandler } from './liars-dice-message-handler';
import { LIARS_DICE_HELP_TEXT } from './liars-dice-constants';

export class LiarsDiceFeature extends Feature {
  public readonly name = "Liar's dice";
  public readonly iCanInfo = "facilitate a game of Liar's dice.";
  public readonly instructions = LIARS_DICE_HELP_TEXT;

  protected readonly messageHandlerType = LiarsDiceMessageHandler;
}
