import { Message } from '../../gradbot/chat/message/message';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';

import {
  LIARS_DICE_HELP_TEXT,
  LIARS_DICE_MOVE,
  LIARS_DICE_CALLER_NUMBER,
} from './liars-dice-constants';
import { LiarsDiceGameState } from './liars-dice-game-state';
import { LiarsDicePlayer } from './liars-dice-player';
import { LiarsDicePlayerData } from './liars-dice-player-data';
import { LiarsDiceStateData } from './liars-dice-state-data';

interface PlayerResult {
  playerNames: string[];
  fail: boolean;
}

interface PlayerNameAndDice {
  name: string;
  dice: string[];
  fail: boolean;
}

export class LiarsDiceMessageHandler extends FeatureMessageHandler {
  private readonly _isLiarsDiceRegex: RegExp = /(^\s*ld\s*$|^\s*ld\s+)/i;
  private readonly _dataStateRetriever = new LiarsDiceStateData(this.message);
  private readonly _dataPlayerRetriever = new LiarsDicePlayerData(this.message);

  // **************************************************************************//
  // **                       MASTER RESPONSE                                **//
  // **************************************************************************//

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const msg: string = this.message.text.trim();

    // check this is for Liar's Dice
    if (this._forLiarsDice(msg)) {
      const cleanMsg: string = this._removeLiarsDiceAddress(msg);

      // deal with help first as if help is requested we don't
      // need to read the db
      if (/^(\s*h(elp)?\s*)$/i.test(cleanMsg)) {
        return this.createMessage(
          LIARS_DICE_HELP_TEXT,
          "I was asked for help with liar's dice.",
        );
      }

      const game: LiarsDiceGameState = new LiarsDiceGameState(
        await this.getStateFromDatabase(),
        await this.getPlayersFromDatabase(),
      );

      if (/^(\s*(status)?\s*)$/i.test(cleanMsg)) {
        return this._getStatus(this.message, game);
      } else if (/^(\s*j(oin)?\s*)$/i.test(cleanMsg)) {
        return this._joinGame(this.message, game);
      } else if (/^\s*quit\s*$/i.test(cleanMsg)) {
        return this._quitGame(this.message, game);
      } else if (/^\s*start\s*$/i.test(cleanMsg)) {
        return this._startGame(this.message, game);
      } else if (/^\s*(bid\s+)?[0-9][0-9]?d[0-9]\s*$/i.test(cleanMsg)) {
        return this._bid(this.message, game, cleanMsg);
      } else if (/^\s*call\s*$/i.test(cleanMsg)) {
        return this._callOrExact(this.message, game, LIARS_DICE_MOVE.CALL);
      } else if (/^\s*exact\s*$/i.test(cleanMsg)) {
        return this._callOrExact(this.message, game, LIARS_DICE_MOVE.EXACT);
      } else if (/^\s*p(eek)?\s*$/i.test(cleanMsg)) {
        return this._peek(this.message, game);
      } else if (/^\s*abort\s*$/i.test(cleanMsg)) {
        return this._abortGame(this.message, game);
      } else if (/^\s*abandon\s*$/i.test(cleanMsg)) {
        return this._abandonGame(this.message, game);
      } else {
        // catch all
        return this.createMessage(
          "The Liar's dice feature did not understand " +
            `the command ${cleanMsg}.`,
          "I was asked for help with Liar's dice.",
        );
      }
    }

    return null;
  }

  // **************************************************************************//
  // **                           DB                                         **//
  // **************************************************************************//

  private async getStateDbRecord() {
    const data = await this._dataStateRetriever.retrieveAlike();
    return data[0];
  }

  private async getStateFromDatabase(): Promise<string | null> {
    const record = await this.getStateDbRecord();

    if (record) {
      return record.state.get();
    }

    return null;
  }

  private getPlayerDbRecords() {
    return this._dataPlayerRetriever.retrieveAlike();
  }

  private getPlayersFromDatabase() {
    return this.getPlayerDbRecords();
  }

  private async deleteGame(): Promise<void> {
    const state = await this.getStateDbRecord();

    if (state) {
      await state.delete();
    }

    const players = await this.getPlayerDbRecords();

    if (players) {
      for (const player of players) {
        await player.delete();
      }
    }
  }

  private async saveGame(game: LiarsDiceGameState): Promise<void> {
    await this.deleteGame();

    // write the state
    await game.stateRecord(this.message).write();

    // then write all the players
    for (const player of game.getPlayers()) {
      await player.playerRecord(this.message).write();
    }
  }

  // **************************************************************************//
  // **                      INTERNAL FUNCTIONS                              **//
  // **************************************************************************//

  private _playersDiceToString(player: LiarsDicePlayer): string {
    return player.dice.sort().join();
  }

  private _forLiarsDice(text: string) {
    return this._isLiarsDiceRegex.test(text);
  }

  private _removeLiarsDiceAddress(text: string): string {
    return text.replace(this._isLiarsDiceRegex, '');
  }

  private async _getPlayerNamesAndDice(
    game: LiarsDiceGameState,
  ): Promise<PlayerNameAndDice[]> {
    const ret: PlayerNameAndDice[] = [];

    for (const player of game.getPlayers()) {
      const userName = await this._getPlayerName(player);

      if (userName) {
        ret.push({ name: userName, dice: player.dice, fail: false });
      } else {
        // failed! return the player id of the name that
        // did not have a known user name
        return [{ name: player.playerId, dice: [], fail: true }];
      }
    }

    // success! return the names with fail set to false
    return ret;
  }

  private _printPlayersAndDiceForChecks(
    playerDetails: PlayerNameAndDice[],
    curFace: number,
  ): string {
    let ret = '';

    for (const det of playerDetails) {
      const diceStr: string = det.dice
        .map((d) => {
          if (Number(d) === curFace) {
            return d;
          } else {
            return 'X';
          }
        })
        .join();
      ret += `${det.name}: [${diceStr}]\n`;
    }

    return ret;
  }

  private async _getPlayerNames(
    game: LiarsDiceGameState,
  ): Promise<PlayerResult> {
    const retPlayerNames: string[] = [];

    for (const player of game.getPlayers()) {
      const userName = await this._getPlayerName(player);

      if (userName) {
        retPlayerNames.push(userName);
      } else {
        // failed! return the player id of the name that
        // did not have a known user name
        return { playerNames: [player.playerId], fail: true };
      }
    }

    // success! return the names with fail set to false
    return { playerNames: retPlayerNames, fail: false };
  }

  private async _getPlayerName(
    player: LiarsDicePlayer,
  ): Promise<string | undefined> {
    const user = await this.getUserWithId(player.playerId);

    if (user) {
      return user.displayName;
    }
  }

  private async _getNextPlayerName(
    game: LiarsDiceGameState,
  ): Promise<string | undefined> {
    return await this._getPlayerName(game.getPlayers()[0]);
  }

  private _getNextPlayerId(game: LiarsDiceGameState): string {
    return game.getPlayers()[0].playerId;
  }

  private async _abortGameDueToMissingUser(
    userId: string,
  ): Promise<FeatureMessage> {
    // only get here if the user's name could not be found
    //
    // in this case the game is corrupted so it is deleted
    await this.deleteGame();
    return this.createMessage(
      `Could not identify user ID ${userId}.

The current Liar's dice game is aborted.`,
      `A user was in the Liar's dice database that was not in the userlist.

Therefore the game was aborted`,
    );
  }

  private async _andTheWinnerIs(
    game: LiarsDiceGameState,
    text: string,
  ): Promise<FeatureMessage> {
    // The game has been won - handle that here

    // get the winner's name
    const winner: string | undefined = await this._getNextPlayerName(game);

    // abort if we fail to get the winner's name
    if (!winner) {
      return this._abortGameDueToMissingUser(game.getPlayers()[0].playerId);
    }

    // the game is now over, so delete
    await this.deleteGame();

    // and return the winning message
    return this.createMessage(
      `${text}\n\n` +
        `And it's all over.

After the long battle ... ${winner} emerged victorious.

Will anyone best ${winner}? Only time will tell.

Until the next exciting Liar's dice game, farewell`,
      `A call/exact/quit caused ${winner} to win!`,
    );
  }

  private _createPeekMsg(
    player: LiarsDicePlayer,
    userName: string,
    operation: string,
  ) {
    return this.createMessageToUser(
      `[${this._playersDiceToString(player)}]`,
      `${userName} ran the ${operation} operation.`,
      player.playerId,
    );
  }

  private async _sendPeekToAllPlayers(
    game: LiarsDiceGameState,
    userName: string,
    operation: string,
  ): Promise<void> {
    await Promise.all([
      game
        .getPlayers()
        .map((p) =>
          this.sendMessage(this._createPeekMsg(p, userName, operation)),
        ),
    ]);
  }

  private async _startNewRound(
    message: Message,
    game: LiarsDiceGameState,
    userName: string,
    move: string,
    displayText: string,
  ): Promise<FeatureMessage> {
    // a new round, so we need fresh dice
    game.reRoll();

    // Reset the bid
    game.resetBid();

    // save game
    await this.saveGame(game);

    // send peeks to all players for their initial dice
    await this._sendPeekToAllPlayers(game, message.user.displayName, move);

    {
      // Check the name is valid
      const nextPlayer: string | undefined =
        await this._getNextPlayerName(game);
      if (nextPlayer === undefined) {
        return this._abortGame(message, game);
      }
    }

    // get the player's ID
    const nextPlayerId: string = this._getNextPlayerId(game);

    // Tell the players about the new round
    return this.createMessage(
      `${displayText}\n\n` +
        `A new round has started, <@${nextPlayerId}>` +
        ' is to start.',
      `${userName} called the current bid in Liar's Dice, and this ` +
        'is the resolution.',
    );
  }

  private _removeDiceFromPlayer(game: LiarsDiceGameState, index: number): void {
    const player: LiarsDicePlayer = game.getPlayerByIndex(index);

    if (game.removeDiceFromPlayer(index)) {
      this.sendMessage(
        this.createMessageToUser(
          `You have been yeeted from the game`,
          `Player has been removed from the game`,
          player.playerId,
        ),
      ).catch((e: Error) => {
        throw e;
      });
    }
  }

  // **************************************************************************//
  // **                         STATUS                                       **//
  // **************************************************************************//

  private async _getStatus(
    message: Message,
    game: LiarsDiceGameState,
  ): Promise<FeatureMessage> {
    const reason =
      "I was asked for the status of the current Liar's dice game.";

    const playerDetails = await this._getPlayerNames(game);

    if (playerDetails.fail) {
      return this._abortGameDueToMissingUser(playerDetails.playerNames[0]);
    }

    const playerNames: string[] = playerDetails.playerNames;

    let statusText = '';

    if (game.isActive()) {
      const curAmount: number = game.currentBidAmount();
      const curFace: number = game.currentBidFace();

      statusText = `Current bid: [${curAmount}d${curFace}]\n`;

      const players: LiarsDicePlayer[] = game.getPlayers();

      // players names and players has to be the same length as the game
      // aborts if we fail to get any names
      let diceTotal = 0;
      for (const p of players) {
        diceTotal += p.dice.length;
      }

      const playersDice: string[] = [];

      for (let _i = 0; _i !== players.length; _i++) {
        playersDice.push(`${playerNames[_i]}/${players[_i].dice.length}`);
      }

      statusText += `Players: [${playersDice.join(', ')}]\n`;
      statusText += `Total dice: [${diceTotal}]`;

      // also send the player the dice information
      const player: LiarsDicePlayer | undefined = game.getPlayer(
        message.user.id,
      );

      if (player) {
        await this.sendMessage(
          this._createPeekMsg(player, message.user.displayName, 'status'),
        );
      }
    } else {
      statusText = `Players: [${playerNames.join(', ')}]`;
    }

    return this.createMessage(statusText, reason);
  }

  // **************************************************************************//
  // **                           JOIN                                       **//
  // **************************************************************************//

  private async _joinGame(
    message: Message,
    game: LiarsDiceGameState,
  ): Promise<FeatureMessage> {
    // logic applied for joining
    //
    // 1) you cannot join if a game is active
    // 2) you cannot join if you have already joined
    // 3) you cannot join if the game is full
    // 4) otherwise - you can join!

    if (game.isActive()) {
      // If the game is active - reject
      return this.createMessage(
        'You cannot join while a game is active.',
        `${message.user.displayName} asked to join the Liar's ` +
          'dice game.' +
          `${message.user.displayName} was rejected as a game` +
          ' was already running.',
      );
    } else if (game.isInGame(message.user.id)) {
      // If the user is already in the game - reject
      return this.createMessage(
        "You are already in the Liar's dice game.",
        `${message.user.displayName} asked to join the Liar's` +
          ' dice game.' +
          ' They were rejected as they are already in list of users.',
      );
    } else if (game.isFull()) {
      // If the game is full - reject
      return this.createMessage(
        `Sorry ${message.user.displayName}, but the game is full.`,
        `${message.user.displayName} asked to join the Liar's` +
          ' dice game.' +
          `${message.user.displayName} was rejected as the game is full.`,
      );
    } else {
      // OK! Now we can add the player
      game.addUser(message.user.id);

      // save the game
      await this.saveGame(game);

      return this.createMessage(
        `${message.user.displayName} has joined the Liar's dice game.`,
        `I added ${message.user.displayName} to the players for the.` +
          "next Liar's dice game.",
      );
    }
  }

  // **************************************************************************//
  // **                           QUIT                                       **//
  // **************************************************************************//

  private async _quitGame(
    message: Message,
    game: LiarsDiceGameState,
  ): Promise<FeatureMessage> {
    // logic applied for quitting
    //
    // 1) you cannot quit the game if you are not in the game
    // 2) otherwise - you can quit!

    if (!game.isInGame(message.user.id)) {
      // If the user is already in the game - reject
      return this.createMessage(
        `You can't quit ${message.user.displayName}, because you ` +
          'are not in the game.',
        `${message.user.displayName} tried to quit but was not in the game.`,
      );
    } else {
      // remove the user
      game.removeUser(message.user.id);

      // quitting text
      const quitText = `${message.user.displayName} has quit the Liar's dice game.`;

      // if the game is active and the number of players has dropped
      // below 2 then that person de facto wins
      if (game.isActive()) {
        // if the game is over, deal with that first
        if (game.isWon()) {
          return this._andTheWinnerIs(game, quitText);
        } else {
          // else, start a new round without the player
          return this._startNewRound(
            message,
            game,
            message.user.displayName,
            'quit',
            quitText,
          );
        }
      }

      if (game.isEmpty()) {
        // if the game is now empty - delete the game
        await this.deleteGame();
      } else {
        // save the game
        await this.saveGame(game);
      }

      // and respond
      return this.createMessage(
        quitText,
        `I removed ${message.user.displayName} from the.` +
          "Liar's dice game as they asked to quit.",
      );
    }
  }

  // **************************************************************************//
  // **                           START                                      **//
  // **************************************************************************//

  private async _startGame(
    message: Message,
    game: LiarsDiceGameState,
  ): Promise<FeatureMessage> {
    // logic applied for starting the game
    //
    // 1) you cannot start the game if you are not in the game
    // 2) you cannot start the game if a game is active
    // 3) you cannot start the game if there aren't enough players
    // 4) a) If the First user does not exist in the user list abort game
    //    b) otherwise - you can start!

    if (!game.isInGame(message.user.id)) {
      // cannot start a game if you are not in the game
      return this.createMessage(
        "You are not in the current Liar's dice game, so you cannot start it.",
        `${message.user.displayName} tried to start a Liar's ` +
          'dice game but is not in the current game.',
      );
    } else if (game.isActive()) {
      // only 1 game instance is supported
      return this.createMessage(
        'A game is already active, cannot start a second.',
        `${message.user.displayName} tried to start a second Liar's ` +
          'dice game but only 1 instance is supported.',
      );
    } else if (!game.enoughPlayersToStart()) {
      // If the user is already in the game - reject
      return this.createMessage(
        "Liar's dice can only start when 2 or more players have joined.",
        `${message.user.displayName} tried to start a second Liar's ` +
          `dice game but there is currently ${game.getNumberOfPlayers()} ` +
          'players and at least 2 are needed to start.',
      );
    } else {
      {
        // abort if we fail to get the player
        const startPlayer: string | undefined =
          await this._getNextPlayerName(game);
        if (!startPlayer) {
          return this._abortGameDueToMissingUser(game.getPlayers()[0].playerId);
        }
      }

      // get the next player's ID
      const startPlayerId: string = this._getNextPlayerId(game);

      // start the game
      game.startGame();

      // save the game
      await this.saveGame(game);

      // send peeks to all players for their initial dice
      await this._sendPeekToAllPlayers(game, message.user.displayName, 'start');

      // and respond
      return this.createMessage(
        `Welcome to a new Liar's dice game!

<@${startPlayerId}> is to kick us off with a bid.`,
        "I started the Liar's dice game.",
      );
    }
  }

  // **************************************************************************//
  // **                           ABORT                                      **//
  // **************************************************************************//

  private async _abortGame(
    message: Message,
    game: LiarsDiceGameState,
  ): Promise<FeatureMessage> {
    // logic applied for aborting the game
    //
    // 1) you cannot abort the game if you are not in the game
    // 2) you can abort!

    if (!game.isActive()) {
      // reject if a game is not running
      return this.createMessage(
        "Cannot abort a Liar's dice game if it is not running.",
        `${message.user.displayName} tried to abort the current Liar's ` +
          'dice game but there is no game running.',
      );
    } else {
      // delete the game
      await this.deleteGame();

      // and respond
      return this.createMessage(
        `${message.user.displayName} has aborted the game.`,
        "I started the Liar's dice game.",
      );
    }
  }

  // **************************************************************************//
  // **                           ABANDON                                    **//
  // **************************************************************************//

  private async _abandonGame(
    message: Message,
    game: LiarsDiceGameState,
  ): Promise<FeatureMessage> {
    // logic applied for aborting the game
    //
    // 1) you cannot abandon the game if it has started
    // 2) you can abort!

    if (game.isActive()) {
      // reject if a game is running
      return this.createMessage(
        "Cannot abandon a Liar's dice game if it is running.",
        `${message.user.displayName} tried to abandon the current Liar's ` +
          'dice game but the game is running.',
      );
    } else {
      // delete the game
      await this.deleteGame();

      // and respond
      return this.createMessage(
        `${message.user.displayName} has abandoned the game.`,
        "The current game of Liar's dice has been abandoned.",
      );
    }
  }

  // **************************************************************************//
  // **                            BID                                       **//
  // **************************************************************************//

  private async _bid(
    message: Message,
    game: LiarsDiceGameState,
    cleanMsg: string,
  ): Promise<FeatureMessage> {
    // logic applied for bidding
    //
    // 1) you cannot bid if a game is not active
    // 2) you cannot bid if you are not in the game
    // 2) you cannot bid if it is not your turn
    // 4) a) Rejected if bid does not match the game rules
    //    b) otherwise - you can bid!

    if (!game.isActive()) {
      // cannot bid if there is no game running
      return this.createMessage(
        "Cannot bid when Liar's dice is not running.",
        `${message.user.displayName} tried to bid but there was` +
          "no game of Liar's dice running.",
      );
    } else if (!game.isInGame(message.user.id)) {
      // cannot bid if you are not in the game
      return this.createMessage(
        "You cannot bid because you are not in the current game of Liar's dice",
        `${message.user.displayName}'s bid was rejected as ` +
          "they are not in the current Liar's dice game.",
      );
    } else if (game.isNotYourTurn(message.user.id)) {
      // cannot bid if it is not your turn
      return this.createMessage(
        'Cannot bid when it is not your turn.',
        `${message.user.displayName}'s bid was rejected because ` +
          'it is not their turn.',
      );
    } else {
      // split the strings, the format has been enforced
      // at the match to be bid ndm or ndm where
      // n can be [0-9][0-9]? and m can be [0-9]

      const dString: string[] = cleanMsg.replace(/bid/, '').trim().split('d');

      const amount = Number(dString[0]);
      const face = Number(dString[1]);

      const curAmount: number = game.currentBidAmount();
      const curFace: number = game.currentBidFace();

      if (amount < curAmount || amount === 0) {
        // failed amount check
        return this.createMessage(
          'Bid rejected: amount value is too low.',
          `${message.user.displayName}'s bid was rejected as ` +
            'bid was for too low an amount of dice.',
        );
      } else if (amount === curAmount && face <= curFace) {
        // failed face check
        return this.createMessage(
          'Bid rejected: face value is too low.',
          `${message.user.displayName}'s bid was rejected as ` +
            'bid was for too low a die face value.',
        );
      } else if (face === 0 || face > 6) {
        // failed face check
        return this.createMessage(
          'Bid rejected: face value is not 1-6.',
          `${message.user.displayName}'s bid was rejected as ` +
            'bid was not for a face value of 1-6.',
        );
      } else {
        // a successful bid!

        // get the bidder's name
        const bidPlayer: string | undefined =
          await this._getNextPlayerName(game);

        // abort if we fail to get the player
        if (!bidPlayer) {
          return this._abortGameDueToMissingUser(game.getPlayers()[0].playerId);
        }

        // story the new bid information
        game.storeBid(amount, face);

        // rotate to the next player
        game.nextTurn();

        // save game
        await this.saveGame(game);

        // get the bidder's name
        const nextPlayer: string | undefined =
          await this._getNextPlayerName(game);

        // abort if we fail to get the player
        if (!nextPlayer) {
          return this._abortGameDueToMissingUser(game.getPlayers()[0].playerId);
        }

        // get the bidder's ID
        const nextPlayerId: string = this._getNextPlayerId(game);

        // respond to the successful bid!
        return this.createMessage(
          `${bidPlayer} has made a bid of ${game.currentBidAmount()} ` +
            `${amount > 1 ? 'dice' : 'die'} of face ` +
            `${game.currentBidFace()}.\n\n` +
            `<@${nextPlayerId}>, you're up next.`,
          `${bidPlayer} made a sucessful bid so i updated the ` +
            `game state, it is now ${nextPlayer}'s turn.`,
        );
      }
    }
  }

  // **************************************************************************//
  // **                         CALL / EXACT                                 **//
  // **************************************************************************//

  private async _callOrExact(
    message: Message,
    game: LiarsDiceGameState,
    move: LIARS_DICE_MOVE,
  ): Promise<FeatureMessage> {
    // logic applied for call
    //
    // 1) you cannot call/exact if a game is not active
    // 2) you cannot call/exact if you are not in the game
    // 3) you cannot call/exact if it is not your turn
    // 4) you cannot call/exact if it is the first turn
    // 5) call is resolved - check for a game winning scenario

    if (!game.isActive()) {
      // cannot call if there is no game running
      return this.createMessage(
        `Cannot ${move} when Liar's dice is not running.`,
        `${message.user.displayName} tried to ${move} but there was` +
          "no game of Liar's dice running.",
      );
    } else if (!game.isInGame(message.user.id)) {
      // cannot call if you are not in the game
      return this.createMessage(
        `You cannot ${move} because you are not in the current` +
          " game of Liar's dice.",
        `${message.user.displayName}'s ${move} was rejected as ` +
          "they are not in the current Liar's dice game.",
      );
    } else if (game.isNotYourTurn(message.user.id)) {
      // cannot call if it is not your turn
      return this.createMessage(
        `Cannot ${move} when it is not your turn.`,
        `${message.user.displayName}'s ${move} was rejected as ` +
          "it is not their turn in the current Liar's dice game.",
      );
    } else if (game.isFirstTurn()) {
      // cannot call if it is the first turn
      return this.createMessage(
        `Cannot ${move} when it is the first turn.`,
        `${message.user.displayName}'s ${move} was rejected as ` +
          "it the first turn in the current Liar's dice game.",
      );
    } else {
      // a successful call!

      // player's details for printing
      const playerDetails = await this._getPlayerNamesAndDice(game);

      if (playerDetails.length === 0 || playerDetails[0].fail) {
        return this._abortGameDueToMissingUser(playerDetails[0].name);
      }

      // get the bidder's name
      const bidPlayer: string | undefined = await this._getPlayerName(
        game.getPlayers()[game.getPlayers().length - 1],
      );

      // abort if we fail to get the player
      if (!bidPlayer) {
        return this._abortGameDueToMissingUser(
          game.getPlayers()[game.getPlayers().length - 1].playerId,
        );
      }

      // get the caller's name
      const callPlayer: string | undefined =
        await this._getNextPlayerName(game);

      // abort if we fail to get the player
      if (!callPlayer) {
        return this._abortGameDueToMissingUser(game.getPlayers()[0].playerId);
      }

      // get the bid amount/face and the count of the current face
      const curAmount: number = game.currentBidAmount();
      const curFace: number = game.currentBidFace();
      const countOfFace = game.amountOfDiceWithFace();

      let winner = '';
      let winnerString = '';

      const playerDiceStr: string = this._printPlayersAndDiceForChecks(
        playerDetails,
        curFace,
      );

      if (move === LIARS_DICE_MOVE.CALL) {
        // call logic

        if (curAmount <= countOfFace) {
          // the bidder was correct!
          //
          // caller loses a dice!
          this._removeDiceFromPlayer(game, LIARS_DICE_CALLER_NUMBER);
          winner = bidPlayer;
          winnerString =
            `${winner} was correct with the bid of ${curAmount}d${curFace},` +
            ` there was in fact ${countOfFace}d${curFace}.\n` +
            playerDiceStr +
            `${callPlayer} loses a die`;
        } else {
          // the caller was correct
          //
          // bidder loses a dice!
          this._removeDiceFromPlayer(game, game.getPlayers().length - 1);
          winner = callPlayer;
          winnerString =
            `${winner} was correct to doubt ${bidPlayer}'s bid of ` +
            `${curAmount}d${curFace}, there was in fact ` +
            `${countOfFace}d${curFace}.\n` +
            playerDiceStr +
            `${bidPlayer} loses a die`;
        }
      } else {
        // exact logic

        if (curAmount === countOfFace) {
          // exacter was correct

          // all other players lose a dice
          // go backwards as removing dice might also eliminate players
          // so going backwards is safer as removals do not affect the
          // next player to lose dice
          for (let _i: number = game.getPlayers().length - 1; _i >= 0; _i--) {
            if (game.getPlayers()[_i].playerId !== message.user.id) {
              this._removeDiceFromPlayer(game, _i);
            }
          }

          winner = callPlayer;
          winnerString =
            '' +
            `Well done to ${winner} for correctly guessing there ` +
            `was exactly ${curAmount}d${curFace}.\n` +
            playerDiceStr +
            `Everyone except ${winner} now loses a die`;
        } else {
          // exacter was wrong
          // they lose a dice
          // this is the same as the caller losing a die
          // so use that mechanism
          this._removeDiceFromPlayer(game, LIARS_DICE_CALLER_NUMBER);

          winnerString =
            `${callPlayer} was incorrect on the exact bid, there was in fact ` +
            `${countOfFace}d${curFace}.\n` +
            playerDiceStr +
            `${callPlayer} loses a die.`;
        }
      }

      // if the game is over, deal with that first
      if (game.isWon()) {
        return this._andTheWinnerIs(game, winnerString);
      } else {
        return this._startNewRound(
          message,
          game,
          callPlayer,
          move,
          winnerString,
        );
      }
    }
  }

  // **************************************************************************//
  // **                           PEEK                                       **//
  // **************************************************************************//

  private async _peek(
    message: Message,
    game: LiarsDiceGameState,
  ): Promise<FeatureMessage> {
    // logic applied for peek
    //
    // 1) you cannot peek if a game is not active
    // 2) you cannot call if you are not in the game
    // 3) else you can peek!

    if (!game.isActive()) {
      // cannot call if there is no game running
      return this.createMessage(
        "Cannot peek when Liar's dice is not running.",
        `${message.user.displayName} tried to peek but there was` +
          "no game of Liar's dice running.",
      );
    } else if (!game.isInGame(message.user.id)) {
      // cannot call if you are not in the game
      return this.createMessage(
        'You cannot peek because you are not in the current' +
          " game of Liar's dice.",
        `${message.user.displayName}'s peek was rejected as ` +
          "they are not in the current Liar's dice game.",
      );
    } else {
      // successful peek!

      const player: LiarsDicePlayer | undefined = game.getPlayer(
        message.user.id,
      );

      // abort if we fail to get the player
      if (!player) {
        return this._abortGameDueToMissingUser(game.getPlayers()[0].playerId);
      }

      // send out peek information
      return this._createPeekMsg(player, message.user.displayName, 'peek');
    }
  }
}
