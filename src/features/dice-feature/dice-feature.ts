import { noIndent } from '../../util/general/string-extensions';
import { Feature } from '../../gradbot/feature/feature';
import { DiceMessageHandler } from './dice-message-handler';

export class DiceFeature extends Feature {
  public readonly name = 'Dice';
  public readonly iCanInfo = 'roll dice';
  public readonly instructions = noIndent(
    `I can roll 1-100 dice with 2-100 sides, use NdM to roll N dice of M sides, e.g.:
     • *g 1d6* – I'll roll one six-sided die
     • *g 12d9* – I'll roll twelve nine-sided dice`,
  );
  protected readonly messageHandlerType = DiceMessageHandler;
}
