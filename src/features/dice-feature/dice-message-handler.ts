import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { getRandomInt } from '../../util/general/random-extensions';

export class DiceMessageHandler extends FeatureMessageHandler {
  private readonly _isDiceRegex: RegExp = new RegExp(
    /^[1-9][0-9]*d[1-9][0-9]*$/,
  );

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    // check this is for gradbot
    if (this.isMessageForGradbot()) {
      // check it is of the format \d+d\d+ (the one we are )
      const messageText: string = this.getSanitisedText();

      if (this._isDiceMessage(messageText)) {
        const split: string[] = messageText.split('d');
        const numOfDie: number = +split[0];
        const numOfsides: number = +split[1];

        const reason: string =
          `${this.message.user.displayName} asked for ` +
          `${numOfDie} rolls of a ${numOfsides} sided die!`;

        // catch the following cases
        // * 69 easter egg -> noice
        // * too many sides/die -> protect the server!

        if (numOfDie === 6 && numOfsides === 9) {
          return Promise.resolve(
            this.createMessage('Noice', reason + ' ... Noice'),
          );
        } else if (numOfDie > 100) {
          return Promise.resolve(
            this.createMessage(
              'Die feature only supports up to 100 dice',
              reason,
            ),
          );
        } else if (numOfsides > 100) {
          return Promise.resolve(
            this.createMessage(
              'Die feature only supports up to 100 sides',
              reason,
            ),
          );
        } else if (numOfsides === 1) {
          return Promise.resolve(
            this.createMessage(
              'Die feature does not support 1 sided die',
              reason,
            ),
          );
        }

        let sum: number = getRandomInt(numOfsides) + 1;
        let stringOutput = `Gradbot kisses the ${
          numOfDie > 1 ? 'dice' : 'die'
        } and rolls,\n${sum}`;

        for (let i = 1; i < numOfDie; i++) {
          const curDieThrow: number = getRandomInt(numOfsides) + 1;

          sum += curDieThrow;
          stringOutput += `, ${curDieThrow}`;
        }

        return Promise.resolve(
          this.createMessage(
            stringOutput + (numOfDie > 1 ? `: total: ${sum}` : ''),
            reason,
          ),
        );
      }
    }

    return Promise.resolve(null);
  }

  private _isDiceMessage(text: string) {
    return this._isDiceRegex.test(text);
  }
}
