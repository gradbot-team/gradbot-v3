import 'reflect-metadata';
import { DiceFeature } from './dice-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import * as randomExtensions from '../../util/general/random-extensions';

const util = FeatureTestUtil.createForFeatureType(DiceFeature);

describe('DiceFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('does not respond to dice message with the wrong pre format', async () =>
    await util.expectReplyToBeNull('g 1gd5'));

  test('does not respond to dice message with the wrong post format', async () =>
    await util.expectReplyToBeNull('g 1dg5'));

  test('does not respond to dice message with zero dice', async () =>
    await util.expectReplyToBeNull('g 0d5'));

  test('does not respond to dice message with zero sides', async () =>
    await util.expectReplyToBeNull('g 1d0'));

  test('responds to dice message with one side', async () =>
    await util.expectReplyToHaveText(
      'g 1d1',
      'Die feature does not support 1 sided die',
    ));

  test('responds to dice message with too many dice', async () =>
    await util.expectReplyToHaveText(
      'g 101d5',
      'Die feature only supports up to 100 dice',
    ));

  test('responds to dice message with too many sides', async () =>
    await util.expectReplyToHaveText(
      'g 1d101',
      'Die feature only supports up to 100 sides',
    ));

  test('responds to "g 6d9" with easter egg', async () =>
    await util.expectReplyToHaveText('g 6d9', 'Noice'));

  test('responds to dice message with the right format', async () =>
    await util.expectReplyToHaveText(
      'g 1d5',
      /^Gradbot kisses the die and rolls[^]*[1-5]$/,
    ));

  test('does not throw a zero', async () =>
    await util.expectReplyToNotHaveTextWithValue('g 100d2', ' 0,'));

  test('does not throw a number too large!', async () =>
    await util.expectReplyToNotHaveTextWithValue('g 100d2', ' 3,'));

  test('Correct result when getRandomInt is mocked', async () => {
    const spy = jest.spyOn(randomExtensions, 'getRandomInt');
    spy.mockReturnValue(0);

    await util.expectReplyToHaveText(
      'g 4d80',
      'Gradbot kisses the dice and rolls,\n' + '1, 1, 1, 1: total: 4',
    );

    expect(spy).toHaveBeenCalledTimes(4);
    expect(spy).toHaveBeenCalledWith(80);
    spy.mockRestore();
  });

  test('responds with a lot of dice correctly', async () =>
    await util.expectReplyToHaveText(
      'g 80d80',
      /^Gradbot kisses the dice and rolls[^]*[0-9]+(, [0-9]+)*: total: [0-9]+$/,
    ));
});
