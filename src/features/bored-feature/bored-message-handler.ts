import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class BoredMessageHandler extends FeatureMessageHandler {
  private readonly BORED_API_URL = 'https://www.boredapi.com/api/activity';
  private readonly BORED_REGEX = /^(what\s+should\s+i\s+do|i'm\s+bored)\W*$/;

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.matchSanitisedLowercaseTextIfForGradbot(this.BORED_REGEX)) {
      const data = await this.fetchAPI<BoredAPIData>(this.BORED_API_URL);

      if (data && data.activity) {
        return this.createMessage(
          data.activity,
          `${this.message.user.displayName} said they were bored`,
        );
      }

      return this.createMessage(
        "I can't think of anything to do right now",
        `${this.message.user.displayName} said they were bored, but API is down`,
      );
    }
    return null;
  }
}

interface BoredAPIData {
  activity?: string;
}
