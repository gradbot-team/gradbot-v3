import 'reflect-metadata';
import { BoredFeature } from './bored-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(BoredFeature);

const setValidAPIResponse = () =>
  (util.api.urlReturnValues['https://www.boredapi.com/api/activity'] = {
    activity: 'test',
  });

describe('BoredFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds to "I\'m bored"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText("Gradbot, I'm bored", 'test');
  });

  test('responds to "what should I do"', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText('g what should I do?', 'test');
  });

  test('responds to "what should I do" with some punctuation', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveText(
      'what  should   I do???!, gradbot',
      'test',
    );
  });

  test('deals with invalid API data', async () => {
    util.api.urlReturnValues['https://www.boredapi.com/api/activity'] = {
      wrong_property: 'test',
    };
    await util.expectReplyToHaveText(
      "Gradbot, I'm bored",
      "I can't think of anything to do right now",
    );
  });

  test('deals with missing API data', async () => {
    util.api.urlReturnValues['https://www.boredapi.com/api/activity'] = null;
    await util.expectReplyToHaveText(
      "Gradbot, I'm bored",
      "I can't think of anything to do right now",
    );
  });
});
