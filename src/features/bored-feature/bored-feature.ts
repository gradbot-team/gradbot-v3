import { Feature } from '../../gradbot/feature/feature';
import { BoredMessageHandler } from './bored-message-handler';

export class BoredFeature extends Feature {
  public readonly name = 'Bored';
  public readonly iCanInfo = 'suggest things to do';
  public readonly instructions = "• *g I'm bored*\n• *g what should I do?*";
  protected readonly messageHandlerType = BoredMessageHandler;
}
