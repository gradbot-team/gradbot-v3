import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { DictionaryFeatureUtil } from './dictionary-feature-util';
import { DictionaryApiResult } from './dictionary-api-result';

export class DictionaryMessageHandler extends FeatureMessageHandler {
  protected readonly triggerProbability = 0.1;

  // Base URL - "en_GB" must be capitalised thus.
  private readonly baseUrl =
    'https://api.dictionaryapi.dev/api/v2/entries/en_GB/';

  private readonly triggerPattern = `${DictionaryFeatureUtil.prefixPattern}[\\w\\s-']+${DictionaryFeatureUtil.suffixPattern}?$`;

  public isMessageTrigger(): boolean {
    return (
      this.getSanitisedText().match(new RegExp(this.triggerPattern, 'i')) !==
      null
    );
  }

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const lookupText = DictionaryFeatureUtil.getRequestWord(
      this.getSanitisedText(),
    );
    if (lookupText.split(new RegExp('[-\\s]+')).length > 3) {
      // If the lookup phrase is more than 3 words we probably don't want a
      // dictionary definition; let the wolfram alpha feature handle it instead.
      return null;
    }
    const apiResults = await this.fetchAPI<DictionaryApiResult[]>(
      this.baseUrl + lookupText,
    ).catch(() => null);

    let result = DictionaryFeatureUtil.parseDictionaryApiResult(apiResults);
    if (!result) {
      result = 'Definition not found';
    }

    return this.createMessage(
      result,
      `${this.message.user.displayName} asked me to look up "${lookupText}" in the dictionary.`,
    );
  }
}
