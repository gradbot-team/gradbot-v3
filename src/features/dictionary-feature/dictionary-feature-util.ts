import { DictionaryApiResult } from './dictionary-api-result';

export class DictionaryFeatureUtil {
  public static readonly prefixPattern =
    "^(define|((what's|what is)( an?)?))\\s";
  public static readonly suffixPattern = '[.?]';

  public static getRequestWord(messageText: string): string {
    return messageText
      .replace(new RegExp(this.prefixPattern, 'i'), '')
      .replace(new RegExp(this.suffixPattern + '$', 'i'), '');
  }

  public static parseDictionaryApiResult(
    apiResults: DictionaryApiResult[] | null,
  ): string | undefined {
    let result;
    if (apiResults !== null && apiResults.length > 0) {
      const resultLines: string[] = [];
      for (let i = 0; i < apiResults.length; i++) {
        const apiResult = apiResults[i];
        const phonetics =
          apiResult.phonetics.length > 0
            ? ` [${apiResult.phonetics[0].text}]`
            : '';
        resultLines.push(`${i + 1}. ${apiResult.word}${phonetics}`);
        for (const meaning of apiResult.meanings) {
          resultLines.push(`  • ${meaning.partOfSpeech}:`);
          for (let j = 0; j < meaning.definitions.length; j++) {
            resultLines.push(
              `    ${j + 1}. ${meaning.definitions[j].definition}`,
            );
          }
        }
      }
      result = resultLines.join('\r');
    }

    return result;
  }
}
