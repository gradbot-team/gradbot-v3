import 'reflect-metadata';
import { DictionaryApiResult } from './dictionary-api-result';
import { DictionaryFeatureUtil } from './dictionary-feature-util';

describe('DictionaryFeatureUtil', () => {
  const testData: { msgTxt: string; lookup: string }[] = [
    { msgTxt: 'define potato', lookup: 'potato' },
    { msgTxt: 'what is potato', lookup: 'potato' },
    { msgTxt: "what's potato", lookup: 'potato' },
    { msgTxt: 'define potato.', lookup: 'potato' },
    { msgTxt: 'define potato?', lookup: 'potato' },
    { msgTxt: 'wHaT iS pOtAtO', lookup: 'pOtAtO' },
    { msgTxt: 'define half-baked', lookup: 'half-baked' },
    { msgTxt: "define potato's", lookup: "potato's" },
    { msgTxt: 'define baked potato', lookup: 'baked potato' },
    { msgTxt: 'what is a potato?', lookup: 'potato' },
    { msgTxt: "what's an apple?", lookup: 'apple' },
  ];

  for (const item of testData) {
    test(`extracts the word or phrase to be looked up from message text '${item.msgTxt}'`, () => {
      // Act
      const output = DictionaryFeatureUtil.getRequestWord(item.msgTxt);

      // Assert
      expect(output).toEqual(item.lookup);
    });
  }

  test('formats output correctly', () => {
    // Arrange
    const apiResults: DictionaryApiResult[] = [
      {
        word: 'test',
        phonetics: [
          {
            text: '/tɛst/',
          },
        ],
        meanings: [
          {
            partOfSpeech: 'verb',
            definitions: [
              {
                definition:
                  'Take measures to check the quality, performance, or reliability of (something), especially before putting it into widespread use or practice.',
              },
            ],
          },
          {
            partOfSpeech: 'noun',
            definitions: [
              {
                definition:
                  'A procedure intended to establish the quality, performance, or reliability of something, especially before it is taken into widespread use.',
              },
              {
                definition: 'short for Test match',
              },
              {
                definition:
                  'A movable hearth in a reverberating furnace, used for separating gold or silver from lead.',
              },
            ],
          },
        ],
      },
      {
        word: 'test',
        phonetics: [
          {
            text: '/tɛst/',
          },
        ],
        meanings: [
          {
            partOfSpeech: 'noun',
            definitions: [
              {
                definition:
                  'The shell or integument of some invertebrates and protozoans, especially the chalky shell of a foraminiferan or the tough outer layer of a tunicate.',
              },
            ],
          },
        ],
      },
    ];

    // Act
    const output = DictionaryFeatureUtil.parseDictionaryApiResult(apiResults);

    // Assert
    expect(output).toEqual(
      '1. test [/tɛst/]\r' +
        '  • verb:\r' +
        '    1. Take measures to check the quality, performance, or reliability of (something), especially before putting it into widespread use or practice.\r' +
        '  • noun:\r' +
        '    1. A procedure intended to establish the quality, performance, or reliability of something, especially before it is taken into widespread use.\r' +
        '    2. short for Test match\r' +
        '    3. A movable hearth in a reverberating furnace, used for separating gold or silver from lead.\r' +
        '2. test [/tɛst/]\r' +
        '  • noun:\r' +
        '    1. The shell or integument of some invertebrates and protozoans, especially the chalky shell of a foraminiferan or the tough outer layer of a tunicate.',
    );
  });
});
