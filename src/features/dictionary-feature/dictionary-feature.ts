import { Feature } from '../../gradbot/feature/feature';
import { DictionaryMessageHandler } from './dictionary-message-handler';

export class DictionaryFeature extends Feature {
  public readonly name = 'Dictionary';
  public readonly iCanInfo = 'look up words and phrases in the dictionary';
  public readonly instructions =
    'Syntax: "g define <word>", "g what is <word>?", "g what\'s <word>".' +
    '\nThe word or phrase can contain letters (including with diacritics), spaces, hyphens, and apostrophes.' +
    ' Only English words and phrases are supported.' +
    ' Pronunciations are as in standard British English, and are denoted using the International Phonetic Alphabet.';
  protected readonly messageHandlerType = DictionaryMessageHandler;
}
