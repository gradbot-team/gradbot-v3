import 'reflect-metadata';
import { DictionaryFeature } from './dictionary-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const featureUtil = FeatureTestUtil.createForFeatureType(DictionaryFeature);

describe('DictionaryFeature', () => {
  test('does not respond to test message', async () =>
    await featureUtil.expectReplyToBeNull('g test'));

  const validTriggers: { msgTxt: string; lookup: string }[] = [
    { msgTxt: 'g define potato', lookup: 'potato' },
    { msgTxt: 'g what is potato', lookup: 'potato' },
    { msgTxt: "g what's potato", lookup: 'potato' },
    { msgTxt: 'g define potato.', lookup: 'potato' },
    { msgTxt: 'g define potato?', lookup: 'potato' },
    { msgTxt: 'g wHaT iS pOtAtO', lookup: 'pOtAtO' },
    { msgTxt: 'g define half-baked', lookup: 'half-baked' },
    { msgTxt: "g define potato's", lookup: "potato's" },
    { msgTxt: 'g define baked potato', lookup: 'baked potato' },
    { msgTxt: 'g what is a potato?', lookup: 'potato' },
    { msgTxt: "g what's an apple?", lookup: 'apple' },
  ];

  for (const item of validTriggers) {
    test(`responds to trigger "${item.msgTxt}"`, async () => {
      featureUtil.api.urlReturnValues[
        `https://api.dictionaryapi.dev/api/v2/entries/en_GB/${item.lookup}`
      ] = [];
      await featureUtil.expectReplyToBeTruthy(item.msgTxt);
    });
  }

  const invalidTriggers = [
    'g potato',
    'g potato?',
    "g what's taters, precious?",
    'g define.potato',
    'g define potato!',
    'g define potato..',
    'g define',
    'g what is a phrase with four words?',
    'g what is this four word phrase',
    'g what is this-four-word-phrase?',
  ];

  for (const trigger of invalidTriggers) {
    test(`does not respond to invalid trigger "${trigger}"`, async () =>
      await featureUtil.expectReplyToBeNull(trigger));
  }
});
