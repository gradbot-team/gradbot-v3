export interface DictionaryApiResult {
  word: string;
  phonetics: { text: string }[];
  meanings: {
    partOfSpeech: string;
    definitions: { definition: string }[];
  }[];
}
