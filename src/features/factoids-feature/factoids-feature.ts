import { Feature } from '../../gradbot/feature/feature';
import { FactoidsMessageHandler } from './factoids-message-handler';
import { FactoidsFeatureUtil as util } from './factoids-feature-util';
import { noIndent } from '../../util/general/string-extensions';

export class FactoidsFeature extends Feature {
  public readonly name = 'Factoids';
  public readonly iCanInfo = 'remember and recall factoids';
  public readonly instructions = noIndent(
    `Get Gradbot to remember a factoid by starting your message with "gr" or "Gradbot, remember".
    Follow this with a message that follows the pattern <trigger> <verb> <batch> <response>,` +
      `where <verb> can be "is", "are", "<reply>", or any custom verb surround by angle brackets <>.
    The verb must be a single word, but the trigger and response phrases can be as long as you like.
    Gradbot will sometimes respond with a factoid it knows when it sees somebody write a message that looks like one of the saved triggers.

    Nested look up can be achieved using {{value}} in the reply.
    When a look up fails it will replace the value with ??value??.
    When a look up is detected to be circular, it will replace it with %%value%%.
    It has a limit of 50 look ups.

    <batch> is an optional argument to batch load items that are comma separated.
    The <batch> tag is not included in the factoid response.

    To see what is stored for a trigger, use "gr" or "Gradbot, remember" ` +
      `Use gr <trigger> [show] n to see page n of the stored records.
    The 0th page is the default, it shows ${util.showAllMax} ` +
      `records per page.

    Use gr <trigger> [delete] n to delete the nth factoid. The ` +
      'number matches that of the show command.',
  );

  protected readonly messageHandlerType = FactoidsMessageHandler;
}
