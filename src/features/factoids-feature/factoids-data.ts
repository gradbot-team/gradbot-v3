import { FeatureDbObject } from '../../gradbot/database/db-objects/feature-db-object';
import { DbIsolationLevel } from '../../gradbot/database/db-objects/db-isolation-level';
import { DbText } from '../../gradbot/database/types/db-text';
import { DBO } from '../../gradbot/database/db-objects/registry/dbo-decorator';

@DBO
export class FactoidsData extends FeatureDbObject<FactoidsData> {
  isolationLevel = DbIsolationLevel.SERVER;

  trigger = new DbText();
  verb = new DbText();
  response = new DbText();
}
