import { IParsedMessage } from './parsed-message';

export class FactoidsFeatureUtil {
  // Matchers
  public static readonly grMatcher = /^gr,?\s+/i;
  public static readonly rememberMatcher = /^remember\s+/i;
  public static readonly dereferenceMatcher = /\{\{([^\{\}]+)\}\}/;
  public static readonly replyMatcher = /<r(eply)?>/i;
  public static readonly replyShortMatcher = /<r>/i;
  public static readonly isMatcher = /\s+is\s+/i;
  public static readonly areMatcher = /\s+are\s+/i;
  public static readonly customVerbMatcher = /<\w+>/i;
  public static readonly batchMatcher = /^<b(atch)?>/;
  public static readonly showMatcher = /\[s(how)?\](\s+[0-9]{1,2})?\s*$/i;
  public static readonly deleteMatcher =
    /\[d(elete)?\]\s+[1-9][0-9]{0,2}(\s*,\s*[1-9][0-9]{0,2})*\s*$/i;

  // max numbers
  public static readonly maxDereference = 50;
  public static readonly showAllMax = 20;

  public static parseMessage(text: string, matcher: string): IParsedMessage {
    const trigger = text.slice(0, text.indexOf(matcher)).trim();
    const verb =
      matcher === '<reply>' || matcher === '<r>'
        ? ''
        : matcher.trim().replace(/[<>]/g, '').toLowerCase();
    let response = text
      .slice(text.indexOf(matcher) + matcher.length, text.length)
      .trim();
    let isBatch = false;

    if (response.match(this.batchMatcher)) {
      isBatch = true;
      response = response.replace(this.batchMatcher, '').trim();
    }

    return { trigger, verb, response, batch: isBatch };
  }

  public static stripRememberKeywordFromMessage(
    messageContent: string,
  ): string {
    if (messageContent.match(FactoidsFeatureUtil.grMatcher)) {
      return messageContent.replace(FactoidsFeatureUtil.grMatcher, '').trim();
    } else if (messageContent.match(FactoidsFeatureUtil.rememberMatcher)) {
      return messageContent
        .replace(FactoidsFeatureUtil.rememberMatcher, '')
        .trim();
    } else {
      return messageContent.trim();
    }
  }
}
