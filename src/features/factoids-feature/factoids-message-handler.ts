import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { FactoidsData } from './factoids-data';
import { FactoidsFeatureUtil as util } from './factoids-feature-util';
import { IParsedMessage } from './parsed-message';
import {
  getRandomInt,
  getTrueWithProbability,
} from '../../util/general/random-extensions';
import { FactoidsBanned } from './factoids-banned';

interface IterationDetails {
  response: string;
  iteration: number;
}

export class FactoidsMessageHandler extends FeatureMessageHandler {
  // TODO: set the probability with which this feature responds to valid
  // triggers. Feature will always attempt to respond if the message is directly
  // addressed to Gradbot.
  protected readonly triggerProbability = 1;

  private readonly triggerRetrieveFactoidProbability = 0.2;
  private readonly maxTriggerLength = 200;

  private lookUpCache = new Map();

  public isMessageTrigger(): boolean {
    return true;
  }

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    this.lookUpCache.clear();
    let ret: FeatureMessage | null = null;
    const messageContent = this.getSanitisedText();

    if (
      this.message.text.match(util.grMatcher) ||
      (this.isMessageForGradbot() && messageContent.match(util.rememberMatcher))
    ) {
      ret = await this.rememberFactoid(messageContent.trim());
    } else if (
      messageContent.length <= this.maxTriggerLength &&
      (this.isMessageForGradbot() ||
        getTrueWithProbability(this.triggerRetrieveFactoidProbability))
    ) {
      ret = await this.retrieveFactoid(messageContent);
    }

    return ret;
  }

  private async getSortedFactoids(trigger: string): Promise<FactoidsData[]> {
    let responses: FactoidsData[] = [];

    const data = new FactoidsData(this.message);
    data.trigger.set(trigger.toLowerCase().trim());
    const matches = await data.retrieveAlike();

    if (matches) {
      responses = matches;
      responses.sort((a, b) =>
        a.response.get().localeCompare(b.response.get()),
      );
    }

    return responses;
  }

  private async showFactoids(text: string): Promise<FeatureMessage> {
    const pageMatch: string[] | null = text.match(/[0-9]{1,2}\s*$/);
    let page = 0;

    if (pageMatch && pageMatch.length > 0) {
      page = Number(pageMatch[0]);
    }

    const trigger: string = text.replace(util.showMatcher, '').trim();

    // grab a sorted array of stored factoids, show all if requested
    const sortedFactoids: FactoidsData[] =
      await this.getSortedFactoids(trigger);

    let replyText: string =
      `Stored responses for "${trigger}" are, ` + `(page ${page})`;

    let i: number = page * util.showAllMax;
    const iMax: number = (page + 1) * util.showAllMax;

    for (; i < sortedFactoids.length && i < iMax; i = i + 1) {
      const fd = sortedFactoids[i];
      replyText += `\n${i + 1}. ${fd.verb.get()} "${fd.response.get()}"`;
    }

    const reasonText: string =
      `${this.message.user.displayName} asked me to show stored factoids ` +
      `for "${trigger}"`;

    return this.cleanMessage(replyText, reasonText);
  }

  private async deleteFactoids(text: string): Promise<FeatureMessage> {
    const numsMatch: string[] | null = text.match(
      /[1-9][0-9]{0,2}(\s*,\s*[1-9][0-9]{0,2})*\s*$$/,
    );
    let num = 0;

    if (!(numsMatch && numsMatch.length > 0)) {
      // should not be possible, but catch just in case
      return this.cleanMessage(
        `Failed to delete with "${text}".`,
        'User asked me to delete, but I could ' +
          'retrieve a number from the request.',
      );
    }

    const trigger: string = text.replace(util.deleteMatcher, '').trim();
    const delElems: string[] = [];
    const delNums: number[] = [];
    const delFactoids: FactoidsData[] = [];

    for (const numStr of numsMatch[0].split(',')) {
      num = Number(numStr) - 1;

      // grab a sorted array of stored factoids, show all if requested
      const sortedFactoids: FactoidsData[] =
        await this.getSortedFactoids(trigger);

      if (sortedFactoids.length === 0) {
        return this.cleanMessage(
          `No factoids for trigger "${trigger}".`,
          'User asked me to delete a factoid that' + "doesn't exist.",
        );
      } else if (num < 0 || num >= sortedFactoids.length) {
        return this.cleanMessage(
          `Number of deletion requested for trigger "${trigger}" is too high` +
            ' or too low.',
          "User asked me to delete number of a factoid that doesn't exist.",
        );
      } else if (delNums.includes(num)) {
        return this.cleanMessage(
          `The number ${num + 1} is duplicated in the requested deletion, ` +
            'rejecting.',
          'User asked me to delete number twice.',
        );
      } else {
        const fd: FactoidsData = sortedFactoids[num];
        delElems.push(`"${trigger}" "${fd.verb.get()}" "${fd.response.get()}"`);
        delFactoids.push(fd);
        delNums.push(num);
      }
    }

    if (delElems.length > 0) {
      for (const fact of delFactoids) {
        await fact.delete();
      }

      let response = 'I have deleted the following factoids,';
      for (const delElem of delElems) {
        response += `\n${delElem}`;
      }

      return this.cleanMessage(
        `${response}`,
        'User asked me to delete one or more factoids.',
      );
    } else {
      return this.cleanMessage(
        'No Factoids deleted.',
        'User asked me to delete one or more factoids, but no .',
      );
    }
  }

  private async rememberFactoid(
    messageContent: string,
  ): Promise<FeatureMessage | null> {
    let ret = null;
    const text = util.stripRememberKeywordFromMessage(messageContent);

    let show = false;
    let deleteFactoid = false;

    // Parse the message
    let rawVerb = '';
    let messageParts: IParsedMessage = {
      trigger: '',
      verb: '',
      response: '',
      batch: false,
    };

    if (text.match(util.replyMatcher)) {
      rawVerb = '<reply>';
      let matchedVerb: string = rawVerb;
      if (text.match(util.replyShortMatcher)) {
        matchedVerb = '<r>';
      }
      messageParts = util.parseMessage(text, matchedVerb);
    } else if (text.match(util.isMatcher)) {
      messageParts = util.parseMessage(text, 'is');
    } else if (text.match(util.areMatcher)) {
      messageParts = util.parseMessage(text, 'are');
    } else if (text.match(util.showMatcher)) {
      show = true;
    } else if (text.match(util.deleteMatcher)) {
      deleteFactoid = true;
    } else {
      const verbs = text.match(util.customVerbMatcher);
      if (verbs && verbs.length > 0 && verbs[0] !== '<b>') {
        const verb = verbs[0];
        messageParts = util.parseMessage(text, verb);
      }
    }

    if (messageParts.trigger !== '' && messageParts.response !== '') {
      if (FactoidsBanned.isBanned(messageParts.trigger)) {
        ret = this.cleanMessage(
          "Sorry, I can't let you do that Dave.",
          'A user tried to store a banned item',
        );
      } else {
        let storeResp: string[] = [messageParts.response];
        let finalResp = '';
        if (messageParts.batch) {
          storeResp = messageParts.response.split(',').map((s) => s.trim());
        }

        const data = new FactoidsData(this.message);
        data.trigger.set(messageParts.trigger.toLowerCase());
        data.verb.set(messageParts.verb);

        for (const resp of storeResp) {
          // Store the factoid
          data.response.set(resp);
          await data.write();

          finalResp =
            finalResp === ''
              ? '"' + resp + '"'
              : finalResp + ', "' + resp + '"';
        }

        // Build the reply
        if (rawVerb === '') {
          rawVerb = messageParts.verb;
        }
        const replyText =
          `Ok, remembering that "${messageParts.trigger}" ` +
          `${rawVerb} ${finalResp}`;
        const reasonText =
          `${this.message.user.displayName} asked me to remember ` +
          `a fact about ${messageParts.trigger}`;
        ret = this.cleanMessage(replyText, reasonText);
      }
    } else if (show) {
      ret = await this.showFactoids(text);
    } else if (deleteFactoid) {
      ret = await this.deleteFactoids(text);
    }

    return ret;
  }

  private async retrieveFactoid(
    trigger: string,
  ): Promise<FeatureMessage | null> {
    let ret = null;
    let replyText = '';
    let reasonText = '';
    let verb = '';
    let response = '';

    const retrieved = await this.retrieve(trigger);
    verb = retrieved.verb;
    response = retrieved.response;

    if (response !== '') {
      replyText = verb.length > 0 ? `${trigger} ${verb} ${response}` : response;
      reasonText = `${this.message.user.displayName} reminded me of a fact I know about ${trigger}`;
      ret = this.cleanMessage(replyText, reasonText);
    }

    return ret;
  }

  private async getRecord(trigger: string): Promise<FactoidsData | null> {
    if (this.lookUpCache.has(trigger)) {
      const cacheMatches = this.lookUpCache.get(trigger) as FactoidsData[];
      return cacheMatches[getRandomInt(cacheMatches.length)];
    }

    const data = new FactoidsData(this.message);
    data.trigger.set(trigger);
    const matches = await data.retrieveAlike();

    if (matches) {
      this.lookUpCache.set(trigger, matches);
      return matches[getRandomInt(matches.length)];
    } else {
      return null;
    }
  }

  private async flattenLookUp(
    response: string,
    prevLookUpsSet: string[],
    iteration: number,
  ): Promise<IterationDetails> {
    // bail early if out of iterations
    if (iteration >= util.maxDereference) {
      return { response, iteration };
    }

    let nestedLookUp = util.dereferenceMatcher.exec(response);

    while (nestedLookUp) {
      iteration = iteration + 1;

      // bail if out of iterations
      if (iteration >= util.maxDereference) {
        break;
      }

      const recursiveTrigger = nestedLookUp[1].toLowerCase();

      if (!prevLookUpsSet.includes(recursiveTrigger)) {
        const record = await this.getRecord(recursiveTrigger);

        if (record) {
          // successfully found a value to replace the {{x}} with
          //
          // Need to check if nested flattens need to occur as well.
          // Also need to ensure no circular dependencies so add
          // this trigger to the list of seen parent triggers

          const newResp = await this.flattenLookUp(
            record.response.get(),
            prevLookUpsSet.concat([recursiveTrigger]),
            iteration,
          );

          // store the flattened results
          response = response.replace(
            util.dereferenceMatcher,
            newResp.response,
          );
          iteration = newResp.iteration;
        } else {
          // failed lookup so indicate using ?? markers
          response = response.replace(util.dereferenceMatcher, '??$1??');
        }
      } else {
        // failed because recursion detected, indicate using %% markers
        response = response.replace(util.dereferenceMatcher, '%%$1%%');
      }

      // find the next value to look up in the response if there is one
      nestedLookUp = util.dereferenceMatcher.exec(response);
    }

    return { response, iteration };
  }

  private async retrieve(
    trigger: string,
  ): Promise<{ verb: string; response: string }> {
    let verb = '';
    let response = '';
    const record: FactoidsData | null = await this.getRecord(
      trigger.trim().toLowerCase(),
    );

    if (record) {
      verb = record.verb.get();
      response = (await this.flattenLookUp(record.response.get(), [], 0))
        .response;
    }

    return { verb, response };
  }
}
