export interface IParsedMessage {
  trigger: string;
  verb: string;
  response: string;
  batch: boolean;
}
