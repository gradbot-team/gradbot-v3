import 'reflect-metadata';
import { FactoidsData } from './factoids-data';
import { FactoidsFeature } from './factoids-feature';
import { FactoidsFeatureUtil } from './factoids-feature-util';
import { FactoidsBanned } from './factoids-banned';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import { getTestMessage } from '../../util/test/get-test-message';
import { IParsedMessage } from './parsed-message';
import { isNullUndefinedOrEmpty } from '../../util/general/string-extensions';
import * as randomExtensions from '../../util/general/random-extensions';

const util = FeatureTestUtil.createForFeatureType(FactoidsFeature);

const buildRememberResponse = (
  trigger: string,
  verb: string,
  response: string,
) => {
  if (isNullUndefinedOrEmpty(verb)) {
    verb = '<reply>';
  }
  return `Ok, remembering that "${trigger}" ${verb} "${response}"`;
};

const buildFactoidResponse = (
  trigger: string,
  verb: string,
  response: string,
) => {
  if (!isNullUndefinedOrEmpty(verb)) {
    return `${trigger} ${verb} ${response}`;
  } else {
    return response;
  }
};

const buildIParsed = (
  trigger: string,
  verb: string,
  response: string,
  batch?: boolean,
) => {
  if (batch) {
    return { trigger, verb, response, batch };
  }
  return { trigger, verb, response, batch: false };
};

const getRecordsForTrigger = async (trigger: string) => {
  const record = new FactoidsData(getTestMessage());
  record.trigger.set(trigger.toLowerCase().trim());
  return await record.retrieveAlike();
};

const expectRecordsToHaveValues = (
  records: FactoidsData[],
  values: IParsedMessage[],
) => {
  expect(records.length).toBe(values.length);

  let record;
  let value;
  for (let i = 0; i < records.length; i++) {
    record = records[i];
    value = values[i];
    expect(record.trigger.get()).toBe(value.trigger);
    expect(record.verb.get()).toBe(value.verb);
    expect(record.response.get()).toBe(value.response);
  }
};

describe('FactoidsFeature', () => {
  afterEach(async () => {
    const records = await new FactoidsData(getTestMessage()).retrieveAll();
    for (const record of records) {
      await record.delete();
    }
  });

  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  const testData: {
    userPrompt: string;
    trigger: string;
    verb: string;
    response: string;
  }[] = [
    {
      userPrompt: 'gradbot, remember War is Peace',
      trigger: 'War',
      verb: 'is',
      response: 'Peace',
    },
    {
      userPrompt: 'g remember Freedom is Slavery',
      trigger: 'Freedom',
      verb: 'is',
      response: 'Slavery',
    },
    {
      userPrompt: 'remember Ignorance is Strength g',
      trigger: 'Ignorance',
      verb: 'is',
      response: 'Strength',
    },
    {
      userPrompt: 'gr All animals are equal',
      trigger: 'All animals',
      verb: 'are',
      response: 'equal',
    },
    {
      userPrompt: 'gr Nice to see you, to see you <reply> nice',
      trigger: 'Nice to see you, to see you',
      verb: '',
      response: 'nice',
    },
    {
      userPrompt: 'gr What is your name? <reply> Arthur, King of the Britons',
      trigger: 'What is your name?',
      verb: '',
      response: 'Arthur, King of the Britons',
    },
    {
      userPrompt: 'gr A <reply> B <reply> C',
      trigger: 'A',
      verb: '',
      response: 'B <reply> C',
    },
    {
      userPrompt: 'gr    D    is    E    is    F',
      trigger: 'D',
      verb: 'is',
      response: 'E    is    F',
    },
    {
      userPrompt: 'gr Money <makes> the world go around',
      trigger: 'Money',
      verb: 'makes',
      response: 'the world go around',
    },
    {
      userPrompt: 'gr MONEY <MAKES> THE WORLD GO AROUND',
      trigger: 'MONEY',
      verb: 'makes',
      response: 'THE WORLD GO AROUND',
    },
  ];

  for (const data of testData) {
    test(`parses and responds to input message ${data.userPrompt}`, async () =>
      await util.expectReplyToHaveText(
        data.userPrompt,
        buildRememberResponse(data.trigger, data.verb, data.response),
      ));

    test(`creates the db record correctly for input message ${data.userPrompt} and responds to the trigger`, async () => {
      // Arrange
      await util.sendMessage(data.userPrompt);

      // Act and Assert
      await util.expectReplyToHaveText(
        `g ${data.trigger}`,
        buildFactoidResponse(data.trigger, data.verb, data.response),
      );
      const records = await getRecordsForTrigger(data.trigger);
      expectRecordsToHaveValues(records, [
        buildIParsed(
          data.trigger.toLowerCase(),
          data.verb.toLowerCase(),
          data.response,
        ),
      ]);
    });
  }

  test('handles being asked to remember a factoid that already exists', async () => {
    // Arrange
    const data = testData[0];
    await util.sendMessage(data.userPrompt);

    // Act and Assert
    await util.expectReplyToHaveText(
      data.userPrompt,
      buildRememberResponse(data.trigger, data.verb, data.response),
    );
    const records = await getRecordsForTrigger(data.trigger);
    expectRecordsToHaveValues(records, [
      buildIParsed(
        data.trigger.toLowerCase(),
        data.verb.toLowerCase(),
        data.response,
      ),
      buildIParsed(
        data.trigger.toLowerCase(),
        data.verb.toLowerCase(),
        data.response,
      ),
    ]);
  });

  test('stores a second response to a factoid that already exists, where the response differs only by case', async () => {
    // Arrange
    await util.sendMessage('gr War is Peace');

    // Act and Assert
    await util.expectReplyToHaveText(
      'gr War is peace',
      buildRememberResponse('War', 'is', 'peace'),
    );
    const records = await getRecordsForTrigger('War');
    expectRecordsToHaveValues(records, [
      buildIParsed('war', 'is', 'Peace'),
      buildIParsed('war', 'is', 'peace'),
    ]);
  });

  test('stores a second response to a factoid that already exists, using the same verb', async () => {
    // Arrange
    await util.sendMessage('gr War is Peace');

    // Act and Assert
    await util.expectReplyToHaveText(
      'gr War is hell',
      buildRememberResponse('War', 'is', 'hell'),
    );
    const records = await getRecordsForTrigger('War');
    expectRecordsToHaveValues(records, [
      buildIParsed('war', 'is', 'Peace'),
      buildIParsed('war', 'is', 'hell'),
    ]);
  });

  test('stores a new response to a factoid that already exists, using a different verb', async () => {
    // Arrange
    await util.sendMessage('gr War is Peace');

    // Act and Assert
    await util.expectReplyToHaveText(
      'gr War <reply> What is it good for?',
      buildRememberResponse('War', '<reply>', 'What is it good for?'),
    );
    const records = await getRecordsForTrigger('War');
    expectRecordsToHaveValues(records, [
      buildIParsed('war', 'is', 'Peace'),
      buildIParsed('war', '', 'What is it good for?'),
    ]);
  });

  for (const trigger of ['War', 'war', 'WAR', '    War      ']) {
    test(`ignores case and leading/trailing whitespace when matching trigger phrase (trigger: ${trigger}),`, async () => {
      // Arrange
      await util.sendMessage('gr War is Peace');

      // Act
      const prompt = `g ${trigger}`;
      const msg = await util.getReplyForMessage(prompt);

      // Assert
      expect(msg).not.toBeNull();
      if (msg) {
        // Response should trim leading/trailing whitespace but preserve case.
        expect(msg.text.startsWith(trigger.trim())).toBe(true);
      }
      const records = await getRecordsForTrigger(trigger);
      expectRecordsToHaveValues(records, [
        buildIParsed(trigger.trim().toLowerCase(), 'is', 'Peace'),
      ]);
    });
  }

  test('stores each batch item separately', async () => {
    // Act and Assert
    await util.expectReplyToHaveText(
      'gr War is <batch> Peace, hell, life',
      buildRememberResponse('War', 'is', 'Peace", "hell", "life'),
    );
    const records = await getRecordsForTrigger('War');
    expectRecordsToHaveValues(records, [
      buildIParsed('war', 'is', 'Peace'),
      buildIParsed('war', 'is', 'hell'),
      buildIParsed('war', 'is', 'life'),
    ]);
  });

  test('stores batch items as 1 when batch is not flagged', async () => {
    // Act and Assert
    await util.expectReplyToHaveText(
      'gr War is Peace, hell, life',
      buildRememberResponse('War', 'is', 'Peace, hell, life'),
    );
    const records = await getRecordsForTrigger('War');
    expectRecordsToHaveValues(records, [
      buildIParsed('war', 'is', 'Peace, hell, life'),
    ]);
  });

  test(
    'dereferences factoids to multiple levels and uses the verb ' +
      'of the first factoid in the chain',
    async () => {
      // Arrange
      await util.sendMessage('gr Emptiness <reply> {{loneliness}}');
      await util.sendMessage('gr Loneliness is {{cleanliness}}');
      await util.sendMessage('gr Cleanliness is {{godliness}}');
      await util.sendMessage('gr Godliness is God is empty.');

      // Act and Assert
      await util.expectReplyToHaveText('g emptiness', 'God is empty.');
    },
  );

  test(
    "does not dereference responses that aren't surrounded by " +
      'double curly brackets {{}}',
    async () => {
      // Arrange
      await util.sendMessage('gr X is Y');
      await util.sendMessage('gr Y is Z');

      // Act and Assert
      await util.expectReplyToHaveText('g X', 'X is Y');
    },
  );

  test(
    "replies with a ?-enclosed response if the content doesn't " +
      'dereference to anything',
    async () => {
      // Arrange
      await util.sendMessage('gr P is {{Q}}');

      // Act and Assert
      await util.expectReplyToHaveText('g P', 'P is ??Q??');
    },
  );

  test(
    'replies with a %-enclosed response if the content dereferences ' +
      'to a circlur dependence',
    async () => {
      // Arrange
      await util.sendMessage('gr step0 is {{step1}}');
      await util.sendMessage('gr step1 is {{step2}}');
      await util.sendMessage('gr step2 is {{step3}}');
      await util.sendMessage('gr step3 is {{step1}}');

      // Act and Assert
      await util.expectReplyToHaveText('g step0', 'step0 is %%step1%%');
    },
  );

  test('max dereference lookup is respected', async () => {
    // Arrange
    for (let i = 0; i < FactoidsFeatureUtil.maxDereference + 10; i = i + 1) {
      await util.sendMessage(`gr step${i} is {{step${i + 1}}}`);
    }

    // Act and Assert
    await util.expectReplyToHaveText(
      'g step0',
      `step0 is {{step${FactoidsFeatureUtil.maxDereference}}}`,
    );
  });

  test('multiple nested look ups are correctly retrieved', async () => {
    // set up random mock
    const randSpy = jest.spyOn(randomExtensions, 'getRandomInt');
    randSpy
      .mockReturnValueOnce(0) // resolve test
      .mockReturnValueOnce(1) // resolve aWord => arrow
      .mockReturnValueOnce(0) // resolve twoBwords
      .mockReturnValueOnce(2) // resolve bWord (0) => bin
      .mockReturnValueOnce(0) // resolve bWord (1) => banana
      .mockReturnValueOnce(0) // resolve aOrbWord => {{aWord}}
      .mockReturnValueOnce(1); // resolve aWord => arrow

    // Arrange
    await util.sendMessage('gr aWord is apple');
    await util.sendMessage('gr aWord is arrow');
    await util.sendMessage('gr aWord is arm');
    await util.sendMessage('gr bWord is banana');
    await util.sendMessage('gr bWord is barrow');
    await util.sendMessage('gr bWord is bin');
    await util.sendMessage('gr aOrbWord is {{aWord}}');
    await util.sendMessage('gr aOrbWord is {{bWord}}');
    await util.sendMessage('gr twoBwords is {{bWord}} and {{bWord}}');
    await util.sendMessage(
      'gr test <reply> there is a {{aWord}} and {{twoBwords}} ' +
        'and {{aOrbWord}}',
    );

    // Act and Assert
    await util.expectReplyToHaveText(
      'g test',
      'there is a arrow and bin and banana and arrow',
    );

    expect(randSpy).toHaveBeenCalledTimes(7);

    randSpy.mockRestore();
  });

  test('show all records when asked', async () => {
    // Arrange
    for (const i of [...Array(FactoidsFeatureUtil.showAllMax + 10).keys()]) {
      await util.sendMessage(`gr showTest is value ${i}`);
    }

    // Act and Assert
    // show page 0, the default
    await util.expectReplyToHaveText(
      'gr showTest [show] ',
      'Stored responses for "showTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 1"\n' +
        '3. is "value 10"\n' +
        '4. is "value 11"\n' +
        '5. is "value 12"\n' +
        '6. is "value 13"\n' +
        '7. is "value 14"\n' +
        '8. is "value 15"\n' +
        '9. is "value 16"\n' +
        '10. is "value 17"\n' +
        '11. is "value 18"\n' +
        '12. is "value 19"\n' +
        '13. is "value 2"\n' +
        '14. is "value 20"\n' +
        '15. is "value 21"\n' +
        '16. is "value 22"\n' +
        '17. is "value 23"\n' +
        '18. is "value 24"\n' +
        '19. is "value 25"\n' +
        '20. is "value 26"',
    );

    // show page 0, this time set 0 explicitly
    await util.expectReplyToHaveText(
      'gr showTest [show] 0',
      'Stored responses for "showTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 1"\n' +
        '3. is "value 10"\n' +
        '4. is "value 11"\n' +
        '5. is "value 12"\n' +
        '6. is "value 13"\n' +
        '7. is "value 14"\n' +
        '8. is "value 15"\n' +
        '9. is "value 16"\n' +
        '10. is "value 17"\n' +
        '11. is "value 18"\n' +
        '12. is "value 19"\n' +
        '13. is "value 2"\n' +
        '14. is "value 20"\n' +
        '15. is "value 21"\n' +
        '16. is "value 22"\n' +
        '17. is "value 23"\n' +
        '18. is "value 24"\n' +
        '19. is "value 25"\n' +
        '20. is "value 26"',
    );

    // show page 1
    await util.expectReplyToHaveText(
      'gr showTest [show] 1',
      'Stored responses for "showTest" are, (page 1)\n' +
        '21. is "value 27"\n' +
        '22. is "value 28"\n' +
        '23. is "value 29"\n' +
        '24. is "value 3"\n' +
        '25. is "value 4"\n' +
        '26. is "value 5"\n' +
        '27. is "value 6"\n' +
        '28. is "value 7"\n' +
        '29. is "value 8"\n' +
        '30. is "value 9"',
    );
  });

  test('Do not show when asked with a malformed message', async () => {
    // Arrange
    await util.sendMessage('gr showFail <reply> test Value');

    // Act and Assert
    // try a bunch of bad message to try and trigger the show, and
    // expect a null response
    await util.expectReplyToBeNull('gr showFail [show] [alla]');
    await util.expectReplyToBeNull('gr showFail [showa] [all]');
    await util.expectReplyToBeNull('gr showFail [showa]');
    await util.expectReplyToBeNull('gr showFail [show] abc');
    await util.expectReplyToBeNull('gr showFail [show] abc [all]');
    await util.expectReplyToBeNull('gr showFail [show]  [all] abc');
    await util.expectReplyToBeNull('gr showFail [show] 0 [all] ');
    await util.expectReplyToBeNull('gr showFail [show] [all] 12 ab ');
    await util.expectReplyToBeNull('gr showFail [show] [all] 100');
    await util.expectReplyToBeNull('gr showFail [show] 111 ');
    await util.expectReplyToBeNull('gr showFail [show] -1 ');
    await util.expectReplyToBeNull('gr showFail [reply]');
  });

  test('delete records when asked', async () => {
    // Arrange
    for (const i of [...Array(5).keys()]) {
      await util.sendMessage(`gr deleteTest is value ${i}`);
    }

    // Act and Assert
    // Currently there are 5 records
    await util.expectReplyToHaveText(
      'gr deleteTest [s] ',
      'Stored responses for "deleteTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 1"\n' +
        '3. is "value 2"\n' +
        '4. is "value 3"\n' +
        '5. is "value 4"',
    );

    // delete delete item 3
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 3',
      'I have deleted the following factoids,\n' +
        '"deleteTest" "is" "value 2"',
    );

    // Currently there are 4 records
    await util.expectReplyToHaveText(
      'gr deleteTest [show] ',
      'Stored responses for "deleteTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 1"\n' +
        '3. is "value 3"\n' +
        '4. is "value 4"',
    );

    // delete items 4 and 1
    await util.expectReplyToHaveText(
      'gr deleteTest [d] 4',
      'I have deleted the following factoids,\n' +
        '"deleteTest" "is" "value 4"',
    );
    await util.expectReplyToHaveText(
      'gr deleteTest [d] 1',
      'I have deleted the following factoids,\n' +
        '"deleteTest" "is" "value 0"',
    );

    // Currently there are 2 records
    await util.expectReplyToHaveText(
      'gr deleteTest [s] ',
      'Stored responses for "deleteTest" are, (page 0)\n' +
        '1. is "value 1"\n' +
        '2. is "value 3"',
    );

    // delete items 2 and 1
    await util.expectReplyToHaveText(
      'gr deleteTest [d] 2',
      'I have deleted the following factoids,\n' +
        '"deleteTest" "is" "value 3"',
    );
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 1',
      'I have deleted the following factoids,\n' +
        '"deleteTest" "is" "value 1"',
    );

    // show but expect no items left
    await util.expectReplyToHaveText(
      'gr deleteTest [s] ',
      'Stored responses for "deleteTest" are, (page 0)',
    );
  });

  test('delete records when asked using comma separated values', async () => {
    // Arrange
    for (const i of [...Array(5).keys()]) {
      await util.sendMessage(`gr deleteTest is value ${i}`);
    }

    // Act and Assert
    // Currently there are 5 records
    await util.expectReplyToHaveText(
      'gr deleteTest [s] ',
      'Stored responses for "deleteTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 1"\n' +
        '3. is "value 2"\n' +
        '4. is "value 3"\n' +
        '5. is "value 4"',
    );

    // delete delete items 2 and 4
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 2, 4',
      'I have deleted the following factoids,\n' +
        '"deleteTest" "is" "value 1"\n' +
        '"deleteTest" "is" "value 3"',
    );

    // Currently there are 4 records
    await util.expectReplyToHaveText(
      'gr deleteTest [show] ',
      'Stored responses for "deleteTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 2"\n' +
        '3. is "value 4"',
    );

    // delete all remaining elements
    await util.expectReplyToHaveText(
      'gr deleteTest [d] 1,  2 , 3',
      'I have deleted the following factoids,\n' +
        '"deleteTest" "is" "value 0"\n' +
        '"deleteTest" "is" "value 2"\n' +
        '"deleteTest" "is" "value 4"',
    );

    // show but expect no items left
    await util.expectReplyToHaveText(
      'gr deleteTest [s] ',
      'Stored responses for "deleteTest" are, (page 0)',
    );
  });

  test('reject delete records when number too high', async () => {
    // Arrange
    for (const i of [...Array(5).keys()]) {
      await util.sendMessage(`gr deleteTest is value ${i}`);
    }

    // Act and Assert
    // Currently there are 5 records
    await util.expectReplyToHaveText(
      'gr deleteTest [s] ',
      'Stored responses for "deleteTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 1"\n' +
        '3. is "value 2"\n' +
        '4. is "value 3"\n' +
        '5. is "value 4"',
    );

    // Reject delete for 6 which is 1 too high
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 6',
      'Number of deletion requested for trigger "deleteTest" ' +
        'is too high or too low.',
    );

    // Reject delete for 6 which is 1 too high (when multiple are called)
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 1, 3, 6',
      'Number of deletion requested for trigger "deleteTest" ' +
        'is too high or too low.',
    );

    // Reject delete for 999 which is max number
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 999',
      'Number of deletion requested for trigger "deleteTest" ' +
        'is too high or too low.',
    );

    // Reject delete for 999 which is max number (when multiple are called)
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 1, 2, 4, 999',
      'Number of deletion requested for trigger "deleteTest" ' +
        'is too high or too low.',
    );
  });

  test('reject delete records when number are duplicated', async () => {
    // Arrange
    for (const i of [...Array(5).keys()]) {
      await util.sendMessage(`gr deleteTest is value ${i}`);
    }

    // Act and Assert
    // Currently there are 5 records
    await util.expectReplyToHaveText(
      'gr deleteTest [s] ',
      'Stored responses for "deleteTest" are, (page 0)\n' +
        '1. is "value 0"\n' +
        '2. is "value 1"\n' +
        '3. is "value 2"\n' +
        '4. is "value 3"\n' +
        '5. is "value 4"',
    );

    // Reject delete for 6 which is 1 too high
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 1, 1',
      'The number 1 is duplicated in the requested deletion, rejecting.',
    );

    // Reject delete for 6 which is 1 too high (when multiple are called)
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 1, 2, 4, 2',
      'The number 2 is duplicated in the requested deletion, rejecting.',
    );
  });

  test('reject delete records when nothing to delete', async () => {
    // Act and Assert
    // Reject delete when nothing to delete
    await util.expectReplyToHaveText(
      'gr deleteTest [delete] 6',
      'No factoids for trigger "deleteTest".',
    );
  });

  test('Do not delete when asked with a malformed message', async () => {
    // Arrange
    await util.sendMessage('gr deleteFail <reply> test Value');

    // Act and Assert
    // try a bunch of bad message to try and trigger the show, and
    // expect a null response
    await util.expectReplyToBeNull('gr deleteFail [delete] [alla]');
    await util.expectReplyToBeNull('gr deleteFail [delete]');
    await util.expectReplyToBeNull('gr deleteFail [delete] 1234');
    await util.expectReplyToBeNull('gr deleteFail [delete] 1, 1234');
    await util.expectReplyToBeNull('gr deleteFail [delete] 1, 0');
    await util.expectReplyToBeNull('gr deleteFail [delete] 0');
    await util.expectReplyToBeNull('gr deleteFail [delete] -2, 1');
    await util.expectReplyToBeNull('gr deleteFail [deletea] 1');
    await util.expectReplyToBeNull('gr deleteFail [deletea] 1, 5');
    await util.expectReplyToBeNull('gr deleteFail [deletea]');
    await util.expectReplyToBeNull('gr deleteFail [delete] abc');
    await util.expectReplyToBeNull('gr deleteFail [delete] 1, 2, abc');
    await util.expectReplyToBeNull('gr deleteFail [delete] 1, 2 2');
    await util.expectReplyToBeNull('gr deleteFail [delete] 3 32');
    await util.expectReplyToBeNull('gr deleteFail [delete] 2 + 3');
  });

  test('Ignore mentions by manipulating them with a space', async () => {
    // Act and Assert
    // Expect triegger and response to be clean of mentions
    await util.expectReplyToHaveText(
      'gr cleanMessageText @value1 <reply> @value2 1 @value3',
      'Ok, remembering that "cleanMessageText @ value1" <reply> ' +
        '"@ value2 1 @ value3"',
    );

    // Expect retrieval to be clean of mentions
    await util.expectReplyToHaveText(
      'g cleanMessageText @value1',
      '@ value2 1 @ value3',
    );

    // Expect show to be clean of mentions
    await util.expectReplyToHaveText(
      'gr cleanMessageText @value1 [s]',
      'Stored responses for "cleanMessageText @ value1" are, (page 0)\n' +
        '1.  "@ value2 1 @ value3"',
    );

    // Expect delete to be clean of mentions
    await util.expectReplyToHaveText(
      'gr cleanMessageText @value1 [d] 1',
      'I have deleted the following factoids,\n' +
        '"cleanMessageText @ value1" "" "@ value2 1 @ value3"',
    );
  });

  test('Do not allow banned words', async () => {
    // try all banned words and expect the rejection result
    for (const bannedWord of FactoidsBanned.BANNED_LIST) {
      await util.expectReplyToHaveText(
        `gr ${bannedWord} <reply> test`,
        "Sorry, I can't let you do that Dave.",
      );
    }
  });

  test('Ensure words that contain verbs can be showed', async () => {
    // Arrange
    await util.sendMessage('gr dare <reply> erad');

    // ensure show works for dare
    await util.expectReplyToHaveText(
      'gr dare [s]',
      'Stored responses for "dare" are, (page 0)\n' + '1.  "erad"',
    );
  });

  test('Ensure that reply shorthand works when retrieved', async () => {
    // set up
    await util.expectReplyToHaveText(
      'gr recruit <r> test',
      'Ok, remembering that "recruit" <reply> "test"',
    );

    // Check the retrieval is correct
    await util.expectReplyToHaveText('g recruit', 'test');
  });

  test('Ensure that malformed messgae is not replied to', async () => {
    // Ensure malformed message is ignored, even with the batch hook
    await util.expectReplyToBeNull('gr recruit <rather than> <b> test1, test2');
  });
});
