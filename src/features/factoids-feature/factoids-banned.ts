export class FactoidsBanned {
  public static readonly BANNED_LIST: string[] = [
    'tell',
    'filmclub',
    'bookclub',
  ];

  public static isBanned(factoid: string): boolean {
    return this.BANNED_LIST.includes(factoid);
  }
}
