import { FactoidsFeatureUtil } from './factoids-feature-util';
import { expectRegexToMatchSubstring } from '../../util/test/general-test-util';

interface IFactoidsFeatureUtilTestData {
  userPrompt: string;
  trigger: string;
  response: string;
}

interface IFactoidsFeatureUtilCustomVerbTestData {
  userPrompt: string;
  trigger: string;
  verb: string;
  response: string;
}

describe('FactoidFeatureUtil', () => {
  const replyPromptData: IFactoidsFeatureUtilTestData[] = [
    {
      userPrompt: 'Nice to see you, to see you <reply> nice',
      trigger: 'Nice to see you, to see you',
      response: 'nice',
    },
    { userPrompt: 'A    <reply>    B', trigger: 'A', response: 'B' },
    {
      userPrompt: 'A <has> B are C is D <reply> E',
      trigger: 'A <has> B are C is D',
      response: 'E',
    },
  ];

  for (const item of replyPromptData) {
    test(`matches the "<reply>" verb in "${item.userPrompt}"`, () => {
      expectRegexToMatchSubstring(
        item.userPrompt,
        FactoidsFeatureUtil.replyMatcher,
        '<reply>',
      );
    });

    test(`correctly parses the phrase ${item.userPrompt} with "<reply>" as the verb`, () => {
      const result = FactoidsFeatureUtil.parseMessage(
        item.userPrompt,
        '<reply>',
      );

      expect(result.trigger).toBe(item.trigger);
      expect(result.verb).toBe('');
      expect(result.response).toBe(item.response);
    });
  }

  const isPromptData: IFactoidsFeatureUtilTestData[] = [
    {
      userPrompt: 'Freedom is Slavery',
      trigger: 'Freedom',
      response: 'Slavery',
    },
    { userPrompt: 'War    is    Peace', trigger: 'War', response: 'Peace' },
    {
      userPrompt: 'A <has> B are C is D <reply> E',
      trigger: 'A <has> B are C',
      response: 'D <reply> E',
    },
  ];

  for (const item of isPromptData) {
    test(`matches the "is" verb in "${item.userPrompt}"`, () => {
      expectRegexToMatchSubstring(
        item.userPrompt,
        FactoidsFeatureUtil.isMatcher,
        'is',
      );
    });

    test(`correctly parses the phrase ${item.userPrompt} with "is" as the verb`, () => {
      const result = FactoidsFeatureUtil.parseMessage(item.userPrompt, 'is');

      expect(result.trigger).toBe(item.trigger);
      expect(result.verb).toBe('is');
      expect(result.response).toBe(item.response);
    });
  }

  const arePromptData: IFactoidsFeatureUtilTestData[] = [
    {
      userPrompt: 'All animals are equal',
      trigger: 'All animals',
      response: 'equal',
    },
    { userPrompt: 'A    are    B', trigger: 'A', response: 'B' },
    {
      userPrompt: 'A <has> B are C is D <reply> E',
      trigger: 'A <has> B',
      response: 'C is D <reply> E',
    },
  ];

  for (const item of arePromptData) {
    test(`matches the "are" verb in "${item.userPrompt}"`, () => {
      expectRegexToMatchSubstring(
        item.userPrompt,
        FactoidsFeatureUtil.areMatcher,
        'are',
      );
    });

    test(`correctly parses the phrase ${item.userPrompt} with "are" as the verb`, () => {
      const result = FactoidsFeatureUtil.parseMessage(item.userPrompt, 'are');

      expect(result.trigger).toBe(item.trigger);
      expect(result.verb).toBe('are');
      expect(result.response).toBe(item.response);
    });
  }

  const customVerbPromptData: IFactoidsFeatureUtilCustomVerbTestData[] = [
    {
      userPrompt: 'Money <makes> the world go around',
      trigger: 'Money',
      verb: 'makes',
      response: 'the world go around',
    },
    {
      userPrompt: 'A    <likes>    B',
      trigger: 'A',
      verb: 'likes',
      response: 'B',
    },
    {
      userPrompt: 'A <has> B are C is D <reply> E',
      trigger: 'A',
      verb: 'has',
      response: 'B are C is D <reply> E',
    },
  ];

  for (const item of customVerbPromptData) {
    test(`matches the custom verb in "${item.userPrompt}"`, () => {
      expectRegexToMatchSubstring(
        item.userPrompt,
        `<${item.verb}>`,
        `<${item.verb}>`,
      );
    });

    test(`correctly parses the phrase ${item.userPrompt} with a custom verb`, () => {
      const result = FactoidsFeatureUtil.parseMessage(
        item.userPrompt,
        `<${item.verb}>`,
      );

      expect(result.trigger).toBe(item.trigger);
      expect(result.verb).toBe(item.verb);
      expect(result.response).toBe(item.response);
    });
  }
});
