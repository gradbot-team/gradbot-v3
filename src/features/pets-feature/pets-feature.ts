import { noIndent } from '../../util/general/string-extensions';
import { PetsMessageHandler } from './pets-message-handler';
import { Feature } from '../../gradbot/feature/feature';

export class PetsFeature extends Feature {
  public readonly name = 'Pets';
  public readonly iCanInfo = 'show you pets';
  public readonly instructions = noIndent(
    `• *g cat* – I'll show you a cat
     • *g show me a dog* – I'll show you a dog
     • *g a pet* – I'll show you either a dog or a cat`,
  );
  protected readonly messageHandlerType = PetsMessageHandler;
}
