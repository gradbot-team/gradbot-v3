import 'reflect-metadata';
import { PetsFeature } from './pets-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(PetsFeature);

const setValidAPIResponse = () => {
  util.api.urlReturnValues['https://random.dog/woof.json?filter=mp4,webm,gif'] =
    { url: 'test' };
  util.api.urlReturnValues['https://aws.random.cat/meow'] = { file: 'test' };
};

describe('PetsFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds to "cat" with an image', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveImage('Gradbot, cat');
  });

  test('responds to "pet" with an image', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveImage('show me a pet, gradbot');
  });

  test('responds to "pet" without text', async () => {
    setValidAPIResponse();
    await util.expectReplyToNotHaveText('show me a pet, gradbot');
  });

  test('responds to "dog" with an image', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveImage('g a dog');
  });

  test('deals with invalid dog API data', async () => {
    util.api.urlReturnValues[
      'https://random.dog/woof.json?filter=mp4,webm,gif'
    ] = { wrong_thing: 'test' };
    await util.expectReplyToHaveText(
      'Gradbot, dog',
      'All my pet images have gone missing!',
    );
  });

  test('deals with missing dog API data', async () => {
    util.api.urlReturnValues[
      'https://random.dog/woof.json?filter=mp4,webm,gif'
    ] = null;
    await util.expectReplyToHaveText(
      'Gradbot, dog',
      'All my pet images have gone missing!',
    );
  });

  test('deals with invalid cat API data', async () => {
    util.api.urlReturnValues['https://aws.random.cat/meow'] = {
      wrong_thing: 'test',
    };
    await util.expectReplyToHaveText(
      'Gradbot, a cat',
      'All my pet images have gone missing!',
    );
  });

  test('deals with missing cat API data', async () => {
    util.api.urlReturnValues['https://aws.random.cat/meow'] = null;
    await util.expectReplyToHaveText(
      'Gradbot, a cat',
      'All my pet images have gone missing!',
    );
  });
});
