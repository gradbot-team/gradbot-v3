import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { Image } from '../../gradbot/image/image';

export class PetsMessageHandler extends FeatureMessageHandler {
  private readonly PETS_REGEX = /^(?:show\W+me\W+)?(?:a\W+)?(dog|cat|pet)\W*$/;
  private readonly DOGS_API_URL =
    'https://random.dog/woof.json?filter=mp4,webm,gif';
  private readonly CATS_API_URL = 'https://aws.random.cat/meow';

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const regexMatch = this.matchSanitisedLowercaseTextIfForGradbot(
      this.PETS_REGEX,
    );

    if (regexMatch) {
      let image: Image | null = null;

      switch (regexMatch[1]) {
        case 'dog':
          image = await this.getDogPic();
          break;
        case 'cat':
          image = await this.getCatPic();
          break;
        case 'pet':
          image =
            Math.random() >= 0.5
              ? await this.getDogPic()
              : await this.getCatPic();
          break;
      }

      if (image) {
        return this.createMessage(
          '',
          `${this.message.user.displayName} asked me to show them a ${regexMatch[1]}`,
        ).withImage(image);
      }

      return this.createMessage(
        'All my pet images have gone missing!',
        `${this.message.user.displayName} asked me to show them a ${regexMatch[1]}, but API is down`,
      );
    }
    return null;
  }

  private async getCatPic() {
    const data = await this.fetchAPI<CatsAPIData>(this.CATS_API_URL);
    if (data && data.file) {
      return await this.getImageFromPath(data.file);
    }
    return null;
  }

  private async getDogPic() {
    const data = await this.fetchAPI<DogsAPIData>(this.DOGS_API_URL);
    if (data && data.url) {
      return await this.getImageFromPath(data.url);
    }
    return null;
  }
}

interface CatsAPIData {
  file?: string;
}
interface DogsAPIData {
  url?: string;
}
