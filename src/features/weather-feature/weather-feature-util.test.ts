import 'reflect-metadata';
import { CloudsData, CloudDataInterface } from './clouds-data';
import { CoordData, CoordDataInterface } from './coord-data';
import { MainData, MainDataInterface } from './main-data';
import { SysData, SysDataInterface } from './sys-data';
import { WeatherData, WeatherDataInterface } from './weather-data';
import { WeatherFeatureUtil } from './weather-feature-util';
import { WindData, WindDataInterface } from './wind-data';

const clouds: CloudsData = new CloudsData(
  JSON.parse('{"all":33}') as CloudDataInterface,
);
const coord: CoordData = new CoordData(
  JSON.parse('{"lon":-0.13,"lat":51.51}') as CoordDataInterface,
);
const main: MainData = new MainData(
  JSON.parse(
    '{"temp":276.76,"feels_like":269.56,"temp_min":275.37,"temp_max":278.15,"pressure":1020,"humidity":57}',
  ) as MainDataInterface,
);
const sys: SysData = new SysData(
  JSON.parse(
    '{"type":1,"id":1414,"country":"GB","sunrise":1582182365,"sunset":1582219373}',
  ) as SysDataInterface,
  0,
);
const weather: WeatherData = new WeatherData(
  JSON.parse(
    '{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}',
  ) as WeatherDataInterface,
);
const wind: WindData = new WindData(
  JSON.parse('{"speed":6.7,"deg":250}') as WindDataInterface,
);

describe('WeatherFeatureUtil', () => {
  test('deals with time zones correctly', () => {
    const timeZoneOffset = 5.75 * 3600; // Nepal's awkward time zone
    const localDate = WeatherFeatureUtil.getDateWithTimeZoneOffset(
      3600,
      timeZoneOffset,
    ); // 1970-01-01 01:00am UTC

    expect(localDate).not.toBe(undefined);
    expect(localDate?.getUTCFullYear()).toBe(1970);
    expect(localDate?.getUTCMonth()).toBe(0);
    expect(localDate?.getUTCDate()).toBe(1);
    expect(localDate?.getUTCHours()).toBe(6);
    expect(localDate?.getUTCMinutes()).toBe(45);
  });

  const dates: { date: Date; text: string }[] = [
    {
      date: new Date(2020, 1, 20, 22, 35, 57),
      text: 'on 20th February 2020 at 22:35',
    },
    {
      date: new Date(2020, 1, 21, 22, 35, 57),
      text: 'on 21st February 2020 at 22:35',
    },
    {
      date: new Date(2020, 1, 22, 22, 35, 57),
      text: 'on 22nd February 2020 at 22:35',
    },
    {
      date: new Date(2020, 1, 23, 22, 35, 57),
      text: 'on 23rd February 2020 at 22:35',
    },
    {
      date: new Date(2020, 1, 11, 22, 35, 57),
      text: 'on 11th February 2020 at 22:35',
    },
    {
      date: new Date(2020, 1, 12, 22, 35, 57),
      text: 'on 12th February 2020 at 22:35',
    },
    {
      date: new Date(2020, 1, 13, 22, 35, 57),
      text: 'on 13th February 2020 at 22:35',
    },
  ];

  dates.forEach((item) =>
    test(`Date and time is formatted correctly: ${item.text}`, () =>
      expect(WeatherFeatureUtil.formatDateAndTime(item.date)).toBe(item.text)),
  );

  test('Date and time is formatted correctly', () => {
    const result = WeatherFeatureUtil.formatDateAndTime(
      new Date(2020, 1, 20, 22, 35, 57),
    );
    expect(result).toBe('on 20th February 2020 at 22:35');
  });

  test('Coords are formatted correctly', () => {
    const result = WeatherFeatureUtil.getCoords(coord);
    expect(result).toBe('lat 51.51°, lon -0.13°');
  });

  test('Cloud cover is formatted correctly', () => {
    const result = WeatherFeatureUtil.getCloudCover(clouds);
    expect(result).toBe('33%');
  });

  test('Description is formatted correctly', () => {
    const result = WeatherFeatureUtil.getDescription(weather);
    expect(result).toBe('Scattered clouds');
  });

  test('Feels like temp is formatted correctly', () => {
    const result = WeatherFeatureUtil.getFeelsLike(main);
    expect(result).toBe('-4°C');
  });

  test('Humidity is formatted correctly', () => {
    const result = WeatherFeatureUtil.getHumidity(main);
    expect(result).toBe('57%');
  });

  test('Location is formatted correctly', () => {
    const result = WeatherFeatureUtil.getLocation('London', sys);
    expect(result).toBe('London, GB');
  });

  test('Pressure is formatted correctly', () => {
    const result = WeatherFeatureUtil.getPressure(main);
    expect(result).toBe('1020mb');
  });

  test('Sunrise is formatted correctly', () => {
    const result = WeatherFeatureUtil.getSunrise(sys);
    expect(result).toBe('07:06');
  });

  test('Sunset is formatted correctly', () => {
    const result = WeatherFeatureUtil.getSunset(sys);
    expect(result).toBe('17:22');
  });

  test('Temperature is formatted correctly', () => {
    const result = WeatherFeatureUtil.getTemp(main);
    expect(result).toBe('3.6°C');
  });

  test('Wind description is formatted correctly', () => {
    const result = WeatherFeatureUtil.getWindDescription(wind);
    expect(result).toBe('a light westerly wind');
  });

  test('Wind direction is formatted correctly', () => {
    const result = WeatherFeatureUtil.getWindDirection(wind);
    expect(result).toBe('westerly 250°');
  });

  test('Wind speed is formatted correctly', () => {
    const result = WeatherFeatureUtil.getWindSpeed(wind);
    expect(result).toBe('24km/h');
  });
});
