import { WeatherFeatureUtil } from './weather-feature-util';

export interface SysDataInterface {
  readonly country?: string;
  readonly id?: number;
  readonly sunrise?: number;
  readonly sunset?: number;
  readonly type?: number;
}

export class SysData {
  public readonly country?: string;
  public readonly id?: number;
  public readonly sunrise?: Date;
  public readonly sunset?: Date;
  public readonly type?: number;

  public constructor(json: SysDataInterface, tz?: number) {
    this.country = json.country;
    this.id = json.id;
    this.type = json.type;
    this.sunrise = WeatherFeatureUtil.getDateWithTimeZoneOffset(
      json.sunrise,
      tz,
    );
    this.sunset = WeatherFeatureUtil.getDateWithTimeZoneOffset(json.sunset, tz);
  }
}

export interface ISysJSON extends JSON {
  readonly sunrise?: number;
  readonly sunset?: number;
}
