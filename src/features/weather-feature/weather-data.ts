export interface WeatherDataInterface {
  readonly description?: string;
  readonly icon?: string;
  readonly id?: number;
  readonly main?: string;
}

export class WeatherData {
  public readonly description?: string;
  public readonly icon?: string;
  public readonly id?: number;
  public readonly main?: string;

  public constructor(json: WeatherDataInterface) {
    this.description = json.description;
    this.icon = json.icon;
    this.id = json.id;
    this.main = json.main;
  }
}
