export interface CloudDataInterface {
  readonly all?: number;
}

export class CloudsData {
  public readonly all?: number;

  public constructor(json: CloudDataInterface) {
    this.all = json.all;
  }
}
