import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import {
  CurrentWeatherData,
  CurrentWeatherDataInterface,
} from './current-weather-data';
import { WeatherFeatureUtil } from './weather-feature-util';
import { getRandomInt } from '../../util/general/random-extensions';
import { toString } from '../../util/general/string-extensions';

/**
 * @description Requests weather for a particular city and optional two-letter
 * country code. Input formats:
 * g weather London
 * g weather Rome, IT
 * g weather more New York
 */
export class WeatherMessageHandler extends FeatureMessageHandler {
  private static readonly APIKeyVariableName: string = 'OPEN_WEATHER_API_KEY';

  // Match strings similar to 'g weather [more] London' or 'g weather [more] Rome, XX',
  // where XX is an optional two-letter country code.
  private readonly isWeatherRegExp = new RegExp(
    /^[Ww]eather\s+(more\s+)?[\w\s\-']+(,\s*\w\w)?\W*$/,
  );
  private readonly moreRegExp = new RegExp(/^[Ww]eather\s+more\s+/);
  private readonly weatherReplaceRegExp = new RegExp(
    /^[Ww]eather\s+(more\s+)?/,
  );

  private readonly url: string =
    'http://api.openweathermap.org/data/2.5/weather?q=';
  private readonly appId: string = '&APPID=';

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.isMessageForGradbot()) {
      const messageText: string = this.getSanitisedText();

      if (messageText.match(this.isWeatherRegExp) !== null) {
        const more: boolean = messageText.match(this.moreRegExp) !== null;
        const params: string[] = messageText
          .replace(this.weatherReplaceRegExp, '')
          .trim()
          .split(',');

        const reqLocation: string = params.join(',');
        const result = await this.fetchAPI<JSON>(
          this.url +
            reqLocation +
            this.appId +
            toString(process.env[WeatherMessageHandler.APIKeyVariableName]),
        );

        let replyText = '';
        let reason = `${this.message.user.displayName} asked me for the weather`;

        if (result === null) {
          const replyTexts = [
            `I couldn't get the weather for ${reqLocation}`,
            `Sorry, I can't find ${reqLocation} in my atlas.`,
            `I dropped my crystal ball and it broke, so I can't see the weather in ${reqLocation}.`,
          ];
          replyText = replyTexts[getRandomInt(replyTexts.length)];
          reason += ` at ${reqLocation}, but the API returned no data.`;
        } else {
          const currentWeather: CurrentWeatherData = new CurrentWeatherData(
            result as CurrentWeatherDataInterface,
          );
          const main = currentWeather.main;
          const sys = currentWeather.sys;

          // Get the city and country - use the request string by default.
          let location = WeatherFeatureUtil.getLocation(
            currentWeather.name,
            sys,
          );
          if (location === undefined) {
            location = reqLocation;
          }

          let description = '';
          const weatherArray = currentWeather.weather;
          if (weatherArray !== undefined && weatherArray.length > 0) {
            description = WeatherFeatureUtil.getDescription(weatherArray[0]);
          }

          const coords = WeatherFeatureUtil.getCoords(currentWeather.coord);
          const feelsLike = WeatherFeatureUtil.getFeelsLike(main);
          const humidity = WeatherFeatureUtil.getHumidity(main);
          const temp = WeatherFeatureUtil.getTemp(main);

          if (more) {
            const cloudCover = WeatherFeatureUtil.getCloudCover(
              currentWeather.clouds,
            );
            const curDt = WeatherFeatureUtil.formatDateAndTime(
              currentWeather.dt,
            );
            const pressure = WeatherFeatureUtil.getPressure(main);
            const sunrise = WeatherFeatureUtil.getSunrise(sys);
            const sunset = WeatherFeatureUtil.getSunset(sys);

            const wind = currentWeather.wind;
            const windDir = WeatherFeatureUtil.getWindDirection(wind);
            const windSpeed = WeatherFeatureUtil.getWindSpeed(wind);

            replyText =
              `Detailed weather for ${location} (${coords}) ${curDt} local time:\n` +
              `General: ${description}\n` +
              `Temperature: ${temp}\n` +
              `Humidity: ${humidity}\n` +
              `Feels like: ${feelsLike}\n` +
              `Wind: ${toString(windDir)}\n` +
              `Speed: ${toString(windSpeed)}\n` +
              `Pressure: ${pressure}\n` +
              `Cloud cover: ${cloudCover}\n` +
              `Sunrise: ${sunrise}\n` +
              `Sunset: ${sunset}`;
          } else if (description === '') {
            // Don't try to construct the short response if we don't have the
            // description.
            replyText = `I don't know what the weather is like in ${location}, my blinds are closed.`;
          } else {
            const windDesc = WeatherFeatureUtil.getWindDescription(
              currentWeather.wind,
            );

            replyText =
              `Here's the latest weather for ${location}:\n` +
              `${description}, with ${windDesc}.\n` +
              `The temperature is ${temp} with ${humidity} humidity, making it feel like ${feelsLike}.`;
          }

          reason += ` at ${location} (${coords}).`;
        }

        return this.createMessage(replyText, reason);
      }
    }
    return null;
  }
}
