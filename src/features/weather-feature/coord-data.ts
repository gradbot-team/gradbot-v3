export interface CoordDataInterface {
  readonly lat?: number;
  readonly lon?: number;
}

export class CoordData {
  public readonly lat?: number;
  public readonly lon?: number;

  public constructor(json: CoordDataInterface) {
    this.lat = json.lat;
    this.lon = json.lon;
  }
}
