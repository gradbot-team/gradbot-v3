import { CloudDataInterface, CloudsData } from './clouds-data';
import { CoordData, CoordDataInterface } from './coord-data';
import { MainData, MainDataInterface } from './main-data';
import { SysData, SysDataInterface } from './sys-data';
import { WeatherData, WeatherDataInterface } from './weather-data';
import { WeatherFeatureUtil } from './weather-feature-util';
import { WindData, WindDataInterface } from './wind-data';

export interface CurrentWeatherDataInterface {
  readonly base?: string;
  readonly cod?: number;
  readonly dt?: number;
  readonly id?: number;
  readonly name?: string | undefined;
  readonly timezone?: number;
  readonly visibility?: number;
  readonly weather?: WeatherDataInterface[];
  readonly coord?: CoordDataInterface;
  readonly clouds?: CloudDataInterface;
  readonly main?: MainDataInterface;
  readonly sys?: SysDataInterface;
  readonly wind?: WindDataInterface;
}

export class CurrentWeatherData {
  public readonly base?: string;
  public readonly cod?: number;
  public readonly dt?: Date;
  public readonly id?: number;
  public readonly name?: string;
  public readonly timezone?: number;
  public readonly visibility?: number;

  public readonly clouds?: CloudsData;
  public readonly coord?: CoordData;
  public readonly main?: MainData;
  public readonly sys?: SysData;
  public readonly wind?: WindData;
  public readonly weather?: WeatherData[];

  public constructor(json: CurrentWeatherDataInterface) {
    this.dt = WeatherFeatureUtil.getDateWithTimeZoneOffset(
      json.dt,
      this.timezone,
    );

    this.base = json.base;
    this.cod = json.cod;
    this.id = json.id;
    this.name = json.name;
    this.timezone = json.timezone;
    this.visibility = json.visibility;

    this.clouds = json.clouds ? new CloudsData(json.clouds) : undefined;
    this.coord = json.coord ? new CoordData(json.coord) : undefined;
    this.main = json.main ? new MainData(json.main) : undefined;
    this.sys = json.sys ? new SysData(json.sys, this.timezone) : undefined;
    this.wind = json.wind ? new WindData(json.wind) : undefined;
    if (json.weather !== undefined && json.weather.length > 0) {
      this.weather = [];
      for (const item of json.weather) {
        this.weather.push(new WeatherData(item));
      }
    }
  }
}
