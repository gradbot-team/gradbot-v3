import 'reflect-metadata';
import { WeatherFeature } from './weather-feature';
import {
  CurrentWeatherData,
  CurrentWeatherDataInterface,
} from './current-weather-data';
import { CloudsData, CloudDataInterface } from './clouds-data';
import { CoordData, CoordDataInterface } from './coord-data';
import { MainData, MainDataInterface } from './main-data';
import { SysData, SysDataInterface } from './sys-data';
import { WeatherData, WeatherDataInterface } from './weather-data';
import { WindData, WindDataInterface } from './wind-data';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(WeatherFeature);

const apiKeyVariableName = 'OPEN_WEATHER_API_KEY';
const apiKey = 'test_api_key';

// mock environmental variable
process.env[apiKeyVariableName] = apiKey;

const cloudsJson = '{"all":33}';
const coordJson = '{"lon":-0.13,"lat":51.51}';
const mainJson =
  '{"temp":276.76,"feels_like":269.56,"temp_min":275.37,"temp_max":278.15,"pressure":1020,"humidity":57}';
const sysJson =
  '{"type":1,"id":1414,"country":"GB","sunrise":1582182365,"sunset":1582219373}';
const weatherJson =
  '{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}';
const windJson = '{"speed":6.7,"deg":250}';
const currentWeatherJson = `{
  "coord":${coordJson},
  "weather":[${weatherJson}],
  "base":"stations",
  "main":${mainJson},
  "visibility":10000,
  "wind":${windJson},
  "clouds":${cloudsJson},
  "dt":1582238157,
  "sys":${sysJson},
  "timezone":0,
  "id":2643743,
  "name":"London",
  "cod":200
}`;

const currentWeatherUndefinedJson = `{
  "coord":${coordJson},
  "base":"stations",
  "main":${mainJson},
  "visibility":10000,
  "wind":${windJson},
  "clouds":${cloudsJson},
  "dt":1582238157,
  "sys":${sysJson},
  "timezone":0,
  "id":2643743,
  "name":"London",
  "cod":200
}`;

/**
 * @todo Test handling of input message formats. Do this when the feature is
 * rewritten to handle "forecast" and "more" requests.
 */
describe('WeatherFeature', () => {
  const reasonText =
    'User asked me for the weather at London, GB (lat 51.51°, lon -0.13°).';

  test('Feature does not respond to test message', async () =>
    util.expectReplyToBeNull('gradbot, test'));

  test('Feature handles missing API data', async () => {
    const replyTexts = [
      "I couldn't get the weather for Nowheresville",
      "Sorry, I can't find Nowheresville in my atlas.",
      "I dropped my crystal ball and it broke, so I can't see the weather in Nowheresville.",
    ];

    util.api.urlReturnValues[
      `http://api.openweathermap.org/data/2.5/weather?q=Nowheresville&APPID=${apiKey}`
    ] = null;

    const reply = await util.getReplyForMessage('g weather Nowheresville');
    expect(reply).not.toBe(null);
    if (reply !== null) {
      expect(replyTexts).toContain(reply.text);
      expect(reply.reason).toBe(
        'User asked me for the weather at Nowheresville, but the API returned no data.',
      );
    }
  });

  test('Feature handles incomplete API data', async () => {
    const json: CurrentWeatherData = new CurrentWeatherData(
      JSON.parse(currentWeatherUndefinedJson) as CurrentWeatherDataInterface,
    );
    util.api.urlReturnValues[
      `http://api.openweathermap.org/data/2.5/weather?q=London&APPID=${apiKey}`
    ] = json;

    const reply = await util.getReplyForMessage('g weather London');
    expect(reply).not.toBe(null);
    if (reply !== null) {
      expect(reply.text).toBe(
        "I don't know what the weather is like in London, GB, my blinds are closed.",
      );
      expect(reply.reason).toBe(reasonText);
    }
  });

  test('Feature builds replies from a successful API request', async () => {
    util.api.urlReturnValues[
      `http://api.openweathermap.org/data/2.5/weather?q=London&APPID=${apiKey}`
    ] = JSON.parse(currentWeatherJson) as JSON;

    const shortReply = await util.getReplyForMessage('g weather London');
    expect(shortReply).not.toBe(null); // linter doesn't like expect(shortReply).not.toBeNull
    if (shortReply !== null) {
      expect(shortReply.text).toMatch(
        new RegExp(/^Here's the latest weather for London, GB:\n.+\n.+$/),
      );
      expect(shortReply.reason).toBe(reasonText);
    }

    const longReply = await util.getReplyForMessage('g weather more London');
    expect(longReply).not.toBe(null);
    if (longReply !== null) {
      expect(longReply.text).toMatch(
        new RegExp(/^Detailed weather for London, GB(.+\n){10}.+$/),
      );
      expect(longReply.reason).toBe(reasonText);
    }
  });
});

describe('WeatherFeature JSON deserialization', () => {
  test('JSON "clouds" block is deserialized correctly', () => {
    const clouds = new CloudsData(JSON.parse(cloudsJson) as CloudDataInterface);

    expect(clouds.all).toBe(33);
  });

  test('JSON "coord" block is deserialized correctly', () => {
    const coord = new CoordData(JSON.parse(coordJson) as CoordDataInterface);

    expect(coord.lat).toBe(51.51);
    expect(coord.lon).toBe(-0.13);
  });

  test('JSON "main" block is deserialized correctly', () => {
    const main = new MainData(JSON.parse(mainJson) as MainDataInterface);

    const feelsLike = expect(main.feelsLike).toEqual(expect.any(Number));
    if (feelsLike !== undefined) {
      expect(feelsLike).toBeCloseTo(-3.59);
    }
    expect(main.humidity).toBe(57);
    expect(main.pressure).toBe(1020);
    const temp = expect(main.temp).toEqual(expect.any(Number));
    if (temp !== undefined) {
      expect(temp).toBeCloseTo(3.61);
    }
    const tempMax = expect(main.tempMax).toEqual(expect.any(Number));
    if (tempMax !== undefined) {
      expect(tempMax).toBeCloseTo(5.0);
    }
    const tempMin = expect(main.tempMin).toEqual(expect.any(Number));
    if (tempMin !== undefined) {
      expect(tempMin).toBeCloseTo(2.22);
    }
  });

  test('JSON "sys" block is deserialized correctly', () => {
    const sys = new SysData(JSON.parse(sysJson) as SysDataInterface, 0);

    expect(sys.country).toBe('GB');
    expect(sys.id).toBe(1414);
    expect(sys.sunrise).toStrictEqual(new Date(2020, 1, 20, 7, 6, 5));
    expect(sys.sunset).toStrictEqual(new Date(2020, 1, 20, 17, 22, 53));
    expect(sys.type).toBe(1);
  });

  test('Items in the JSON "weather" block are serialized correctly', () => {
    const weather = new WeatherData(
      JSON.parse(weatherJson) as WeatherDataInterface,
    );

    expect(weather.description).toBe('scattered clouds');
    expect(weather.icon).toBe('03n');
    expect(weather.id).toBe(802);
    expect(weather.main).toBe('Clouds');
  });

  test('JSON "wind" block is deserialized correctly', () => {
    const wind = new WindData(JSON.parse(windJson) as WindDataInterface);

    expect(wind.deg).toBe(250);
    expect(wind.speed).toBe(6.7);
  });

  test('JSON for current weather is deserialized correctly', () => {
    const currentWeatherData = new CurrentWeatherData(
      JSON.parse(currentWeatherJson) as CurrentWeatherDataInterface,
    );

    expect(currentWeatherData.base).toBe('stations');
    expect(currentWeatherData.cod).toBe(200);
    expect(currentWeatherData.dt).toStrictEqual(
      new Date(2020, 1, 20, 22, 35, 57),
    );
    expect(currentWeatherData.id).toBe(2643743);
    expect(currentWeatherData.name).toBe('London');
    expect(currentWeatherData.timezone).toBe(0);

    expect(currentWeatherData.weather).toBeDefined();
    if (currentWeatherData.weather) {
      expect(currentWeatherData.weather.length).toBe(1);
    }
  });
});
