/**
 * @description Provides a generic object for deserialization of JSON data.
 */
export abstract class WeatherBaseData<T extends JSON> {
  public constructor(json: T) {
    return Object.assign(this, json);
  }
}
