export interface WindDataInterface {
  readonly deg?: number;
  readonly speed?: number;
}

export class WindData {
  public readonly deg?: number;
  public readonly speed?: number;

  public constructor(json: WindDataInterface) {
    this.deg = json.deg;
    this.speed = json.speed;
  }
}
