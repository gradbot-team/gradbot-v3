import { CloudsData } from './clouds-data';
import { CoordData } from './coord-data';
import { MainData } from './main-data';
import { SysData } from './sys-data';
import { WeatherData } from './weather-data';
import { WindData } from './wind-data';
import { getOrdinalNumberSuffix } from '../../util/general/ordinals';
import { or } from '../../util/general/string-extensions';

export class WeatherFeatureUtil {
  /**
   * @description Provides a hacky way to handle time zones without having to
   * manually mess around with calendar data. Use the Date.getUTC...() getters
   * to retrieve components of the date/time in the original local time zone.
   * @param unixTime The unix time in seconds. If undefined, the function will
   * return undefined.
   * @param timeZoneOffset The current timezone offset at the original location.
   * Defaults to 0 if undefined.
   */
  public static getDateWithTimeZoneOffset(
    unixTime: number | undefined,
    timeZoneOffset: number | undefined,
  ) {
    return unixTime
      ? new Date((unixTime + (timeZoneOffset ? timeZoneOffset : 0)) * 1000)
      : undefined;
  }

  /**
   * @description Get a nice normal string for the date and time.
   */
  public static formatDateAndTime(date: Date | undefined): string {
    let dt = '';
    if (date !== undefined) {
      // Use the UTC getters because we've accounted for the time zone offset
      // already. It's hacky but saves us having to handle calendar info like
      // days per month, leap years, etc. manually.
      const hour = date.getUTCHours();
      const min = date.getUTCMinutes();
      const day = date.getUTCDate();
      const month = WeatherFeatureUtil.months[date.getUTCMonth()];
      const year = date.getUTCFullYear();

      dt = `on ${day}${or(
        getOrdinalNumberSuffix(day),
        '',
      )} ${month} ${year} at ${hour}:${min.toString().padStart(2, '0')}`;
    }

    return dt;
  }

  /** @description Get the geographic coordinates of the city in the API response. */
  public static getCoords(coord: CoordData | undefined): string {
    let coords = '{unknown}';
    if (
      coord !== undefined &&
      coord.lat !== undefined &&
      coord.lon !== undefined
    ) {
      coords = `lat ${coord.lat}°, lon ${coord.lon}°`;
    }
    return coords;
  }

  /** @description Get the percentage cloud cover. */
  public static getCloudCover(clouds: CloudsData | undefined): string {
    let cloudCover = '{unknown}';
    if (clouds !== undefined && clouds.all !== undefined) {
      cloudCover = `${clouds.all}%`;
    }
    return cloudCover;
  }

  /** @description Get a general description of the weather conditions. */
  public static getDescription(weather: WeatherData | undefined): string {
    let description = '';
    if (weather !== undefined) {
      const desc = weather.description;
      if (desc !== undefined && desc.length > 0) {
        description = desc[0].toUpperCase() + desc.substring(1, desc.length);
      }
    }
    return description;
  }

  /** @description Get the feels-like temperature in Celsius. */
  public static getFeelsLike(main: MainData | undefined): string {
    let feelsLike = 'scorchio!';
    if (main !== undefined && main.feelsLike !== undefined) {
      feelsLike = `${Math.round(main.feelsLike)}°C`;
    }
    return feelsLike;
  }

  /** @description Get the percentage humidity. */
  public static getHumidity(main: MainData | undefined): string {
    let humidity = 'swamp-arse inducing';
    if (main !== undefined && main.humidity !== undefined) {
      humidity = `${main.humidity}%`;
    }
    return humidity;
  }

  /** @description Get the city and country code in the format "City, CC" */
  public static getLocation(
    name: string | undefined,
    sys: SysData | undefined,
  ): string | undefined {
    let location: string | undefined;
    if (name !== undefined) {
      location = name;
      if (sys !== undefined && sys.country !== undefined) {
        location += ', ' + sys.country;
      }
    }
    return location;
  }

  /** @description Get the air pressure in mb. */
  public static getPressure(main: MainData | undefined): string {
    let pressure = '{unknown}';
    if (main !== undefined && main.pressure !== undefined) {
      pressure = `${main.pressure}mb`;
    }
    return pressure;
  }

  /** @description Get the time of today's sunrise. */
  public static getSunrise(sys: SysData | undefined): string {
    let sunrise = '{unknown}';
    if (sys !== undefined && sys.sunrise !== undefined) {
      sunrise = WeatherFeatureUtil.getHoursAndMinutesString(sys.sunrise);
    }
    return sunrise;
  }

  /** @description Get the time of today's sunset. */
  public static getSunset(sys: SysData | undefined): string {
    let sunset = '{unknown}';
    if (sys !== undefined && sys.sunset !== undefined) {
      sunset = WeatherFeatureUtil.getHoursAndMinutesString(sys.sunset);
    }
    return sunset;
  }

  /** @description Get the temperature in Celsius to 1d.p. */
  public static getTemp(main: MainData | undefined): string {
    let temp = "maftin'";
    if (main !== undefined && main.temp !== undefined) {
      temp = `${main.temp.toFixed(1)}°C`;
    }
    return temp;
  }

  /**
   * @description Gets a plain-English description of the wind direction and strength
   */
  public static getWindDescription(wind: WindData | undefined): string {
    let desc =
      "some wind, probably, but I'm looking out of my window so I can't tell.";
    if (wind !== undefined) {
      const direction: string = wind.deg
        ? WeatherFeatureUtil.getWindDirectionDescription(wind.deg) + ' '
        : '';

      // Translate wind speed in m/s into pseudo Beaufort or Saffir-Simpson
      // scale descriptors.
      const speed = wind.speed;
      if (speed !== undefined) {
        if (speed <= 1.5) {
          desc = 'no noticeable wind';
        } else if (speed > 1.5 && speed <= 3.3) {
          desc = `a light ${direction}breeze`;
        } else if (speed > 3.3 && speed <= 5.5) {
          desc = `a gentle ${direction}breeze`;
        } else if (speed > 5.5 && speed <= 8.0) {
          desc = `a light ${direction}wind`;
        } else if (speed > 8.0 && speed <= 10.8) {
          desc = `a moderate ${direction}wind`;
        } else if (speed > 10.8 && speed <= 13.9) {
          desc = `a strong ${direction}wind`;
        } else if (speed > 13.9 && speed <= 17.2) {
          desc = `very strong ${direction}winds`;
        } else if (speed > 17.2 && speed <= 20.7) {
          desc = `${direction}gales`;
        } else if (speed > 20.7 && speed <= 24.5) {
          desc = `strong ${direction}gales`;
        } else if (speed > 24.5 && speed <= 28.4) {
          desc = `storm force ${direction}winds`;
        } else if (speed > 28.4 && speed <= 32.6) {
          desc = `violent storm force ${direction}winds`;
        } else if (speed > 32.6 && speed <= 42) {
          desc = `category-1 hurricane force ${direction}winds`;
        } else if (speed > 42 && speed <= 49) {
          desc = `category-2 hurricane force ${direction}winds`;
        } else if (speed > 49 && speed <= 58) {
          desc = `category-3 major hurricane force ${direction}winds`;
        } else if (speed > 58 && speed <= 70) {
          desc = `category-4 major hurricane force ${direction}winds`;
        } else if (speed > 70) {
          desc = `category-5 major hurricane force ${direction}winds`;
        }
      } else if (direction !== '') {
        desc = `${direction}winds`;
      }
      // If we have neither speed nor direction, fall back to the default.
    }

    return desc;
  }

  /** @description Get the wind direction with bearing. */
  public static getWindDirection(wind: WindData | undefined): string {
    let direction = '{unknown}';
    if (wind !== undefined && wind.deg !== undefined) {
      direction = `${this.getWindDirectionDescription(wind.deg)} ${wind.deg}°`;
    }
    return direction;
  }

  /** @description Get the wind speed in km/h. */
  public static getWindSpeed(wind: WindData | undefined) {
    let speed = '{unknown}';
    if (wind !== undefined && wind.speed !== undefined) {
      speed = `${((wind.speed * 3600) / 1000).toFixed(0)}km/h`;
      return speed;
    }
  }

  private static months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  private static getHoursAndMinutesString(date: Date): string {
    return `${date.getHours().toString().padStart(2, '0')}:${date
      .getMinutes()
      .toString()
      .padStart(2, '0')}`;
  }

  /**
   * @description Gets a plain-English description of the wind direction.
   */
  private static getWindDirectionDescription(bearing: number): string {
    let direction = '';

    if (bearing > 22.5 && bearing <= 67.5) {
      direction = 'northeasterly';
    } else if (bearing > 67.5 && bearing <= 112.5) {
      direction = 'easterly';
    } else if (bearing > 112.5 && bearing <= 157.5) {
      direction = 'southeasterly';
    } else if (bearing > 157.5 && bearing <= 202.5) {
      direction = 'southerly';
    } else if (bearing > 202.5 && bearing <= 247.5) {
      direction = 'southwesterly';
    } else if (bearing > 247.5 && bearing <= 292.5) {
      direction = 'westerly';
    } else if (bearing > 292.5 && bearing <= 337.5) {
      direction = 'northwesterly';
    } else if (
      (bearing > 337.5 && bearing <= 360) ||
      (bearing >= 0 && bearing <= 22.5)
    ) {
      direction = 'northerly';
    }

    return direction;
  }
}
