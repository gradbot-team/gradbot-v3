export interface MainDataInterface {
  readonly feels_like?: number;
  readonly humidity?: number;
  readonly pressure?: number;
  readonly temp?: number;
  readonly temp_max?: number;
  readonly temp_min?: number;
}

export class MainData {
  /**
   * @description Convert a temperature in Kelvin to Celsius.
   */
  private static kelvinToCelsius(temp: number | undefined): number | undefined {
    return temp ? temp - 273.15 : undefined;
  }

  public readonly feelsLike?: number;
  public readonly humidity?: number;
  public readonly pressure?: number;
  public readonly temp?: number;
  public readonly tempMax?: number;
  public readonly tempMin?: number;

  public constructor(json: MainDataInterface) {
    this.humidity = json.humidity;
    this.pressure = json.pressure;
    if (json.feels_like !== undefined) {
      this.feelsLike = MainData.kelvinToCelsius(json.feels_like);
    }
    if (json.temp !== undefined) {
      this.temp = MainData.kelvinToCelsius(json.temp);
    }
    if (json.temp_max !== undefined) {
      this.tempMax = MainData.kelvinToCelsius(json.temp_max);
    }
    if (json.temp_min !== undefined) {
      this.tempMin = MainData.kelvinToCelsius(json.temp_min);
    }
  }
}
