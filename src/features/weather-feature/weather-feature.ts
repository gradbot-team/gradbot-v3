import { noIndent } from '../../util/general/string-extensions';
import { Feature } from '../../gradbot/feature/feature';
import { WeatherMessageHandler } from './weather-message-handler';

export class WeatherFeature extends Feature {
  public readonly name = 'Weather';
  public readonly iCanInfo = 'tell you the weather';
  public readonly instructions = noIndent(
    `• *g weather London*
     • *g weather Rome, IT*
     • *g weather more New York* – more detailed`,
  );
  protected readonly messageHandlerType = WeatherMessageHandler;
}
