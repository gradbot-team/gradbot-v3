import { Feature } from '../../gradbot/feature/feature';
import { MemeMessageHandler } from './meme-message-handler';

export class MemeFeature extends Feature {
  public readonly name = 'Meme';
  public readonly iCanInfo = 'send memes';
  public readonly instructions = '• *g meme*\n• *g send memes*';
  protected readonly messageHandlerType = MemeMessageHandler;
}
