import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';

export class MemeMessageHandler extends FeatureMessageHandler {
  private readonly MEME_REGEX = /^(send\W+)?(a\W+)?(meme)s?\W*$/;
  private readonly MEME_API_URL = 'https://meme-api.herokuapp.com/gimme';

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.matchSanitisedLowercaseTextIfForGradbot(this.MEME_REGEX)) {
      const data = await this.fetchAPI<MemeAPIData>(this.MEME_API_URL);

      if (data && data.url) {
        const image = await this.getImageFromPath(data.url);

        if (image) {
          return this.createMessage(
            '',
            `${this.message.user.displayName} wanted a meme`,
          ).withImage(image);
        }
      }

      return this.createMessage(
        'My meme guy is unavailable right now',
        `${this.message.user.displayName} wanted a meme, but API is down`,
      );
    }
    return null;
  }
}

interface MemeAPIData {
  url?: string;
}
