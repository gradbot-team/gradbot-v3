import 'reflect-metadata';
import { MemeFeature } from './meme-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';

const util = FeatureTestUtil.createForFeatureType(MemeFeature);

const setValidAPIResponse = () =>
  (util.api.urlReturnValues['https://meme-api.herokuapp.com/gimme'] = {
    url: 'test',
  });

describe('MemeFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('gradbot, test'));

  test('responds with no text to "meme"', async () => {
    setValidAPIResponse();
    await util.expectReplyToNotHaveText('Gradbot, meme');
  });

  test('responds with no text to "send a meme"', async () => {
    setValidAPIResponse();
    await util.expectReplyToNotHaveText('Gradbot, send a meme');
  });

  test('responds with no text to "send memes"', async () => {
    setValidAPIResponse();
    await util.expectReplyToNotHaveText('Gradbot, send memes');
  });

  test('responds with one image', async () => {
    setValidAPIResponse();
    await util.expectReplyToHaveImage('g meme');
  });

  test('deals with invalid API data', async () => {
    util.api.urlReturnValues['https://meme-api.herokuapp.com/gimme'] = {
      wrong_property: 'test',
    };
    await util.expectReplyToHaveText(
      'Gradbot, meme',
      'My meme guy is unavailable right now',
    );
  });

  test('deals with missing API data', async () => {
    util.api.urlReturnValues['https://meme-api.herokuapp.com/gimme'] = null;
    await util.expectReplyToHaveText(
      'Gradbot, meme',
      'My meme guy is unavailable right now',
    );
  });
});
