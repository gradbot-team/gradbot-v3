import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { FeatureTimeout } from '../../gradbot/constants/constants';
import { Fen } from './fen';
import { ChessBoard } from './chess-board';
import { ChessPiece } from './chess-piece';
import { ChessKing } from './chess-king';
import { ChessQueen } from './chess-queen';
import { ChessBishop } from './chess-bishop';
import { ChessKnight } from './chess-knight';
import { ChessRook } from './chess-rook';
import { ChessPawn } from './chess-pawn';
import { ChessEmpty } from './chess-empty';
import { ChessConstants } from './chess-constants';
import { ChessData } from './chess-data';

export class ChessMessageHandler extends FeatureMessageHandler {
  // Chess image 'API' but not really, let's see how long it lasts
  private readonly CHESS_IMAGE_URL = 'https://chessboardimage.com/';
  private readonly CHESS_IMAGE_TIMEOUT = FeatureTimeout / 2;

  private readonly dataRetriever = new ChessData(this.message);

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (this.isMessageForGradbot()) {
      const messageText = this.getSanitisedText();
      // Work out which command we're dealing with...
      const tokens = messageText.split(' ');
      if (tokens[0] !== 'chess' || tokens.length < 2) {
        return null;
      }
      if (tokens[1] === 'help') {
        return this.showHelp();
      }

      const fen = await this.getFenFromDatabase();

      if (tokens[1] === 'start') {
        return this.startGame(fen);
      }
      if (tokens[1] === 'resign') {
        return this.resign(fen);
      }
      if (tokens[1] === 'show') {
        return this.showGame(fen);
      }
      if (tokens[1] === 'fen') {
        return this.showFen(fen);
      }

      // Otherwise try and parse this as a move.
      return this.makeMove(tokens[1], fen);
    }
    return null;
  }

  private async getDbRecord() {
    const data = await this.dataRetriever.retrieveAlike();
    return data[0];
  }

  private async getFenFromDatabase(): Promise<string | undefined> {
    const record = await this.getDbRecord();

    if (record) {
      return record.fen.get();
    }

    return;
  }

  private async writeFenToDatabase(fen: string): Promise<void> {
    await this.deleteGame();
    const chessData = new ChessData(this.message);
    chessData.fen.set(fen);
    await chessData.write();
  }

  private async deleteGame(): Promise<void> {
    const record = await this.getDbRecord();

    if (record) {
      await record.delete();
    }
  }

  private createNewFen(): Fen {
    // Returns a new game FEN string.
    return this.readFen(
      'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1',
    );
  }

  private readFen(rawFen: string): Fen {
    const tokens = rawFen.split(' ');
    const player =
      tokens[1] === ChessConstants.FenWhite
        ? ChessConstants.PlayerWhite
        : ChessConstants.PlayerBlack;
    return new Fen(tokens[0], player, rawFen);
  }

  private getGridPosition(x: number, y: number) {
    const rank = ChessConstants.Ranks[y];
    const file = ChessConstants.Files[7 - x];
    return rank + file;
  }

  private createFenForApiFromBoard(board: ChessBoard): string {
    // We only need everything up to the first space.
    let fen = this.createFenFromBoard(board);
    fen = fen.split(' ')[0];
    fen = fen.replace(/\//g, '');
    return fen;
  }

  private createFenFromBoard(board: ChessBoard): string {
    // Do the board string
    let boardString = '';
    let blankCounter = 0;
    for (const [i, row] of board.boardState.entries()) {
      for (const [
        {
          /* j*/
        },
        piece,
      ] of row.entries()) {
        if (piece.character === ChessConstants.EmptySpace) {
          blankCounter += 1;
        } else {
          if (blankCounter > 0) {
            boardString += blankCounter;
            blankCounter = 0;
          }
          boardString += piece.character;
        }
      }
      if (blankCounter > 0) {
        boardString += blankCounter;
        blankCounter = 0;
      }
      if (i !== 7) {
        boardString += '/';
      }
    }

    // Switch the board player
    const player = board.isWhiteTurn
      ? ChessConstants.FenBlack
      : ChessConstants.FenWhite;

    // Maintain castles as stored in board
    const castles = board.castles;

    // Enpassant target needs translating
    let enpassantSquare = ChessConstants.FenBlank;
    if (board.enpassantSquare !== null) {
      enpassantSquare = this.getGridPosition(
        board.enpassantSquare[0],
        board.enpassantSquare[1],
      );
    }

    // Maintain halfmove clock
    const halfmoveClock = board.halfmoveClock;

    // Maintain fullmove counter
    const fullmoveCounter = board.fullmoveCounter;

    const fenString = `${boardString} ${player} ${castles} ${enpassantSquare} ${halfmoveClock} ${fullmoveCounter}`;
    return fenString;
  }

  private getIndicesFromPosition(position: string): [number, number] | null {
    // Assumption - valid position or '-' only
    if (position === ChessConstants.EmptySpace) {
      return null;
    }
    const file = position[0].charCodeAt(0) - 97; // charcode of 'a' is 65
    const rank = 8 - parseInt(position[1], 10);
    return [rank, file];
  }

  private getChessPiece(character: string, x: number, y: number): ChessPiece {
    if (/[rR]/.test(character)) {
      return new ChessRook(x, y, character);
    } else if (/[nN]/.test(character)) {
      return new ChessKnight(x, y, character);
    } else if (/[bB]/.test(character)) {
      return new ChessBishop(x, y, character);
    } else if (/[qQ]/.test(character)) {
      return new ChessQueen(x, y, character);
    } else if (/[kK]/.test(character)) {
      return new ChessKing(x, y, character);
    } else if (/[pP]/.test(character)) {
      return new ChessPawn(x, y, character);
    } else {
      return new ChessEmpty(x, y, character);
    }
  }

  private populateBoardFromFen(fenObject: Fen): ChessBoard {
    // Print the FEN using full-width characters.
    // Assumes well-formed FEN as input.
    // Example FEN: rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
    const tokens = fenObject.rawFen.split(' ');
    const rows = tokens[0].split('/');
    const boardState: ChessPiece[][] = [];
    for (const [i, row] of rows.entries()) {
      boardState[i] = [];
      let realJ = 0;
      for (const [
        {
          /* j*/
        },
        character,
      ] of row.split('').entries()) {
        // Love those nested loops
        if (/[a-zA-Z]/.test(character)) {
          boardState[i][realJ] = this.getChessPiece(character, i, realJ);
        }
        if (/[0-9]/.test(character)) {
          // Replace it with that many full-width dashes
          const numDashes = parseInt(character, 10);
          for (let dashNum = 0; dashNum < numDashes; dashNum++) {
            boardState[i][realJ + dashNum] = this.getChessPiece(
              ChessConstants.EmptySpace,
              i,
              realJ + dashNum,
            );
          }
          realJ += numDashes - 1;
        }
        realJ += 1;
      }
    }
    const isWhiteTurn = tokens[1] === ChessConstants.FenWhite;
    const castles = tokens[2];
    const enpassantTarget = this.getIndicesFromPosition(tokens[3]);
    const halfmoveClock = parseInt(tokens[4], 10);
    const fullmoveCounter = parseInt(tokens[5], 10);
    return new ChessBoard(
      boardState,
      isWhiteTurn,
      enpassantTarget,
      halfmoveClock,
      fullmoveCounter,
      castles,
    );
  }

  private getFullwidthCharacter(character: string) {
    return String.fromCharCode(character.charCodeAt(0) + 0xfee0);
  }

  private getBoardString(boardObject: ChessBoard): string {
    // Print the board using full-width characters.
    let boardString = '';
    for (let i = 0; i < 8; i++) {
      for (let j = 0; j < 8; j++) {
        let character = '';
        if (boardObject.boardState[i][j]) {
          character = this.getFullwidthCharacter(
            boardObject.boardState[i][j].character,
          );
        } else {
          character = String.fromCharCode(0xff0d); // Full-width dash
        }
        boardString += character;
      }
      // Print the rank number at the end of the rank
      boardString += this.getFullwidthCharacter(`${8 - i}`);
      boardString += '\n';
    }
    // Print the file letters
    for (const char of ChessConstants.Ranks) {
      boardString += this.getFullwidthCharacter(char);
    }
    boardString += '\n';
    return boardString;
  }

  private castlingEligibilityCheck(
    board: ChessBoard,
    movePiece: ChessPiece,
    capturedPiece: ChessPiece,
  ): void {
    // Assumption - called on your turn as part of makeMove

    // If last move was non-castle, see if move was king or rook
    // If king, remove all castling eligibility for this side
    let castlingString = board.castles;

    if (movePiece.name === ChessConstants.King) {
      if (board.isWhiteTurn) {
        castlingString = castlingString.replace(
          ChessConstants.WhiteKingsideCastleIndicator,
          '',
        );
        castlingString = castlingString.replace(
          ChessConstants.WhiteQueensideCastleIndicator,
          '',
        );
      } else {
        castlingString = castlingString.replace(
          ChessConstants.BlackKingsideCastleIndicator,
          '',
        );
        castlingString = castlingString.replace(
          ChessConstants.BlackQueensideCastleIndicator,
          '',
        );
      }
    }

    // If rook, remove the relevant eligibility for this side.
    // Only works on their first move, but it only has to work
    // once.
    if (movePiece.name === ChessConstants.Rook) {
      castlingString = this.castlingEligibilityCheckHelper(
        castlingString,
        movePiece,
        board.isWhiteTurn,
      );
    }

    // If a rook was captured, also remove eligibility for the opponent,
    // assuming the rook is at their home position. If not,
    // eligibility was already removed.
    if (capturedPiece && capturedPiece.name === ChessConstants.Rook) {
      castlingString = this.castlingEligibilityCheckHelper(
        castlingString,
        capturedPiece,
        !board.isWhiteTurn,
      );
    }

    // If we removed the last eligibility indicator, change it to '-'
    if (castlingString === '') {
      castlingString = ChessConstants.FenBlank;
    }

    board.castles = castlingString;
  }

  private castlingEligibilityCheckHelper(
    castlingString: string,
    piece: ChessPiece,
    whiteTurn: boolean,
  ): string {
    // Remove king/queen depending on their file
    let removeKingEligibility = false;
    let removeQueenEligibility = false;
    if (piece.y === 0) {
      removeQueenEligibility = true;
    }
    if (piece.y === 7) {
      removeKingEligibility = true;
    }
    // We remove for the other side.
    if (whiteTurn) {
      if (removeKingEligibility) {
        castlingString = castlingString.replace(
          ChessConstants.WhiteKingsideCastleIndicator,
          '',
        );
      }
      if (removeQueenEligibility) {
        castlingString = castlingString.replace(
          ChessConstants.WhiteQueensideCastleIndicator,
          '',
        );
      }
    } else {
      if (removeKingEligibility) {
        castlingString = castlingString.replace(
          ChessConstants.BlackKingsideCastleIndicator,
          '',
        );
      }
      if (removeQueenEligibility) {
        castlingString = castlingString.replace(
          ChessConstants.BlackQueensideCastleIndicator,
          '',
        );
      }
    }
    return castlingString;
  }

  private performCheckCheck(board: ChessBoard): boolean {
    const enemyPieces: ChessPiece[] = [];
    let king: ChessPiece | null = null;

    // Different strategy for gathering pieces with one fewer nested loop
    for (const row of board.boardState) {
      for (const piece of row) {
        if (
          board.isWhiteTurn !== piece.isWhite &&
          piece.character !== ChessConstants.EmptySpace
        ) {
          enemyPieces.push(piece);
        }
        if (
          board.isWhiteTurn === piece.isWhite &&
          piece.name === ChessConstants.King
        ) {
          king = piece;
        }
      }
    }

    if (!king) {
      // Err... Well, I'm sure there's a good reason for this.
      return false;
    }

    // Go through every possible move and see if the king is capturable
    for (const enemyPiece of enemyPieces) {
      for (const enemyMove of enemyPiece.validMoves(board)) {
        if (enemyMove[1][0] === king.x && enemyMove[1][1] === king.y) {
          return true;
        }
      }
    }
    return false;
  }

  private performCheckmateCheck(board: ChessBoard): boolean {
    // Step 1: Check for enemy check
    const storedFenString: string = this.createFenFromBoard(board);
    const storedFen: Fen = this.readFen(storedFenString);
    const storedBoard = this.populateBoardFromFen(storedFen);
    storedBoard.isWhiteTurn = !board.isWhiteTurn;
    if (!this.performCheckCheck(storedBoard)) {
      return false; // Can't be checkmated if you're not in check!
    }

    // If they're in check and are also in a stalemate situation,
    // they're checkmated.
    return this.performStalemateCheck(board);
  }

  private performStalemateCheck(board: ChessBoard): boolean {
    let playerStalemated = true;
    const moves: [string, [number, number]][] = [];
    const storedFenString: string = this.createFenFromBoard(board);
    const storedFen: Fen = this.readFen(storedFenString);
    // Step 1: Loop through opponent's valid moves (a bit expensive?)
    for (const row of board.boardState) {
      for (const potentialPiece of row) {
        if (potentialPiece.isWhite !== board.isWhiteTurn) {
          for (const potentialMove of potentialPiece.validMoves(board)) {
            // Only consider pieces belong to the current player.
            moves.push(potentialMove);
          }
        }
      }
    }
    // Step 2: Remove moves that cause check
    for (const move of moves) {
      const storedBoard = this.populateBoardFromFen(storedFen);
      storedBoard.isWhiteTurn = !board.isWhiteTurn;
      const [failedMove] = this.performMove(storedBoard, move[0]);
      if (!failedMove) {
        playerStalemated = false;
        break;
      }
    }
    // Step 3: If no moves exist after this, end game.
    return playerStalemated;
  }

  private performMove(
    board: ChessBoard,
    move: string,
  ): [
    boolean, // move not valid
    string, // reason not valid
    string, // Gradbot explanation
    [string, [number, number]], // moveObject
    ChessPiece, // Moved piece
    ChessPiece,
  ] {
    // Captured piece
    // Helper for when erroring out with board state change
    const errorMoveValue: [string, [number, number]] = ['', [0, 0]];
    const errorBoardStateValue = board.boardState[0][0];

    // Loop through all the living pieces, find an algebraic move match
    // and then attempt to perform that move
    const potentialPieces: ChessPiece[] = [];
    const potentialMoves: [string, [number, number]][] = [];

    for (const row of board.boardState) {
      for (const potentialPiece of row) {
        if (potentialPiece.isWhite === board.isWhiteTurn) {
          for (const potentialMove of potentialPiece.validMoves(board)) {
            // Only consider pieces belong to the current player.
            if (potentialMove[0] === move) {
              potentialPieces.push(potentialPiece);
              potentialMoves.push(potentialMove);
            }
          }
        }
      }
    }

    // Found move check: Confirm at least one piece exists that matches the move.
    if (potentialPieces.length === 0) {
      return [
        true,
        `${move} is not a valid move.`,
        'I rejected to make a move because it was invalid.',
        errorMoveValue,
        errorBoardStateValue,
        errorBoardStateValue,
      ];
    }

    // Unique move check: Confirm this move doesn't match multiple pieces
    if (potentialPieces.length > 1) {
      return [
        true,
        'That is an ambiguous move, please be more specific.',
        'I rejected to make a move because it was ambiguous.',
        errorMoveValue,
        errorBoardStateValue,
        errorBoardStateValue,
      ];
    }

    const piece: ChessPiece = potentialPieces[0];
    const moveObject: [string, [number, number]] = potentialMoves[0]; // All moves will be equivalent

    // Do move
    // Step 1: Remove piece from old location
    //         This works for most cases including capture
    board.boardState[piece.x][piece.y] = this.getChessPiece(
      ChessConstants.EmptySpace,
      piece.x,
      piece.y,
    );
    // Step 2: Put piece in new location
    let targetPiece = piece.character;
    // Step 2.5: Account for pawn promotion
    if (moveObject[0].includes(ChessConstants.Promote)) {
      // Target promotion is the last character of the move
      targetPiece = moveObject[0][moveObject[0].length - 1];
      // Don't give the wrong player the piece..!
      if (board.isWhiteTurn) {
        targetPiece = targetPiece.toUpperCase();
      } else {
        targetPiece = targetPiece.toLowerCase();
      }
    }
    const capturedPiece = board.boardState[moveObject[1][0]][moveObject[1][1]];
    board.boardState[moveObject[1][0]][moveObject[1][1]] = this.getChessPiece(
      targetPiece,
      moveObject[1][0],
      moveObject[1][1],
    );

    // Enpassant capture
    // If move was a pawn capture and the capture square is the enpassant square,
    // we need to remove the pawn in front of it.
    if (
      piece.name === ChessConstants.Pawn &&
      moveObject[0].includes(ChessConstants.Capture) &&
      board.enpassantSquare
    ) {
      if (
        moveObject[1][0] === board.enpassantSquare[0] &&
        moveObject[1][1] === board.enpassantSquare[1]
      ) {
        if (board.isWhiteTurn) {
          // Pawn to remove is 'down' the board
          board.boardState[moveObject[1][0] + 1][moveObject[1][1]] =
            this.getChessPiece(
              ChessConstants.EmptySpace,
              moveObject[1][0] + 1,
              moveObject[1][1],
            );
        } else {
          // Pawn to remove is 'up' the board
          board.boardState[moveObject[1][0] - 1][moveObject[1][1]] =
            this.getChessPiece(
              ChessConstants.EmptySpace,
              moveObject[1][0] - 1,
              moveObject[1][1],
            );
        }
      }
    }

    // Castling
    // See if the last move was a castle
    // If castle, king will be positioned correctly, so move the relevant rook to be beside the king
    // Kingside
    if (moveObject[0] === ChessConstants.KingsideCastle) {
      const [targetRank, targetFile] = moveObject[1];
      // Move the rook in file 8 (idx:7) to the position left of the king
      board.boardState[targetRank][targetFile - 1] =
        board.boardState[targetRank][7];
      board.boardState[targetRank][7] = this.getChessPiece(
        ChessConstants.EmptySpace,
        targetRank,
        7,
      );
    }

    // Queenside
    if (moveObject[0] === ChessConstants.QueensideCastle) {
      const [targetRank, targetFile] = moveObject[1];
      // Move the rook in file 1 (idx:0) to the position right of the king
      board.boardState[targetRank][targetFile + 1] =
        board.boardState[targetRank][0];
      board.boardState[targetRank][0] = this.getChessPiece(
        ChessConstants.EmptySpace,
        targetRank,
        0,
      );
    }

    // Check check: Does this move cause or not end check against the current player?
    // Loop through enemy pieces, error out if any could capture king
    const selfInCheck = this.performCheckCheck(board);

    if (selfInCheck) {
      return [
        true,
        `${moveObject[0]} causes or persists check against yourself.`,
        'I rejected to make a move because it caused or persisted check.',
        ['', [0, 0]],
        board.boardState[0][0],
        board.boardState[0][0],
      ];
    }

    return [false, '', '', moveObject, piece, capturedPiece];
  }

  private async createBoardMessage(
    boardObject: ChessBoard,
    message: string,
    explainer: string,
  ): Promise<FeatureMessage> {
    const chessImageUrl =
      this.CHESS_IMAGE_URL +
      this.createFenForApiFromBoard(boardObject) +
      '.png';
    const image = await this.getImageFromPath(
      chessImageUrl,
      this.CHESS_IMAGE_TIMEOUT,
    ); // Timeout fairly quickly.

    if (image) {
      return this.createMessage(`${message}`, `${explainer}`).withImage(image);
    }

    // Image generation failed for some reason, fallback to using the strings.
    const boardString = this.getBoardString(boardObject);
    return this.createMessage(
      `${boardString}
${message}`,
      `${explainer}`,
    );
  }

  private async resign(rawFen?: string): Promise<FeatureMessage> {
    // Check if a game already exists
    if (!rawFen) {
      return this.createMessage(
        'There is no ongoing game to resign from.',
        'I rejected a resignation as no chess match is ongoing.',
      );
    }

    // Get the current player
    const fen: Fen = this.readFen(rawFen);
    const player = fen.currentPlayer;

    // Delete the game
    await this.deleteGame();

    // Show the game resign message
    return this.createMessage(
      `${player} resigns.`,
      'I ended a chess game at request of resignation.',
    );
  }

  private async makeMove(
    move: string,
    rawFen?: string,
  ): Promise<FeatureMessage> {
    // Check if a game already exists
    if (!rawFen) {
      return this.createMessage(
        "A game is not currently running. Use 'chess start' to start a game.",
        'I rejected to make a move because no chess match was ongoing.',
      );
    }

    // Get the current game state
    const fen: Fen = this.readFen(rawFen);
    const board: ChessBoard = this.populateBoardFromFen(fen);
    const player = fen.currentPlayer;

    // Backup the board in case we need to roll it back (like in case of check)
    // Very lazy 'deep' copy to get the board state
    const preMoveBoard: ChessBoard = Object.create(board) as ChessBoard;
    preMoveBoard.boardState = board.boardState.slice();
    for (let i = 0; i < 8; i++) {
      preMoveBoard.boardState[i] = board.boardState[i].slice();
    }

    const [
      moveFailed,
      failureReason,
      gradbotText,
      moveObject,
      movePiece,
      capturedPiece,
    ] = this.performMove(board, move);

    if (moveFailed) {
      return await this.createBoardMessage(
        preMoveBoard,
        failureReason,
        gradbotText,
      );
    }

    // Check for checkmate (first, before stalemate, because programming)
    const enemyCheckmated = this.performCheckmateCheck(board);
    if (enemyCheckmated) {
      await this.deleteGame(); // Game over!
      return await this.createBoardMessage(
        board,
        `${movePiece.name} to ${movePiece.getGridPosition(
          moveObject[1][0],
          moveObject[1][1],
        )}. Checkmate, ${player} wins.`,
        gradbotText,
      );
    }

    // Check for stalemate
    const enemyInStalemate = this.performStalemateCheck(board);
    if (enemyInStalemate) {
      await this.deleteGame(); // Game over man.
      return await this.createBoardMessage(
        board,
        `${movePiece.name} to ${movePiece.getGridPosition(
          moveObject[1][0],
          moveObject[1][1],
        )}. Stalemate, draw.`,
        gradbotText,
      );
    }

    // Castling eligibility
    this.castlingEligibilityCheck(board, movePiece, capturedPiece);

    // Game isn't over, increment counts and update the board state
    // Reset halfturn clock if pawn moved or a capture was done
    if (
      movePiece.name === ChessConstants.Pawn ||
      moveObject[0].includes(ChessConstants.Capture)
    ) {
      board.halfmoveClock = 0;
    } else {
      board.halfmoveClock += 1;
    }

    // Increment fullmove counter if black just moved
    if (!board.isWhiteTurn) {
      board.fullmoveCounter += 1;
    }

    // Set the enpassant indices if a pawn just did a double move
    if (
      movePiece.name === ChessConstants.Pawn &&
      moveObject[0].includes('4') &&
      movePiece.x === 6 &&
      board.isWhiteTurn
    ) {
      board.enpassantSquare = [movePiece.x - 1, movePiece.y];
    } else if (
      movePiece.name === ChessConstants.Pawn &&
      moveObject[0].includes('5') &&
      movePiece.x === 1 &&
      !board.isWhiteTurn
    ) {
      board.enpassantSquare = [movePiece.x + 1, movePiece.y];
    } else {
      board.enpassantSquare = null;
    }

    // Generate FEN for new position
    const newFen: string = this.createFenFromBoard(board);

    // Store FEN in DB
    await this.writeFenToDatabase(newFen);

    // Report outcome
    const position = movePiece.getGridPosition(
      moveObject[1][0],
      moveObject[1][1],
    );

    return await this.createBoardMessage(
      board,
      `${movePiece.name} to ${position}. Your move, ${
        board.isWhiteTurn
          ? ChessConstants.PlayerBlack
          : ChessConstants.PlayerWhite
      }.`,
      'I accepted a move.',
    );
  }

  private async showGame(rawFen?: string): Promise<FeatureMessage> {
    // Check if a game already exists
    if (!rawFen) {
      return this.createMessage(
        'There is no ongoing game to show.',
        'I rejected to show a game as no chess match was ongoing.',
      );
    }

    // Get the current player
    const fen: Fen = this.readFen(rawFen);
    const player = fen.currentPlayer;

    // Get the board to show
    const boardObject: ChessBoard = this.populateBoardFromFen(fen);

    // Show the game message
    return await this.createBoardMessage(
      boardObject,
      `It is currently ${player}'s turn.`,
      'I showed the ongoing chess game.',
    );
  }

  private showFen(rawFen?: string): FeatureMessage {
    // Check if a game already exists
    if (!rawFen) {
      return this.createMessage(
        'There is no ongoing game to show.',
        'I rejected to show a game as no chess match was ongoing.',
      );
    }

    // Get the current player
    const fen: Fen = this.readFen(rawFen);

    // Show the game message
    return this.createMessage(
      `${fen.rawFen}`,
      'I showed the FEN of the ongoing chess game.',
    );
  }

  private async startGame(rawFen?: string): Promise<FeatureMessage> {
    // Check if a game already exists
    if (rawFen) {
      return this.createMessage(
        "A game is already running. Use 'chess resign' to quit.",
        'I rejected a new chess match as one was already running.',
      );
    }

    // Create game
    const newFen = this.createNewFen();
    await this.writeFenToDatabase(newFen.rawFen);

    // Show the game start message
    const boardObject: ChessBoard = this.populateBoardFromFen(newFen);

    return await this.createBoardMessage(
      boardObject,
      "A new game has started. It is White's turn.",
      'I started a new chess game.',
    );
  }

  private showHelp(): FeatureMessage {
    return this.createMessage(
      this.feature.instructions,
      'I helped someone learn the commands for chess.',
    );
  }
}
