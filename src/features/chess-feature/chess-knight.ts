import { ChessPiece } from './chess-piece';
import { ChessBoard } from './chess-board';
import { ChessConstants } from './chess-constants';

export class ChessKnight extends ChessPiece {
  public name: string = ChessConstants.Knight;

  public validMoves(boardObject: ChessBoard): [string, [number, number]][] {
    // Knights move in L shapes. They have possible eight moves:
    // all possible combinations of two moves in one direction, and one move at a right-angle.
    let moves: [string, [number, number]][] = [];

    moves = moves.concat(
      this.validKnightMovesHelper(boardObject, [2, -2], [1, -1]),
    );

    moves = moves.concat(
      this.validKnightMovesHelper(boardObject, [1, -1], [2, -2]),
    );
    return moves;
  }

  private validKnightMovesHelper(
    boardObject: ChessBoard,
    vertMoves: [number, number],
    horiMoves: [number, number],
  ): [string, [number, number]][] {
    let moves: [string, [number, number]][] = [];

    for (const vertMove of vertMoves) {
      for (const horiMove of horiMoves) {
        const targetRank = this.x + vertMove;
        const targetFile = this.y + horiMove;
        if (
          targetRank >= 0 &&
          targetRank < 8 &&
          targetFile >= 0 &&
          targetFile < 8
        ) {
          const piece = boardObject.getPieceAt(targetRank, targetFile);

          // You can collapse this into a single if, but I'm not going to.
          if (piece && piece.isWhite !== this.isWhite) {
            moves = moves.concat(
              this.buildVariantMoves(targetRank, targetFile, true),
            );
          } else if (!piece) {
            moves = moves.concat(
              this.buildVariantMoves(targetRank, targetFile, false),
            );
          }
        }
      }
    }

    return moves;
  }
}
