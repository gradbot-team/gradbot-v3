import { ChessPiece } from './chess-piece';
import { ChessBoard } from './chess-board';

export class ChessEmpty extends ChessPiece {
  public name = 'Empty';

  public validMoves({}: /* board*/ ChessBoard): [string, [number, number]][] {
    return [];
  }
}
