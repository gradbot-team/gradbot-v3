import 'reflect-metadata';
import { ChessFeature } from './chess-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import { ChessData } from './chess-data';
import { getTestMessage } from '../../util/test/get-test-message';

const util = FeatureTestUtil.createForFeatureType(ChessFeature);

const setReturnImage = (returnImage: boolean) =>
  (util.imageGenerator.returnImage = returnImage);

const zerothTurn = `ｒｎｂｑｋｂｎｒ８
ｐｐｐｐｐｐｐｐ７
－－－－－－－－６
－－－－－－－－５
－－－－－－－－４
－－－－－－－－３
ＰＰＰＰＰＰＰＰ２
ＲＮＢＱＫＢＮＲ１
ａｂｃｄｅｆｇｈ

A new game has started. It is White's turn.`;

const whitesTurn = `ｒｎｂｑｋｂｎｒ８
ｐｐｐｐｐｐｐｐ７
－－－－－－－－６
－－－－－－－－５
－－－－－－－－４
－－－－－－－－３
ＰＰＰＰＰＰＰＰ２
ＲＮＢＱＫＢＮＲ１
ａｂｃｄｅｆｇｈ

It is currently White's turn.`;

const helpText = `Available commands:
• *g chess start* – start a new game
• *g chess <move>* – makes the move
• *g chess show* – displays the board state
• *g chess resign* – quits the current game`;

const deleteDb = async (): Promise<void> => {
  const data = await new ChessData(getTestMessage()).retrieveAlike();
  if (data[0]) {
    await data[0].delete();
  }
};

const writeDb = async (s: string): Promise<void> => {
  await deleteDb();

  const data = new ChessData(getTestMessage());
  data.fen.set(s);
  await data.write();
};

const readDb = async (): Promise<string | null> => {
  const data = await new ChessData(getTestMessage()).retrieveAlike();

  if (data[0]) {
    return data[0].fen.get();
  }

  return null;
};

describe('ChessFeature', () => {
  afterEach(async () => {
    // By default we want the text response
    setReturnImage(false);
    // Hacky - resign from any running game to clear the DB.
    await deleteDb();
  });

  test('does not show the game state when requested if no game is running', async () => {
    setReturnImage(false);
    await util.expectReplyToHaveText(
      'gradbot, chess show',
      'There is no ongoing game to show.',
    );
  });

  test('starts game between two new players', async () =>
    await util.expectReplyToHaveText('gradbot, chess start', zerothTurn));

  test('starts game between two new players - image test', async () => {
    // This is also the only image test - not much point in having others.
    setReturnImage(true);
    await util.expectReplyToHaveImage('gradbot, chess start');
  });

  test('rejects new game if existing game between two players is running', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    await util.expectReplyToHaveText(
      'gradbot, chess start',
      "A game is already running. Use 'chess resign' to quit.",
    );
  });

  test('rejects a move if a game is not running', async () =>
    util.expectReplyToHaveText(
      'gradbot, chess e5',
      "A game is not currently running. Use 'chess start' to start a game.",
    ));

  test('shows the game state when requested', async () => {
    await util.getReplyForMessage('gradbot, chess start');
    await util.expectReplyToHaveText('gradbot, chess show', whitesTurn);
  });

  test('shows the available commands', async () =>
    util.expectReplyToHaveText('gradbot, chess help', helpText));

  test('allows both players to make a move in their game', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    await util.expectReplyToHaveText(
      'gradbot, chess e4',
      `ｒｎｂｑｋｂｎｒ８
ｐｐｐｐｐｐｐｐ７
－－－－－－－－６
－－－－－－－－５
－－－－Ｐ－－－４
－－－－－－－－３
ＰＰＰＰ－ＰＰＰ２
ＲＮＢＱＫＢＮＲ１
ａｂｃｄｅｆｇｈ

Pawn to e4. Your move, Black.`,
    );

    await util.expectReplyToHaveText(
      'gradbot, chess e5',
      `ｒｎｂｑｋｂｎｒ８
ｐｐｐｐ－ｐｐｐ７
－－－－－－－－６
－－－－ｐ－－－５
－－－－Ｐ－－－４
－－－－－－－－３
ＰＰＰＰ－ＰＰＰ２
ＲＮＢＱＫＢＮＲ１
ａｂｃｄｅｆｇｈ

Pawn to e5. Your move, White.`,
    );
  });

  test('disallows illegal moves', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    const featureResponseWhiteMove =
      await util.getReplyForMessage('gradbot, chess e2');

    expect(featureResponseWhiteMove).toHaveProperty(
      'text',
      `ｒｎｂｑｋｂｎｒ８
ｐｐｐｐｐｐｐｐ７
－－－－－－－－６
－－－－－－－－５
－－－－－－－－４
－－－－－－－－３
ＰＰＰＰＰＰＰＰ２
ＲＮＢＱＫＢＮＲ１
ａｂｃｄｅｆｇｈ

e2 is not a valid move.`,
    );

    await util.getReplyForMessage('gradbot, chess resign');
    expect(await readDb()).toBeNull();
  });

  test('ends game on checkmate, deletes game record', async () => {
    await writeDb('4K3/8/4k3/8/8/q7/8/8 b - - 0 0');

    const featureResponse = await util.getReplyForMessage('gradbot, chess Qe7');

    expect(featureResponse).toHaveProperty(
      'text',
      `－－－－Ｋ－－－８
－－－－ｑ－－－７
－－－－ｋ－－－６
－－－－－－－－５
－－－－－－－－４
－－－－－－－－３
－－－－－－－－２
－－－－－－－－１
ａｂｃｄｅｆｇｈ

Queen to e7. Checkmate, Black wins.`,
    );

    // DB check...
    expect(await readDb()).toBeNull();
  });

  test('ends game on stalemate, deletes game record', async () => {
    // White
    await writeDb('8/8/8/2R5/k2K4/2R5/8/8 w - - 0 0');

    let featureResponse = await util.getReplyForMessage('gradbot, chess Kc4');

    expect(featureResponse).toHaveProperty(
      'text',
      `－－－－－－－－８
－－－－－－－－７
－－－－－－－－６
－－Ｒ－－－－－５
ｋ－Ｋ－－－－－４
－－Ｒ－－－－－３
－－－－－－－－２
－－－－－－－－１
ａｂｃｄｅｆｇｈ

King to c4. Stalemate, draw.`,
    );

    // DB check...
    expect(await readDb()).toBeNull();

    // Black
    await writeDb('8/8/8/2r5/K2k4/2r5/8/8 b - - 0 0');

    featureResponse = await util.getReplyForMessage('gradbot, chess Kc4');

    expect(featureResponse).toHaveProperty(
      'text',
      `－－－－－－－－８
－－－－－－－－７
－－－－－－－－６
－－ｒ－－－－－５
Ｋ－ｋ－－－－－４
－－ｒ－－－－－３
－－－－－－－－２
－－－－－－－－１
ａｂｃｄｅｆｇｈ

King to c4. Stalemate, draw.`,
    );

    // DB check...
    expect(await readDb()).toBeNull();
  });

  test('allows resignation, deletes game record', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    const featureResponse = await util.getReplyForMessage(
      'gradbot, chess resign',
    );

    expect(featureResponse).toHaveProperty('text', 'White resigns.');
    // TODO: Do a DB check to make sure the game is cleared.
  });

  test('not allow resignation when no game is running', async () => {
    const featureResponse = await util.getReplyForMessage(
      'gradbot, chess resign',
    );

    expect(featureResponse).toHaveProperty(
      'text',
      'There is no ongoing game to resign from.',
    );
    // TODO: Do a DB check to make sure the game is cleared.
  });

  test('allows castling on white, kingside', async () => {
    // Test castling when indicator says kingside is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w KQkq - 0 0');
    let featureResponse = await util.getReplyForMessage('gradbot, chess O-O');
    expect(await readDb()).toEqual(
      'r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R4RK1 b kq - 1 0',
    );

    // Test castling failure when indicator does NOT say kingside is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w Qkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: /O-O is not a valid move./,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces has a piece in it
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3KP1R w KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces is under attack
    await writeDb('r3k2r/pppppppp/8/8/8/5q2/PPPPP1PP/R3K2R w KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when the other intermediate space is under attack
    await writeDb('r3k2r/pppppppp/8/8/8/6q1/PPPPPP1P/R3K2R w KQkq -');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when king is in check
    await writeDb('r3k2r/pppppppp/8/8/7q/8/PPPPP2P/R3K2R w KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });
  });

  test('allows castling on black, kingside', async () => {
    // Test castling when indicator says kingside is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    let featureResponse = await util.getReplyForMessage('gradbot, chess O-O');
    expect(await readDb()).toEqual(
      'r4rk1/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w KQ - 1 1',
    );

    // Test castling failure when indicator does NOT say kingside is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(await readDb()).toEqual(
      'r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQq - 0 0',
    );
    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces has a piece in it
    await writeDb('r3kb1r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces is under attack
    await writeDb('r3k2r/ppppp2p/5R2/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when the other intermediate space is under attack
    await writeDb('r3k2r/ppppp2p/6R1/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when king is in check
    await writeDb('r3k2r/ppppp2p/8/7Q/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });
  });

  test('allows castling on black, queenside', async () => {
    // Test castling when indicator says queen is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    let featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');
    expect(await readDb()).toEqual(
      '2kr3r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w KQ - 1 1',
    );

    // Test castling failure when indicator does NOT say queenside is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQk - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces has a piece in it
    await writeDb('r2pk2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces is under attack
    await writeDb('r3k2r/p3pppp/3R4/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when the other intermediate space is under attack
    await writeDb('r3k2r/p3pppp/2R5/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling SUCCESS when the rook intermediate space is under attack
    await writeDb('r3k2r/p3pppp/1R6/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');
    expect(await readDb()).toEqual(
      '2kr3r/p3pppp/1R6/8/8/8/PPPPPPPP/R3K2R w KQ - 1 1',
    );

    // Test castling failure when king is in check
    await writeDb('r3k2r/p3pppp/8/8/Q7/8/PPPPPPPP/R3K2R b KQkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });
  });

  test('allows castling on white, queenside', async () => {
    // Test castling when indicator says queenside is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w KQkq - 0 0');
    let featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');
    expect(await readDb()).toEqual(
      'r3k2r/pppppppp/8/8/8/8/PPPPPPPP/2KR3R b kq - 1 0',
    );

    // Test castling failure when indicator does NOT say kingside is available
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w Kkq - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces has a piece in it
    await writeDb('r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R2PK2R w KQ - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when one of the intermediate spaces is under attack
    await writeDb('r3k2r/pppppppp/8/8/8/3r4/P3PPPP/R3K2R w KQ - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling failure when the other intermediate space is under attack
    await writeDb('r3k2r/pppppppp/8/8/8/2r5/P3PPPP/R3K2R w KQ - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });

    // Test castling SUCCESS when the rook intermediate space is under attack
    // Bonus test - test that we write '-' when no castling moves exist
    await writeDb('r3k2r/pppppppp/8/8/8/1r6/P3PPPP/R3K2R w KQ - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');
    expect(await readDb()).toEqual(
      'r3k2r/pppppppp/8/8/8/1r6/P3PPPP/2KR3R b - - 1 0',
    );

    // Test castling failure when king is in check
    await writeDb('r3k2r/pppppppp/8/q7/8/8/P3PPPP/R3K2R w KQ - 0 0');
    featureResponse = await util.getReplyForMessage('gradbot, chess O-O-O');

    expect(featureResponse).not.toBeNull();
    expect(featureResponse).toMatchObject({
      _reason: expect.anything() as unknown,
      _text: expect.stringMatching(/O-O-O is not a valid move./) as unknown,
      featureName: expect.anything() as unknown,
    });
  });

  test('allows enpassant', async () => {
    await writeDb(
      'rnbqkbnr/p2ppppp/8/5PP1/1pp5/8/PPPPP2P/RNBQKBNR w KQkq - 0 0',
    );

    // Test both left and right capture for both sides

    // Move white forward + capture with black
    await util.getReplyForMessage('gradbot, chess a4');

    await util.getReplyForMessage('gradbot, chess bxa3');

    expect(await readDb()).toEqual(
      'rnbqkbnr/p2ppppp/8/5PP1/2p5/p7/1PPPP2P/RNBQKBNR w KQkq - 0 1',
    );

    // Repeat
    await util.getReplyForMessage('gradbot, chess d4');

    await util.getReplyForMessage('gradbot, chess cxd3');

    expect(await readDb()).toEqual(
      'rnbqkbnr/p2ppppp/8/5PP1/8/p2p4/1PP1P2P/RNBQKBNR w KQkq - 0 2',
    );

    // Waste white turn
    await util.getReplyForMessage('gradbot, chess e3');

    // Move black forward capture with white
    await util.getReplyForMessage('gradbot, chess e5');

    await util.getReplyForMessage('gradbot, chess fxe6');

    expect(await readDb()).toEqual(
      'rnbqkbnr/p2p1ppp/4P3/6P1/8/p2pP3/1PP4P/RNBQKBNR b KQkq - 0 3',
    );

    // Repeat
    await util.getReplyForMessage('gradbot, chess h5');

    await util.getReplyForMessage('gradbot, chess gxh6');

    expect(await readDb()).toEqual(
      'rnbqkbnr/p2p1pp1/4P2P/8/8/p2pP3/1PP4P/RNBQKBNR b KQkq - 0 4',
    );
  });

  test('allow promotion of pawns', async () => {
    // White
    // Queen
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');

    await util.getReplyForMessage('gradbot, chess d8=Q');

    expect(await readDb()).toEqual(
      'rnbQkbnr/pP3pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQkq - 0 0',
    );

    // Bishop
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess d8=B');
    expect(await readDb()).toEqual(
      'rnbBkbnr/pP3pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQkq - 0 0',
    );

    // Rook
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess d8=R');
    expect(await readDb()).toEqual(
      'rnbRkbnr/pP3pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQkq - 0 0',
    );

    // Knight
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess d8=N');
    expect(await readDb()).toEqual(
      'rnbNkbnr/pP3pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQkq - 0 0',
    );

    // Capture - Queen / Also tests queenside eligibility removal!
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess bxa8=Q');
    expect(await readDb()).toEqual(
      'Qnb1kbnr/p2P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQk - 0 0',
    );

    // Capture - Rook / Also tests queenside eligibility removal!
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess bxa8=R');
    expect(await readDb()).toEqual(
      'Rnb1kbnr/p2P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQk - 0 0',
    );

    // Capture - Bishop / Also tests queenside eligibility removal!
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess bxa8=B');
    expect(await readDb()).toEqual(
      'Bnb1kbnr/p2P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQk - 0 0',
    );

    // Capture - Knight / Also tests queenside eligibility removal!
    await writeDb('rnb1kbnr/pP1P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR w KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess bxa8=N');
    expect(await readDb()).toEqual(
      'Nnb1kbnr/p2P1pp1/7P/8/8/p2pP3/2P4P/RNBQKBNR b KQk - 0 0',
    );

    // Black
    // Capture - Queen
    await writeDb('rnb1kbnr/pPP2pp1/7P/8/8/4P3/K1P2ppP/RNBQ2NR b KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess gxh1=Q');
    expect(await readDb()).toEqual(
      'rnb1kbnr/pPP2pp1/7P/8/8/4P3/K1P2p1P/RNBQ2Nq w Qkq - 0 1',
    );

    // Queen
    await writeDb('rnb1kbnr/pPP2pp1/7P/8/8/4P3/K1P2ppP/RNBQ2NR b KQkq - 0 0');
    await util.getReplyForMessage('gradbot, chess f1=Q');
    expect(await readDb()).toEqual(
      'rnb1kbnr/pPP2pp1/7P/8/8/4P3/K1P3pP/RNBQ1qNR w KQkq - 0 1',
    );
  });

  test('allows promotion of pawns + checkmate', async () => {
    await writeDb('4k3/2P5/4KR2/8/8/8/8/8 w - - 0 0');

    const featureResponse = await util.getReplyForMessage(
      'gradbot, chess c8=Q',
    );

    expect(featureResponse).toHaveProperty(
      'text',
      `－－Ｑ－ｋ－－－８
－－－－－－－－７
－－－－ＫＲ－－６
－－－－－－－－５
－－－－－－－－４
－－－－－－－－３
－－－－－－－－２
－－－－－－－－１
ａｂｃｄｅｆｇｈ

Pawn to c8. Checkmate, White wins.`,
    );
  });

  // This test and the ones that follow are just a replays of famous matches to make sure
  // we haven't missed anything basic. All moves are valid, so these are effectively our
  // valid move checks (instead of writing billions of tests for each piece's valid moves).
  test('allows for the 1992 match between Fischer and Spassky', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    await util.getReplyForMessage('gradbot, chess e4');
    await util.getReplyForMessage('gradbot, chess e5');
    await util.getReplyForMessage('gradbot, chess Nf3');
    await util.getReplyForMessage('gradbot, chess Nc6');
    await util.getReplyForMessage('gradbot, chess Bb5');
    await util.getReplyForMessage('gradbot, chess a6');
    await util.getReplyForMessage('gradbot, chess Ba4');
    await util.getReplyForMessage('gradbot, chess Nf6');
    await util.getReplyForMessage('gradbot, chess O-O');
    await util.getReplyForMessage('gradbot, chess Be7');
    await util.getReplyForMessage('gradbot, chess Re1');
    await util.getReplyForMessage('gradbot, chess b5');
    await util.getReplyForMessage('gradbot, chess Bb3');
    await util.getReplyForMessage('gradbot, chess d6');
    await util.getReplyForMessage('gradbot, chess c3');
    await util.getReplyForMessage('gradbot, chess O-O');
    await util.getReplyForMessage('gradbot, chess h3');
    await util.getReplyForMessage('gradbot, chess Nb8');
    await util.getReplyForMessage('gradbot, chess d4');
    await util.getReplyForMessage('gradbot, chess Nbd7');
    await util.getReplyForMessage('gradbot, chess c4');
    await util.getReplyForMessage('gradbot, chess c6');
    await util.getReplyForMessage('gradbot, chess cxb5');
    await util.getReplyForMessage('gradbot, chess axb5');
    await util.getReplyForMessage('gradbot, chess Nc3');
    await util.getReplyForMessage('gradbot, chess Bb7');
    await util.getReplyForMessage('gradbot, chess Bg5');
    await util.getReplyForMessage('gradbot, chess b4');
    await util.getReplyForMessage('gradbot, chess Nb1');
    await util.getReplyForMessage('gradbot, chess h6');
    await util.getReplyForMessage('gradbot, chess Bh4');
    await util.getReplyForMessage('gradbot, chess c5');
    await util.getReplyForMessage('gradbot, chess dxe5');
    await util.getReplyForMessage('gradbot, chess Nxe4');
    await util.getReplyForMessage('gradbot, chess Bxe7');
    await util.getReplyForMessage('gradbot, chess Qxe7');
    await util.getReplyForMessage('gradbot, chess exd6');
    await util.getReplyForMessage('gradbot, chess Qf6');
    await util.getReplyForMessage('gradbot, chess Nbd2');
    await util.getReplyForMessage('gradbot, chess Nxd6');
    await util.getReplyForMessage('gradbot, chess Nc4');
    await util.getReplyForMessage('gradbot, chess Nxc4');
    await util.getReplyForMessage('gradbot, chess Bxc4');
    await util.getReplyForMessage('gradbot, chess Nb6');
    await util.getReplyForMessage('gradbot, chess Ne5');
    await util.getReplyForMessage('gradbot, chess Rae8');
    await util.getReplyForMessage('gradbot, chess Bxf7');
    await util.getReplyForMessage('gradbot, chess Rxf7');
    await util.getReplyForMessage('gradbot, chess Nxf7');
    await util.getReplyForMessage('gradbot, chess Rxe1');
    await util.getReplyForMessage('gradbot, chess Qxe1');
    await util.getReplyForMessage('gradbot, chess Kxf7');
    await util.getReplyForMessage('gradbot, chess Qe3');
    await util.getReplyForMessage('gradbot, chess Qg5');
    await util.getReplyForMessage('gradbot, chess Qxg5');
    await util.getReplyForMessage('gradbot, chess hxg5');
    await util.getReplyForMessage('gradbot, chess b3');
    await util.getReplyForMessage('gradbot, chess Ke6');
    await util.getReplyForMessage('gradbot, chess a3');
    await util.getReplyForMessage('gradbot, chess Kd6');
    await util.getReplyForMessage('gradbot, chess axb4');
    await util.getReplyForMessage('gradbot, chess cxb4');
    await util.getReplyForMessage('gradbot, chess Ra5');
    await util.getReplyForMessage('gradbot, chess Nd5');
    await util.getReplyForMessage('gradbot, chess f3');
    await util.getReplyForMessage('gradbot, chess Bc8');
    await util.getReplyForMessage('gradbot, chess Kf2');
    await util.getReplyForMessage('gradbot, chess Bf5');
    await util.getReplyForMessage('gradbot, chess Ra7');
    await util.getReplyForMessage('gradbot, chess g6');
    await util.getReplyForMessage('gradbot, chess Ra6');
    await util.getReplyForMessage('gradbot, chess Kc5');
    await util.getReplyForMessage('gradbot, chess Ke1');
    await util.getReplyForMessage('gradbot, chess Nf4');
    await util.getReplyForMessage('gradbot, chess g3');
    await util.getReplyForMessage('gradbot, chess Nxh3');
    await util.getReplyForMessage('gradbot, chess Kd2');
    await util.getReplyForMessage('gradbot, chess Kb5');
    await util.getReplyForMessage('gradbot, chess Rd6');
    await util.getReplyForMessage('gradbot, chess Kc5');
    await util.getReplyForMessage('gradbot, chess Ra6');
    await util.getReplyForMessage('gradbot, chess Nf2');
    await util.getReplyForMessage('gradbot, chess g4');
    await util.getReplyForMessage('gradbot, chess Bd3');
    await util.getReplyForMessage('gradbot, chess Re6');

    expect(await readDb()).toEqual(
      '8/8/4R1p1/2k3p1/1p4P1/1P1b1P2/3K1n2/8 b - - 2 43',
    );
  });

  test('allows for the immortal game', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    await util.getReplyForMessage('gradbot, chess e4');
    await util.getReplyForMessage('gradbot, chess e5');
    await util.getReplyForMessage('gradbot, chess f4');
    await util.getReplyForMessage('gradbot, chess exf4');
    await util.getReplyForMessage('gradbot, chess Bc4');
    await util.getReplyForMessage('gradbot, chess Qh4');
    await util.getReplyForMessage('gradbot, chess Kf1');
    await util.getReplyForMessage('gradbot, chess b5');
    await util.getReplyForMessage('gradbot, chess Bxb5');
    await util.getReplyForMessage('gradbot, chess Nf6');
    await util.getReplyForMessage('gradbot, chess Nf3');
    await util.getReplyForMessage('gradbot, chess Qh6');
    await util.getReplyForMessage('gradbot, chess d3');
    await util.getReplyForMessage('gradbot, chess Nh5');
    await util.getReplyForMessage('gradbot, chess Nh4');
    await util.getReplyForMessage('gradbot, chess Qg5');
    await util.getReplyForMessage('gradbot, chess Nf5');
    await util.getReplyForMessage('gradbot, chess c6');
    await util.getReplyForMessage('gradbot, chess g4');
    await util.getReplyForMessage('gradbot, chess Nf6');
    await util.getReplyForMessage('gradbot, chess Rg1');
    await util.getReplyForMessage('gradbot, chess cxb5');
    await util.getReplyForMessage('gradbot, chess h4');
    await util.getReplyForMessage('gradbot, chess Qg6');
    await util.getReplyForMessage('gradbot, chess h5');
    await util.getReplyForMessage('gradbot, chess Qg5');
    await util.getReplyForMessage('gradbot, chess Qf3');
    await util.getReplyForMessage('gradbot, chess Ng8');
    await util.getReplyForMessage('gradbot, chess Bxf4');
    await util.getReplyForMessage('gradbot, chess Qf6');
    await util.getReplyForMessage('gradbot, chess Nc3');
    await util.getReplyForMessage('gradbot, chess Bc5');
    await util.getReplyForMessage('gradbot, chess Nd5');
    await util.getReplyForMessage('gradbot, chess Qxb2');
    await util.getReplyForMessage('gradbot, chess Bd6');
    await util.getReplyForMessage('gradbot, chess Bxg1');
    await util.getReplyForMessage('gradbot, chess e5');
    await util.getReplyForMessage('gradbot, chess Qxa1');
    await util.getReplyForMessage('gradbot, chess Ke2');
    await util.getReplyForMessage('gradbot, chess Na6');
    await util.getReplyForMessage('gradbot, chess Nxg7');
    await util.getReplyForMessage('gradbot, chess Kd8');
    await util.getReplyForMessage('gradbot, chess Qf6');
    await util.getReplyForMessage('gradbot, chess Nxf6');
    expect(await readDb()).toEqual(
      'r1bk3r/p2p1pNp/n2B1n2/1p1NP2P/6P1/3P4/P1P1K3/q5b1 w - - 0 23',
    ); // Just before checkmate

    await util.getReplyForMessage('gradbot, chess Be7');

    expect(await readDb()).toBeNull(); // Checkmate
  });

  test('allows for the Milov - Tomofeev 2004 match (promotions, queenside castling)', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    await util.getReplyForMessage('gradbot, chess d4');
    await util.getReplyForMessage('gradbot, chess d5');
    await util.getReplyForMessage('gradbot, chess c4');
    await util.getReplyForMessage('gradbot, chess e6');
    await util.getReplyForMessage('gradbot, chess Nc3');
    await util.getReplyForMessage('gradbot, chess c6');
    await util.getReplyForMessage('gradbot, chess e3');
    await util.getReplyForMessage('gradbot, chess Nf6');
    await util.getReplyForMessage('gradbot, chess Nf3');
    await util.getReplyForMessage('gradbot, chess Nbd7');
    await util.getReplyForMessage('gradbot, chess Bd3');
    await util.getReplyForMessage('gradbot, chess dxc4');
    await util.getReplyForMessage('gradbot, chess Bxc4');
    await util.getReplyForMessage('gradbot, chess b5');
    await util.getReplyForMessage('gradbot, chess Be2');
    await util.getReplyForMessage('gradbot, chess Bb7');
    await util.getReplyForMessage('gradbot, chess e4');
    await util.getReplyForMessage('gradbot, chess b4');
    await util.getReplyForMessage('gradbot, chess e5');
    await util.getReplyForMessage('gradbot, chess bxc3');
    await util.getReplyForMessage('gradbot, chess exf6');
    await util.getReplyForMessage('gradbot, chess cxb2');
    await util.getReplyForMessage('gradbot, chess fxg7');
    await util.getReplyForMessage('gradbot, chess bxa1=Q');
    await util.getReplyForMessage('gradbot, chess gxh8=Q');
    await util.getReplyForMessage('gradbot, chess Qa5');
    await util.getReplyForMessage('gradbot, chess Nd2');
    await util.getReplyForMessage('gradbot, chess Q5c3');
    await util.getReplyForMessage('gradbot, chess O-O');
    await util.getReplyForMessage('gradbot, chess Qxd4');
    await util.getReplyForMessage('gradbot, chess Qxh7');
    await util.getReplyForMessage('gradbot, chess Nf6');
    await util.getReplyForMessage('gradbot, chess Qh3');
    await util.getReplyForMessage('gradbot, chess O-O-O');
    await util.getReplyForMessage('gradbot, chess Qc2');
    await util.getReplyForMessage('gradbot, chess Qb4');
    await util.getReplyForMessage('gradbot, chess Nb3');
    await util.getReplyForMessage('gradbot, chess Qe5');
    await util.getReplyForMessage('gradbot, chess Bb2');
    await util.getReplyForMessage('gradbot, chess Qef4');
    await util.getReplyForMessage('gradbot, chess Bc1');
    await util.getReplyForMessage('gradbot, chess Qfe4');
    await util.getReplyForMessage('gradbot, chess Bd3');
    await util.getReplyForMessage('gradbot, chess Qd5');
    await util.getReplyForMessage('gradbot, chess Bd2');
    await util.getReplyForMessage('gradbot, chess Qbd6');
    await util.getReplyForMessage('gradbot, chess Bc4');
    await util.getReplyForMessage('gradbot, chess Qe4');
    await util.getReplyForMessage('gradbot, chess Qc1');
    await util.getReplyForMessage('gradbot, chess Nd5');
    await util.getReplyForMessage('gradbot, chess Re1');
    await util.getReplyForMessage('gradbot, chess Qg6');
    await util.getReplyForMessage('gradbot, chess Na5');
    await util.getReplyForMessage('gradbot, chess Nb6');
    await util.getReplyForMessage('gradbot, chess Nxb7');
    await util.getReplyForMessage('gradbot, chess Kxb7');
    await util.getReplyForMessage('gradbot, chess Be3');
    await util.getReplyForMessage('gradbot, chess Qb4');
    await util.getReplyForMessage('gradbot, chess Bf1');
    await util.getReplyForMessage('gradbot, chess Bg7');
    await util.getReplyForMessage('gradbot, chess Qf3');
    await util.getReplyForMessage('gradbot, chess Nd5');
    await util.getReplyForMessage('gradbot, chess Bc5');
    await util.getReplyForMessage('gradbot, chess Qa5');
    await util.getReplyForMessage('gradbot, chess Qca3');
    await util.getReplyForMessage('gradbot, chess Qxa3');
    await util.getReplyForMessage('gradbot, chess Qxa3');
    await util.getReplyForMessage('gradbot, chess Ra8');
    await util.getReplyForMessage('gradbot, chess Bd6');
    await util.getReplyForMessage('gradbot, chess Kc8');
    await util.getReplyForMessage('gradbot, chess Qa6');
    await util.getReplyForMessage('gradbot, chess Kd8');
    await util.getReplyForMessage('gradbot, chess Qb7');
    await util.getReplyForMessage('gradbot, chess Rc8');
    await util.getReplyForMessage('gradbot, chess Ba6');
    await util.getReplyForMessage('gradbot, chess Rc7');
    await util.getReplyForMessage('gradbot, chess Bxc7');
    await util.getReplyForMessage('gradbot, chess Nxc7');
    await util.getReplyForMessage('gradbot, chess Rd1');
    await util.getReplyForMessage('gradbot, chess Nd5');
    await util.getReplyForMessage('gradbot, chess Qxc6');
    await util.getReplyForMessage('gradbot, chess Qg4');
    await util.getReplyForMessage('gradbot, chess Qc8');
    await util.getReplyForMessage('gradbot, chess Ke7');
    await util.getReplyForMessage('gradbot, chess Rxd5');
    await util.getReplyForMessage('gradbot, chess Qa4');
    expect(await readDb()).toEqual(
      '2Q5/p3kpb1/B3p3/3R4/q7/8/P4PPP/6K1 w - - 1 44',
    ); // Just before checkmate

    await util.getReplyForMessage('gradbot, chess Qd8');

    expect(await readDb()).toBeNull(); // Checkmate
  });

  test('allows for the Carsen - van Wely 2006 match (two white en passant captures)', async () => {
    await util.getReplyForMessage('gradbot, chess start');

    await util.getReplyForMessage('gradbot, chess e4');
    await util.getReplyForMessage('gradbot, chess c5');
    await util.getReplyForMessage('gradbot, chess Nf3');
    await util.getReplyForMessage('gradbot, chess e6');
    await util.getReplyForMessage('gradbot, chess d4');
    await util.getReplyForMessage('gradbot, chess cxd4');
    await util.getReplyForMessage('gradbot, chess Nxd4');
    await util.getReplyForMessage('gradbot, chess a6');
    await util.getReplyForMessage('gradbot, chess Nc3');
    await util.getReplyForMessage('gradbot, chess Qc7');
    await util.getReplyForMessage('gradbot, chess Bd3');
    await util.getReplyForMessage('gradbot, chess Nf6');
    await util.getReplyForMessage('gradbot, chess O-O');
    await util.getReplyForMessage('gradbot, chess Bc5');
    await util.getReplyForMessage('gradbot, chess Nb3');
    await util.getReplyForMessage('gradbot, chess Be7');
    await util.getReplyForMessage('gradbot, chess f4');
    await util.getReplyForMessage('gradbot, chess d6');
    await util.getReplyForMessage('gradbot, chess a4');
    await util.getReplyForMessage('gradbot, chess Nc6');
    await util.getReplyForMessage('gradbot, chess a5');
    await util.getReplyForMessage('gradbot, chess b5');
    await util.getReplyForMessage('gradbot, chess axb6');
    await util.getReplyForMessage('gradbot, chess Qxb6');
    await util.getReplyForMessage('gradbot, chess Kh1');
    await util.getReplyForMessage('gradbot, chess O-O');
    await util.getReplyForMessage('gradbot, chess Qe2');
    await util.getReplyForMessage('gradbot, chess a5');
    await util.getReplyForMessage('gradbot, chess Be3');
    await util.getReplyForMessage('gradbot, chess Qc7');
    await util.getReplyForMessage('gradbot, chess Nb5');
    await util.getReplyForMessage('gradbot, chess Qb8');
    await util.getReplyForMessage('gradbot, chess c3');
    await util.getReplyForMessage('gradbot, chess d5');
    await util.getReplyForMessage('gradbot, chess e5');
    await util.getReplyForMessage('gradbot, chess Ne4');
    await util.getReplyForMessage('gradbot, chess Bxe4');
    await util.getReplyForMessage('gradbot, chess dxe4');
    await util.getReplyForMessage('gradbot, chess Nc5');
    await util.getReplyForMessage('gradbot, chess Bxc5');
    await util.getReplyForMessage('gradbot, chess Bxc5');
    await util.getReplyForMessage('gradbot, chess Ba6');
    await util.getReplyForMessage('gradbot, chess c4');
    await util.getReplyForMessage('gradbot, chess Rd8');
    await util.getReplyForMessage('gradbot, chess Nd6');
    await util.getReplyForMessage('gradbot, chess f5');
    await util.getReplyForMessage('gradbot, chess exf6');
    await util.getReplyForMessage('gradbot, chess Rxd6');
    await util.getReplyForMessage('gradbot, chess Qxe4');
    await util.getReplyForMessage('gradbot, chess Bb7');
    await util.getReplyForMessage('gradbot, chess Bxd6');
    await util.getReplyForMessage('gradbot, chess Qxd6');
    await util.getReplyForMessage('gradbot, chess Rad1');
    await util.getReplyForMessage('gradbot, chess Nd8');
    await util.getReplyForMessage('gradbot, chess f7');
    await util.getReplyForMessage('gradbot, chess Kxf7');
    await util.getReplyForMessage('gradbot, chess Qxh7');
    await util.getReplyForMessage('gradbot, chess Qc6');
    await util.getReplyForMessage('gradbot, chess Rf2');
    await util.getReplyForMessage('gradbot, chess Qe4');
    await util.getReplyForMessage('gradbot, chess f5');
    await util.getReplyForMessage('gradbot, chess e5');
    await util.getReplyForMessage('gradbot, chess Rfd2');
    await util.getReplyForMessage('gradbot, chess Bc6');
    await util.getReplyForMessage('gradbot, chess Qg6');
    await util.getReplyForMessage('gradbot, chess Ke7');
    await util.getReplyForMessage('gradbot, chess Rd7');
    expect(await readDb()).toEqual(
      'r2n4/3Rk1p1/2b3Q1/p3pP2/2P1q3/8/1P4PP/3R3K b - - 5 34',
    ); // Black resigns
  });

  test('does not respond to test message', async () => {
    const featureResponse = await util.getReplyForMessage('gradbot, test');

    expect(featureResponse).toBeNull();
  });
});
