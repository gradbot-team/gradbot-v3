import { ChessPiece } from './chess-piece';
import { ChessBoard } from './chess-board';
import { ChessConstants } from './chess-constants';

export class ChessPawn extends ChessPiece {
  public name: string = ChessConstants.Pawn;

  public validMoves(boardObject: ChessBoard): [string, [number, number]][] {
    const validMoves: [string, [number, number]][] = [];
    // Pawns can:
    // 0) if on their own second rank, they can move forward two squares if not blocked on either
    if (this.x === this.getSecondRank()) {
      const pieceInFront = boardObject.getPieceAt(this.getNextRank(), this.y);
      const nextPieceInFront = boardObject.getPieceAt(
        this.getNextRank(2),
        this.y,
      );
      if (!pieceInFront && !nextPieceInFront) {
        validMoves.push(this.buildMoves(this.getNextRank(2), this.y, false));
      }
    }

    // 1) if not on penultimate rank, move forward one square if not blocked by same or different colour
    if (this.x !== this.getPenultimateRank()) {
      const pieceInFront = boardObject.getPieceAt(this.getNextRank(), this.y);
      if (!pieceInFront) {
        validMoves.push(this.buildMoves(this.getNextRank(), this.y, false));
      }
    }
    // 2) move diagonally one square forward if blocked by different colour (capture)
    if (this.x !== this.getPenultimateRank()) {
      const pieceInFrontLeft = boardObject.getPieceAt(
        this.getNextRank(),
        this.y - 1,
      );
      const pieceInFrontRight = boardObject.getPieceAt(
        this.getNextRank(),
        this.y + 1,
      );
      if (pieceInFrontLeft && pieceInFrontLeft.isWhite !== this.isWhite) {
        validMoves.push(this.buildMoves(this.getNextRank(), this.y - 1, true));
      }
      if (pieceInFrontRight && pieceInFrontRight.isWhite !== this.isWhite) {
        validMoves.push(this.buildMoves(this.getNextRank(), this.y + 1, true));
      }
    }
    // 3) move diagonally one square to capture enpassant (capture)
    if (boardObject.enpassantSquare) {
      if (
        boardObject.enpassantSquare[0] === this.getNextRank() &&
        (boardObject.enpassantSquare[1] === this.y - 1 ||
          boardObject.enpassantSquare[1] === this.y + 1)
      ) {
        validMoves.push(
          this.buildMoves(
            boardObject.enpassantSquare[0],
            boardObject.enpassantSquare[1],
            true,
          ),
        );
      }
    }
    // 4) if on penultimate rank, move forward one square and promote to any piece except king or pawn
    if (this.x === this.getPenultimateRank()) {
      const pieceInFront = boardObject.getPieceAt(this.getNextRank(), this.y);
      if (!pieceInFront) {
        for (const promote of ['R', 'Q', 'N', 'B']) {
          validMoves.push(
            this.buildMoves(this.getNextRank(), this.y, false, promote),
          );
        }
      }
    }
    // 5) if on penultimate rank, move diagonally one square forward if blocked by different colour (capture)
    //    and promote to any piece except king or pawn
    if (this.x === this.getPenultimateRank()) {
      const pieceInFrontLeft = boardObject.getPieceAt(
        this.getNextRank(),
        this.y - 1,
      );
      const pieceInFrontRight = boardObject.getPieceAt(
        this.getNextRank(),
        this.y + 1,
      );
      if (pieceInFrontLeft) {
        for (const promote of ['R', 'Q', 'N', 'B']) {
          validMoves.push(
            this.buildMoves(this.getNextRank(), this.y - 1, true, promote),
          );
        }
      }
      if (pieceInFrontRight) {
        for (const promote of ['R', 'Q', 'N', 'B']) {
          validMoves.push(
            this.buildMoves(this.getNextRank(), this.y + 1, true, promote),
          );
        }
      }
    }
    return validMoves;
  }

  private getNextRank(numRanks = 1): number {
    // The only piece where it matters - forwards is different if you're white or black
    // Assumption: this.y is never 0 or 7, else the pawn would already be promoted
    // White moves up (towards 0), black moves down (towards 7)
    return this.isWhite ? this.x - numRanks : this.x + numRanks;
  }

  private getPenultimateRank(): number {
    // Penultimate rank for white is rank 7 (idx 1), black is 2 (idx 6)
    return this.isWhite ? 1 : 6;
  }

  private getSecondRank(): number {
    // Second rank for white is rank 2 (idx 6), black is 7 (idx 1)
    return this.isWhite ? 6 : 1;
  }

  private buildMoves(
    rank: number,
    file: number,
    isCapture: boolean,
    promotePiece: string | null = null,
  ): [string, [number, number]] {
    let moveString = '';
    moveString += this.getGridPosition(rank, file);
    if (isCapture) {
      moveString = this.getFile() + ChessConstants.Capture + moveString;
    }
    if (promotePiece) {
      moveString += ChessConstants.Promote + promotePiece;
    }
    return [moveString, [rank, file]];
  }
}
