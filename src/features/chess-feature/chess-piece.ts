import { ChessBoard } from './chess-board';
import { ChessConstants } from './chess-constants';
import { log } from '../../util/log/log';

export abstract class ChessPiece {
  public isWhite: boolean;
  public abstract name: string;

  constructor(
    readonly x: number,
    readonly y: number,
    readonly character: string,
  ) {
    this.isWhite = /[A-Z]/.test(character);
  }

  // Converts an index pair into a grid position
  // Should probably be a helper/static...
  public getGridPosition(x: number, y: number) {
    const rank = ChessConstants.Ranks[y];
    const file = ChessConstants.Files[7 - x];
    return rank + file;
  }

  public getFile() {
    const rank = ChessConstants.Ranks[this.y];
    return rank;
  }

  public getRank() {
    const file = ChessConstants.Files[7 - this.x];
    return file;
  }

  // Returns the list of moves possible for the piece in algebraic notation in all
  // forms (P<square>, P<row><square>, P<col><square, P<col><row><square>, and the
  // capture-alternatives, like Px<square> or P<row>x<square>).
  public abstract validMoves(
    boardObject: ChessBoard,
  ): [string, [number, number]][];

  protected buildVariantMoves(
    rank: number,
    file: number,
    isCapture: boolean,
  ): [string, [number, number]][] {
    // Other than pawns and kings, all other pieces can have ambiguous moves.
    // eg. Two queens able to move to the same square.
    // Disambiguation is not always necessary, but we produce all disambiguation variants anyway so players
    // can enter whatever they want.
    const moves: [string, [number, number]][] = [];
    let moveSuffix = '';
    moveSuffix += this.getGridPosition(rank, file);
    if (isCapture) {
      moveSuffix = ChessConstants.Capture + moveSuffix;
    }
    // Disambiguation example for a queen at b3: 'Q', 'Q3', 'Qb', 'Qb3'
    const movePrefixes = [
      this.character.toUpperCase(),
      this.character.toUpperCase() + this.getFile(),
      this.character.toUpperCase() + this.getRank(),
      this.character.toUpperCase() + this.getFile() + this.getRank(),
    ];

    for (const movePrefix of movePrefixes) {
      moves.push([movePrefix + moveSuffix, [rank, file]]);
    }
    return moves;
  }

  // Returns all 'regular' moves - for use by bishop, rook, queen, king.
  protected getCardinalIntercardinalMoves(
    boardObject: ChessBoard,
    cardinal: boolean,
    intercardinal: boolean,
  ): [string, [number, number]][] {
    let validMoves: [string, [number, number]][] = [];

    if (cardinal) {
      // North...
      for (let northRank = this.x - 1; northRank >= 0; northRank--) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            northRank,
            this.y,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
      // South...
      for (let southRank = this.x + 1; southRank < 8; southRank++) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            southRank,
            this.y,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
      // East...
      for (let eastFile = this.y + 1; eastFile < 8; eastFile++) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            this.x,
            eastFile,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
      // West...
      for (let westFile = this.y - 1; westFile >= 0; westFile--) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            this.x,
            westFile,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
    }

    if (intercardinal) {
      // Northeast...
      for (
        let [northRank, eastFile] = [this.x - 1, this.y + 1];
        northRank >= 0 && eastFile < 8;
        northRank--, eastFile++
      ) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            northRank,
            eastFile,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
      // Northwest...
      for (
        let [northRank, westFile] = [this.x - 1, this.y - 1];
        northRank >= 0 && westFile >= 0;
        northRank--, westFile--
      ) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            northRank,
            westFile,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
      // Southeast...
      for (
        let [southRank, eastFile] = [this.x + 1, this.y + 1];
        southRank < 8 && eastFile < 8;
        southRank++, eastFile++
      ) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            southRank,
            eastFile,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
      // Southwest...
      for (
        let [southRank, westFile] = [this.x + 1, this.y - 1];
        southRank < 8 && westFile >= 0;
        southRank++, westFile--
      ) {
        const [shouldBreak, newMoves] =
          this.getCardinalIntercardinalMovesHelper(
            boardObject,
            southRank,
            westFile,
          );
        validMoves = validMoves.concat(newMoves);
        if (shouldBreak) {
          break; // The break is why we're stuck with dupe code
        }
      }
    }
    return validMoves;
  }

  private getCardinalIntercardinalMovesHelper(
    boardObject: ChessBoard,
    rank: number,
    file: number,
  ): [boolean, [string, [number, number]][]] {
    // Helper for the cardinal/intercardinal function - the inner body of every loop is basically the same
    // Returns 'should break' and a list of valid moves. Always break if king - ray only goes one tile.
    const pieceAtPosition = boardObject.getPieceAt(rank, file);
    const isKing = this.name === ChessConstants.King;
    if (!pieceAtPosition) {
      return [isKing, this.buildVariantMoves(rank, file, false)];
    }
    if (pieceAtPosition) {
      if (pieceAtPosition.isWhite !== this.isWhite) {
        // Allow capture if it's an enemy.
        return [true, this.buildVariantMoves(rank, file, true)];
      }
      // Can't go through pieces!
      return [true, []];
    }
    // Theoretically not getting here, as parent loop will end beforehand
    log('Somehow reached end of cardinal helper function..!');
    return [true, []];
  }
}
