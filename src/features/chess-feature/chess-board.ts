import { ChessPiece } from './chess-piece';

export class ChessBoard {
  public boardState: ChessPiece[][];
  public enpassantSquare: [number, number] | null;
  public isWhiteTurn: boolean;
  public halfmoveClock: number;
  public fullmoveCounter: number;
  public castles: string;

  constructor(
    boardState: ChessPiece[][],
    isWhiteTurn: boolean,
    enpassantSquare: [number, number] | null,
    halfmoveClock: number,
    fullmoveCounter: number,
    castles: string,
  ) {
    this.boardState = boardState;
    this.enpassantSquare = enpassantSquare;
    this.isWhiteTurn = isWhiteTurn;
    this.halfmoveClock = halfmoveClock;
    this.fullmoveCounter = fullmoveCounter;
    this.castles = castles;
  }

  // Primary function used by pieces
  // Excludes empty pieces - use direct array access if you need those
  public getPieceAt(x: number, y: number): ChessPiece | null {
    if (this.boardState[x][y] && this.boardState[x][y].character !== '-') {
      return this.boardState[x][y];
    }
    return null;
  }

  // Used by king to decide on castling eligibility, and as helper
  // for checkmate checks.
  public isSpaceUnderAttack(x: number, y: number): boolean {
    for (const rank of this.boardState) {
      for (const piece of rank) {
        if (piece.isWhite !== this.isWhiteTurn) {
          for (const move of piece.validMoves(this)) {
            if (move[1][0] === x && move[1][1] === y) {
              return true;
            }
          }
        }
      }
    }

    return false;
  }
}
