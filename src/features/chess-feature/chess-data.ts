import { DBO } from '../../gradbot/database/db-objects/registry/dbo-decorator';
import { DbText } from '../../gradbot/database/types/db-text';
import { FeatureDbObject } from '../../gradbot/database/db-objects/feature-db-object';
import { DbIsolationLevel } from '../../gradbot/database/db-objects/db-isolation-level';

@DBO
export class ChessData extends FeatureDbObject<ChessData> {
  isolationLevel = DbIsolationLevel.SERVER;

  fen = new DbText();
}
