export abstract class ChessConstants {
  // Piece names
  public static readonly Rook: string = 'Rook';
  public static readonly Bishop: string = 'Bishop';
  public static readonly Knight: string = 'Knight';
  public static readonly King: string = 'King';
  public static readonly Queen: string = 'Queen';
  public static readonly Pawn: string = 'Pawn';

  // Players
  public static readonly PlayerWhite: string = 'White';
  public static readonly PlayerBlack: string = 'Black';

  // Board
  public static readonly Ranks: string[] = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
  ];
  public static readonly Files: string[] = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
  ];

  // Fen data
  public static readonly FenBlack: string = 'b';
  public static readonly FenWhite: string = 'w';
  public static readonly FenBlank: string = '-';

  // Castling constants
  public static readonly WhiteKingsideCastleIndicator = 'K';
  public static readonly WhiteQueensideCastleIndicator = 'Q';
  public static readonly BlackKingsideCastleIndicator = 'k';
  public static readonly BlackQueensideCastleIndicator = 'q';

  // Notation
  public static readonly KingsideCastle: string = 'O-O';
  public static readonly QueensideCastle: string = 'O-O-O';
  public static readonly Capture: string = 'x';
  public static readonly Promote: string = '=';

  // Empty space
  public static readonly EmptySpace: string = '-';

  // Database
  public static readonly ChessTable = 'boring_chess_table';
  public static readonly Fen = 'fen';
}
