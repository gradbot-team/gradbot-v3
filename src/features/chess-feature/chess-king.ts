import { ChessPiece } from './chess-piece';
import { ChessBoard } from './chess-board';
import { ChessConstants } from './chess-constants';

export class ChessKing extends ChessPiece {
  public name: string = ChessConstants.King;

  public validMoves(boardObject: ChessBoard): [string, [number, number]][] {
    const cardinal = true;
    const intercardinal = true;
    let moves: [string, [number, number]][] = [];

    moves = this.getCardinalIntercardinalMoves(
      boardObject,
      cardinal,
      intercardinal,
    );
    moves = moves.concat(this.getCastlingMoves(boardObject));
    return moves;
  }

  private getCastlingMoves(
    boardObject: ChessBoard,
  ): [string, [number, number]][] {
    const moves: [string, [number, number]][] = [];
    // You can hit an infinite loop here if this method is called as part of the OTHER
    // king's valid move check (white castling eligibility -> space under attack ->
    // black castling eligiblity -> space under attack...)
    // So don't consider castling moves if it's not this piece's turn.
    // Has no effect on checkmate/stalemate checks - if you can castle, you can move your rook.
    if (this.isWhite !== boardObject.isWhiteTurn) {
      return [];
    }

    // Can't castle when in check
    if (boardObject.isSpaceUnderAttack(this.x, this.y)) {
      return [];
    }

    const kingsideEligible = this.isWhite
      ? boardObject.castles.includes(
          ChessConstants.WhiteKingsideCastleIndicator,
        )
      : boardObject.castles.includes(
          ChessConstants.BlackKingsideCastleIndicator,
        );
    const queensideEligible = this.isWhite
      ? boardObject.castles.includes(
          ChessConstants.WhiteQueensideCastleIndicator,
        )
      : boardObject.castles.includes(
          ChessConstants.BlackQueensideCastleIndicator,
        );

    // Kingside castling
    if (kingsideEligible) {
      // The two spaces to the right files of the king must be empty and
      // not under attack.
      if (
        !boardObject.getPieceAt(this.x, this.y + 1) &&
        !boardObject.getPieceAt(this.x, this.y + 2)
      ) {
        if (
          !boardObject.isSpaceUnderAttack(this.x, this.y + 1) &&
          !boardObject.isSpaceUnderAttack(this.x, this.y + 2)
        ) {
          moves.push([ChessConstants.KingsideCastle, [this.x, this.y + 2]]);
        }
      }
    }

    if (queensideEligible) {
      // The two spaces to the left files of the king must be empty and
      // not under attack. The third space to the left must be empty,
      // but not have the attack restriction (ie. the rook can move
      // through attacked spaces).
      if (
        !boardObject.getPieceAt(this.x, this.y - 1) &&
        !boardObject.getPieceAt(this.x, this.y - 2) &&
        !boardObject.getPieceAt(this.x, this.y - 3)
      ) {
        if (
          !boardObject.isSpaceUnderAttack(this.x, this.y - 1) &&
          !boardObject.isSpaceUnderAttack(this.x, this.y - 2)
        ) {
          moves.push([ChessConstants.QueensideCastle, [this.x, this.y - 2]]);
        }
      }
    }
    return moves;
  }
}
