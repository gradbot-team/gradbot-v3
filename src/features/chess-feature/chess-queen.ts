import { ChessPiece } from './chess-piece';
import { ChessBoard } from './chess-board';
import { ChessConstants } from './chess-constants';

export class ChessQueen extends ChessPiece {
  public name: string = ChessConstants.Queen;

  public validMoves(boardObject: ChessBoard): [string, [number, number]][] {
    const cardinal = true;
    const intercardinal = true;
    return this.getCardinalIntercardinalMoves(
      boardObject,
      cardinal,
      intercardinal,
    );
  }
}
