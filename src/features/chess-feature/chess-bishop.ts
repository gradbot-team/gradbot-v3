import { ChessPiece } from './chess-piece';
import { ChessBoard } from './chess-board';
import { ChessConstants } from './chess-constants';

export class ChessBishop extends ChessPiece {
  public name: string = ChessConstants.Bishop;

  public validMoves(boardObject: ChessBoard): [string, [number, number]][] {
    const noCardinal = false;
    const intercardinal = true;
    return this.getCardinalIntercardinalMoves(
      boardObject,
      noCardinal,
      intercardinal,
    );
  }
}
