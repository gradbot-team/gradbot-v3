import { noIndent } from '../../util/general/string-extensions';
import { Feature } from '../../gradbot/feature/feature';
import { ChessMessageHandler } from './chess-message-handler';

export class ChessFeature extends Feature {
  public readonly name = 'Chess';
  public readonly iCanInfo = 'facilitate a chess game';
  public readonly instructions = noIndent(
    `Available commands:
     • *g chess start* – start a new game
     • *g chess <move>* – makes the move
     • *g chess show* – displays the board state
     • *g chess resign* – quits the current game`,
  );
  protected readonly messageHandlerType = ChessMessageHandler;
}
