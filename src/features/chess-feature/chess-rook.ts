import { ChessPiece } from './chess-piece';
import { ChessBoard } from './chess-board';
import { ChessConstants } from './chess-constants';

export class ChessRook extends ChessPiece {
  public name: string = ChessConstants.Rook;

  public validMoves(boardObject: ChessBoard): [string, [number, number]][] {
    const cardinal = true;
    const noIntercardinal = false;
    return this.getCardinalIntercardinalMoves(
      boardObject,
      cardinal,
      noIntercardinal,
    );
  }
}
