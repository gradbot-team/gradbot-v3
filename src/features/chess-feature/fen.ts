export class Fen {
  constructor(
    readonly boardData: string,
    readonly currentPlayer: string,
    readonly rawFen: string,
  ) {}
}
