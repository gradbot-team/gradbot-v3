import 'reflect-metadata';
import { ClubFeature } from './club-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import { getTestMessage } from '../../util/test/get-test-message';
import { ClubData } from './club-data';
import { ClubDoneData } from './club-done-data';
import { User } from '../../gradbot/chat/user/user';
import { SUPPORTED_CLUBS, CLUB_HELP_TEXT, SANITISER } from './club-constants';

const util = FeatureTestUtil.createForFeatureType(ClubFeature);

util.addUsers(['Zeus', 'Hera', 'Poseidon', 'Hades', 'Athena']);

interface ClubDataSet {
  itemName: string;
  votes: string[];
}

interface ClubDoneDataSet {
  itemName: string;
  votes: number;
}

const illegalChar = 'Ｔ';

// ---------------- //
// lambda functions //
// ---------------- //

const deleteDb = async (): Promise<void> => {
  const clubRecords = await new ClubData(getTestMessage()).retrieveAlike();
  const clubDoneRecords = await new ClubDoneData(
    getTestMessage(),
  ).retrieveAlike();

  if (clubRecords) {
    for (const clubRecord of clubRecords) {
      await clubRecord.delete();
    }
  }

  if (clubDoneRecords) {
    for (const clubDoneRecord of clubDoneRecords) {
      await clubDoneRecord.delete();
    }
  }
};

const writeDb = async (
  clubName: string,
  clubDataSets: ClubDataSet[],
): Promise<void> => {
  const rec: ClubData = new ClubData(getTestMessage());
  rec.clubName.set(clubName);

  for (const clubDataSet of clubDataSets) {
    rec.itemName.set(clubDataSet.itemName);

    for (const vote of clubDataSet.votes) {
      rec.userId.set(util.getIdFromName(vote));
      await rec.write();
    }
  }
};

const writeDoneDb = async (
  clubName: string,
  clubDoneDataSets: ClubDoneDataSet[],
): Promise<void> => {
  const rec: ClubDoneData = new ClubDoneData(getTestMessage());
  rec.clubName.set(clubName);

  for (const cd of clubDoneDataSets) {
    rec.itemName.set(cd.itemName);
    rec.noVotes.set(cd.votes);
    await rec.write();
  }
};

// ---------------- //
// ---------------- //
// ---------------- //

afterEach(async () => {
  await deleteDb();
});

describe('ClubFeature', () => {
  test('Does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  test('Return help text when asked', async () => {
    for (const club of SUPPORTED_CLUBS) {
      await util.expectReplyToHaveText(`${club}club help`, CLUB_HELP_TEXT);
    }
  });

  test('Return default text when the wrong number of arguments is used', async () => {
    // commands where only 1 argument is valid
    for (const cmd of ['all', 'help']) {
      await util.expectReplyToHaveText(
        `filmclub ${cmd} fail`,
        'There are currently no items to vote on.',
      );
    }

    // commands where only 2 or more arguments is valid
    for (const cmd of ['vote', 'add', 'delete', 'unvote']) {
      await util.expectReplyToHaveText(
        `filmclub ${cmd}`,
        'There are currently no items to vote on.',
      );
    }
  });

  test('Shows votes when asked', async () => {
    await util.expectReplyToHaveText(
      'bookclub',
      'There are currently no items to vote on.',
    );

    await writeDb('bookclub', [
      { itemName: '1984', votes: ['Zeus', 'Hera'] },
      { itemName: 'Moby Dick', votes: ['Zeus', 'Hera', 'Poseidon'] },
      { itemName: 'G.', votes: ['Hera', 'Poseidon'] },
      {
        itemName: 'The Narrow Road to the Deep North',
        votes: ['Athena', 'Hades', 'Zeus', 'Poseidon', 'Hera'],
      },
      { itemName: 'The Sellout', votes: ['Hera', 'Poseidon', 'Zeus'] },
      { itemName: 'Girl, woman, other', votes: ['Zeus'] },
    ]);

    await writeDb('filmclub', [
      { itemName: "The King's Speech", votes: ['Zeus', 'Hera'] },
      { itemName: 'Shrek', votes: ['Zeus', 'Hera', 'Poseidon'] },
      { itemName: 'The Room', votes: ['Hera', 'Poseidon'] },
      {
        itemName: 'Birdman or (The Unexpected Virtue of Ignorance)',
        votes: ['Athena', 'Hades', 'Zeus', 'Poseidon', 'Hera'],
      },
      { itemName: 'Parasite', votes: ['Hera', 'Poseidon', 'Zeus'] },
      { itemName: 'Napolean Dynamite', votes: ['Hera'] },
      { itemName: 'Scream', votes: ['Zeus'] },
      { itemName: 'Scream 2', votes: ['Zeus'] },
      { itemName: 'Scream 3', votes: ['Zeus'] },
      { itemName: 'Scream 4', votes: ['Zeus'] },
    ]);

    // Check that the subset shows correctly
    await util.expectReplyToHaveText(
      'bookclub',
      '1. "The Narrow Road to the Deep North" [5]\n' +
        '2. "Moby Dick" [3]\n' +
        '3. "The Sellout" [3]\n' +
        '4. "1984" [2]\n' +
        '5. "G." [2]\n' +
        '6. "Girl, woman, other" [1]',
    );

    await util.expectReplyToHaveText(
      'filmclub',
      '1.  "Birdman or (The Unexpected Virtue of Ignorance)" [5]\n' +
        '2.  "Shrek" [3]\n' +
        '3.  "Parasite" [3]\n' +
        '4.  "The King\'s Speech" [2]\n' +
        '5.  "The Room" [2]\n' +
        '6.  "Napolean Dynamite" [1]\n' +
        '7.  "Scream" [1]\n' +
        '8.  "Scream 2" [1]\n' +
        '9.  "Scream 3" [1]\n' +
        '10. "Scream 4" [1]',
    );

    // Check that all shows correctly
    await util.expectReplyToHaveText(
      'bookclub all',
      '1. "The Narrow Road to the Deep North" [5]\n' +
        '2. "Moby Dick" [3]\n' +
        '3. "The Sellout" [3]\n' +
        '4. "1984" [2]\n' +
        '5. "G." [2]\n' +
        '6. "Girl, woman, other" [1]',
    );

    await util.expectReplyToHaveText(
      'filmclub all',
      '1.  "Birdman or (The Unexpected Virtue of Ignorance)" [5]\n' +
        '2.  "Shrek" [3]\n' +
        '3.  "Parasite" [3]\n' +
        '4.  "The King\'s Speech" [2]\n' +
        '5.  "The Room" [2]\n' +
        '6.  "Napolean Dynamite" [1]\n' +
        '7.  "Scream" [1]\n' +
        '8.  "Scream 2" [1]\n' +
        '9.  "Scream 3" [1]\n' +
        '10. "Scream 4" [1]',
    );

    // Check that "mine" shows correctly
    const poseidonId = util.getIdFromName('Poseidon');
    await util.expectPublicReplyToHaveText(
      'bookclub mine',
      '1. "The Narrow Road to the Deep North" [5]\n' +
        '2. "Moby Dick" [3]\n' +
        '3. "The Sellout" [3]\n' +
        '4. "G." [2]',
      new User(poseidonId, 'Poseidon'),
    );

    await util.expectPublicReplyToHaveText(
      'filmclub mine',
      '1. "Birdman or (The Unexpected Virtue of Ignorance)" [5]\n' +
        '2. "Shrek" [3]\n' +
        '3. "Parasite" [3]\n' +
        '4. "The Room" [2]',
      new User(poseidonId, 'Poseidon'),
    );
  });

  test('Reject add when there is a maximum amount of items', async () => {
    // add 20 shrek films
    for (const i of [...Array(20).keys()]) {
      await writeDb('filmclub', [
        { itemName: `Shrek${i}`, votes: ['Zeus', 'Hera'] },
      ]);
    }

    // try to add the 20th in the shrek saga, but get rejected!
    await util.expectReplyToHaveText(
      'filmclub add Shrek20',
      'No more items can be added until items are completed or deleted.',
      util.getUserFromName('Zeus'),
    );
  });

  test('Reject add when item already exists', async () => {
    await writeDb('filmclub', [{ itemName: 'Shrek', votes: ['Zeus', 'Hera'] }]);

    await util.expectReplyToHaveText(
      'filmclub add Shrek',
      'You cannot add an item twice.',
      util.getUserFromName('Zeus'),
    );
  });

  test('Reject add when item has an illegal character', async () => {
    for (const item of ['G*', `G${illegalChar}`, 'G£', 'G^']) {
      await util.expectReplyToHaveText(
        `filmclub add ${item}`,
        `The item '${item}' does not match the sanitiser ` +
          `regex ${SANITISER.toString()}.`,
      );
    }
  });

  test('Allows user to add items', async () => {
    await util.expectReplyToHaveText(
      "filmclub add The King's Speech",
      "Thank you for adding and voting for 'The King's Speech'.",
    );

    await util.expectReplyToHaveText('filmclub', '1. "The King\'s Speech" [1]');
  });

  test('Reject vote for item that is not in the db', async () => {
    await util.expectReplyToHaveText(
      'filmclub vote Shrek',
      'No item has been found that matches your vote. ' +
        'Please use the add command if you wish to add ' +
        'new items.',
      util.getUserFromName('Zeus'),
    );
  });

  test('Reject vote for an item the user has already voted on', async () => {
    await writeDb('filmclub', [{ itemName: 'Shrek', votes: ['Zeus'] }]);

    await util.expectReplyToHaveText(
      'filmclub vote Shrek',
      'You cannot vote for "Shrek" twice.',
      util.getUserFromName('Zeus'),
    );
  });

  test('Reject vote for an item when the name is ambiguous', async () => {
    await writeDb('filmclub', [
      { itemName: 'Sun Dance', votes: ['Zeus'] },
      { itemName: 'Sun And Moon', votes: ['Zeus'] },
    ]);

    await util.expectReplyToHaveText(
      'filmclub vote sun',
      'Cannot identify a unique item to vote on, please ' +
        'refine your vote string.\n' +
        'Items matched: ["Sun Dance","Sun And Moon"]',
      util.getUserFromName('Hera'),
    );
  });

  test('Allow user to vote for an item', async () => {
    await writeDb('filmclub', [{ itemName: 'Shrek', votes: ['Hera'] }]);

    // works because of regex matching
    await util.expectReplyToHaveText(
      'filmclub vote s',
      'Thank you for voting for "Shrek".',
      util.getUserFromName('Zeus'),
    );
  });

  test('Reject ambiguous vote, but allow user to vote when the full name is used', async () => {
    await writeDb('filmclub', [
      { itemName: 'Sun', votes: ['Zeus'] },
      { itemName: 'Sun And Moon', votes: ['Zeus'] },
    ]);

    await util.expectReplyToHaveText(
      'filmclub vote Su',
      'Cannot identify a unique item to vote on, please ' +
        'refine your vote string.\n' +
        'Items matched: ["Sun","Sun And Moon"]',
      util.getUserFromName('Hera'),
    );

    await util.expectReplyToHaveText(
      'filmclub vote Sun',
      'Thank you for voting for "Sun".',
      util.getUserFromName('Hera'),
    );
  });

  test('Reject deletion when the command does not use a number', async () => {
    // try to delete not using a number
    await util.expectReplyToHaveText(
      'filmclub delete toy story',
      'The user did not enter a number matching 1-20 ' + 'i.e. "toy story"',
    );
  });

  test('Deletion rejected because the item is not in the the db', async () => {
    // add 3 films
    for (const i of ['Lord of the rings', 'The godfather', 'Bill and Ted']) {
      await util.getReplyForMessage(`filmclub add ${i}`);
    }

    await util.expectReplyToHaveText(
      'filmclub delete 4',
      'Deletion rejected because 4 is higher than the number of items 3.',
    );

    await util.expectReplyToHaveText(
      'filmclub delete 0',
      'The user did not enter a number matching 1-20 i.e. "0"',
    );
  });

  test('Accept deletions', async () => {
    // add 5 car films
    for (const i of [...Array(5).keys()]) {
      await util.getReplyForMessage(`filmclub add Cars${i + 1}`);
    }

    // check the stats
    await util.expectReplyToHaveText(
      'filmclub',
      '1. "Cars1" [1]\n' +
        '2. "Cars2" [1]\n' +
        '3. "Cars3" [1]\n' +
        '4. "Cars4" [1]\n' +
        '5. "Cars5" [1]',
    );

    // delete all 5 cars films
    for (const i of [...Array(5).keys()]) {
      await util.expectReplyToHaveText(
        'filmclub delete 1',
        `The item 'Cars${i + 1}' has been deleted.`,
      );
    }
  });

  test('Reject unvote when user has not voted', async () => {
    await util.expectReplyToHaveText(
      'filmclub unvote Scott Pilgrim',
      "You have not voted for 'Scott Pilgrim'.",
    );
  });

  test('Accept unvotes', async () => {
    await util.getReplyForMessage(
      "filmclub add emperor's new groove",
      util.getUserFromName('Zeus'),
    );
    await util.getReplyForMessage(
      "filmclub vote emperor's new groove",
      util.getUserFromName('Hera'),
    );

    // check the stats
    await util.expectReplyToHaveText(
      'filmclub',
      '1. "emperor\'s new groove" [2]',
    );

    // try to add the 20th in the shrek saga, but get rejected!
    await util.expectReplyToHaveText(
      "filmclub unvote emperor's new groove",
      "Zeus's vote for 'emperor's new groove' has been deleted.",
      util.getUserFromName('Zeus'),
    );

    // check the stats
    await util.expectReplyToHaveText(
      'filmclub',
      '1. "emperor\'s new groove" [1]',
    );

    // try to add the 20th in the shrek saga, but get rejected!
    await util.expectReplyToHaveText(
      "filmclub unvote emperor's new groove",
      "Hera's vote for 'emperor's new groove' has been deleted.",
      util.getUserFromName('Hera'),
    );

    // check the stats
    await util.expectReplyToHaveText(
      'filmclub',
      'There are currently no items to vote on.',
    );
  });

  test('Reject marking an item as complete when the item is not in the db', async () => {
    await util.expectReplyToHaveText(
      'filmclub complete RoboCop',
      "The item 'RoboCop' you are trying to mark as complete " +
        'does not exist in the db.',
    );
  });

  test('Accept complete', async () => {
    await util.getReplyForMessage(
      'filmclub add Spider-man',
      util.getUserFromName('Zeus'),
    );
    await util.getReplyForMessage(
      'filmclub add Batman',
      util.getUserFromName('Hera'),
    );

    // check the stats
    await util.expectReplyToHaveText(
      'filmclub',
      '1. "Spider-man" [1]\n' + '2. "Batman" [1]',
    );

    // try to add the 20th in the shrek saga, but get rejected!
    await util.expectReplyToHaveText(
      'filmclub complete Spider-man',
      "The item 'Spider-man' has been marked as complete.",
      util.getUserFromName('Zeus'),
    );

    // check the stats
    await util.expectReplyToHaveText('filmclub', '1. "Batman" [1]');

    await util.expectReplyToHaveText(
      'filmclub complete',
      '1. "Spider-man" [1]',
    );
  });

  test('Show complete', async () => {
    await util.expectReplyToHaveText(
      'bookclub complete',
      'There are currently no complete bookclub items.',
    );

    await writeDoneDb('bookclub', [
      { itemName: '1984', votes: 1 },
      { itemName: 'Moby Dick', votes: 3 },
      { itemName: 'G.', votes: 4 },
      { itemName: 'The Narrow Road to the Deep North', votes: 25 },
      { itemName: 'The Sellout', votes: 8 },
    ]);

    await writeDoneDb('filmclub', [
      { itemName: "The King's Speech", votes: 1 },
      { itemName: 'Shrek', votes: 5 },
      { itemName: 'The Room', votes: 21 },
      {
        itemName: 'Birdman or (The Unexpected Virtue of Ignorance)',
        votes: 15,
      },
      { itemName: 'Parasite', votes: 6 },
    ]);

    await util.expectReplyToHaveText(
      'bookclub complete',
      '1. "1984" [1]\n' +
        '2. "Moby Dick" [3]\n' +
        '3. "G." [4]\n' +
        '4. "The Narrow Road to the Deep North" [25]\n' +
        '5. "The Sellout" [8]',
    );

    await util.expectReplyToHaveText(
      'filmclub complete',
      '1. "The King\'s Speech" [1]\n' +
        '2. "Shrek" [5]\n' +
        '3. "The Room" [21]\n' +
        '4. "Birdman or (The Unexpected Virtue of Ignorance)" [15]\n' +
        '5. "Parasite" [6]',
    );
  });
});
