import { DBO } from '../../gradbot/database/db-objects/registry/dbo-decorator';
import { FeatureDbObject } from '../../gradbot/database/db-objects/feature-db-object';
import { DbText } from '../../gradbot/database/types/db-text';
import { DbIsolationLevel } from '../../gradbot/database/db-objects/db-isolation-level';

@DBO
export class ClubData extends FeatureDbObject<ClubData> {
  isolationLevel = DbIsolationLevel.SERVER;

  clubName = new DbText();
  itemName = new DbText();
  userId = new DbText();
}
