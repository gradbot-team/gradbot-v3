import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import {
  SUPPORTED_CLUBS,
  MAX_CONCURRENT_ITEMS,
  SHOW_SUBSET,
  SANITISER,
  CLUB_HELP_TEXT,
} from './club-constants';
import { ClubData } from './club-data';
import { ClubDoneData } from './club-done-data';

export class ClubMessageHandler extends FeatureMessageHandler {
  private readonly dataClubRetriever = new ClubData(this.message);
  private readonly dataDoneClubRetriever = new ClubDoneData(this.message);

  // always trigger
  protected readonly triggerProbability = 1;

  readonly clubHookRegex: RegExp = new RegExp(
    `^(${SUPPORTED_CLUBS.join('club|')}club)`,
    'i',
  );

  public isMessageTrigger(): boolean {
    return true;
  }

  public async getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    // always reset the data retriever
    this.dataClubRetriever.clubName.reset();
    this.dataClubRetriever.itemName.reset();
    this.dataClubRetriever.userId.reset();

    const msg: string = this.message.text.trim();

    if (this.clubHookRegex.test(msg)) {
      const words: string[] = msg.split(/\s+/);
      const clubName: string = words[0].toLowerCase();

      if (words.length === 1) {
        // the default case is to show the current votes!
        return await this.show(clubName, SHOW_SUBSET);
      } else if (words.length === 2) {
        // only all/complete/help/mine is valid for no 1 argument
        const command: string = words[1];

        switch (command) {
          case 'all':
            return await this.show(clubName);

          case 'help':
            return this.createMessage(
              CLUB_HELP_TEXT,
              'I was asked for help so I showed the help text.',
            );

          case 'complete':
            return await this.showComplete(clubName);

          case 'mine':
            return await this.showMine(clubName);

          default:
            return await this.show(clubName, SHOW_SUBSET);
        }
      } else if (words.length >= 2) {
        // in the words array, 0 is the club name, 1 is the command
        // and the rest is considered to be the item name
        const command: string = words[1];
        const itemName: string = words.splice(2).join(' ');

        if (SANITISER.test(itemName)) {
          switch (command) {
            case 'vote':
              return await this.vote(clubName, itemName);

            case 'add':
              return await this.add(clubName, itemName);

            case 'delete':
              return await this.deleteItem(clubName, itemName);

            case 'unvote':
              return await this.unvote(clubName, itemName);

            case 'complete':
              return await this.complete(clubName, itemName);

            default:
              return await this.show(clubName, SHOW_SUBSET);
          }
        } else {
          // the item name did not fit our sanitiser
          return this.createMessage(
            `The item '${itemName}' does not match the sanitiser ` +
              `regex ${SANITISER.toString()}.`,
            `I was asked to do ${command} yet the item name ${itemName}` +
              `did not match the sanitiser regex ${SANITISER.toString()}`,
          );
        }
      }

      // catch all
      return this.createMessage(
        'The club feature did not understand ' + `the command ${msg}.`,
        `I was sent a command I did not understand: '${msg}'.`,
      );
    }

    return null;
  }

  private async getSortedItems(
    clubName: string,
    userId?: string,
    num?: number,
  ): Promise<[string, number][]> {
    this.dataClubRetriever.clubName.set(clubName);
    const clubData: ClubData[] = await this.dataClubRetriever.retrieveAlike();
    let sortedData: [string, number][] = this.SortData(clubData, num);
    if (userId) {
      const myItems: string[] = clubData
        .filter((c) => c.userId.get() === userId)
        .map((c) => c.itemName.get());
      sortedData = sortedData.filter((x) => myItems.includes(x[0]));
    }
    return sortedData;
  }

  private SortData(clubData: ClubData[], num?: number): [string, number][] {
    let retData: [string, number][] = [];

    if (clubData.length > 0) {
      const itemMap = new Map<string, number>();

      // count up the votes
      for (const itemName of clubData.map((x) => x.itemName.get())) {
        if (!itemMap.has(itemName)) {
          // first time seeing the item so create it
          itemMap.set(itemName, 1);
        } else {
          // else increment the vote
          itemMap.set(itemName, (itemMap.get(itemName) as number) + 1);
        }
      }

      // sort our map into an array (take entry and sort with a custom
      // function on the key/values pair)
      retData = [...itemMap.entries()].sort((a, b) => b[1] - a[1]);

      // if a number has been supplied, only show that much
      if (num) {
        retData = retData.slice(0, num);
      }
    }

    return retData;
  }

  private formatItemsList(sortedData: [string, number][]): string {
    let itemVotes = '';
    let itemNo = 1;
    const padWidth: number = sortedData.length.toString().length + 1;

    for (const [key, value] of sortedData) {
      const prefix: string = itemNo.toString() + '.';
      itemVotes +=
        `${itemNo !== 1 ? '\n' : ''}` +
        `${prefix.padEnd(padWidth)} "${key}" [${value}]`;
      itemNo++;
    }

    return itemVotes;
  }

  private async show(clubName: string, num?: number): Promise<FeatureMessage> {
    const sortedData = await this.getSortedItems(clubName, undefined, num);
    let itemVotes = 'There are currently no items to vote on.';
    if (sortedData.length > 0) {
      itemVotes = this.formatItemsList(sortedData);
    }

    return this.createMessage(
      itemVotes,
      `I was asked for a vote count for ${clubName}`,
    );
  }

  private async showMine(clubName: string): Promise<FeatureMessage> {
    const userId = this.message.user.id;
    const sortedData = await this.getSortedItems(clubName, userId, undefined);
    let itemVotes = 'You have not voted on anything currently in the list.';
    if (sortedData.length > 0) {
      itemVotes = this.formatItemsList(sortedData);
    }

    return this.createMessage(
      itemVotes,
      `${this.message.user.displayName} asked to see their votes for ${clubName}`,
    );
  }

  private async vote(
    clubName: string,
    itemMatch: string,
  ): Promise<FeatureMessage> {
    // first try to get the name directly
    this.dataClubRetriever.clubName.set(clubName);
    this.dataClubRetriever.itemName.set(itemMatch);

    // try and find all votes for the item and club
    let clubData = await this.dataClubRetriever.retrieveAlike();

    let hasVotedForItem = false;
    let replyText = '';
    let reasonText = '';
    const matches: string[] = [];
    const prevVotedMatches: string[] = [];

    if (clubData.length > 0) {
      // we found exact matches so update the matches

      if (clubData.some((cd) => cd.userId.get() === this.message.user.id)) {
        // if the user has already voted for this item then set
        // the previously voted error
        hasVotedForItem = true;
      } else {
        // we are voting for an exact match for the first time
        // so set the match
        matches.push(itemMatch);
      }
    } else {
      // there wasn't an exact match so fall back to trying
      // to find a regex match

      let name = '';
      const matchRegex = new RegExp(itemMatch, 'i');

      // blank out the item name so we get all item for the club
      const allClubData = new ClubData(this.message);
      allClubData.clubName.set(clubName);

      // find all items for the club
      clubData = await allClubData.retrieveAlike();

      for (const item of clubData) {
        name = item.itemName.get();

        // test the following
        // * if we have matched before
        // * if we have matched before but also previously voted
        // * if the name matches the regex suppied
        if (
          !matches.includes(name) &&
          !prevVotedMatches.includes(name) &&
          matchRegex.test(name)
        ) {
          // there is a new item name to investigate

          // if the user hasn't voted for the item then this is a valid
          // case so record it. And set the string
          if (
            !clubData.some(
              (cd) =>
                cd.itemName.get() === name &&
                cd.userId.get() === this.message.user.id,
            )
          ) {
            // add the item to our list of possible matches
            matches.push(name);
          } else {
            // else add to the previous voted list - only used to make
            // sure we don't look this up multiple times!
            prevVotedMatches.push(name);
          }
        }
      }
    }

    if (hasVotedForItem) {
      // no cheating!
      //
      // Error only used for full match logic so can use the itemMatch
      // as the match name
      replyText = `You cannot vote for "${itemMatch}" twice.`;
      reasonText =
        'I rejected a vote as they tried to vote twice for the ' + 'same item.';
    } else if (matches.length === 0) {
      // Cannot not vote if no valid match has been found
      replyText =
        'No item has been found that matches your vote. Please ' +
        'use the add command if you wish to add new items.';
      reasonText =
        'I rejected a vote as no item matched the requested ' + 'vote item.';
    } else if (matches.length > 1) {
      // cannot identify a unique item to vote on!
      replyText =
        'Cannot identify a unique item to vote on, please refine ' +
        'your vote string.\n' +
        `Items matched: ["${matches.join('","')}"]`;
      reasonText =
        'I rejected a vote as multiple items matches the ' +
        'requested vote item.';
    } else {
      // 1 match has been found that the user hasn't previously
      // voted on. So commit to the db now
      const match: string = matches[0];

      // a new Vote! Commit this to the db
      this.dataClubRetriever.clubName.set(clubName);
      this.dataClubRetriever.itemName.set(match);
      this.dataClubRetriever.userId.set(this.message.user.id);

      await this.dataClubRetriever.write();

      replyText = `Thank you for voting for "${match}".`;
      reasonText = `I added a vote for "${match}".`;
    }

    return this.createMessage(replyText, reasonText);
  }

  private async add(
    clubName: string,
    itemName: string,
  ): Promise<FeatureMessage> {
    this.dataClubRetriever.clubName.set(clubName);

    const clubData = await this.dataClubRetriever.retrieveAlike();

    let isNew = true;
    let replyText = '';
    let reasonText = '';

    const itemMap = new Map();

    // count up the unique items
    for (const cd of clubData) {
      if (!itemMap.has(cd.itemName.get())) {
        itemMap.set(cd.itemName.get(), 1);
      }

      if (cd.itemName.get() === itemName) {
        // Can only add items if they are new
        // so if we find the item then reject the add
        isNew = false;
        break;
      }
    }

    if (itemMap.size >= MAX_CONCURRENT_ITEMS && isNew) {
      // no room in the inn
      replyText =
        'No more items can be added until items ' + 'are completed or deleted.';
      reasonText = 'I rejected a vote as there is no space for more votes.';
    } else if (!isNew) {
      // item already exists!
      replyText = 'You cannot add an item twice.';
      reasonText = 'I rejected an add as it was already in the db.';
    } else {
      // a new item!
      this.dataClubRetriever.clubName.set(clubName);
      this.dataClubRetriever.itemName.set(itemName);
      this.dataClubRetriever.userId.set(this.message.user.id);

      await this.dataClubRetriever.write();

      replyText = `Thank you for adding and voting for '${itemName}'.`;
      reasonText =
        `I added '${itemName}' and 1 vote for it from ` +
        `'${this.message.user.displayName}'.`;
    }

    return this.createMessage(replyText, reasonText);
  }

  private async deleteItem(
    clubName: string,
    num: string,
  ): Promise<FeatureMessage> {
    let replyText = '';
    let reasonText = '';

    if (!/^([1-9]|1[0-9]|20)$/.test(num)) {
      // failed to get a number so error
      replyText =
        'The user did not enter a number matching 1-20 ' + `i.e. "${num}"`;
      reasonText = `The user did not give a recongised number ${num}.`;
    } else {
      // off set the index as users use 1-20 and the index is 0-19
      const index: number = Number(num) - 1;
      const sortedData = await this.getSortedItems(clubName);

      if (index >= sortedData.length) {
        // User has requested to delete a higher number than is in the db
        replyText =
          `Deletion rejected because ${num} is higher than the ` +
          `number of items ${sortedData.length}.`;
        reasonText =
          'I rejected a deletion because the number requested ' +
          'is higher than the number of items in the db,';
      } else if (index < 0) {
        // This should be unreachable code, but in case somehow a negative
        // number is set as the index catch it here
        replyText = 'Deletion does not support negative numbers.';
        reasonText =
          'I rejected a deletion because the number requested ' +
          'is was less than zero. This should not be possible.';
      } else {
        // we have accepted the deletion and so go ahead and delete
        const itemName: string = sortedData[index][0];

        this.dataClubRetriever.clubName.set(clubName);
        this.dataClubRetriever.itemName.set(itemName);

        const clubData = await this.dataClubRetriever.retrieveAlike();

        // delete all the records
        for (const cd of clubData) {
          await cd.delete();
        }

        replyText = `The item '${itemName}' has been deleted.`;
        reasonText = `'${itemName}' has been deleted.`;
      }
    }

    return this.createMessage(replyText, reasonText);
  }

  private async unvote(
    clubName: string,
    itemName: string,
  ): Promise<FeatureMessage> {
    this.dataClubRetriever.clubName.set(clubName);
    this.dataClubRetriever.itemName.set(itemName);
    this.dataClubRetriever.userId.set(this.message.user.id);

    const clubData = await this.dataClubRetriever.retrieveAlike();

    let replyText = '';
    let reasonText = '';

    if (clubData.length === 0) {
      // No item to delete
      replyText = `You have not voted for '${itemName}'.`;
      reasonText =
        'I rejected a deletion because ' +
        `'${this.message.user.displayName}' has not voted for` +
        ` the item '${itemName}'.`;
    } else {
      // We have record(s) to delete!

      // delete all the records (there should only be 1 record)
      for (const cd of clubData) {
        await cd.delete();
      }

      replyText =
        `${this.message.user.displayName}'s vote for ` +
        `'${itemName}' has been deleted.`;
      reasonText =
        `${this.message.user.displayName} vote for ` +
        `'${itemName}' has been deleted.`;
    }

    return this.createMessage(replyText, reasonText);
  }

  private async complete(
    clubName: string,
    itemName: string,
  ): Promise<FeatureMessage> {
    this.dataClubRetriever.clubName.set(clubName);
    this.dataClubRetriever.itemName.set(itemName);

    const clubData = await this.dataClubRetriever.retrieveAlike();

    let replyText = '';
    let reasonText = '';

    if (clubData.length === 0) {
      // No item to delete
      replyText =
        `The item '${itemName}' you are trying to mark as ` +
        'complete does not exist in the db.';
      reasonText =
        'I rejected a move to complete because the item ' +
        `'${itemName}' is not in the db,`;
    } else {
      // Ok - lets move to complete

      // store completed
      this.dataDoneClubRetriever.clubName.set(clubName);
      this.dataDoneClubRetriever.itemName.set(itemName);
      this.dataDoneClubRetriever.noVotes.set(clubData.length);

      await this.dataDoneClubRetriever.write();

      // delete all the items delete all the records
      for (const cd of clubData) {
        await cd.delete();
      }

      replyText = `The item '${itemName}' has been marked as complete.`;
      reasonText = `'${itemName}' has been marked as complete.`;
    }

    return this.createMessage(replyText, reasonText);
  }

  private async showComplete(clubName: string): Promise<FeatureMessage> {
    this.dataDoneClubRetriever.clubName.set(clubName);

    const clubDoneData = await this.dataDoneClubRetriever.retrieveAlike();

    let itemVotes: string =
      `There are currently no complete ${clubName} ` + 'items.';

    if (clubDoneData.length > 0) {
      itemVotes = '';

      let itemNo = 1;
      const padWidth: number = clubDoneData.length > 9 ? 3 : 2;

      // construct the votes string
      for (const cd of clubDoneData) {
        const name: string = cd.itemName.get();
        const votes: number = cd.noVotes.get();
        const prefix: string = itemNo.toString() + '.';

        itemVotes +=
          `${itemNo !== 1 ? '\n' : ''}` +
          `${prefix.padEnd(padWidth)} "${name}" [${votes}]`;
        itemNo++;
      }
    }

    return this.createMessage(
      itemVotes,
      `I was asked to show the complete list for ${clubName}`,
    );
  }
}
