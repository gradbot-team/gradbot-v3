import { Feature } from '../../gradbot/feature/feature';
import { ClubMessageHandler } from './club-message-handler';
import { CLUB_HELP_TEXT } from './club-constants';

export class ClubFeature extends Feature {
  public readonly name = 'Club';
  public readonly iCanInfo = 'help store votes for clubs.';
  public readonly instructions = CLUB_HELP_TEXT;
  protected readonly messageHandlerType = ClubMessageHandler;
}
