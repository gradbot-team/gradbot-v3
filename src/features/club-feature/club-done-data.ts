import { DBO } from '../../gradbot/database/db-objects/registry/dbo-decorator';
import { FeatureDbObject } from '../../gradbot/database/db-objects/feature-db-object';
import { DbText } from '../../gradbot/database/types/db-text';
import { DbInteger } from '../../gradbot/database/types/db-integer';
import { DbIsolationLevel } from '../../gradbot/database/db-objects/db-isolation-level';

@DBO
export class ClubDoneData extends FeatureDbObject<ClubDoneData> {
  isolationLevel = DbIsolationLevel.SERVER;

  clubName = new DbText();
  itemName = new DbText();
  noVotes = new DbInteger();
}
