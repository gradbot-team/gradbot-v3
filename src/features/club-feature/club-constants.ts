import { noIndent } from '../../util/general/string-extensions';

export const SUPPORTED_CLUBS: string[] = ['book', 'film'];

export const MAX_CONCURRENT_ITEMS = 20;
export const SHOW_SUBSET = 20;
export const SANITISER = /^[0-9A-Z\-\s':,\.\?!\(\)\+=]+$/i;

// help/instructions text
export const CLUB_HELP_TEXT: string = noIndent(
  `Welcome to the club feature!

  Available clubs are
   • book club
   • film club

  Available commands are
   • bookclub/filmclub add    'book/film name'   - add item to vote on
   • bookclub/filmclub vote   'book/film name'   - vote, uses regex
   • bookclub/filmclub unvote 'book/film name'   - retract your vote
   • bookclub/filmclub delete 'book/film name' n - deletes the ` +
    `nth votable item
   • bookclub/filmclub                           - see the top ` +
    `${SHOW_SUBSET} items by vote/alphatical order
   • bookclub/filmclub all                       - see all items ` +
    `currently being voted on
   • bookclub/filmclub mine                      - see all items ` +
    `in the list that you have voted for
   • bookclub/filmclub complete 'book/film name' - mark the item as being done
   • bookclub/filmclub complete                  - view all completed items
   • bookclub/filmclub help                      - view help items`,
);
