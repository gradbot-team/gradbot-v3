import 'reflect-metadata';
import { DeciderFeatureUtil } from './decider-feature-util';

describe('DeciderFeatureUtil', () => {
  const testData: { msgText: string; options: string[] }[] = [
    { msgText: 'A or B', options: ['A', 'B'] },
    { msgText: 'A or B.', options: ['A', 'B.'] },
    { msgText: 'A     or    B', options: ['A', 'B'] },
    { msgText: 'A or B or C', options: ['A', 'B', 'C'] },
    { msgText: 'A, B, or C', options: ['A, B,', 'C'] },
    { msgText: 'A or B or C or D', options: ['A', 'B', 'C', 'D'] },
    { msgText: '1 or 2', options: ['1', '2'] },
    {
      msgText: 'A or !"£$%^&*()`[]{}-=_+;\'¬¦#:@~,./<>?\\|',
      options: ['A', '!"£$%^&*()`[]{}-=_+;\'¬¦#:@~,./<>?\\|'],
    },
    { msgText: 'a b c or d e f', options: ['a b c', 'd e f'] },
    { msgText: 'a or b or', options: ['a', 'b or'] },
    { msgText: 'or a or b', options: ['or a', 'b'] },
    { msgText: 'a OR b', options: ['a', 'b'] },
    { msgText: 'a or b OR c', options: ['a', 'b', 'c'] },
  ];

  for (const data of testData) {
    test(`splits the message text "${data.msgText}" into options correctly`, () => {
      // Act
      const options = DeciderFeatureUtil.splitMessageText(data.msgText);

      // Assert
      expect(options).toHaveLength(data.options.length);
      for (const option of data.options) {
        expect(data.options).toContain(option);
      }
    });
  }
});
