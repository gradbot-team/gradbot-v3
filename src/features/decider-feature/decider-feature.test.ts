import 'reflect-metadata';
import { DeciderFeature } from './decider-feature';
import { FeatureTestUtil } from '../../util/test/feature-test-util';
import * as randomExtensions from '../../util/general/random-extensions';

const util = FeatureTestUtil.createForFeatureType(DeciderFeature);

describe('DeciderFeature', () => {
  test('does not respond to test message', async () =>
    await util.expectReplyToBeNull('g test'));

  const validTriggers = [
    'g A or B',
    'g A or B.',
    'g   A     or    B   ',
    'g A or B or C',
    'g A or B or C or D',
    'g 1 or 2',
    'g A or "£$%^&*()`[]{}-=_+;\'¬¦#:@~,/<>\\|',
    'g a b c or d e f',
    'g a or b or ',
    'g or a or b',
    'g a OR b',
    'g a or b OR c',
    'g 1.2m or 2.3m',
    'g a or b?',
    'g a or b!',
    'g a or b...',
    'g a or b?!?!',
  ];

  for (const trigger of validTriggers) {
    test(`responds to valid trigger "${trigger}"`, async () => {
      await util.expectReplyToBeTruthy(trigger);
    });
  }

  test('reponds with the correct format', async () => {
    const phrase = '   this, i5   my Response   ';

    await util.expectPublicReplyToHaveText(
      `g ${phrase} or ${phrase.trimEnd()}`,
      phrase.trim(),
    );
  });

  const invalidTriggers = [
    'g or b',
    'g a or',
    'g a or    ',
    'g aor b',
    'g a orb',
    'g norm',
    'g Jeremy Corbyn',
    'g a ... or b',
    'g a sentence! a thing or another thing',
    'g a sentence? a thing or another thing',
    'g a sentence. a thing or another thing',
  ];

  for (const trigger of invalidTriggers) {
    test(`does not respond to invalid trigger ${trigger}`, async () => {
      await util.expectReplyToBeNull(trigger);
    });
  }

  const mockRandomTestData = [
    { msgText: 'g a or b', exclusiveMax: 2 },
    { msgText: 'g a or b or ', exclusiveMax: 2 },
    { msgText: 'g a or b or c', exclusiveMax: 3 },
    { msgText: 'g a or b or c or d or e or f or g or h', exclusiveMax: 8 },
  ];

  for (const data of mockRandomTestData) {
    test(`calls getRandomInt correctly with trigger ${data.msgText}`, async () => {
      // Arrange
      const mock = jest.spyOn(randomExtensions, 'getRandomInt');

      // Act
      await util.sendMessage(data.msgText);

      // Assert
      expect(mock).toHaveBeenCalledTimes(1);
      expect(mock).toHaveBeenCalledWith(data.exclusiveMax);

      // Cleanup
      mock.mockRestore();
    });
  }
});
