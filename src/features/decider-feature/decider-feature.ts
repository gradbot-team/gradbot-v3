import { Feature } from '../../gradbot/feature/feature';
import { DeciderMessageHandler } from './decider-message-handler';

export class DeciderFeature extends Feature {
  public readonly name = 'Decider';
  public readonly iCanInfo = 'choose one of two or more options';
  public readonly instructions = 'Syntax: "g <A> or <B>( or <C>... etc.)';
  protected readonly messageHandlerType = DeciderMessageHandler;
}
