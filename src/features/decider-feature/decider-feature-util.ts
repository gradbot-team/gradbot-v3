export class DeciderFeatureUtil {
  public static contentPattern = '';

  public static splitMessageText(messageText: string): string[] {
    return messageText.length > 0
      ? messageText.split(/(?<=\S)\s+or\s+(?=\S)/i)
      : [];
  }
}
