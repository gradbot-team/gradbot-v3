import { FeatureMessageHandler } from '../../gradbot/feature/feature-message-handler';
import { FeatureMessage } from '../../gradbot/feature/feature-message';
import { DeciderFeatureUtil } from './decider-feature-util';
import * as Random from '../../util/general/random-extensions';

export class DeciderMessageHandler extends FeatureMessageHandler {
  protected readonly triggerProbability = 0.1;

  private readonly triggerRegExp = /\S\s+or\s+\S/i;
  private readonly endSentenceRegExp = /([.](?!\d)|[?]|!)+/;

  public isMessageTrigger(): boolean {
    const text = this.getSanitisedText();
    return (
      text.length < 150 &&
      text.match(this.triggerRegExp) !== null &&
      text.replace(/[.?!]+$/, '').match(this.endSentenceRegExp) === null
    );
  }

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    const options = DeciderFeatureUtil.splitMessageText(
      this.getSanitisedText(),
    );
    if (options.length > 0) {
      const index = Random.getRandomInt(options.length);
      const selection = options[index].trim();
      return Promise.resolve(
        this.createMessage(
          selection,
          `${this.message.user.displayName} asked me to choose from a set of options.`,
        ),
      );
    } else {
      return Promise.resolve(null);
    }
  }
}
