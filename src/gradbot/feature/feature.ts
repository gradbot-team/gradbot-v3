import { Message } from '../chat/message/message';
import { FeatureMessageHandler } from './feature-message-handler';

export abstract class Feature {
  public abstract readonly name: string;
  public abstract readonly iCanInfo: string;
  public abstract readonly instructions: string;
  protected abstract readonly messageHandlerType: new (
    message: Message,
    feature: Feature,
  ) => FeatureMessageHandler;

  public getMessageHandler(message: Message) {
    return new this.messageHandlerType(message, this);
  }
}
