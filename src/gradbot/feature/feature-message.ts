import { Image } from '../image/image';
import { Metadata } from '../chat/message/metadata';
import { Channel } from '../chat/message/channel';

export class FeatureMessage {
  private _image?: Image;

  constructor(
    private _text: string,
    private _reason: string,
    readonly featureName: string,
    readonly metadata: Metadata,
    readonly channelOverride?: Channel,
  ) {
    this._text = this._text.trim();
    this._reason = this._reason.trim();
  }

  get text() {
    return this._text;
  }

  get reason() {
    return this._reason;
  }

  get image() {
    return this._image;
  }

  public withImage(image: Image) {
    if (this._image) {
      throw new Error('Only one image allowed per message');
    }
    this._image = image;
    return this;
  }
}
