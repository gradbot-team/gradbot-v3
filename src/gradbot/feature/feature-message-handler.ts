import { Message } from '../chat/message/message';
import { FeatureMessage } from './feature-message';
import { FeatureTimeout } from '../constants/constants';
import { container } from 'tsyringe';
import { CronBuilder } from '../scheduler/cron-builder';
import { Scheduler } from '../scheduler/scheduler';
import { API } from '../api/api';
import { ImageGenerator } from '../image/image-generator';
import { Dictionary } from '../dictionary/dictionary';
import { Feature } from './feature';
import { Chat } from '../chat/chat';
import { User } from '../chat/user/user';
import { Channel } from '../chat/message/channel';
import { getTrueWithProbability } from '../../util/general/random-extensions';
import { log } from '../../util/log/log';

export abstract class FeatureMessageHandler {
  private static readonly GradbotAddressRegex =
    /^[\s¿¡]*g(radbot)?[.,\-~!?]*\s|,?\s+g(radbot)?[\s.!?]*$/i;
  private static readonly GradbotAddressReplaceRegex =
    /^[\s¿¡]*g(radbot)?[.,\-~!?]*\s+|,?\s+g(radbot)?[\s.!?]*$/i;
  private static readonly GradbotCleanMentions = /@(\S)/gi;

  /**
   * Override with a number between 0 and 1 to set the probability with which
   * the feature responds to a valid trigger. Default value is 1 (feature will
   * always respond to a valid trigger).
   *
   * Messages directly addressed to Gradbot - assuming they pass the checks in
   * isMessageTrigger() - will always prompt a response, regardless of the value
   * of triggerProbability.
   */
  protected readonly triggerProbability: number = 1;

  private readonly chat: Chat;
  private readonly scheduler: Scheduler;
  private readonly api: API;
  private readonly imageGenerator: ImageGenerator;
  private readonly dictionary: Dictionary;

  public constructor(
    protected readonly message: Message,
    protected readonly feature: Feature,
  ) {
    this.chat = container.resolve<Chat>(Chat.name);
    this.scheduler = container.resolve<Scheduler>(Scheduler.name);
    this.api = container.resolve<API>(API.name);
    this.imageGenerator = container.resolve<ImageGenerator>(
      ImageGenerator.name,
    );
    this.dictionary = container.resolve<Dictionary>(Dictionary.name);
  }

  /**
   * Checks whether the message is a valid trigger for the feature, then
   * calls getResponsePromiseForMessage() if:
   * - the message is addressed directly to Gradbot, or
   * - Math.random() generates a number that is greater then or equal to
   * this.triggerProbability.
   */
  public async getResponse(): Promise<FeatureMessage | null> {
    if (this.isMessageTrigger()) {
      if (
        this.isMessageForGradbot() ||
        getTrueWithProbability(this.triggerProbability)
      ) {
        // Message is a valid trigger for the feature and either is addressed to
        // Gradbot, or has passed the probability check. Build the response.
        return await this.getResponsePromiseForMessage();
      }
    }
    return null;
  }

  /**
   * Override in feature-specific subclass to control which messages can trigger
   * a response from the feature. For example, feature may only respond to
   * messages containing a certain keyword or phrase.
   *
   * Note that, when isMessageTrigger() returns true, getResponsePromiseToMessage()
   * is called only if:
   * - the message is addressed directly to Gradbot, or
   * - Math.random() generates a number that is greater then or equal to
   * this.triggerProbability.
   */
  public isMessageTrigger(): boolean {
    return true;
  }

  /**
   * This function implements the logic by which a feature builds its response to a message. It must
   * return a promise with a `FeatureMessage` object or null. It is only called if the checks in
   * FeatureMessage.getResponse() pass.
   *
   * It's best if the function is left as `async`, then it's valid to return `FeatureMessage` or `null`
   * rather than creating a promise.
   */
  public abstract getResponsePromiseForMessage(): Promise<FeatureMessage | null>;

  /**
   * Checks whether a Chat message is addressed to Gradbot (i.e. starts or ends with 'g' or 'gradbot').
   *
   * @param message
   */
  protected isMessageForGradbot(): boolean {
    return (
      this.message.text.match(FeatureMessageHandler.GradbotAddressRegex) !==
      null
    );
  }

  /**
   * Returns a Chat message text with Gradbot address removed (i.e. 'g' or 'gradbot') from
   * beggining/end of message.
   *
   * @param message
   */
  protected getSanitisedText(): string {
    return this.message.text
      .replace(FeatureMessageHandler.GradbotAddressReplaceRegex, '')
      .trim();
  }

  /**
   * If the message is for Gradbot (starts/ends with 'g' or 'gradbot') turn its text lower case,
   * sanitze it and return regex match on that string.
   *
   * @param message
   */
  protected matchSanitisedLowercaseTextIfForGradbot(
    regex: RegExp,
  ): RegExpMatchArray | null {
    if (this.isMessageForGradbot()) {
      return this.getSanitisedText().toLowerCase().match(regex);
    }
    return null;
  }

  /**
   * Sends a message from Gradbot to Chat. **Consider using built-in scheduling instead!**
   *
   * @param message
   */
  protected async sendMessage(message: FeatureMessage) {
    await this.chat.sendMessage(message);
  }

  /**
   * Schedules a cron message, similarly to a cron job. These are not preserved when Gradbot is restarted.
   *
   * @param id - cron message id, can be used for cancellation. It's automatically prefixed
   * with feature name to avoid conflicts.
   * @param cronBuilder - Error-safe cron builder
   * @param message
   */
  protected scheduleCronMessage(
    id: string,
    cronBuilder: CronBuilder,
    message: FeatureMessage,
  ) {
    this.scheduler.scheduleCronMessage(
      this.getScheduledMessageId(id),
      cronBuilder,
      message,
    );
  }

  /**
   * Cancels cron message scheduled by `scheduleRepeatingMessage()`
   *
   * @param id - cron message id
   */
  protected cancelCronMessage(id: string) {
    this.scheduler.cancelCronMessage(this.getScheduledMessageId(id));
  }

  /**
   * Schedules a message at a specific date. These will survive Gradbot restarts.
   *
   * @param id - scheduled message id, can be used for cancellation. It's automatically prefixed
   * with feature name to avoid conflicts.
   * @param date - date at which to send the message, London time-zone. The message won't be sent
   * if the date is in the past.
   * @param message
   */
  protected async scheduleMessage(
    id: string,
    date: Date,
    message: FeatureMessage,
  ) {
    await this.scheduler.scheduleMessage(
      this.getScheduledMessageId(id),
      date,
      message,
    );
  }

  /**
   * Schedules a message a number of seconds in the future. These will survive Gradbot restarts.
   *
   * @param id - scheduled message id, can be used for cancellation. It's automatically prefixed
   * with feature name to avoid conflicts.
   * @param timeoutSeconds - number of seconds in the future.
   * @param message
   */
  protected async scheduleMessageWithTimeout(
    id: string,
    timeoutSeconds: number,
    message: FeatureMessage,
  ) {
    await this.scheduler.scheduleMessageWithTimeout(
      this.getScheduledMessageId(id),
      timeoutSeconds,
      message,
    );
  }

  /**
   * Cancels message scheduled by `scheduleMessageWithTimeout()` or `scheduleMessage()`
   *
   * @param id - scheduled message id
   */
  protected cancelScheduledMessage(id: string) {
    this.scheduler
      .cancelScheduledMessage(this.getScheduledMessageId(id))
      .catch((e) => log('Failed to cancel schedule message', e));
  }

  /**
   * Returns a promise resolving to a `ChatUser` object, containing either a real username or a replacement
   * name if the user was deleted.
   *
   * @param userId
   */
  protected getUserWithId(userId: string): Promise<User | undefined> {
    return this.chat.getUserWithId(userId, this.message.metadata);
  }

  /**
   * Returns a promise resolving to a `ChatUser` object or null if user's unknown.
   *
   * @param userId
   */
  protected getUserWithDisplayName(
    displayName: string,
  ): Promise<User | undefined> {
    return this.chat.getUserWithDisplayName(displayName, this.message.metadata);
  }

  protected async getUserWithDisplayNameOrId(
    nameOrId: string,
  ): Promise<User | undefined> {
    let user = await this.getUserWithDisplayName(nameOrId);
    if (!user) {
      user = await this.getUserWithId(nameOrId);
    }
    return user;
  }

  /**
   * Creates a message object
   *
   * @param text - Gradbot's message text
   * @param reason - Reason for the message, it should continue the sentence `I said {{text}} because...`
   * @param channelOverride - Channel to which the message will be sent if different then source
   *
   * @returns Gradbot message
   */
  protected createMessage(
    text: string,
    reason: string,
    channelOverride?: Channel,
  ): FeatureMessage {
    return new FeatureMessage(
      text,
      reason,
      this.feature.name,
      this.message.metadata,
      channelOverride,
    );
  }

  /**
   * Creates a message object
   *
   * @param text - Gradbot's message text
   * @param reason - Reason for the message, it should continue the sentence `I said {{text}} because...`
   * @param channelId - Channel to which the message will be sent if different then source
   *
   * @returns Gradbot message
   */
  protected createMessageToChannel(
    text: string,
    reason: string,
    channelId: string,
  ): FeatureMessage {
    return this.createMessage(text, reason, new Channel(channelId));
  }

  /**
   * Creates a message object and cleans the text of mentions
   *
   * @param text - Gradbot's message text
   * @param reason - Reason for the message, it should continue the sentence `I said {{text}} because...`
   * @param channelOverride - Channel to which the message will be sent if different then source
   *
   * @returns Gradbot message
   */
  protected cleanMessage(
    text: string,
    reason: string,
    channelOverride?: Channel,
  ): FeatureMessage {
    const cleanText: string = text.replace(
      FeatureMessageHandler.GradbotCleanMentions,
      '@ $1',
    );
    const cleanReason: string = reason.replace(
      FeatureMessageHandler.GradbotCleanMentions,
      '@ $1',
    );

    return this.createMessage(cleanText, cleanReason, channelOverride);
  }

  /**
   * Creates a message object
   *
   * @param text - Gradbot's message text
   * @param reason - Reason for the message, it should continue the sentence `I said {{text}} because...`
   * @param userId - User to which the message will be sent
   *
   * @returns Gradbot message
   */
  protected createMessageToUser(
    text: string,
    reason: string,
    userId: string,
  ): FeatureMessage {
    return this.createMessage(text, reason, new Channel(userId, true));
  }

  /**
   * Returns a promise resolving to an `Image` object, that can be attached to a `FeatureMessage`.
   *
   * @param path - URL or (unlikely) local
   */
  protected getImageFromPath(path: string, timeout: number = FeatureTimeout) {
    return this.imageGenerator.fromPath(path, timeout);
  }

  /**
   * Returns a promise resolving to an `Image` object, that can be attached to a `FeatureMessage`.
   *
   * @param buffer
   */
  protected getImageFromBuffer(buffer: Buffer) {
    return this.imageGenerator.fromBuffer(buffer);
  }

  /**
   * Returns a promise resolving to an `String` object, containing the full dictionary text.
   */
  protected getDictionary() {
    return this.dictionary.getDict();
  }

  /**
   * Returns a promise resolving to a `T` type object after fetching a JSON API.
   *
   * @param url
   */
  protected async fetchAPI<T>(url: string): Promise<T | null> {
    return this.api.fetch<T>(url);
  }

  /**
   * Returns a promise resolving to a string representing the text of an API response.
   *
   * @param url
   */
  protected async fetchAPIText(url: string): Promise<string | null> {
    return this.api.fetchText(url);
  }

  private getScheduledMessageId(id: string): string {
    return `${this.feature.name}__${id}`.replace(/\W+/g, '_');
  }
}
