import { noIndent } from '../../../util/general/string-extensions';
import { Feature } from '../feature';
import { ServiceMessageHandler } from './service-message-handler';
import { Message } from '../../chat/message/message';
import { FeatureMessageHandler } from '../feature-message-handler';
import { Chat } from '../../chat/chat';

export class ServiceFeature extends Feature {
  public readonly name = 'Service';
  public readonly iCanInfo = 'give you service info';
  public readonly instructions = noIndent(
    `• *g are you ok/alive?* – I'll tell you how I am
     • *g what was that?* – I'll explain the last message (optionally add a number)
     • *g help/what can you do?* – I'll list my features
     • *g help <feature name>* – I'll give you instructions for a feature
     • *g who are you?* – I'll introduce myself`,
  );
  protected readonly messageHandlerType = ServiceMessageHandler;
  private readonly timeStarted = new Date();

  constructor(
    private readonly orderedFeatures: Feature[],
    private readonly serviceChat: Chat,
  ) {
    super();
  }

  public getMessageHandler(message: Message): FeatureMessageHandler {
    return new ServiceMessageHandler(
      message,
      this,
      this.orderedFeatures,
      this.serviceChat,
      this.timeStarted,
    );
  }
}
