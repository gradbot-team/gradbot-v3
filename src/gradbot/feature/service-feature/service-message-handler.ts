import {
  HumanizeDurationLanguage,
  HumanizeDuration,
} from 'humanize-duration-ts';
import { FeatureMessageHandler } from '../feature-message-handler';
import { FeatureMessage } from '../feature-message';
import { Feature } from '../feature';
import { Message } from '../../chat/message/message';
import { Chat } from '../../chat/chat';
import { getRandomInt } from '../../../util/general/random-extensions';

export class ServiceMessageHandler extends FeatureMessageHandler {
  private static readonly HistorySnippetLength = 30;
  private readonly durationHumanizer = new HumanizeDuration(
    new HumanizeDurationLanguage(),
  );
  private readonly iAmStrings = [
    'I am indeed!',
    'Yes.',
    'Yup!',
    'Yeah!',
    'Very much so!',
  ];

  public constructor(
    message: Message,
    feature: Feature,
    private readonly orderedFeatures?: Feature[],
    private readonly serviceChat?: Chat,
    private readonly timeStarted?: Date,
  ) {
    super(message, feature);
  }

  public getResponsePromiseForMessage(): Promise<FeatureMessage | null> {
    if (!this.isMessageForGradbot()) {
      return Promise.resolve(null);
    }

    const messageText = this.getSanitisedText().toLowerCase();
    const whatWasThatMatch = messageText.match(
      /^\W*(?:what was that|why did you say that)\W*([0-9]*)\W*$/,
    );
    const featureHelpMatch = messageText.match(/^\W*help\s+(.*)\W*$/);
    let responseText = '';

    if (whatWasThatMatch) {
      const messageHistory = this.serviceChat
        ? this.serviceChat.getMessageHistory(this.message.metadata)
        : [];

      if (messageHistory.length === 0) {
        responseText =
          "I can't remember saying anything. That can happen if I was restarted recently 😟";
      }

      let numberOfResponsesRequested = 1;

      if (whatWasThatMatch[1]) {
        numberOfResponsesRequested = parseInt(whatWasThatMatch[1], 10);
      }

      for (
        let i = 0;
        i < numberOfResponsesRequested && i < messageHistory.length;
        i++
      ) {
        const messageHistoryText =
          messageHistory[i].text.length <
          ServiceMessageHandler.HistorySnippetLength
            ? messageHistory[i].text
            : `${messageHistory[i].text.substr(
                0,
                ServiceMessageHandler.HistorySnippetLength,
              )}...`;

        responseText += `I said "${messageHistoryText}" because ${messageHistory[i].reason} (${messageHistory[i].featureName} Feature)\n`;
      }
    } else if (featureHelpMatch) {
      const featureName = featureHelpMatch[1].trim();
      const matchingFeatures = [
        this.feature,
        ...(this.orderedFeatures || []),
      ].filter((feature) => feature.name.toLowerCase() === featureName);

      if (matchingFeatures.length === 1) {
        const feature = matchingFeatures[0];
        responseText = `*${feature.name} Feature*\n${feature.instructions}`;
      } else {
        responseText = "I don't recognise this feature";
      }
    } else if (messageText.match(/^\W*help|what can you do\W*$/)) {
      responseText = 'I can do many things:\n';
      [this.feature, ...(this.orderedFeatures || [])].forEach(
        (feature) =>
          (responseText += `• ${feature.iCanInfo} (${feature.name})\n`),
      );
      responseText += '\n*g help <feature name>* for details';
    } else if (
      messageText.match(/^\W*(?:are)?\W*you\W*(?:alive|ok|awake|alright)\W*$/)
    ) {
      const durationAlive = this.durationHumanizer.humanize(
        new Date().getTime() -
          (this.timeStarted ? this.timeStarted.getTime() : 0),
        { conjunction: ' and ', serialComma: false, round: true },
      );

      responseText = `${
        this.iAmStrings[getRandomInt(this.iAmStrings.length)]
      } I have been awake for ${durationAlive}`;
    } else if (messageText.match(/^\W*who\W*are\W*you\W*$/)) {
      responseText = "I'm but a humble bot. Name's Gradbot. Who are you?";
    }

    return Promise.resolve(
      responseText ? this.createMessage(responseText, '') : null,
    );
  }
}
