import { Message } from './chat/message/message';
import { OrderedFeatureTypes } from '../features/ordered-features';
import { FeatureMessage } from './feature/feature-message';
import { ServiceFeature } from './feature/service-feature/service-feature';
import { log } from '../util/log/log';
import { toString } from '../util/general/string-extensions';
import { container } from 'tsyringe';
import { FeatureTimeout } from './constants/constants';
import { Feature } from './feature/feature';
import { Chat } from './chat/chat';

export class Gradbot {
  private readonly features: Feature[];
  private readonly chat: Chat;

  public constructor() {
    this.chat = container.resolve<Chat>(Chat.name);

    const orderedFeatures: Feature[] = [];

    for (const T of OrderedFeatureTypes) {
      const feature = new T();
      orderedFeatures.push(feature);
      log(`${feature.name} Feature initialised`);
    }

    this.features = [
      new ServiceFeature(orderedFeatures, this.chat),
      ...orderedFeatures,
    ];
  }

  public start() {
    this.chat.incomingMessages$.subscribe((message) => {
      (async (m: Message) => this.processMessage(m))(message);
    });
  }

  private async processMessage(message: Message): Promise<void> {
    const response = await this.raceResponses(
      this.features.map((feature) =>
        feature
          .getMessageHandler(message)
          .getResponse()
          .catch((error) => {
            if (error instanceof Error) {
              log(
                'Feature exception',
                `${feature.name}: ${toString(error.stack)}`,
              );
            } else {
              log('Unknown feature exception', `${feature.name}:`);
            }
            return null;
          }),
      ),
    );

    if (response) {
      await this.chat.sendMessage(response);
    }
  }

  private async raceResponses(
    responsePromises: Promise<FeatureMessage | null>[],
  ): Promise<FeatureMessage | null> {
    const responses = await Promise.all(
      responsePromises.map((response) =>
        Promise.race([
          response,
          new Promise<null>((resolve) =>
            setTimeout(() => resolve(null), FeatureTimeout),
          ),
        ]),
      ),
    );
    const validResponse = responses.find((response) => response != null);
    return validResponse ? validResponse : null;
  }
}
