/**
 * Builds a cron string.
 *
 * | time unit        | valid values               |
 * |------------------|----------------------------|
 * | minute           | 0 - 59                     |
 * | hour             | 0 - 23                     |
 * | day of the month | 1 - 31                     |
 * | month            | 1 - 12                     |
 * | day of the week  | 0 - 6 (Sunday to Saturday) |
 *
 * This builder supports chaining e.g. to schedule a message to be sent every Monday at 8:30:
 * ```
 * DCCronBuilder.create().withDayOfWeek(1).withHour(8).withMinute(30);
 * ```
 */
export class CronBuilder {
  /**
   * This builder s
   */
  public static create() {
    return new CronBuilder();
  }

  private minute = '*';
  private hour = '*';
  private dayOfMonth = '*';
  private month = '*';
  private dayOfWeek = '*';

  private constructor(private readonly cronString?: string) {}

  /**
   * @param minute - 0-59
   */
  public withMinute(minute: number) {
    minute = Math.floor(minute);

    if (minute >= 0 && minute <= 59) {
      this.minute = minute.toString();
    }
    return this;
  }

  /**
   * @param hour - 0-23
   */
  public withHour(hour: number) {
    hour = Math.floor(hour);

    if (hour >= 0 && hour <= 23) {
      this.hour = hour.toString();
    }
    return this;
  }

  /**
   * @param dayOfMonth - 1-31
   */
  public withDayOfMonth(dayOfMonth: number) {
    dayOfMonth = Math.floor(dayOfMonth);

    if (dayOfMonth >= 1 && dayOfMonth <= 31) {
      this.dayOfMonth = dayOfMonth.toString();
    }
    return this;
  }

  /**
   * @param dayOfWeek - 0-6 (Sunday to Saturday)
   */
  public withDayOfWeek(dayOfWeek: number) {
    dayOfWeek = Math.floor(dayOfWeek);

    if (dayOfWeek >= 1 && dayOfWeek <= 7) {
      this.dayOfWeek = dayOfWeek.toString();
    }
    return this;
  }

  /**
   * @param month - 1-12
   */
  public withMonth(month: number) {
    month = Math.floor(month);

    if (month >= 1 && month <= 12) {
      this.month = month.toString();
    }
    return this;
  }

  public build() {
    return this.cronString
      ? this.cronString
      : `${this.minute} ${this.hour} ${this.dayOfMonth} ${this.month} ${this.dayOfWeek}`;
  }
}
