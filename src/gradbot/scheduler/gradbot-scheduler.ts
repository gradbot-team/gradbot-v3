import { FeatureMessage } from '../feature/feature-message';
import { scheduleJob, cancelJob, Job } from 'node-schedule';
import { CronBuilder } from './cron-builder';
import { log } from '../../util/log/log';
import { MAXINT32 } from '../../util/general/constants';
import { Scheduler } from './scheduler';
import { Metadata } from '../chat/message/metadata';
import { Chat } from '../chat/chat';
import { Channel } from '../chat/message/channel';
import { SchedulerData } from './scheduler-data';

export class GradbotScheduler extends Scheduler {
  private readonly cronTasks: { [key: string]: Job } = {};
  private readonly scheduledTasks: { [key: string]: NodeJS.Timeout } = {};

  constructor(private readonly chat: Chat) {
    super();
    void this.loadFromDb();
  }

  public scheduleCronMessage(
    id: string,
    cronBuilder: CronBuilder,
    message: FeatureMessage,
  ) {
    if (this.cronTasks[id]) {
      cancelJob(this.cronTasks[id]);
    }
    this.cronTasks[id] = scheduleJob(cronBuilder.build(), () => {
      this.chat
        .sendMessage(message)
        .catch((e) => log('Failed to send scheduled message', e));
    });
  }

  public cancelCronMessage(id: string) {
    if (this.cronTasks[id]) {
      cancelJob(this.cronTasks[id]);
      delete this.cronTasks[id];
    }
  }

  public async scheduleMessage(
    id: string,
    date: Date,
    message: FeatureMessage,
  ): Promise<void> {
    if (message.image) {
      throw new Error('Scheduling messages with images not supported');
    }
    return this.saveScheduledMessage(id, date, message);
  }

  public async cancelScheduledMessage(id: string): Promise<void> {
    if (this.scheduledTasks[id]) {
      clearTimeout(this.scheduledTasks[id]);
    }
    delete this.scheduledTasks[id];

    const schedulerData = await this.getDataForId(id);

    if (schedulerData) {
      await schedulerData.delete();
    }
  }

  private async saveScheduledMessage(
    id: string,
    date: Date,
    message: FeatureMessage,
  ): Promise<void> {
    const taskTime = date.getTime();
    const nowTime = new Date().getTime();

    if (taskTime > nowTime) {
      if (this.scheduledTasks[id]) {
        clearTimeout(this.scheduledTasks[id]);
      }

      let schedulerData = await this.getDataForId(id);

      if (!schedulerData) {
        schedulerData = new SchedulerData();
        schedulerData.id.set(id);
      }

      if (schedulerData) {
        const diff = taskTime - nowTime;
        if (diff > MAXINT32) {
          // setTimeout limit is MAX_INT32=(2^31-1)
          const reschedule = () => {
            (async () => await this.saveScheduledMessage(id, date, message))();
          };
          this.scheduledTasks[id] = setTimeout(reschedule, MAXINT32);
        } else {
          this.scheduledTasks[id] = setTimeout(() => {
            this.chat
              .sendMessage(message)
              .catch((e) => log('Failed to send scheduled message', e));
          }, diff);
        }
        schedulerData.messageText.set(message.text);
        schedulerData.messageReason.set(message.reason);
        schedulerData.feature.set(message.featureName);
        schedulerData.time.set(date);
        schedulerData.metadataMessageId.set(message.metadata.messageId);
        schedulerData.metadataChatId.set(message.metadata.chatId);
        schedulerData.metadataServerId.set(message.metadata.serverId);
        schedulerData.metadataChannelId.set(message.metadata.channel.id);
        schedulerData.metadataChannelIsPrivate.set(
          message.metadata.channel.isPrivate,
        );

        if (message.channelOverride) {
          schedulerData.channelOverrideId.set(message.channelOverride.id);
          schedulerData.channelOverrideIsPrivate.set(
            message.channelOverride.isPrivate,
          );
        }

        await schedulerData.write();
      }
    }
  }

  private async loadFromDb(): Promise<void> {
    try {
      const scheduleTasks: Promise<void>[] = [];

      const dataRetriever = new SchedulerData();
      const schedulerDataList = await dataRetriever.retrieveAlike();

      schedulerDataList.forEach((schedulerData) => {
        (async (data: SchedulerData) => {
          const channelOverride = new Channel(
            data.channelOverrideId.get(),
            data.channelOverrideIsPrivate.get(),
          );

          const message = new FeatureMessage(
            data.messageText.get(),
            data.messageReason.get(),
            data.feature.get(),
            new Metadata(
              data.metadataMessageId.get(),
              data.metadataChatId.get(),
              new Channel(
                data.metadataChannelId.get(),
                data.metadataChannelIsPrivate.get(),
              ),
              data.metadataServerId.get(),
            ),
            channelOverride,
          );

          await data.delete();

          scheduleTasks.push(
            this.saveScheduledMessage(data.id.get(), data.time.get(), message),
          );
        })(schedulerData);
      });

      await Promise.all(scheduleTasks);
      log('Scheduler initialised');
    } catch (err) {
      log('Initialising Scheduler failed', err);
      process.exit(1);
    }
  }

  private async getDataForId(id: string) {
    const dataRetriever = new SchedulerData();
    dataRetriever.id.set(id);
    const existingData = await dataRetriever.retrieveAlike();

    if (existingData.length === 1) {
      return existingData[0];
    }
  }
}
