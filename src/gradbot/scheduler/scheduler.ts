import { FeatureMessage } from '../feature/feature-message';
import { CronBuilder } from './cron-builder';

export abstract class Scheduler {
  public abstract scheduleCronMessage(
    id: string,
    cronBuilder: CronBuilder,
    message: FeatureMessage,
  ): void;

  public abstract cancelCronMessage(id: string): void;

  public abstract scheduleMessage(
    id: string,
    date: Date,
    message: FeatureMessage,
  ): Promise<void>;

  public scheduleMessageWithTimeout(
    id: string,
    timeoutSeconds: number,
    message: FeatureMessage,
  ): Promise<void> {
    const date = new Date();
    date.setSeconds(date.getSeconds() + timeoutSeconds);
    return this.scheduleMessage(id, date, message);
  }

  public abstract cancelScheduledMessage(id: string): Promise<void>;
}
