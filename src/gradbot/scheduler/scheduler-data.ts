import { DbText } from '../database/types/db-text';
import { DbDate } from '../database/types/db-date';
import { DbBoolean } from '../database/types/db-boolean';
import { DBO } from '../database/db-objects/registry/dbo-decorator';
import { SystemDbObject } from '../database/db-objects/system-db-object';

@DBO
export class SchedulerData extends SystemDbObject<SchedulerData> {
  id = new DbText();
  messageText = new DbText();
  messageReason = new DbText();
  feature = new DbText();
  time = new DbDate();
  metadataMessageId = new DbText();
  metadataChatId = new DbText();
  metadataChannelId = new DbText();
  metadataChannelIsPrivate = new DbBoolean();
  metadataServerId = new DbText();
  channelOverrideId = new DbText();
  channelOverrideIsPrivate = new DbBoolean();
}
