import { Jimp, JimpMime } from 'jimp';
import { ImageGenerator } from './image-generator';
import { Image } from './image';
import { FeatureTimeout } from '../constants/constants';
import { log } from '../../util/log/log';
import { MaxImageSize, MaxThumbnailSize } from '../constants/constants';

// FIXME Create a stupid local class to enable typescript annotations to work,
// look to get rid of this ASAP
class JimpClass extends Jimp {}

export class GradbotImageGenerator extends ImageGenerator {
  public async fromPath(
    path: string,
    reqTimeout: number = FeatureTimeout,
  ): Promise<Image | null> {
    let timeoutPid: NodeJS.Timeout;
    const timeout = new Promise((resolve, reject): null => {
      timeoutPid = setTimeout(
        (): void =>
          reject(new Error(`Promise timed out after ${reqTimeout}ms`)),
        reqTimeout,
      );
      return null;
    });

    const promise = Promise.race([Jimp.read(path), timeout])

      .then((result) => result)
      .catch((err: Error) => {
        // Didn't manage to get the image - log why, return null.
        log('ImageGenerator error for URL ' + path + ':' + err.message);
        return null;
      })
      .finally(() => {
        if (timeoutPid) {
          clearTimeout(timeoutPid);
        }
      });

    const image = await promise;

    if (image) {
      return await this.getImage(path, image as JimpClass);
    } else {
      return null;
    }
  }

  public async fromBuffer(buffer: Buffer): Promise<Image | null> {
    return await this.getImage(
      '',
      (await Jimp.fromBuffer(buffer)) as JimpClass,
    );
  }

  // FIXME Typescript appears to have some difficultly with the Jimp type in this
  // function, so disable the warnings for now
  /* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access */
  private async getImage(url: string, image: JimpClass): Promise<Image | null> {
    if (image === null || image === undefined) {
      return null;
    }

    const thumbnail: JimpClass = image.clone();
    return new Image(
      await image
        .scaleToFit({ w: MaxImageSize, h: MaxImageSize })
        .getBuffer(JimpMime.jpeg, { quality: 70 }),

      await thumbnail
        .cover({ w: MaxThumbnailSize, h: MaxThumbnailSize })
        .getBuffer(JimpMime.jpeg, { quality: 70 }),

      url,
    );
  }
  /* eslint-enable */
}
