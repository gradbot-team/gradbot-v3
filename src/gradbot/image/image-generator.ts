import { Image } from './image';

export abstract class ImageGenerator {
  public abstract fromPath(
    path: string,
    reqTimeout: number,
  ): Promise<Image | null>;
  public abstract fromBuffer(buffer: Buffer): Promise<Image | null>;
}
