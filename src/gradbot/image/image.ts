export class Image {
  public constructor(
    public readonly imageBuffer: Buffer,
    public readonly thumbnailBuffer: Buffer,
    public readonly url: string,
  ) {}
}
