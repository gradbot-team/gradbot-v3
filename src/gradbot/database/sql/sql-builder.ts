enum SQLBuilderDirections {
  ASCENDING = 'ASC',
  DESCENDING = 'DESC',
}

export class SQLBuilder {
  private fromString = '';
  private whereStrings: string[] = [];
  private valueStrings: string[] = [];
  private orderByColumns: string[] = [];
  private orderByDirection: SQLBuilderDirections[] = [];

  private selectString = '';
  private insertString = '';
  private updateString = '';
  private deleteString = '';

  public static select(statement: string): SQLBuilder {
    const builder = new SQLBuilder();
    builder.selectString = statement;
    return builder;
  }

  public static insertInto(statement: string): SQLBuilder {
    const builder = new SQLBuilder();
    builder.insertString = statement;
    return builder;
  }

  public static deleteFrom(statement: string): SQLBuilder {
    const builder = new SQLBuilder();
    builder.deleteString = statement;
    return builder;
  }

  public static update(statement: string): SQLBuilder {
    const builder = new SQLBuilder();
    builder.updateString = statement;
    return builder;
  }

  public from(statement: string): SQLBuilder {
    this.fromString = statement;
    return this;
  }

  public where(statement: string): SQLBuilder {
    this.whereStrings.push(statement);
    return this;
  }

  public orderByAscend(statement: string): SQLBuilder {
    this.orderByColumns.push(statement);
    this.orderByDirection.push(SQLBuilderDirections.ASCENDING);
    return this;
  }

  public orderByDescend(statement: string): SQLBuilder {
    this.orderByColumns.push(statement);
    this.orderByDirection.push(SQLBuilderDirections.DESCENDING);
    return this;
  }

  public value(statement: string): SQLBuilder {
    this.valueStrings.push(statement);
    return this;
  }

  public isInsert(): boolean {
    return this.insertString !== '';
  }

  private generateWhereClause(): string {
    if (this.whereStrings.length) {
      return ' WHERE ' + this.whereStrings.join(' AND ');
    }
    return '';
  }

  private generateOrderByClause(): string {
    if (this.orderByColumns.length > 0) {
      const orderByStrings: string[] = [];

      for (let _i = 0; _i < this.orderByColumns.length; _i++) {
        orderByStrings.push(
          this.orderByColumns[_i] + ' ' + this.orderByDirection[_i],
        );
      }
      return ' ORDER BY ' + orderByStrings.join(', ');
    }
    return '';
  }

  public build(): string {
    let sqlString = '';
    if (this.selectString) {
      sqlString = 'SELECT ' + this.selectString + ' FROM ' + this.fromString;
    } else if (this.insertString) {
      sqlString = 'INSERT INTO ' + this.insertString + ' (';
      sqlString += this.valueStrings.join(', ') + ') VALUES (';
      sqlString += '?, '.repeat(this.valueStrings.length - 1) + '?)';
    } else if (this.updateString) {
      sqlString = 'UPDATE ' + this.updateString + ' SET ';
      sqlString += this.valueStrings.join(' = ?, ') + ' = ?';
    } else if (this.deleteString) {
      sqlString = 'DELETE FROM ' + this.deleteString;
    }

    if (!this.insertString) {
      sqlString += this.generateWhereClause();
    }

    if (this.selectString) {
      sqlString += this.generateOrderByClause();
    }

    return sqlString;
  }
}
