export class SQLStatement {
  constructor(
    public readonly sql: string,
    public readonly parameters?: (string | number)[],
  ) {}
}
