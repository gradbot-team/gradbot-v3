import { Database as SQLiteDatabase } from 'sqlite3';
import { log } from '../../util/log/log';
import { TableDefinition } from './definitions/table-definition';
import { dbObjectRegistry } from './db-objects/registry/db-objects-registry';
import { DbObject } from './db-objects/db-object';
import { SQLStatement } from './sql/sql-statement';

export class Database {
  private readonly db: SQLiteDatabase;

  public constructor(dbPath: string) {
    this.db = new SQLiteDatabase(dbPath);
  }

  public async prepareTables() {
    const tableDefinitionPromises: Promise<void>[] = [];

    for (const DbObjectType of dbObjectRegistry.dbObjects) {
      // we create an object to get column names and types, but skipping SQLDbObject implementation
      // because that one requires a message. We cast directly to DbObject instead.
      const dbObject = new (DbObjectType as any as new () => DbObject)();
      tableDefinitionPromises.push(
        this.createTableIfNotExisting(dbObject.getTableDefinition()),
      );
    }

    await Promise.all(tableDefinitionPromises);
  }

  public write(sqlStatement: SQLStatement): Promise<void> {
    return new Promise((resolve, reject) => {
      const callback = (err: Error | null) => (err ? reject(err) : resolve());
      const preparedStatement = this.db.prepare(sqlStatement.sql);
      preparedStatement.run(sqlStatement.parameters, callback);
      preparedStatement.finalize();
    });
  }

  public read(sqlStatement: SQLStatement): Promise<{ [key: string]: any }[]> {
    return new Promise((resolve, reject) => {
      const callback = (err: Error | null, rows: { [key: string]: any }[]) =>
        err ? reject(err) : resolve(rows);
      this.db.all(sqlStatement.sql, sqlStatement.parameters, callback);
    });
  }

  private async createTableIfNotExisting(
    tableDefinition: TableDefinition,
  ): Promise<void> {
    try {
      const existingTableRows = await this.read(
        new SQLStatement(
          "SELECT name FROM sqlite_master WHERE type='table' AND name = ?;",
          [tableDefinition.tableName],
        ),
      );

      if (existingTableRows.length === 0) {
        await this.write(
          new SQLStatement(
            `CREATE TABLE ${tableDefinition.tableName} ` +
              `(${this.getColumnsFromDefinition(tableDefinition).join(', ')});`,
          ),
        );
        log(`Table '${tableDefinition.tableName}' created`);
      } else {
        log(
          `Table '${tableDefinition.tableName}' already exists, checking columns`,
        );
        const columns: { [key: string]: any }[] = await this.read(
          new SQLStatement(`PRAGMA table_info(${tableDefinition.tableName});`),
        );
        const columnsToAdd = this.getColumnsFromDefinition(
          tableDefinition,
          columns.map((column) => column.name as string),
        );

        if (columnsToAdd.length > 0) {
          for (const col of columnsToAdd) {
            await this.write(
              new SQLStatement(
                `ALTER TABLE ${tableDefinition.tableName} ADD ${col};`,
              ),
            );
            log(`Added column: ${col} to table ${tableDefinition.tableName}`);
          }
          log(
            `Added all required columns added to table ${tableDefinition.tableName}`,
          );
        } else {
          log(
            `Table '${tableDefinition.tableName}' has all the required columns`,
          );
        }
      }
    } catch (err) {
      log(`Error creating table '${tableDefinition.tableName}'`, err);
      process.exit(1);
    }
  }

  private getColumnsFromDefinition(
    tableDefinition: TableDefinition,
    except: string[] = [],
  ): string[] {
    const columnsWithTypes: string[] = [];
    for (const fieldDefinition of tableDefinition.fieldDefinitions) {
      if (!except.includes(fieldDefinition.name)) {
        columnsWithTypes.push(
          `${fieldDefinition.name} ${fieldDefinition.type}`,
        );
      }
    }
    return columnsWithTypes;
  }
}
