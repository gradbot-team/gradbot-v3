import { FieldDefinition } from './field-definition';
import {
  IntegrationIsolation,
  ChannelIsolation,
  ServerIsolation,
  SQLiteTextType,
} from '../db-constants';

export class TableDefinition {
  public readonly fieldDefinitions: FieldDefinition[] = [
    new FieldDefinition(IntegrationIsolation, SQLiteTextType),
    new FieldDefinition(ChannelIsolation, SQLiteTextType),
    new FieldDefinition(ServerIsolation, SQLiteTextType),
  ];

  public constructor(public readonly tableName: string) {}
}
