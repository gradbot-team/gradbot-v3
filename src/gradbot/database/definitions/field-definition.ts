export class FieldDefinition {
  constructor(
    public readonly name: string,
    public readonly type: string,
  ) {}
}
