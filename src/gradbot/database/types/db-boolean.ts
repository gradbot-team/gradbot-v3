import { SQLValue } from './sql-value';
import { SQLiteIntegerType } from '../db-constants';

export class DbBoolean extends SQLValue<boolean> {
  public readonly SQLType = SQLiteIntegerType;
  protected value = false;

  public setFromSQL(sqliteValue: number) {
    this.value = sqliteValue === 1;
  }

  public getForSQL() {
    return this.get() ? 1 : 0;
  }

  protected _reset(): void {
    this.value = false;
  }
}
