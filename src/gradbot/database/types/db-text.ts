import { SQLValue } from './sql-value';
import { SQLiteTextType } from '../db-constants';

export class DbText extends SQLValue<string> {
  public readonly SQLType = SQLiteTextType;
  protected value = '';

  protected _reset(): void {
    this.value = '';
  }
}
