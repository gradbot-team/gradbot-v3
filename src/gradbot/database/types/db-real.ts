import { SQLValue } from './sql-value';
import { SQLiteRealType } from '../db-constants';

export class DbReal extends SQLValue<number> {
  public readonly SQLType = SQLiteRealType;
  protected value = 0;

  protected _reset(): void {
    this.value = 0;
  }
}
