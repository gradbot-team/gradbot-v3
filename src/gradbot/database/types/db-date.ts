import { SQLValue } from './sql-value';
import { SQLiteIntegerType } from '../db-constants';

export class DbDate extends SQLValue<Date> {
  public readonly SQLType = SQLiteIntegerType;
  protected value = new Date();

  public setFromSQL(sqliteValue: number) {
    this.value = new Date(sqliteValue);
  }

  public getForSQL() {
    return this.get().getTime();
  }

  protected _reset(): void {
    this.value = new Date();
  }
}
