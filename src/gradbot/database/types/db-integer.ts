import { SQLValue } from './sql-value';
import { SQLiteIntegerType } from '../db-constants';

export class DbInteger extends SQLValue<number> {
  public readonly SQLType = SQLiteIntegerType;
  protected value = 0;

  public getForSQL() {
    return Math.floor(this.value);
  }

  protected _reset(): void {
    this.value = 0;
  }
}
