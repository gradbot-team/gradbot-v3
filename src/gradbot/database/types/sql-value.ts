export abstract class SQLValue<DataType> {
  protected abstract value: DataType;
  public isModified = false;

  constructor(public readonly fieldNameOverride?: string) {}

  public get() {
    return this.value;
  }

  public set(value: DataType) {
    this.isModified = true;
    this.value = value;
  }

  public reset() {
    this.isModified = false;
    this._reset();
  }

  public setFromSQL(sqliteValue: number | string) {
    this.value = sqliteValue as any as DataType;
  }

  public getForSQL(): number | string {
    return this.get() as any as number | string;
  }

  public abstract readonly SQLType: string;

  protected abstract _reset(): void;
}
