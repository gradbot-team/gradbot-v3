export const SQLiteTextType = 'TEXT';
export const SQLiteIntegerType = 'INTEGER';
export const SQLiteRealType = 'REAL';
export const SQLiteRowIdColumn = 'rowid';

export const IntegrationIsolation = 'integration_isolation';
export const ServerIsolation = 'server_isolation';
export const ChannelIsolation = 'channel_isolation';
