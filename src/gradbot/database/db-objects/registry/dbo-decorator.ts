import { dbObjectRegistry } from './db-objects-registry';
import { DbObject } from '../db-object';

export const DBO = (target: unknown) => {
  dbObjectRegistry.addDbObject(target as new () => DbObject);
};
