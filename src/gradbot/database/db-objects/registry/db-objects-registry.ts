import { DbObject } from '../db-object';
import { log } from '../../../../util/log/log';

class DbObjectRegistry {
  public readonly dbObjects: (typeof DbObject)[] = [];

  public addDbObject(dbObject: typeof DbObject) {
    log(`${dbObject.name} database object found`);
    this.dbObjects.push(dbObject);
  }
}

export const dbObjectRegistry = new DbObjectRegistry();
