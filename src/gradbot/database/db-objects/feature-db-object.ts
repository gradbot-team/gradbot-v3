import { DbObject } from './db-object';
import { Message } from '../../chat/message/message';
import { SQLBuilder } from '../sql/sql-builder';
import { SQLDbObject } from './sql-db-object';
import { DbIsolationLevel } from './db-isolation-level';
import {
  IntegrationIsolation,
  ChannelIsolation,
  ServerIsolation,
  SQLiteRowIdColumn,
} from '../db-constants';

export abstract class FeatureDbObject<
  T extends DbObject,
> extends SQLDbObject<T> {
  protected abstract readonly isolationLevel: DbIsolationLevel;

  constructor(
    private readonly message: Message,
    rowId?: string,
  ) {
    super(rowId);
  }

  protected getInsertStatementBase() {
    const builder = SQLBuilder.insertInto(this.tableName)
      .value(IntegrationIsolation)
      .value(ServerIsolation);

    const parameters = [
      this.message.metadata.chatId,
      this.message.metadata.serverId,
    ];

    if (this.isolationLevel === DbIsolationLevel.CHANNEL) {
      builder.value(ChannelIsolation);
      parameters.push(this.message.metadata.channel.id);
    }

    return { builder, parameters };
  }

  protected getRetrieveAlikeStatementBase() {
    const builder = SQLBuilder.select(`*, ${SQLiteRowIdColumn}`)
      .from(this.tableName)
      .where(`${IntegrationIsolation} = ?`)
      .where(`${ServerIsolation} = ?`);

    const parameters = [
      this.message.metadata.chatId,
      this.message.metadata.serverId,
    ];

    if (this.isolationLevel === DbIsolationLevel.CHANNEL) {
      builder.where(`${ChannelIsolation} = ?`);
      parameters.push(this.message.metadata.channel.id);
    }

    return { builder, parameters };
  }

  protected getNewDbObjectOfSameType(typeRowId: string) {
    const SameType = this.constructor as new (
      message: Message,
      constructorArg: string,
    ) => T;
    return new SameType(this.message, typeRowId);
  }
}
