import { SQLValue } from '../types/sql-value';
import { TableDefinition } from '../definitions/table-definition';
import { FieldDefinition } from '../definitions/field-definition';

export abstract class DbObject {
  public readonly tableName = this.constructor.name;

  public getFieldWithName(fieldName: string) {
    return this.getFieldProperties().find(
      (fieldProperty) => fieldProperty.fieldName === fieldName,
    );
  }

  public getTableDefinition() {
    const tableDefinition = new TableDefinition(this.tableName);

    this.getFieldProperties().forEach((fieldProperty) => {
      tableDefinition.fieldDefinitions.push(
        new FieldDefinition(
          fieldProperty.fieldName,
          fieldProperty.value.SQLType,
        ),
      );
    });

    return tableDefinition;
  }

  protected getFieldProperties(modifiedOnly = false): FieldProperty[] {
    return Object.entries(this)
      .filter(
        ([
          {
            /* name*/
          },
          value,
        ]) => value instanceof SQLValue,
      )
      .map(([name, value]) => {
        return {
          propertyName: name,
          fieldName: (value as SQLValue<any>).fieldNameOverride || name,
          value: value as SQLValue<any>,
        };
      })
      .filter((fieldProperty) =>
        modifiedOnly ? fieldProperty.value.isModified : true,
      );
  }
}

export interface FieldProperty {
  readonly propertyName: string;
  readonly fieldName: string;
  readonly value: SQLValue<any>;
}
