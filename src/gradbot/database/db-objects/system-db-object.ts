import { DbObject } from './db-object';
import { SQLBuilder } from '../sql/sql-builder';
import { SQLiteRowIdColumn } from '../db-constants';
import { SQLDbObject } from './sql-db-object';

export abstract class SystemDbObject<
  T extends DbObject,
> extends SQLDbObject<T> {
  constructor(rowId?: string) {
    super(rowId);
  }

  protected getInsertStatementBase() {
    return {
      builder: SQLBuilder.insertInto(this.tableName),
      parameters: [],
    };
  }

  protected getRetrieveAlikeStatementBase() {
    return {
      builder: SQLBuilder.select(`*, ${SQLiteRowIdColumn}`).from(
        this.tableName,
      ),
      parameters: [],
    };
  }

  protected getNewDbObjectOfSameType(typeRowId: string) {
    const SameType = this.constructor as new (constructorArg: string) => T;
    return new SameType(typeRowId);
  }
}
