import 'reflect-metadata';
import { FeatureDbObject } from './feature-db-object';
import { DbIsolationLevel } from './db-isolation-level';
import { dbObjectRegistry } from './registry/db-objects-registry';
import { DbObject } from './db-object';
import { DbText } from '../types/db-text';
import { container } from 'tsyringe';
import { Database } from '../database';
import { User } from '../../chat/user/user';
import { Message } from '../../chat/message/message';
import { Metadata } from '../../chat/message/metadata';
import { Channel } from '../../chat/message/channel';

class ServerIsolatedDBO extends FeatureDbObject<ServerIsolatedDBO> {
  protected isolationLevel = DbIsolationLevel.SERVER;
  field = new DbText();
}

class ChannelIsolatedDBO extends FeatureDbObject<ServerIsolatedDBO> {
  protected isolationLevel = DbIsolationLevel.CHANNEL;
  field = new DbText();
}

dbObjectRegistry.addDbObject(ServerIsolatedDBO as any as new () => DbObject);
dbObjectRegistry.addDbObject(ChannelIsolatedDBO as any as new () => DbObject);

beforeEach(async () => {
  const db = new Database(':memory:');
  container.register<Database>(Database.name, { useValue: db });
  return await db.prepareTables();
});

const integrationA = 'integrationA';
const integrationB = 'integrationB';
const channelA = 'channelA';
const channelB = 'channelB';
const serverA = 'serverA';
const serverB = 'serverB';
const textA = 'textA';
const textB = 'textB';

export const getTestMessage = (
  integrationId: string,
  channelId: string,
  serverId?: string,
) =>
  new Message(
    new Metadata('', integrationId, new Channel(channelId), serverId),
    '',
    new Date(),
    new User('', ''),
  );

const writeServerIsolated = (
  text: string,
  integrationId: string,
  channelId: string,
  serverId?: string,
) => {
  const dbo = new ServerIsolatedDBO(
    getTestMessage(integrationId, channelId, serverId),
  );

  dbo.field.set(text);
  return dbo.write();
};

const retrieveServerIsolated = (
  integrationId: string,
  channelId: string,
  serverId?: string,
) => {
  const dbo = new ServerIsolatedDBO(
    getTestMessage(integrationId, channelId, serverId),
  );

  return dbo.retrieveAlike();
};

const writeChannelIsolated = (
  text: string,
  integrationId: string,
  channelId: string,
  serverId?: string,
) => {
  const dbo = new ChannelIsolatedDBO(
    getTestMessage(integrationId, channelId, serverId),
  );

  dbo.field.set(text);
  return dbo.write();
};

const retrieveChannelIsolated = (
  integrationId: string,
  channelId: string,
  serverId?: string,
) => {
  const dbo = new ChannelIsolatedDBO(
    getTestMessage(integrationId, channelId, serverId),
  );

  return dbo.retrieveAlike();
};

describe('FeatureDbObject', () => {
  test('returns correct records with the same server id when server isolation enabled', async () => {
    await writeServerIsolated(textA, integrationA, channelA, serverA);
    const retrieved = await retrieveServerIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('returns correct records with missing server id when server isolation enabled', async () => {
    await writeServerIsolated(textA, integrationA, channelA);
    const retrieved = await retrieveServerIsolated(integrationA, channelA);
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('ignores records with different server id when server isolation enabled', async () => {
    await writeServerIsolated(textA, integrationA, channelA, serverA);
    await writeServerIsolated(textB, integrationA, channelA, serverB);
    const retrieved = await retrieveServerIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('ignores records with different integration when server isolation enabled', async () => {
    await writeServerIsolated(textA, integrationA, channelA, serverA);
    await writeServerIsolated(textB, integrationB, channelA, serverA);
    const retrieved = await retrieveServerIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('ignores channel id when server isolation enabled', async () => {
    await writeServerIsolated(textA, integrationA, channelA, serverA);
    await writeServerIsolated(textB, integrationA, channelB, serverA);
    const retrieved = await retrieveServerIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(2);
    expect(retrieved[0].field.get()).toEqual(textA);
    expect(retrieved[1].field.get()).toEqual(textB);
  });

  test('returns correct records with the same server and chat ids when channel isolation enabled', async () => {
    await writeChannelIsolated(textA, integrationA, channelA, serverA);
    const retrieved = await retrieveChannelIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('returns correct records with missing server id and same channel when channel isolation enabled', async () => {
    await writeChannelIsolated(textA, integrationA, channelA);
    const retrieved = await retrieveChannelIsolated(integrationA, channelA);
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('ignores records with different channel id when channel isolation enabled', async () => {
    await writeChannelIsolated(textA, integrationA, channelA, serverA);
    await writeChannelIsolated(textB, integrationA, channelB, serverA);
    const retrieved = await retrieveChannelIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('ignores records with different integration when channel isolation enabled', async () => {
    await writeChannelIsolated(textA, integrationA, channelA, serverA);
    await writeChannelIsolated(textB, integrationB, channelA, serverA);
    const retrieved = await retrieveChannelIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });

  test('ignores records with different server id when channel isolation enabled', async () => {
    await writeChannelIsolated(textA, integrationA, channelA, serverA);
    await writeChannelIsolated(textB, integrationA, channelA, serverB);
    const retrieved = await retrieveChannelIsolated(
      integrationA,
      channelA,
      serverA,
    );
    expect(retrieved).toHaveLength(1);
    expect(retrieved[0].field.get()).toEqual(textA);
  });
});
