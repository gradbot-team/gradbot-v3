import { DbObject, FieldProperty } from './db-object';
import { container } from 'tsyringe';
import { Database } from '../../database/database';
import { SQLBuilder } from '../sql/sql-builder';
import { SQLStatement } from '../sql/sql-statement';
import { SQLiteRowIdColumn } from '../db-constants';

export abstract class SQLDbObject<T extends DbObject> extends DbObject {
  constructor(protected readonly rowId?: string) {
    super();
  }

  public write() {
    const statement = this.getWriteStatement();
    return this.db.write(statement);
  }

  public delete() {
    const statement = this.getDeleteStatement();
    return this.db.write(statement);
  }

  public async retrieveAlike() {
    const statement = this.getRetrieveAlikeStatement();
    const records = await this.db.read(statement);
    return this.processRecords(records);
  }

  public async retrieveAll() {
    const statement = this.getRetrieveAllStatement();
    const records = await this.db.read(statement);
    return this.processRecords(records);
  }

  public getWriteStatement() {
    return this.rowId
      ? this.getUpdateStatement(this.rowId)
      : this.getInsertStatement();
  }

  public getRetrieveAlikeStatement() {
    const fieldProperties = this.getFieldProperties(true);

    const builderBase = this.getRetrieveAlikeStatementBase();
    const builder = builderBase.builder;
    const parameters = builderBase.parameters;

    for (const fieldProperty of fieldProperties) {
      const value = fieldProperty.value.getForSQL();
      builder.where(`${fieldProperty.fieldName} = ?`);
      parameters.push(value);
    }

    builder.orderByAscend(SQLiteRowIdColumn);

    return new SQLStatement(builder.build(), parameters);
  }

  public getRetrieveAllStatement() {
    const builderBase = this.getRetrieveAlikeStatementBase();
    return new SQLStatement(
      builderBase.builder.build(),
      builderBase.parameters,
    );
  }

  public getDeleteStatement() {
    if (this.rowId) {
      return new SQLStatement(
        SQLBuilder.deleteFrom(this.tableName)
          .where(`${SQLiteRowIdColumn} = ?`)
          .build(),
        [this.rowId],
      );
    } else {
      throw new Error(`Cannot delete object without ${SQLiteRowIdColumn}`);
    }
  }

  protected getUpdateStatement(rowId: string) {
    const fieldProperties = this.getFieldProperties(true);

    const builder = SQLBuilder.update(this.tableName);
    const parameters: (string | number)[] = [];

    for (const fieldProperty of fieldProperties) {
      const value = fieldProperty.value.getForSQL();
      builder.value(fieldProperty.fieldName);
      parameters.push(value);
    }

    builder.where(`${SQLiteRowIdColumn} = ?`);
    parameters.push(rowId);

    return new SQLStatement(builder.build(), parameters);
  }

  protected getInsertStatement() {
    const fieldProperties = this.getFieldProperties();

    const builderBase = this.getInsertStatementBase();
    const builder = builderBase.builder;
    const parameters = builderBase.parameters;

    for (const fieldProperty of fieldProperties) {
      const value = fieldProperty.value.getForSQL();
      builder.value(fieldProperty.fieldName);
      parameters.push(value);
    }

    return new SQLStatement(builder.build(), parameters);
  }

  protected abstract getInsertStatementBase(): SQLStatementBase;
  protected abstract getRetrieveAlikeStatementBase(): SQLStatementBase;
  protected abstract getNewDbObjectOfSameType(rowId: string): T;

  private processRecords(records: { [key: string]: any }[]): T[] {
    const dbObjects: T[] = [];

    for (const record of records) {
      dbObjects.push(this.processRecord(record));
    }

    return dbObjects;
  }

  private processRecord(record: { [key: string]: any }): T {
    const dbObject: T = this.getNewDbObjectOfSameType(
      record[SQLiteRowIdColumn] as string,
    );

    for (const fieldName in record) {
      if (record.hasOwnProperty(fieldName)) {
        const propertyForField: FieldProperty | undefined =
          dbObject.getFieldWithName(fieldName);
        if (propertyForField) {
          propertyForField.value.setFromSQL(
            record[fieldName] as string | number,
          );
        }
      }
    }

    return dbObject;
  }

  private get db() {
    return container.resolve<Database>(Database.name);
  }
}

interface SQLStatementBase {
  readonly builder: SQLBuilder;
  readonly parameters: (string | number)[];
}
