export abstract class Dictionary {
  public abstract init(): null;
  public abstract getDict(): string;
}
