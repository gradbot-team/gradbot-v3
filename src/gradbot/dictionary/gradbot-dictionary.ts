import { Dictionary } from './dictionary';
import * as fs from 'fs';
import * as path from 'path';

export class GradbotDictionary extends Dictionary {
  private dictionary = '';

  public init(): null {
    // Load the dictionary
    const base = require.resolve('dictionary-en');
    const dictBuf = fs.readFileSync(
      path.join(path.dirname(base), 'index.dic'),
      'utf-8',
    );
    this.dictionary = dictBuf;
    return null;
  }

  public getDict(): string {
    return this.dictionary;
  }
}
