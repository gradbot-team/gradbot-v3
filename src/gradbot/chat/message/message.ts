import { User } from '../user/user';
import { Metadata } from './metadata';

export class Message {
  private static readonly UnwantedCharactersRegex =
    /[\u200B-\u200D\uFEFF\u202E]/g;

  constructor(
    readonly metadata: Metadata,
    readonly text: string,
    readonly timestamp: Date,
    readonly user: User,
  ) {
    // Trim out all the bad characters.
    this.text = text.replace(Message.UnwantedCharactersRegex, '');
  }
}
