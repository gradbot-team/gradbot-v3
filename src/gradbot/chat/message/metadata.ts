import { Channel } from './channel';

export class Metadata {
  constructor(
    public readonly messageId: string,
    public readonly chatId: string,
    public readonly channel: Channel,
    public readonly serverId = '',
  ) {}
}
