export class Channel {
  constructor(
    public readonly id: string,
    public readonly isPrivate = false,
  ) {}
}
