import { FeatureMessage } from '../../feature/feature-message';
import { Channel } from './channel';
import { MaxStoredHistoryMessages } from '../../constants/constants';

export class MessageHistoryList {
  private readonly _items: FeatureMessage[] = [];

  constructor(
    private readonly serverId: string,
    private readonly channel: Channel,
    private readonly chatId: string,
  ) {}

  public get items() {
    return [...this._items];
  }

  public hasServerChannelChat(
    serverId: string,
    channel: Channel,
    chatId: string,
  ) {
    return (
      this.serverId === serverId &&
      this.channel.id === channel.id &&
      this.channel.isPrivate === channel.isPrivate &&
      this.chatId === chatId
    );
  }

  public addMessage(message: FeatureMessage) {
    if (message.reason.length > 0) {
      this._items.unshift(message);

      while (this._items.length > MaxStoredHistoryMessages) {
        this._items.pop();
      }
    }
  }
}
