import { ChatIntegration } from './integration/chat-integration';
import { ReplaySubject, Observable } from 'rxjs';
import { Message } from './message/message';
import { Metadata } from './message/metadata';
import { FeatureMessage } from '../feature/feature-message';
import { Chat } from './chat';

export class ChatIntegrationAggregator extends Chat {
  public static async forIntegrations(chatsIntegrations: ChatIntegration[]) {
    const instance = new ChatIntegrationAggregator(chatsIntegrations);
    await instance.init();
    return instance;
  }

  private _incomingMessages$ = new ReplaySubject<Message>(1);

  private constructor(private readonly chatsIntegrations: ChatIntegration[]) {
    super();
  }

  get incomingMessages$(): Observable<Message> {
    return this._incomingMessages$.asObservable();
  }

  public async getUserWithDisplayName(displayName: string, metadata: Metadata) {
    const chat = this.getChatWithId(metadata.chatId);
    return chat
      ? chat.getUserWithDisplayName(displayName, metadata)
      : undefined;
  }

  public async getUserWithId(userId: string, metadata: Metadata) {
    const chat = this.getChatWithId(metadata.chatId);
    return chat ? chat.getUserWithId(userId, metadata) : undefined;
  }

  public getMessageHistory(metadata: Metadata) {
    const chat = this.getChatWithId(metadata.chatId);
    return chat ? chat.getMessageHistory(metadata) : [];
  }

  public async sendMessage(message: FeatureMessage) {
    const chat = this.getChatWithId(message.metadata.chatId);
    if (chat) {
      await chat.sendMessage(message);
    }
  }

  public async init() {
    await Promise.all(this.chatsIntegrations.map((chat) => chat.init()));
    this.chatsIntegrations.forEach((chat) =>
      chat.incomingMessages$.subscribe((message) =>
        this._incomingMessages$.next(message),
      ),
    );
  }

  private getChatWithId(chatId: string) {
    return this.chatsIntegrations.find((chat) => chat.id === chatId);
  }
}
