import { ChatIntegrationAggregator } from './chat-aggregator';
import { log } from '../../util/log/log';
import { ChatIntegrationProviders } from '../../chat-integrations/chat-integration-providers';

export class ChatProvider {
  public static async createChatAggregator(integrationNames: string[]) {
    if (integrationNames.length === 0) {
      log(
        'At least one chat integration needed, [DCIntegration, DiscordIntegration]',
      );
      process.exit(1);
    }

    const integrations = integrationNames.map((integrationName) =>
      this.getValidatedIntegrationProvider(integrationName).getInstance(),
    );

    const aggregator =
      await ChatIntegrationAggregator.forIntegrations(integrations);

    log(
      'Loaded Chat Integration Aggregator with integrations: ' +
        integrations.map((integration) => integration.id).join(', '),
    );

    return aggregator;
  }

  public static getAllowedChatIntegrations() {
    return ChatIntegrationProviders.map((p) => p.name);
  }

  private static getValidatedIntegrationProvider(name: string) {
    const provider = ChatIntegrationProviders.find((p) => p.name === name);

    if (!provider) {
      log(`Cannot find chat integration ${name}`);
      process.exit(1);
    }

    for (const envVar of provider.requiredEnvVars || []) {
      if (!process.env[envVar]) {
        log(`${envVar} variable required by ${provider.name} is not set`);
        process.exit(1);
      }
    }

    return provider;
  }
}
