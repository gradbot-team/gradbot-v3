import { ChatIntegration } from './chat-integration';

export class ChatIntegrationProvider {
  constructor(
    public readonly name: string,
    private readonly instanceProvider: () => ChatIntegration,
    public readonly requiredEnvVars?: string[],
  ) {}

  public getInstance() {
    return this.instanceProvider();
  }
}
