import { FeatureMessage } from '../../feature/feature-message';
import { Observable, ReplaySubject } from 'rxjs';
import { User } from '../user/user';
import { Metadata } from '../message/metadata';
import { Chat } from '../chat';
import { MessageHistoryList } from '../message/message-history-list';
import { UserList } from '../user/user-list';
import { Channel } from '../message/channel';
import { Image } from '../../image/image';
import { Message } from '../message/message';

export abstract class ChatIntegration extends Chat {
  public readonly id = this.constructor.name;
  protected readonly usersLists: UserList[] = [];
  private readonly messageHistoryLists: MessageHistoryList[] = [];
  private readonly _incomingMessages$ = new ReplaySubject<Message>(1);

  public get incomingMessages$(): Observable<Message> {
    return this._incomingMessages$.asObservable();
  }

  public getMessageHistory(metadata: Metadata): FeatureMessage[] {
    const messageHistory = this.getMessageHistoryListForServerChannel(
      metadata.serverId,
      metadata.channel,
    );
    return messageHistory.items;
  }

  public async sendMessage(message: FeatureMessage) {
    if (message.text.length > 0 || message.image) {
      const channel = message.channelOverride || message.metadata.channel;

      await this.sendChatMessage(
        message.text,
        channel,
        message.metadata.serverId,
        message.image,
      );
      this.getMessageHistoryListForServerChannel(
        message.metadata.serverId,
        channel,
      ).addMessage(message);
    }
  }

  public async getUserWithId(
    userId: string,
    metadata: Metadata,
  ): Promise<User | undefined> {
    const userList = this.getUserListForServer(metadata.serverId);

    let user = userList.items.find((u) => u.id === userId);

    if (!user) {
      await this.fetchUsers(userList);
      user = userList.items.find((u) => u.id === userId);
    }
    return user;
  }

  public async getUserWithDisplayName(
    displayName: string,
    metadata: Metadata,
  ): Promise<User | undefined> {
    const userList = this.getUserListForServer(metadata.serverId);

    let user = userList.items.find(
      (u) => u.displayName.toLowerCase() === displayName.toLowerCase(),
    );

    if (!user) {
      await this.fetchUsers(userList);
      user = userList.items.find(
        (u) => u.displayName.toLowerCase() === displayName.toLowerCase(),
      );
    }
    return user;
  }

  protected createMetadata(
    messageId: string,
    channel: Channel,
    serverId: string = this.id,
  ) {
    return new Metadata(messageId, this.id, channel, serverId);
  }

  protected getUserListForServer(serverId: string) {
    let userList = this.usersLists.find((list) =>
      list.hasServerChat(serverId, this.id),
    );

    if (!userList) {
      userList = new UserList(serverId, this.id);
      this.usersLists.push(userList);
    }
    return userList;
  }

  protected sendMessageToFeatures(message: Message) {
    this._incomingMessages$.next(message);
  }

  protected abstract sendChatMessage(
    text: string,
    channel: Channel,
    serverId: string,
    image?: Image,
  ): Promise<void>;

  protected abstract fetchUsers(userList: UserList): Promise<void>;

  private getMessageHistoryListForServerChannel(
    serverId: string,
    channel: Channel,
  ) {
    let messageHistoryList = this.messageHistoryLists.find((list) =>
      list.hasServerChannelChat(serverId, channel, this.id),
    );

    if (!messageHistoryList) {
      messageHistoryList = new MessageHistoryList(serverId, channel, this.id);
      this.messageHistoryLists.push(messageHistoryList);
    }

    return messageHistoryList;
  }
}
