import { FeatureMessage } from '../feature/feature-message';
import { Message } from './message/message';
import { Observable } from 'rxjs';
import { Metadata } from './message/metadata';
import { User } from './user/user';

export abstract class Chat {
  public abstract readonly incomingMessages$: Observable<Message>;
  public abstract getUserWithDisplayName(
    displayName: string,
    metadata: Metadata,
  ): Promise<User | undefined>;
  public abstract getUserWithId(
    userId: string,
    metadata: Metadata,
  ): Promise<User | undefined>;
  public abstract getMessageHistory(metadata: Metadata): FeatureMessage[];
  public abstract sendMessage(message: FeatureMessage): Promise<void>;
  public abstract init(): Promise<void>;
}
