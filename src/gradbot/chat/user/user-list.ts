import { User } from './user';

export class UserList {
  public _items: User[] = [];
  constructor(
    public readonly serverId: string,
    private readonly chatId: string,
  ) {}

  public hasServerChat(serverId: string, chatId: string) {
    return this.serverId === serverId && this.chatId === chatId;
  }

  public set items(items: User[]) {
    this._items = items;
  }

  public get items() {
    return [...this._items];
  }
}
