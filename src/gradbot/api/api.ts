export abstract class API {
  public abstract fetch<T>(url: string): Promise<T | null>;
  public abstract fetchText(url: string): Promise<string | null>;
}
