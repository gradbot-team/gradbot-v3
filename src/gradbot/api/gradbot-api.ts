import { API } from './api';
import fetch from 'node-fetch';
import { FeatureTimeout } from '../constants/constants';
import { log } from '../../util/log/log';

export class GradbotAPI extends API {
  public async fetch<T>(url: string): Promise<T | null> {
    const response = await this.fetchInternal(url);
    if (response && response.ok) {
      return await response
        .json()
        .then((json) => json as T)
        .catch((e) => this.logError(url, e));
    }
    return null;
  }

  public async fetchText(url: string): Promise<string | null> {
    const response = await this.fetchInternal(url);
    if (response && response.ok) {
      return await response.text().catch((e) => this.logError(url, e));
    }
    return null;
  }

  private async fetchInternal(url: string) {
    return await fetch(url, { timeout: FeatureTimeout }).catch(() => null);
  }

  private logError(url: string, e: any) {
    log(`Failed API request to ${url}`, e);
    return null;
  }
}
