export const FeatureTimeout = 10000;
export const MaxStoredHistoryMessages = 10;

export const MaxImageSize = 800;
export const MaxThumbnailSize = 192;

export const SQLiteDbRelativePath = 'db/gradbot.db';
