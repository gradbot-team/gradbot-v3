import { container } from 'tsyringe';
import { GradbotScheduler } from '../scheduler/gradbot-scheduler';
import { Database } from '../database/database';
import { Scheduler } from '../scheduler/scheduler';
import { API } from '../api/api';
import { GradbotAPI } from '../api/gradbot-api';
import { ImageGenerator } from '../image/image-generator';
import { GradbotImageGenerator } from '../image/gradbot-image-generator';
import { Dictionary } from '../dictionary/dictionary';
import { GradbotDictionary } from '../dictionary/gradbot-dictionary';
import { Chat } from '../chat/chat';

export class Injector {
  public static async inject(chat: Chat, databasePath: string) {
    const db = new Database(databasePath);
    container.register<Database>(Database.name, { useValue: db });
    await db.prepareTables();
    container.register<Scheduler>(Scheduler.name, {
      useValue: new GradbotScheduler(chat),
    });
    container.register<Chat>(Chat.name, { useValue: chat });
    container.register<API>(API.name, { useValue: new GradbotAPI() });
    container.register<ImageGenerator>(ImageGenerator.name, {
      useValue: new GradbotImageGenerator(),
    });
    const dict = new GradbotDictionary();
    container.register<Dictionary>(Dictionary.name, { useValue: dict });
    dict.init();
  }
}
