import { Message } from '../../gradbot/chat/message/message';
import { User } from '../../gradbot/chat/user/user';
import { log } from '../../util/log/log';
import { ChatIntegration } from '../../gradbot/chat/integration/chat-integration';
import * as Discord from 'discord.js';
import { DMChannel, GatewayIntentBits } from 'discord.js';
import { Channel } from '../../gradbot/chat/message/channel';
import { Image } from '../../gradbot/image/image';
import { ChatIntegrationProvider } from '../../gradbot/chat/integration/chat-integration-provider';
import { UserList } from '../../gradbot/chat/user/user-list';

export class DiscordIntegration extends ChatIntegration {
  private static readonly DiscordSecretEnvVariable = 'DISCORD_SECRET';

  public static readonly Provider = new ChatIntegrationProvider(
    DiscordIntegration.name,
    () =>
      new DiscordIntegration(
        process.env[DiscordIntegration.DiscordSecretEnvVariable],
      ),
    [DiscordIntegration.DiscordSecretEnvVariable],
  );

  private static readonly ReadyEvent = 'ready';
  private static readonly MessageEvent = 'messageCreate';

  private bot: Discord.Client | null = null;

  public constructor(private readonly discordSecret?: string) {
    super();
  }

  public async init() {
    await this.connectToDiscord();
  }

  public async fetchUsers(userList: UserList): Promise<void> {
    // Get users from Discord for the given userList
    if (!this.bot) {
      log('Discord: Attempted to retrieve user list when disconnected!');
      return;
    }
    const guild = this.bot.guilds.resolve(userList.serverId);
    if (guild) {
      const members = await guild.members.fetch();
      userList.items = members.map(
        (member) => new User(member.id, member.displayName),
      );
      log('Discord: All Discord users retrieved for ' + guild.name);
    } else {
      log('Discord: Failed to retrieve guild with ID ' + userList.serverId);
    }
  }

  protected async sendChatMessage(
    text: string,
    channelInfo: Channel,
    _serverId: string,
    image?: Image,
  ): Promise<void> {
    const targetChannel = await this.getChannel(channelInfo);

    // Discord has a hard limit no 2,000 characters for string messages, as a
    // minute appproach, trim all messages to 2'000
    if (text.length > 2000) {
      text = text.substr(0, 1997) + '...';
    }

    if (targetChannel) {
      let attachment: string | null = null;
      if (image) {
        // Discord appears to be moving away from accepting image buffers, so
        // pass the url from which we got the message
        attachment = image.url;
      }
      if (attachment !== null) {
        await targetChannel.send({ content: text, files: [{ attachment }] });
      } else {
        await targetChannel.send(text);
      }
    } else {
      log('Discord: Did not find a target channel for ID ' + channelInfo.id);
    }
  }

  private async fetchAllUsers(): Promise<void> {
    // Get users from Discord, but more complicated as we potentially have multiple servers to deal with
    if (!this.bot) {
      log('Discord: Attempted to retrieve user list when disconnected!');
      return;
    }
    const guilds = this.bot.guilds.cache.values();
    for (const guild of guilds) {
      const userList = this.getUserListForServer(guild.id);
      await this.fetchUsers(userList);
    }
    log('Discord: All users retrieved');
  }

  private async getChannel(
    channelInfo: Channel,
  ): Promise<Discord.TextChannel | Discord.DMChannel | null> {
    if (!this.bot) {
      log('Discord: Attempted to retrieve channel object when disconnected!');
      return null;
    }
    // If this is a private message, the ID is a user ID, and we need to get the DM channel ID.
    // Otherwise the ID is the server ID and can be used directly.
    const channelId = channelInfo.id;
    if (channelInfo.isPrivate) {
      const user = await this.bot.users.fetch(channelId);
      return await user.createDM();
    } else {
      // This makes the most of caching, so it isn't always an API call.
      const channel = await this.bot.channels.fetch(channelId);
      return channel as Discord.TextChannel;
    }
  }

  private async connectToDiscord() {
    log('Starting discord: ');
    this.bot = new Discord.Client({
      intents: [
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildWebhooks,
      ],
    });

    this.bot.on(DiscordIntegration.ReadyEvent, () => {
      log('Discord: Connected');
      this.fetchAllUsers().catch((e) => log('Fetching users failed', e));
    });
    await this.bot.login(this.discordSecret);

    this.bot.on(DiscordIntegration.MessageEvent, (message: Discord.Message) => {
      // On message, figure out who is sending it...
      if (message.author.bot) {
        // Don't reply to bots.
        return;
      }
      log('Got message: ' + message.content);

      // Usernames can differ between channels, and in DMs the username is used.
      const username = message.member
        ? message.member.displayName
        : message.author.username;
      const user: User = new User(message.author.id, username);

      // Server ID is either the user ID or the guild ID depending on if this message comes from a text or dm channel
      const serverId = message.member
        ? message.member.guild.id
        : message.author.id;

      // Process the message
      this.sendMessageToFeatures(
        new Message(
          this.createMetadata(
            message.id,
            new Channel(
              message.channel.id,
              message.channel instanceof DMChannel,
            ),
            serverId,
          ),
          message.content,
          new Date(message.createdTimestamp),
          user,
        ),
      );
    });
  }
}
