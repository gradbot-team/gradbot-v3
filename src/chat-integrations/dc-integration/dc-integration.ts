import { Message } from '../../gradbot/chat/message/message';
import { Metadata } from '../../gradbot/chat/message/metadata';
import { User } from '../../gradbot/chat/user/user';
import * as admin from 'firebase-admin';
import { Firestore, Timestamp } from '@google-cloud/firestore';
import { Bucket } from '@google-cloud/storage';
import { log } from '../../util/log/log';
import { ChatIntegration } from '../../gradbot/chat/integration/chat-integration';
import * as uuid from 'uuid';
import { Image } from '../../gradbot/image/image';
import { UserList } from '../../gradbot/chat/user/user-list';
import { Channel } from '../../gradbot/chat/message/channel';
import { ChatIntegrationProvider } from '../../gradbot/chat/integration/chat-integration-provider';

export class DCIntegration extends ChatIntegration {
  public static readonly Provider = new ChatIntegrationProvider(
    DCIntegration.name,
    () => new DCIntegration(),
    ['FIREBASE_CONFIG', 'GOOGLE_APPLICATION_CREDENTIALS'],
  );

  // Collections
  private static readonly DisplayNamesCollection = 'display_names';
  private static readonly DammitChatMessagesCollection = 'dammit_chat_messages';

  // Fields
  private static readonly TimestampField = 'timestamp';
  private static readonly DisplayNameField = 'displayName';
  private static readonly UserIdField = 'userId';
  private static readonly TextField = 'text';
  private static readonly Addressees = 'addressees';
  private static readonly Images = 'images';
  private static readonly Image = 'image';
  private static readonly Thumbnail = 'thumbnail';

  private static readonly GreaterOperator = '>';
  private static readonly ArrayContainsAnyOperator = 'array-contains-any';
  private static readonly FirestoreAdded = 'added';

  private static readonly GradbotUserId = 'gradbot';
  private static readonly PublicMessage = 'PUBLIC_MESSAGE';

  private static readonly DCSubscriberDocLimit = 10;
  private static readonly DCSubscriberTimeout = 1000 * 60 * 60 * 3;

  private static readonly JPGSuffix = '.jpg';
  private static readonly DCImagesStoragePath = 'dc_images/';
  private static readonly JPGMime = 'image/jpeg';

  private readonly firestore: Firestore;
  private readonly bucket: Bucket;

  private dcObserverStartDate = new Date();
  private subscriptionTimeout: NodeJS.Timeout | null = null;

  public constructor() {
    super();
    admin.initializeApp();
    this.firestore = admin.firestore();
    this.bucket = admin.storage().bucket();
  }

  public async init() {
    const userList = this.getUserListForServer(this.id);
    await this.fetchUsers(userList);
    this.subscribeToDammitChat();
  }

  public async fetchUsers(userList: UserList): Promise<void> {
    const snapshots = await this.firestore
      .collection(DCIntegration.DisplayNamesCollection)
      .get();
    userList.items = snapshots.docs.map(
      (doc) =>
        new User(doc.id, doc.data()[DCIntegration.DisplayNameField] as string),
    );
    log('DC users retrieved');
  }

  protected async sendChatMessage(
    text: string,
    channel: Channel,
    _serverId: string,
    image?: Image,
  ): Promise<void> {
    await this.firestore
      .collection(DCIntegration.DammitChatMessagesCollection)
      .add({
        [DCIntegration.TextField]: text,
        [DCIntegration.UserIdField]: DCIntegration.GradbotUserId,
        [DCIntegration.TimestampField]:
          admin.firestore.FieldValue.serverTimestamp(),
        [DCIntegration.Images]: image ? [await this.processImage(image)] : [],
        [DCIntegration.Addressees]: !channel.isPrivate
          ? [DCIntegration.PublicMessage]
          : [DCIntegration.GradbotUserId, channel.id],
      });
  }

  private async processImage(image: Image) {
    const paths = await Promise.all([
      this.uploadImageAndGetPath(image.imageBuffer),
      this.uploadImageAndGetPath(image.thumbnailBuffer),
    ]);

    return {
      [DCIntegration.Image]: paths[0],
      [DCIntegration.Thumbnail]: paths[1],
    };
  }

  private async uploadImageAndGetPath(dataBuffer: Buffer) {
    const file = this.bucket.file(
      `${DCIntegration.DCImagesStoragePath}${uuid.v4()}${
        DCIntegration.JPGSuffix
      }`,
    );
    await file.save(dataBuffer, {
      contentType: DCIntegration.JPGMime,
      gzip: true,
    });
    const signedUrl = await file.getSignedUrl({
      action: 'read',
      expires: '03-09-2491',
    });
    return signedUrl[0];
  }

  private subscribeToDammitChat() {
    const unsubscribe = this.firestore
      .collection(DCIntegration.DammitChatMessagesCollection)
      .where(
        DCIntegration.TimestampField,
        DCIntegration.GreaterOperator,
        this.dcObserverStartDate,
      )
      .where(DCIntegration.Addressees, DCIntegration.ArrayContainsAnyOperator, [
        DCIntegration.PublicMessage,
        DCIntegration.GradbotUserId,
      ])
      .onSnapshot(
        (querySnapshot) => {
          querySnapshot.docChanges().forEach((change) => {
            (async () => {
              if (change.type === DCIntegration.FirestoreAdded) {
                const doc = change.doc;
                const data = doc.data();

                const userId: string = data[
                  DCIntegration.UserIdField
                ] as string;
                const text: string = data[DCIntegration.TextField] as string;
                const timestamp: Timestamp = data[
                  DCIntegration.TimestampField
                ] as Timestamp;
                const addressees: string = data[
                  DCIntegration.Addressees
                ] as string;

                if (
                  doc.id &&
                  userId &&
                  text &&
                  timestamp &&
                  userId !== DCIntegration.GradbotUserId
                ) {
                  const isPrivate = !addressees.includes(
                    DCIntegration.PublicMessage,
                  );
                  const channel: Channel = new Channel(
                    isPrivate ? userId : '',
                    isPrivate,
                  );
                  const metadata: Metadata = this.createMetadata(
                    doc.id,
                    channel,
                  );
                  const user = await this.getUserWithId(userId, metadata);

                  if (user) {
                    this.sendMessageToFeatures(
                      new Message(metadata, text, timestamp.toDate(), user),
                    );
                  }
                  if (
                    querySnapshot.docs.length >=
                    DCIntegration.DCSubscriberDocLimit
                  ) {
                    // Well... This restarts the subscriber every few messages and there's a reason for that...
                    // https://github.com/firebase/firebase-admin-node/issues/271
                    // Apparently using limit() could cause issues. But we don't wanna read all the messages
                    // every time, and so we're sorta simulating this limit that way. Soz.
                    log(
                      'Restarting DC messages subscriber due to reaching doc limit',
                    );
                    this.dcObserverStartDate = timestamp.toDate();
                    if (this.subscriptionTimeout) {
                      clearTimeout(this.subscriptionTimeout);
                    }
                    unsubscribe();
                    this.subscribeToDammitChat();
                  }
                }
              }
            })();
          });
        },
        (err) => {
          log('Restarting DC messages subscriber due to error', err);
          if (this.subscriptionTimeout) {
            clearTimeout(this.subscriptionTimeout);
          }
          unsubscribe();
          this.subscribeToDammitChat();
        },
      );

    // Similar to the comment above, in case there aren't enough messages, still restart
    // every few hours.
    this.subscriptionTimeout = setTimeout(() => {
      log('Restarting DC messages subscriber due to reaching timeout');
      this.dcObserverStartDate = new Date();
      unsubscribe();
      this.subscribeToDammitChat();
    }, DCIntegration.DCSubscriberTimeout);
  }
}
