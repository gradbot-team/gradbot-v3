import { DCIntegration } from './dc-integration/dc-integration';
import { DiscordIntegration } from './discord-integration/discord-integration';

export const ChatIntegrationProviders = [
  DCIntegration.Provider,
  DiscordIntegration.Provider,
];
