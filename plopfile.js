module.exports = function (plop) {
  const orderedFeaturesPath = 'src/features/ordered-features.ts';
  const chatIntegrationProvidersPath =
    'src/chat-integrations/chat-integration-providers.ts';

  plop.setGenerator('feature', {
    prompts: [
      {
        type: 'input',
        name: 'featureName',
        message:
          "What is the name of the new feature? (e.g. 'Echo' or 'Train schedule')",
        validate: async (input) =>
          input.match(/^[A-Za-z][A-Za-z0-9 ]*[A-Za-z]$/)
            ? true
            : 'Alphanumeric and spaces, starts and ends with a letter',
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'src/features/{{kebabCase featureName}}-feature/{{kebabCase featureName}}-feature.ts',
        templateFile: 'templates/feature/feature.template',
      },
      {
        type: 'add',
        path: 'src/features/{{kebabCase featureName}}-feature/{{kebabCase featureName}}-message-handler.ts',
        templateFile: 'templates/feature/feature-message-handler.template',
      },
      {
        type: 'add',
        path: 'src/features/{{kebabCase featureName}}-feature/{{kebabCase featureName}}-feature.test.ts',
        templateFile: 'templates/feature/feature.test.template',
      },
      {
        type: 'modify',
        path: orderedFeaturesPath,
        pattern: /\n\nexport const/,
        template:
          "\nimport { {{ pascalCase featureName }}Feature } from './{{kebabCase featureName}}-feature/{{kebabCase featureName}}-feature';\n\nexport const",
      },
      {
        type: 'modify',
        path: orderedFeaturesPath,
        pattern: /  NumberGameFeature,\n  EchoFeature\n];/,
        template:
          '  {{ pascalCase featureName }}Feature,\n  NumberGameFeature,\n  EchoFeature\n];',
      },
    ],
  });

  plop.setGenerator('integration', {
    prompts: [
      {
        type: 'input',
        name: 'integrationName',
        message:
          "What is the name of the new integration? (e.g. 'Facebook Messenger' or 'WhatsApp')",
        validate: async (input) =>
          input.match(/^[A-Za-z][A-Za-z0-9 ]*[A-Za-z]$/)
            ? true
            : 'Alphanumeric and spaces, starts and ends with a letter',
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'src/chat-integrations/{{kebabCase integrationName}}-integration/{{kebabCase integrationName}}-integration.ts',
        templateFile: 'templates/integration/integration.template',
      },
      {
        type: 'modify',
        path: chatIntegrationProvidersPath,
        pattern: /\n\nexport const/,
        template:
          "\nimport { {{ pascalCase integrationName }}Integration } from './{{kebabCase integrationName}}-integration/{{kebabCase integrationName}}-integration';\n\nexport const",
      },
      {
        type: 'modify',
        path: chatIntegrationProvidersPath,
        pattern: /\n];/,
        template:
          ',\n  {{ pascalCase integrationName }}Integration.Provider\n];',
      },
    ],
  });
};
